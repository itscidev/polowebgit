<?php
//path defined
if (isset($_SERVER['HTTP_HOST']) && (
	$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
	|| $_SERVER['HTTP_HOST']=='172.16.1.19'
)){
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/';
	$pathcook = '/poloweb/';
}else{
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/';
	$pathcook = '/';
}

//browser version detect
require_once 'plugin/browser-detector/Browser.php';
$browser = new Browser();
$allowbrowser = array(
	'Edge'=>38,'Opera'=>30,'Opera Mini'=>30,'Firefox'=>51,'Safari'=>10,'Chrome'=>56,'Konqueror'=>5,
	'iCab'=>5,
		
	'Internet Explorer'=>15,'Phoenix'=>15,'Firebird'=>15,'Pocket Internet Explorer'=>15,'OmniWeb'=>15,
	'Amaya'=>15,'Lynx'=>15,'Netscape Navigator'=>15,'Galeon'=>15,
);
$detbrowser = $browser->getBrowser();
$detversion = intval($browser->getVersion());
if (isset($allowbrowser[$detbrowser])){
	if ($detversion < $allowbrowser[$detbrowser]){
		header("location:".$path."oldbrowser.html");
		exit();
	}
}

//Draft/Live detection
$draftmode = 0;$draftdiv = '';
if (isset($_COOKIE['draftstatus']) && $_COOKIE['draftstatus']==1){
	$draftmode = 1;
	$draftdiv = '<div class="drafton">DRAFT MODE</div>';
} 

//special case for favicon
$HTTP_HOST = array(
	'localhost','172.16.30.12','172.16.1.19',
	'poloindonesia.com','www.poloindonesia.com',
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
);

if (!isset($_SERVER['SCRIPT_NAME']) || !isset($_SERVER['HTTP_HOST']) || (
	isset($_SERVER['HTTP_HOST']) && !in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)
)){
	header("location:".$path."index.html");
	exit();
}else{
	
	include_once 'cms/configuration/connect.php';
	include_once 'cms/configuration/function-ui.php';
	include_once 'cms/model/cart.php';
	//for login detection
	$memid = 0;
	if (isset($_COOKIE["loginmem"])){
		//visitorID:timestampEnd:rememberMe
		$explvst = explode(':',$_COOKIE["loginmem"]);
		if (time() < $explvst[1]){
			$memid = $explvst[0];
			$profvst = mysqli_fetch_assoc(mysqli_query($sqlcon,"select * from member where mm_id=".$memid));
			if (isset($profvst['mm_id']) && $profvst['mm_id']>0 
			&& isset($profvst['mm_banned']) && $profvst['mm_banned']<8){
				//expired 7 hour
				$endsession = time()+60*60*7;
				if ($explvst[2]==1){
					//expired 7 days
					$endsession = time()+60*60*24*7;
				}
				setcookie( "loginmem",$explvst[0].":".$endsession.":".$explvst[2],$endsession,$pathcook );
				
				$vstfoto = 'images/icon/avatar.jpg';
				$vstname = $profvst['mm_name'];
				if ($profvst['mm_foto']!='' && file_exists("images/foto/".$profvst['mm_foto'])){
					$vstfoto = 'images/icon/'.$profvst['mm_foto'];
				}
			}else{
				$memid = 0;
				unset($_COOKIE["loginmem"]);
				setcookie("loginmem", null, -1, "/".$pathcook[1]."/");
			}
		}else{
			unset($_COOKIE["loginmem"]);
			setcookie("loginmem", null, -1, "/".$pathcook[1]."/");
		}
	}
	//for user notif
	$notifcount = 0;
	$notifbadge = '';
	$pendingbadge = ' hide';
	$pendingcount = 0;
	if ($memid != 0){
		$field = "count(cr_id) jpending";
		$join  = "inner join member_address on mma_id=cr_mma_id";
		//status : 0='Waiting Payment',1='Canceled',2='Paid',3='Failed',4='On Packaging',5='On Delivery',6='Delivered');
		$where = "mma_mm_id=".$memid." and cr_status in (0,2,4,5)";
		$totnotif = cart_s($sqlcon,$field,$join,$where);
		if (is_array($totnotif)){
			$totnotif1 = mysqli_fetch_assoc($totnotif[1]);
			if ($totnotif1['jpending']>0){
				$pendingbadge = '';
				$pendingcount = $totnotif1['jpending'];
				$notifcount = $notifcount + $pendingcount;
			}
		}
	}
	if ($notifcount > 0){
		$notifbadge = ' active';
	}
	$pendingcountp = number_format($pendingcount,0,',','.');
	$notifcountp = number_format($notifcount,0,',','.');
	
	//for cart session
	$cartitem = '';
	$cartbadge = '';
	$cartcount = 0;
	$cartsubtotal = 0;
	if (isset($_COOKIE["cartvst"])){
		include_once 'cms/model/productvariety.php';
		include_once 'cms/model/productimage.php';
		//idprodvariety=>qty,idprodvariety=>qty
		$cartvst = json_decode($_COOKIE["cartvst"],true);
		if (count($cartvst) > 0){
			foreach ($cartvst as $idvar => $qty){
				$field = "pv_qty,pv_price,pv_pr_id,pr_name,pr_weight,pv_st_id,st_name,pv_sz_id,sz_name,".
				"pv_ci_id,ci_image,cl_name";
				$join = "inner join product on pr_id=pv_pr_id ".
					"left outer join style on st_id=pv_st_id ".
					"left outer join size on sz_id=pv_sz_id ".
					"left outer join color_item on ci_id=pv_ci_id ".
					"left outer join color on cl_id=ci_cl_id";
				$where = "pv_qty>0 and pv_status=1 and pr_status=1 and pv_id=".$idvar;
				$cekpv = productvariety_s($sqlcon,$field,$join,$where);
				if (is_array($cekpv) && $cekpv[0]>0){
					$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
					if ($cekpv1['pv_qty'] < $qty){
						$cartvst[$idvar] = $cekpv1['pv_qty'];
						$cartcount = $cartcount + $cekpv1['pv_qty'];
					}else{
						$cartcount = $cartcount + $qty;
					}
					$cartbadge = ' active';
					$cartitem .= '<div class="cartitem cartitem'.$idvar.'">';
					$cartitem .= '<div class="cartitemimg">';
					$field = "pi_image";
					$where = "pi_pr_id=".$cekpv1['pv_pr_id']." and pi_st_id=".$cekpv1['pv_st_id']." ".
							"and pi_ci_id=".$cekpv1['pv_ci_id']." and pi_status=1";
					$order = "pi_sequence asc";
					$getimg = productimage_s($sqlcon,$field,'',$where,'',$order,1);
					if (is_array($getimg) && $getimg[0]>0){
						$getimg1 = mysqli_fetch_assoc($getimg[1]);
						if ($getimg1['pi_image']!='' && file_exists("images/productitem/".$getimg1['pi_image'])){
							$cartitem .= '<img alt="'.htmlentities($cekpv1['pr_name']).'" '.
								'src="'.$path.'images/productitem/'.$getimg1['pi_image'].'">';
						}	
					}
					$cartitem .= '</div>';
					$cartitem .= '<div class="cartitemdesc">';
					$stname = '';
					if ($cekpv1['pv_st_id']!=0){
						$stname = ' - ';
						if ($cekpv1['st_name']==''){
							$stname .= 'Undefined!';
						}else{
							$stname .= $cekpv1['st_name'];
						}
					}
					$cartitem .= '<p>'.htmlentities($cekpv1['pr_name'].$stname).'</p>';
					if ($cekpv1['pv_ci_id'] != 0){
						$coloritm = 'Undefined!';
						if ($cekpv1['ci_image']!='' && file_exists("images/productcolor/".$cekpv1['ci_image'])){
							$coloritm = '<img alt="'.htmlentities($cekpv1['cl_name']).'" '.
								'src="'.$path.'images/productcolor/'.$cekpv1['ci_image'].'">';
						}
						$cartitem .= '<p>COLOR : '.$coloritm.'</p>';						
					}
					if ($cekpv1['pv_sz_id'] != 0){
						if ($cekpv1['sz_name']==''){
							$cartitem .= '<p>SIZE : Undefined!</p>';
						}else{
							$cartitem .= '<p>SIZE : '.$cekpv1['sz_name'].'</p>';
						}
					}
					$cartitem .= '<p>QTY : '.number_format($qty,0,',','.').'</p>';
					$cartitem .= '<p>Rp '.number_format($cekpv1['pv_price'],0,',','.').'</p>';
					$cartitem .= '</div>';
					$cartitem .= '<div class="cartitemdel">';
					$cartitem .= '<i class="fa fa-times pointer" onclick="cartdelitem('.$idvar.');return false;"></i>';
					$cartitem .= '</div>';
					$cartitem .= '</div>';
					
					$cartsubtotal = $cartsubtotal + ($qty*$cekpv1['pv_price']);
				}else{
					unset($cartvst[$idvar]);
				}
			}
		}
		unset($stname,$coloritm);
		setcookie( 'cartvst',json_encode($cartvst),time()+60*60*24*30,$pathcook );
	}
	$cartcountp = number_format($cartcount,0,',','.');
	$cartsubtotalp = 'Rp '.number_format($cartsubtotal,0,',','.');
	
	//for language session
	$lang = 'EN';
	$arrlang = array('EN'=>'English','ID'=>'Bahasa');
	if (isset($_COOKIE["lang"])){
		$lang = $_COOKIE["lang"];
		setcookie( 'lang',$lang,time()+60*60*24*30,$pathcook );
	}
	$language = '';
	foreach ($arrlang as $idlang => $contlang){
		$classlang = '';
		if ($idlang == $lang){
			$classlang = ' class="active"';
		}
		$language .= '<li'.$classlang.' attr="'.$idlang.'"><i class="fa fa-check"></i>'.$contlang.'</li>';
	}
	unset($lang,$arrlang,$idlang,$contlang,$classlang);
	
	//for heads attribute & js footer
	$addhead = '';
	$genhead = @mysqli_query($sqlcon,"select * from pages where pg_name='general'");
	$genhead1 = 0;
	if ($genhead){
		$genhead2 = mysqli_fetch_array($genhead);
		if (isset($genhead2['pg_title']) && $genhead2['pg_title']!=''){
			$addhead .= '<title>'.htmlentities($genhead2['pg_title']).'</title>';
		}
		if (isset($genhead2['pg_keyword']) && $genhead2['pg_keyword']!=''){
			$addhead .= '<meta name="keywords" content="'.htmlentities($genhead2['pg_keyword']).'">';
		}
		if (isset($genhead2['pg_desc']) && $genhead2['pg_desc']!=''){
			$addhead .= '<meta name="description" content="'.htmlentities($genhead2['pg_desc']).'">';
		}
	}
	//for heads attribute : overwrite
	$strohead = '';
	if (isset($_SERVER['SCRIPT_NAME'])){
		if ($strohead != ''){$strohead .= ",";}
		$strohead = "'".$_SERVER['SCRIPT_NAME']."'";
	}
	if (isset($_SERVER['REDIRECT_URL'])){
		$redu = explode('/',$_SERVER['REDIRECT_URL']);
		$toarr = '';
		foreach ($redu as $redu1){
			if ($redu1!=null && $redu1!=''){
				$toarr .= '/'.$redu1;
				if ($strohead != ''){$strohead .= ",";}
				$strohead .= "'".$toarr."'";
			}
		}
	}
	if ($strohead != ''){
		$sqlohead = "select * from pages where pg_name in (".$strohead.") order by pg_name desc limit 1";
		//echo $sqlohead;
		$genohead = @mysqli_query($sqlcon,$sqlohead);
		$genohead1 = 0;
		if ($genohead){$genohead1 = mysqli_num_rows($genohead);}
		if ($genohead1 > 0){
			$genohead2 = mysqli_fetch_array($genohead);
			$addhead = '';
			if (isset($genohead2['pg_title']) && $genohead2['pg_title']!=''){
				$addhead .= '<title>'.htmlentities($genohead2['pg_title']).'</title>';
			}
			if (isset($genohead2['pg_keyword']) && $genohead2['pg_keyword']!=''){
				$addhead .= '<meta name="keywords" content="'.htmlentities($genohead2['pg_keyword']).'">';
			}
			if (isset($genohead2['pg_desc']) && $genohead2['pg_desc']!=''){
				$addhead .= '<meta name="description" content="'.htmlentities($genohead2['pg_desc']).'">';
			}
		}
	}
	
	// construct menu listing
	$posnow = $_SERVER ['SCRIPT_NAME'];
	$exppos = explode ( '/', $posnow );
	$exppos = $exppos [ (count($exppos)-1) ];
	//list accessable file :
	$listpagear = array(
		'errorpages.php' => 'kalkulasierrorpages.php',
		'index.php' => 'kalkulasiindex.php',
		'productparent.php' => 'kalkulasiproductparent.php',
		'product.php' => 'kalkulasiproduct.php',
		'productdetail.php' => 'kalkulasiproductdetail.php',
		'contact.php' => 'kalkulasicontact.php',
		'stores.php' => 'kalkulasistores.php',
		'about.php' => 'kalkulasiabout.php',
		'regulations.php' => 'kalkulasiregulations.php',
		'cart.php' => 'kalkulasicart.php',
		'event.php' => 'kalkulasievent.php',
		'eventdetail.php' => 'kalkulasieventdetail.php',
		'loginpurpose.php' => 'kalkulasiloginpurpose.php',
		'profile.php' => 'kalkulasiprofile.php',
		'paymentstatus.php' => 'kalkulasipaymentstatus.php',
		'orderhistory.php' => 'kalkulasiorderhistory.php'
	);
	if (array_key_exists ( $exppos, $listpagear )) {
		$menutree = getTrees($sqlcon,$path,$tingkat = 0,$parent = 0);
		$posisi = 0;
		include_once $listpagear[$exppos];
		$bannertextcarousel = bannertextcarousel($sqlcon,$posisi);
		$searchpolo = '';
		if (isset($_GET['search']) && $_GET['search']!=''){
			$searchpolo = trim($_GET['search']);
		}
	} else {
		header ( 'location:'.$path.'index.html' );
		exit ();
	}
	if (isset($sqlcon)){mysqli_close($sqlcon);}
}
?>