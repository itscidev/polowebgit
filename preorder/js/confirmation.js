function formReset(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$("#nama").val('');
	$("#hp").val('');
	$("#email").val('');
	$("#bukti").val('');
	var now = new Date(Date.now());
	$('#tgltrf').data('daterangepicker').setStartDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#tgltrf').data('daterangepicker').setEndDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
}
$(document).ready(function(){
	/* slide promo 
	var slidepromo = new Swiper('#slidepromo', {
		speed: 400,
		grabCursor: true,
		lazy: true,
		autoplay: {
			delay: 8000
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		loop: true
	});
	$("#slidepromo").hover(function(){
		slidepromo.autoplay.stop();
	}, function(){
		slidepromo.autoplay.start();
	});*/

	//Date range picker
	$('#tgltrf,#bn_to').daterangepicker({
		singleDatePicker: true,
        timePicker: false,
        locale: {
            format: 'MM/DD/YYYY'
        }
    });
	$("button.backbutton").click(function (){
		window.location.href = 'index.html';
	});
	$("button.submitbtn").click(function(){
		if ( $( '.has-error' ).hasClass( 'has-error' ) ){
			$('.has-error').removeClass('has-error');
		}
		$("div.submitmsg").html('Please wait...');
		$("button.submitbtn").hide();
		$.ajax({
			type: "POST",
			url: $("p.urlpost").html(),
			contentType: false,
			cache: false,
			processData: false,
			data: new FormData($( "#confirmation" )[0]),
			success: function( out ){
				if (out.hasOwnProperty('Error')){
					if (typeof out['Error'] == 'string'){
						$("div.submitmsg").html(out['Error']);
					}else{
						$("div.submitmsg").html('Ada kesalahan pengisian!');
						if (out['Error'].hasOwnProperty('nama')){
							$('#nama').parent().addClass( "has-error" );
						}
						if (out['Error'].hasOwnProperty('hp')){
							$('#hp').parent().addClass( "has-error" );
						}
						if (out['Error'].hasOwnProperty('email')){
							$('#email').parent().addClass( "has-error" );
						}
						if (out['Error'].hasOwnProperty('tgltrf')){
							$('#tgltrf').parent().addClass( "has-error" );
						}
						if (out['Error'].hasOwnProperty('bukti')){
							$('#bukti').parent().addClass( "has-error" );
						}
					}
				}else if (out.hasOwnProperty('Status')){
					formReset();
					$("div.submitmsg").html(out['Status']);
					setTimeout(function (){$("div.submitmsg").html('');},3000);
				}
				$("button.submitbtn").show();
			},
			error: function(jqXHR, textStatus, errorThrown){
				$("div.submitmsg").html(jqXHR);
				$("button.submitbtn").show();
			}
		});
	});
});