function formReset(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$("#nama").val('');
	$("#hp").val('');
	$("#email").val('');
	$("#alamat").val('');
	$("#warna").val('');
	hitungTotal();
}
function hitungTotal(){
	$("input.warna").each(function() {
		if ($(this).val()!=''){
			if (parseInt($(this).val()) < 2){
				$(this).val(2);
			}else{
				$(this).val( parseInt($(this).val()) );
			}
		}else{
			$(this).val(2);
		}
		$(this).closest( "tr" ).find('td.total').html(parseInt($(this).val()) * 50);
	});
}
$(document).ready(function(){
	/* slide promo 
	var slidepromo = new Swiper('#slidepromo', {
		speed: 400,
		grabCursor: true,
		lazy: true,
		autoplay: {
			delay: 8000
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		loop: true
	});
	$("#slidepromo").hover(function(){
		slidepromo.autoplay.stop();
	}, function(){
		slidepromo.autoplay.start();
	});*/
	$( "input.warna" ).keyup(function() {
		hitungTotal();
	});
	$( "input.warna" ).click(function() {
		hitungTotal();
	});
	//ajax post
	$("button.submitbtn").click(function(){
		if ( $( '.has-error' ).hasClass( 'has-error' ) ){
			$('.has-error').removeClass('has-error');
		}
		$("div.submitmsg").html('Please wait...');
		$("button.submitbtn").hide();
		$.ajax({
			type: "POST",
			url: $("p.urlpost").html(),
			contentType: false,
			cache: false,
			processData: false,
			data: new FormData($( "#order" )[0]),
			success: function( out ){
				if (out.hasOwnProperty('Error')){
					if (typeof out['Error'] == 'string'){
						$("div.submitmsg").html(out['Error']);
					}else{
						$("div.submitmsg").html('Ada kesalahan pengisian!');
						if (out['Error'].hasOwnProperty('nama')){
							$('#nama').parent().addClass( "has-error" );
						}
						if (out['Error'].hasOwnProperty('hp')){
							$('#hp').parent().addClass( "has-error" );
						}
						if (out['Error'].hasOwnProperty('email')){
							$('#email').parent().addClass( "has-error" );
						}
						if (out['Error'].hasOwnProperty('alamat')){
							$('#alamat').parent().addClass( "has-error" );
						}
						if (out['Error'].hasOwnProperty('warna')){
							$('#warna').parent().addClass( "has-error" );
						}
					}
				}else if (out.hasOwnProperty('Status')){
					formReset();
					$("div.submitmsg").html(out['Status']);
					setTimeout(function (){$("div.submitmsg").html('');},3000);
				}
				$("button.submitbtn").show();
			},
			error: function(jqXHR, textStatus, errorThrown){
				$("div.submitmsg").html(jqXHR);
				$("button.submitbtn").show();
			}
		});
	});
});