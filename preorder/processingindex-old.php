<?php
include_once("configuration/connect.php");
$result=array();

if (!isset($_SERVER['HTTP_REFERER'])){
	$result['Error']='Ilegal access detected!';
}elseif (substr_count($_SERVER['HTTP_REFERER'], '/preorder/')==0 && substr_count($_SERVER['HTTP_REFERER'], '/preorder.poloindonesia.com')==0){
	$result['Error']='Ilegal access detected!';
}
if (count($_POST)>0 && !isset($result['Error'])){
	if (isset($_POST['nama'])){$nama=trim($_POST['nama']);}else{$nama='';}
	if (isset($_POST['email'])){$email=trim($_POST['email']);}else{$email='';}
	if (isset($_POST['hp'])){$hp=trim($_POST['hp']);}else{$hp='';}
	if (isset($_POST['alamat'])){$alamat=trim($_POST['alamat']);}else{$alamat='';}
	if (isset($_POST['keterangan'])){$keterangan=trim($_POST['keterangan']);}else{$keterangan='';}
	if (isset($_POST['warnaungu'])){$warnaungu=intval($_POST['warnaungu']);}else{$warnaungu=0;}
	if (isset($_POST['warnakuning'])){$warnakuning=intval($_POST['warnakuning']);}else{$warnakuning=0;}
	if (isset($_POST['warnahijau'])){$warnahijau=intval($_POST['warnahijau']);}else{$warnahijau=0;}
	if (isset($_POST['warnabiru'])){$warnabiru=intval($_POST['warnabiru']);}else{$warnabiru=0;}
	if (isset($_POST['warnaputih'])){$warnaputih=intval($_POST['warnaputih']);}else{$warnaputih=0;}
	
	if ($nama==''){
		$result['Error']['nama']=1;
	}
	if ($email==''){
		$result['Error']['email']=1;
	}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$result['Error']['email']=1;
	}
	if ($hp==''){$result['Error']['hp']=1;}
	if ($alamat==''){$result['Error']['alamat']=1;}
	$pesanan = array();
	if ($warnaputih==0 && $warnakuning==0 && $warnabiru==0 && $warnahijau==0 && $warnaungu==0){
		$result['Error']['warnaputih']=1;
		$result['Error']['warnakuning']=1;
		$result['Error']['warnabiru']=1;
		$result['Error']['warnahijau']=1;
		$result['Error']['warnaungu']=1;
	}
	if ($warnaputih!=0){
		$pesanan['putih'] = $warnaputih;
	}
	if ($warnakuning!=0){
		$pesanan['kuning'] = $warnakuning;
	}
	if ($warnabiru!=0){
		$pesanan['biru'] = $warnabiru;
	}
	if ($warnahijau!=0){
		$pesanan['hijau'] = $warnahijau;
	}
	if ($warnaungu!=0){
		$pesanan['ungu'] = $warnaungu;
	}
	
	if (!isset($result['Error'])){
		mysqli_query( $sqlcon,
		"insert into preorder (".
			"po_nama,po_hp,po_email,".
			"po_alamat,po_keterangan,".
			"po_pesanan,po_status,".
			"po_create,po_updateby,po_update".
		") values (".
			"'".mysqli_real_escape_string( $sqlcon,$nama )."',".
			"'".mysqli_real_escape_string( $sqlcon,$hp )."',".
			"'".mysqli_real_escape_string( $sqlcon,$email )."',".
			"'".mysqli_real_escape_string( $sqlcon,$alamat )."',".
			"'".mysqli_real_escape_string( $sqlcon,$keterangan )."',".
			"'".mysqli_real_escape_string( $sqlcon,json_encode($pesanan) )."',0,".
			"'".date("Y-m-d H:i:s")."',0,'".date("Y-m-d H:i:s")."'".
		")"
		);
		
		$result['Status']='Penyimpanan berhasil!';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>