<?php include_once 'kalkulasiconfirmation.php';?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Polo Indonesia - Payment Type Preorder Confirmation</title>
	<meta name="description" content="Fasilitas khusus untuk pemesanan masker" />
	<meta name="keywords" content="pesan masker, masker murah" />
	<meta name="author" content="" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />
    <!-- Favicons -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png">
	<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="plugin/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="css/mystyle.css">
    <!-- Ionicons 
    <link rel="stylesheet" href="css/ionicons.min.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="container topbottomspace">
  		<div class="row">
  			<div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-12">
  				<div class="panel panel-primary">
					<div class="panel-heading">
						<h2 class="panel-title">Formulir Konfirmasi Pembayaran</h2>
					</div>
					<div class="panel-body">
<p>Jika Anda sudah di konfirmasi oleh team polo, Anda dapat melakukan pembayaran, dan melakukan pengisian formulir konfirmasi pembayaran di kolom bawah ini :</p>
<p>Jika ada pertanyaan perihal Pengembalian Dana dan Konfirmasi Pembayaran dapat Whatsapp ke Nomor dibawah ini :<br>
WA 0821-5392-2835 (Yhani) - WILAYAH xxx<br>
WA 0888-5428-123  (Anas) - WILAYAH xxx<br>
WA 0812-1321-7308 (Novellia) - WILAYAH xxx<br>
WA 0812-9731-1246 (Erna) - WILAYAH xxx<br>
WA 0817-181-588   (Vera) - WILAYAH xxx<br>
Jam Oprasional 09:30 - 22:00 WIB (Senin - Minggu)</p>
<p>Untuk Pemabayaran hanya dapat di Transfer ke:<br>
BCA Cabang XXX<br>
Nama Acc: PT. Primajaya Pantes Garment<br>
Acc# 161.301.5322</p>
					<form name="confirmation" id="confirmation" autocomplete="off">
					<div class="form-group">
						<label for="nama">Nama Pemesan</label>
						<input name="nama" type="text" class="form-control" id="nama" placeholder="Nama">
					</div>
					<div class="form-group">
						<label for="hp">Nomor Handphone Pemesan</label>
						<input name="hp" type="text" class="form-control" id="hp" placeholder="No HP">
					</div>
					<div class="form-group">
						<label for="email">Email Pemesan</label>
						<input name="email" type="email" class="form-control" id="email" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="tgltrf">Tanggal Transfer</label>
						<input name="tgltrf" type="text" class="form-control" id="tgltrf" placeholder="Tanggal Transfer" value="<?php echo $tgltrf;?>">
					</div>
					<div class="form-group">
						<label for="bukti">Upload Bukti Transfer <small>format : jpg / png</small></label>
						<input name="bukti" type="file" class="form-control" id="bukti" placeholder="bukti transfer">
					</div>
					</form>
					</div>
					<div class="panel-footer">
						<div class="submitmsg"></div>
						<p class="hide urlpost"><?php echo $urlpost;?></p>
						<button type="button" class="btn btn-default backbutton">Back</button>
						<button type="button" class="btn btn-primary submitbtn">Submit</button>
					</div>
  				</div>
  			</div>
  		</div>
  	</div>
    <!-- jQuery 2.1.4 -->
    <script src="plugin/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="plugin/bootstrap/js/bootstrap.min.js"></script>
    <!-- date-range-picker -->
	<script src="plugin/daterangepicker/moment.min.js"></script>
	<script src="plugin/daterangepicker/daterangepicker.js"></script>
    <script src="js/confirmation.js"></script>
  </body>
</html>
