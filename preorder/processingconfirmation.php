<?php
include_once("configuration/connect.php");
$result=array();

if (!isset($_SERVER['HTTP_REFERER'])){
	$result['Error']='Ilegal access detected!';
}elseif (substr_count($_SERVER['HTTP_REFERER'], '/confirmation.html')==0){
	$result['Error']='Ilegal access detected!';
}
if (count($_POST) > 0){
	if (isset($_POST['nama'])){$nama=trim($_POST['nama']);}else{$nama='';}
	if (isset($_POST['email'])){$email=trim($_POST['email']);}else{$email='';}
	if (isset($_POST['hp'])){$hp=trim($_POST['hp']);}else{$hp='';}
	if (isset($_POST['tgltrf'])){
		$exp = explode('/', substr($_POST['tgltrf'], 0,10));
		$tgltrf=$exp[2].'-'.$exp[0].'-'.$exp[1];
	}else{$tgltrf='';}
	
	if ($nama==''){$result['Error']['nama']=1;}
	if ($email==''){
		$result['Error']['email']=1;
	}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$result['Error']['email']=1;
	}
	if ($hp==''){$result['Error']['hp']=1;}
	if ($tgltrf==''){$result['Error']['tgltrf']=1;}
	if (!isset($_FILES['bukti']['name']) || (
			isset($_FILES['bukti']['name']) && $_FILES['bukti']['name']==''
		)
	){
		$result['Error']['bukti']=1;
	}elseif (
		strtolower($_FILES['bukti']['type'])!='image/jpeg' && strtolower($_FILES['bukti']['type'])!='image/png'
	){
		$result['Error']['bukti']=1;
	}
		
	if (!isset($result['Error'])){
		mysqli_query( $sqlcon,
		"insert into preorder_confirmation (".
			"pc_nama,pc_hp,pc_email,".
			"pc_bukti,pc_status,pc_tgltrf,".
			"pc_create,pc_updateby,pc_update".
		") values (".
			"'".mysqli_real_escape_string( $sqlcon,$nama )."',".
			"'".mysqli_real_escape_string( $sqlcon,$hp )."',".
			"'".mysqli_real_escape_string( $sqlcon,$email )."',".
			"'',0,'".$tgltrf."',".
			"'".date("Y-m-d H:i:s")."',0,'".date("Y-m-d H:i:s")."'".
		")"
		);
		$insertid = mysqli_insert_id( $sqlcon );

		$strrep = array("'",'"',' ','/','\\','#');
		$bukti = str_replace($strrep,'', $insertid.'-'.$_FILES['bukti']['name']);
		move_uploaded_file(
			$_FILES['bukti']['tmp_name'],'bukti/'.$bukti
		);
		
		mysqli_query( $sqlcon,
			"update preorder_confirmation set pc_bukti='".mysqli_real_escape_string( $sqlcon,$bukti )."' ".
			"where pc_id=".$insertid
		);

		$result['Status']='Penyimpanan berhasil!';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>