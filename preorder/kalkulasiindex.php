<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/index.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on'){
	$urlpost = 'https://';
}else{
	$urlpost = 'http://';
}
$urlpost .= $_SERVER['HTTP_HOST'];
if (substr_count($_SERVER['SCRIPT_NAME'],'/poloweb/')>0){
	$urlpost .= "/poloweb/preorder/processingindex.html";
}else{
	$urlpost .= "/processingindex.html";
}
?>