<?php include_once 'kalkulasiindex.php';?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Polo Indonesia - Preorder</title>
	<meta name="description" content="Fasilitas khusus untuk pemesanan masker" />
	<meta name="keywords" content="pesan masker, masker murah" />
	<meta name="author" content="" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />
    <!-- Favicons -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png">
	<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="plugin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/mystyle.css">
    <!-- Ionicons 
    <link rel="stylesheet" href="css/ionicons.min.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="container topbottomspace">
  		<div class="row">
  			<div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-12">
  				<div class="panel panel-primary">
					<div class="panel-heading">
						<h2 class="panel-title">Formulir Pemesanan Masker</h2>
					</div>
					<div class="panel-body">
<p>Untuk pemesanan mohon kesediannya untuk isi data berikut ini :</p>
<!-- <p>Jika Anda sudah selesai melakukan pengisian formulir pemesanan ini, artinya pre order produk masker Anda sudah masuk dalam list antrian kami.</p>
<p>Pembayaran dapat dilakukan jika sudah mendapatkan konfirmasi dari team Polo bahwa produk sudah siap dikirimkan.</p>
<p>Untuk Pemabayaran hanya dapat di Transfer ke:<br>
BCA Cabang XXX<br>
Nama Acc: PT. Primajaya Pantes Garment<br>
Acc# 161.301.5322</p> -->

<!-- <p>
(Sabtu,Minggu dan hari libur nasional tidak termasuk dalam hari kerja)<br>
*Ongkos Kirim ditanggung penerima</p>
<p>Berat Packing Masker -/+ 0.6kg<br>
Minimal Pembelian 50pcs</p>
<p>Jika ada pertanyaan perihal Pemesanan Produk,Penggiriman dan Konfirmasi Pembayaran dapat Whatsapp ke Nomor dibawah ini :<br>
WA 0821-5392-2835 (Yhani)<br>
WA 0888-5428-123  (Anas)<br>
WA 0812-9731-1246 (Erna)<br>
Jam Oprasional 09:30 - 22:00 WIB (Senin - Minggu)</p> -->
<p><span class="red">*</span> Mandatory</p>
					<form name="order" id="order" autocomplete="off">
					<div class="form-group">
						<label for="nama">Nama Pemesan <span class="red">*</span></label>
						<input name="nama" type="text" class="form-control" id="nama" placeholder="Nama">
					</div>
					<div class="form-group">
						<label for="hp">Nomor Handphone Pemesan <span class="red">*</span></label>
						<input name="hp" type="text" class="form-control" id="hp" placeholder="No HP">
					</div>
					<div class="form-group">
						<label for="email">Email Pemesan <span class="red">*</span></label>
						<input name="email" type="email" class="form-control" id="email" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="alamat">Alamat Lengkap (RT, RW, Kelurahan dan Kodepos) <span class="red">*</span></label>
						<textarea name="alamat" id="alamat" class="form-control" rows="3" placeholder="Alamat"></textarea>
					</div>
					<p>Qty pemesanan kelipatan 50pcs. Minimum order 100pcs. Warna random.</p>
					<table class="table table-striped">
				      <thead>
				        <!-- <tr>
				          <th width="10%">#</th>
				          <th width="50%">Warna</th>
				          <th width="20%">Qty (Pack) <span class="red">*</span></th>
				          <th width="20%">Total Pcs</th>
				        </tr> -->
				        <tr>
				          <th width="40%">Qty (Pack) <span class="red">*</span></th>
				          <th width="60%">Total Pcs</th>
				        </tr>
				      </thead>
				      <tbody>
				      	<tr>
				          <td><div class="form-group">
				          	<input name="warna" type="number" class="form-control warna" id="warna" placeholder="1/2/3..." value="2" min="2">
				          </div></td>
				          <td class="total">100</td>
				        </tr>
				        <!-- <tr>
				          <th scope="row">1</th>
				          <td><div class="optwarna" style="background-color: #f1c4e3;"></div><span>Ungu</span></td>
				          <td><div class="form-group">
				          	<input name="warnaungu" type="number" class="form-control warna" id="warnaungu" placeholder="1/2/3...">
				          </div></td>
				          <td class="total"></td>
				        </tr>
				        <tr>
				          <th scope="row">2</th>
				          <td><div class="optwarna" style="background-color: #f6e31a;"></div><span>Kuning</span></td>
				          <td><div class="form-group">
				          	<input name="warnakuning" type="number" class="form-control warna" id="warnakuning" placeholder="1/2/3...">
				          </div></td>
				          <td class="total"></td>
				        </tr>
				        <tr>
				          <th scope="row">3</th>
				          <td><div class="optwarna" style="background-color: #a7d12f;"></div><span>Hijau</span></td>
				          <td><div class="form-group">
				          	<input name="warnahijau" type="number" class="form-control warna" id="warnahijau" placeholder="1/2/3...">
				          </div></td>
				          <td class="total"></td>
				        </tr>
				        <tr>
				          <th scope="row">4</th>
				          <td><div class="optwarna" style="background-color: #385aa1;"></div><span>Biru</span></td>
				          <td><div class="form-group">
				          	<input name="warnabiru" type="number" class="form-control warna" id="warnabiru" placeholder="1/2/3...">
				          </div></td>
				          <td class="total"></td>
				        </tr>
				        <tr>
				          <th scope="row">5</th>
				          <td><div class="optwarna" style="background-color: #f7f7f7;"></div><span>Putih</span></td>
				          <td><div class="form-group">
				          	<input name="warnaputih" type="number" class="form-control warna" id="warnaputih" placeholder="1/2/3...">
				          </div></td>
				          <td class="total"></td>
				        </tr>
				        <tr>
				          <th scope="row" colspan="2">TOTAL</th>
				          <td class="packtot"></td>
				          <td class="pcstot"></td>
				        </tr> -->
				      </tbody>
				    </table>
				    <div class="form-group">
						<label for="keterangan">Keterangan</label>
						<textarea name="keterangan" id="keterangan" class="form-control" rows="3" placeholder="keterangan"></textarea>
					</div>
					</form>
					</div>
					<div class="panel-footer">
						<div class="submitmsg"></div>
						<p class="hide urlpost"><?php echo $urlpost;?></p>
						<button type="button" class="btn btn-primary submitbtn">Submit</button>
					</div>
<p>Jika sudah melakukan pembayaran Anda dapat klik link berikut ini untuk konfirmasi pembayaran :<br>
<a href="confirmation.html" title="pre-order payment confirmation">http://preorder.poloindonesia.com/confirmation.html</a></p>

  				</div>
  			</div>
  		</div>
  	</div>
    <!-- jQuery 2.1.4 -->
    <script src="plugin/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="plugin/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/index1.js"></script>
  </body>
</html>
