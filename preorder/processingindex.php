<?php
include_once("configuration/connect.php");
$result=array();

if (!isset($_SERVER['HTTP_REFERER'])){
	$result['Error']='Ilegal access detected!';
}elseif (substr_count($_SERVER['HTTP_REFERER'], '/preorder/')==0 && substr_count($_SERVER['HTTP_REFERER'], '/preorder.poloindonesia.com')==0){
	$result['Error']='Ilegal access detected!';
}
if (count($_POST)>0 && !isset($result['Error'])){
	if (isset($_POST['nama'])){$nama=trim($_POST['nama']);}else{$nama='';}
	if (isset($_POST['email'])){$email=trim($_POST['email']);}else{$email='';}
	if (isset($_POST['hp'])){$hp=trim($_POST['hp']);}else{$hp='';}
	if (isset($_POST['alamat'])){$alamat=trim($_POST['alamat']);}else{$alamat='';}
	if (isset($_POST['keterangan'])){$keterangan=trim($_POST['keterangan']);}else{$keterangan='';}
	if (isset($_POST['warna'])){$warna=intval($_POST['warna']);}else{$warna=0;}
	
	if ($nama==''){
		$result['Error']['nama']=1;
	}
	if ($email==''){
		$result['Error']['email']=1;
	}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$result['Error']['email']=1;
	}
	if ($hp==''){$result['Error']['hp']=1;}
	if ($alamat==''){$result['Error']['alamat']=1;}
	$pesanan = array();
	if ($warna==0){
		$result['Error']['warna']=1;
	}
	if ($warna!=0){
		$pesanan['random'] = $warna;
	}

	if (!isset($result['Error'])){
		mysqli_query( $sqlcon,
		"insert into preorder (".
			"po_nama,po_hp,po_email,".
			"po_alamat,po_keterangan,".
			"po_pesanan,po_status,".
			"po_create,po_updateby,po_update".
		") values (".
			"'".mysqli_real_escape_string( $sqlcon,$nama )."',".
			"'".mysqli_real_escape_string( $sqlcon,$hp )."',".
			"'".mysqli_real_escape_string( $sqlcon,$email )."',".
			"'".mysqli_real_escape_string( $sqlcon,$alamat )."',".
			"'".mysqli_real_escape_string( $sqlcon,$keterangan )."',".
			"'".mysqli_real_escape_string( $sqlcon,json_encode($pesanan) )."',0,".
			"'".date("Y-m-d H:i:s")."',0,'".date("Y-m-d H:i:s")."'".
		")"
		);
		
		$result['Status']='Penyimpanan berhasil!';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>