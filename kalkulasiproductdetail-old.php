<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/productdetail.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$errMsg = '';
$mapcolor = $mapstyle = $mapsize = array();
$arrvari = array();
if (isset($_GET['id']) && $_GET['id']!=''){
	$expid = explode('/', $_GET['id']);
	$id = $expid[( count($expid)-1 )];
	$strqry = "select pr.pr_id,pr.pr_ca_id,pr.pr_name,pr.pr_tagline,pr.pr_desc,pr.pr_info,pr.pr_care,".
		"pr.pr_price ".
	"from product pr ".
	"inner join product_variety on pv_pr_id=pr_id ".
	"where pr_status=1 and pv_status=1 and ".
		"lower(pr_name)='".mysqli_real_escape_string($sqlcon,strtolower($id))."' ".
	"group by pr_id limit 1";
	$prod = mysqli_fetch_assoc(mysqli_query($sqlcon,$strqry));
	if (isset($prod['pr_id']) && $prod['pr_id']>0){
		//get product image
		$prodimg = mysqli_query($sqlcon,
			"select pv.pv_name,pv.pv_qty,pv.pv_price,st_id,st_name,sz_id,sz_name,ci_id,ci_image ".
			"from product_variety pv ".
			"left outer join style on st_id=pv_st_id ".
			"left outer join size on sz_id=pv_sz_id ".
			"left outer join color_item on ci_id=pv_ci_id ".
			"where pv_status=1 and pv_pr_id=".$prod['pr_id']
		);
		while ($prodimg1 = mysqli_fetch_assoc($prodimg)){
			
		}
		//get variety
		$vari = mysqli_query($sqlcon,
			"select pv.pv_name,pv.pv_qty,pv.pv_price,st_id,st_name,sz_id,sz_name,ci_id,ci_image ".
			"from product_variety pv ".
			"left outer join style on st_id=pv_st_id ".
			"left outer join size on sz_id=pv_sz_id ".
			"left outer join color_item on ci_id=pv_ci_id ".
			"where pv_status=1 and pv_pr_id=".$prod['pr_id']
		);
		while ($vari1 = mysqli_fetch_assoc($vari)){
			
		}
	}else{
		$errMsg = 'Product not found!';
	}
}else{
	$errMsg = 'Are you lost or something?<br />Click here to '.
		'<a href="'.$path.'" title="Polo Indonesia Home Page">home page</a>';
}
//temp DB
// if (isset($_GET['id']) && $_GET['id']!=''){
// 	$expid = explode('/', $_GET['id']);
// 	$id = $expid[( count($expid)-1 )];
// }else{
// 	$id = 'Wangky';
// }
/* var optprod = {
	blue:["d1.jpg", "d2.jpg", "d3.jpg", "d4.jpg"],
	red:["e1.jpg", "e2.jpg", "e3.jpg", "e4.jpg"]
};
//thumbnail image
<div class="swiper-slide"><img src="<?php echo $path;?>images/productitem/d1.jpg"/></div>
//large image
<div class="swiper-slide"><img src="<?php echo $path;?>images/productitem/d1.jpg"/></div>
//color option
<div class="det_nav1">
	<h4>Select colour :</h4>
	<div class="sky-form col col-4">
		<img dataprod="blue" class="multiopt active" alt="blue" src="<?php echo $path;?>images/productcolor/c-blue-orange.jpg">
		<img dataprod="red" class="multiopt" alt="black" src="<?php echo $path;?>images/productcolor/c-grey-purple.jpg">
		<img dataprod="blue" class="multiopt" alt="red" src="<?php echo $path;?>images/productcolor/c-pink-blue.jpg">
		<img dataprod="red" class="multiopt" alt="white" src="<?php echo $path;?>images/productcolor/c-yellow-blue.jpg">
	</div>
</div>*/

$arrdb = array(
	'Wangky' => array(
		'opt' => 'var optprod = {yellow:["WANGKY-yellow.jpg"],white:["WANGKY-white.jpg"],tosca:["WANGKY-toscamuda.jpg"],orange:["WANGKY-orange.jpg"],merah:["WANGKY-merahstg.jpg"],birumuda:["WANGKY-birumuda.jpg"],navy:["WANGKY-birunavy.jpg"],biru:["WANGKY-biru.jpg"]};',
		'img' => '<div class="swiper-slide"><img src="'.$path.'images/productitem/WANGKY-yellow.jpg"/></div>',
		'color' => '<div class="det_nav1">'.
	'<h4>Select colour :</h4>'.
	'<div class="sky-form col col-4">'.
		'<img dataprod="yellow" class="multiopt active" alt="blue" src="'.$path.'images/productcolor/c-yellow-blue.jpg">'.
		'<img dataprod="white" class="multiopt" alt="black" src="'.$path.'images/productcolor/c-grey-purple.jpg">'.
		'<img dataprod="tosca" class="multiopt" alt="red" src="'.$path.'images/productcolor/c-blue-grey.jpg">'.
		'<img dataprod="orange" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-orange-blue.jpg">'.
		'<img dataprod="merah" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-pink-black.jpg">'.
		'<img dataprod="birumuda" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-blue-purple.jpg">'.
		'<img dataprod="navy" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-blue-red.jpg">'.
		'<img dataprod="biru" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-blue-yellow.jpg">'.
	'</div>'.
'</div>'
	),
	'Flag Edition Brasil' => array(
		'opt' => 'var optprod = {brasil:["FlagEdition-Brasil.jpg"]};',
		'img' => '<div class="swiper-slide"><img src="'.$path.'images/productitem/FlagEdition-Brasil.jpg"/></div>',
		'color' => ''
	),
	'Flag Edition England' => array(
		'opt' => 'var optprod = {england:["FlagEdition-England.jpg"]};',
		'img' => '<div class="swiper-slide"><img src="'.$path.'images/productitem/FlagEdition-England.jpg"/></div>',
		'color' => ''
	),
	'Flag Edition France' => array(
		'opt' => 'var optprod = {france:["FlagEdition-France.jpg"]};',
		'img' => '<div class="swiper-slide"><img src="'.$path.'images/productitem/FlagEdition-France.jpg"/></div>',
		'color' => ''
	)
);

$thumbimg = $normalimg = $coloroption = $optprod = '';
$optprod = $arrdb[$id]['opt'];
$thumbimg = $arrdb[$id]['img'];
$normalimg = $thumbimg;
$coloroption = $arrdb[$id]['color'];
?>