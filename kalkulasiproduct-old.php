<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/product.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}
function buildGet($path,$skip='',$kode=''){
	//parsing & build url query
	$url = '';
	if (isset($_GET) && count($_GET)>0){foreach ($_GET as $parget => $isiget){
		//default skip : categories & search
		if ($parget!='categories' && $parget!='search' && $skip!=$parget){
			if ($url != ''){$url .= '&';}
			$url .= $parget . '=' . rawurlencode(urlencode($isiget));
		}
	}}
	//echo $_SERVER['REDIRECT_URL'];
	$REQUEST_URI = explode('?',$_SERVER['REQUEST_URI']);
	$REQUEST_URI = $REQUEST_URI[0];
	if ($url == '' ){
		$url = $path.str_replace('/poloweb/','',$REQUEST_URI).'?'.$skip.'=';
	}else{
		$url = $path.str_replace('/poloweb/','',$REQUEST_URI).'?'.$url."&".$skip.'=';
	}
	$arrsatuan = array('sort');
	$arrmultiple = array('size','style','color','cat');
	if (in_array($skip,$arrsatuan)){
		$url .= $kode;
	}elseif (in_array($skip,$arrmultiple)){
		if ($kode == ''){
			if (substr_count($url,'?'.$skip.'=') > 0){
				$url = str_replace('?'.$skip.'=','',$url);
			}elseif (substr_count($url,'&'.$skip.'=') > 0){
				$url = str_replace('&'.$skip.'=','',$url);
			}
		}else{
			$url .= $kode;			
		}
	}
	return $url;
}
function catcek($sqlcon,$parent='',$catname=''){
	//categories checking
	$errMsg = '';
	$strsql = "select ca_id from category ".
	"where ca_name='".mysqli_real_escape_string($sqlcon,$catname)."' and ca_ca_id=".intval($parent);
	$cek = mysqli_query($sqlcon,$strsql);
	$cek1 = 0;
	if ($cek){$cek1 = mysqli_num_rows($cek);}
	if ($cek1 != 1){
		$errMsg = "Invalid selected categories!";
	}else{
		$cek2 = mysqli_fetch_assoc($cek);
		$errMsg = array($cek2['ca_id']);
	}
	return $errMsg;
}
function catchlcek($sqlcon,$parentid){
	$idreturn = array();
	$qrycek = "select ca_id from category where ca_status=1 and ca_ca_id=".$parentid;
	$cek = mysqli_query($sqlcon,$qrycek);
	$cek1 = 0;
	if ($cek){$cek1 = mysqli_num_rows($cek);}
	if ($cek1 > 0){while ($cek2 = mysqli_fetch_assoc($cek)){
		array_push($idreturn,$cek2['ca_id']);
		$idreturn = array_merge($idreturn,catchlcek($sqlcon,$cek2['ca_id']));
	}}
	return $idreturn;
}
function catfilter($sqlcon,$path='',$parent=0){
	$result = '';
	$choosencat = array();
	if (isset($_GET['cat']) && $_GET['cat']!=''){
		$choosencat = explode(',',$_GET['cat']);
	}
	$getpar = mysqli_query($sqlcon,
		"select ca.*,count(pr_id) jumprod from category ca ".
		"left outer join product on pr_ca_id=ca_id and pr_status=1 ".
		"where ca_ca_id=".$parent." and ca_status=1 ".
		"group by ca_id order by ca_sequence asc"
	);
	$getpar1 = 0;
	if ($getpar){$getpar1 = mysqli_num_rows($getpar);}
	if ($getpar1 > 0){while ($getpar2 = mysqli_fetch_assoc($getpar)){
		$divactive = $iactive = '';
		if ($getpar2['ca_ca_id'] == 0){
			$divactive = 'active';
		}
		$cekchld = mysqli_num_rows(mysqli_query($sqlcon,
			"select ca_id from category where ca_ca_id=".$getpar2['ca_id']." and ca_status=1"
		));
		$result .= '<div class="'.$divactive.'">';
		if ($cekchld > 0){
			$result .= '<span class="togcat">'.
				htmlentities($getpar2['ca_name']).'<i class="fa fa-angle-up '.$iactive.'"></i>'.
			'</span>';
			$result .= catfilter($sqlcon,$path,$getpar2['ca_id'],'');
		}else{
			$checked = '';
			$kodechoosen = $choosencat;
			if (in_array($getpar2['ca_id'],$choosencat)){
				$checked = ' checked="checked"';
				$key = array_search($getpar2['ca_id'], $kodechoosen);
				unset($kodechoosen[$key]);
			}else{
				array_push($kodechoosen,$getpar2['ca_id']);
			}
			$kode = '';
			if (count($kodechoosen) > 0){foreach ($kodechoosen as $indexid){
				if ($kode != ''){$kode .= ',';}
				$kode .= $indexid;
			}}
			$result .= '<label class="checkbox">'.
				'<input class="filterchecked" type="checkbox"'.$checked.' url="'.buildGet($path,'cat',$kode).'"><i></i>'.
				htmlentities($getpar2['ca_name'])." (".number_format($getpar2['jumprod'],0,',','.').")".
			'</label>';
		}
		$result .= '</div>';
	}}
	return $result;
}
//echo "<pre>";print_r($_SERVER);echo "</pre>";
//echo "<pre>";print_r($_GET);echo "</pre>";
function pagingsection($jprod1,$limit,$page){
	$spread = 2;
	$path = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REDIRECT_URL']."?";
	$no = 0;
	if (isset($_GET) && count($_GET)>0){foreach ($_GET as $getpar => $getcont){
		if ($getpar!='categories' && $getpar!='search' && $getpar!='page'){
			if ($no > 0){$path .= '&';}
			$path .= $getpar.'='.rawurlencode($getcont);
			$no++;
		}
	}}
	if ($no > 0){$path .= '&';}
	$res = '';
	$jpage = ceil($jprod1/$limit);
	$btsbwh = $page - $spread;
	if ($btsbwh < 1){$btsbwh = 1;}
	$btsats = $page + $spread;
	if ($btsats > $jpage){$btsats = $jpage;}
	if (($btsats-$btsbwh) < ($spread*2)){
		if ($btsbwh != 1){
			$btsbwh = $btsbwh - (($spread*2)-($btsats-$btsbwh));
			if ($btsbwh < 1){$btsbwh = 1;}
		}
		if ($btsats != $jpage){
			$btsats = $btsats + (($spread*2)-($btsats-$btsbwh));
			if ($btsats > $jpage){$btsats= $jpage;}
		}
	}
	for ($i=$btsbwh;$i<=$btsats;$i++){
		$active = '';
		if ($i == $page){$active = ' class="active"';}
		$res .= '<a href="'.$path.'page='.$i.'" title="Go to page '.$i.'"'.$active.'>'.$i.'</a>';
	}
	if ($btsbwh > 2){
		$res = '<a href="'.$path.'page=1" class="fullborder">1</a>'.
			'<a href="#" class="noborder" onclick="return false;">...</a>'.$res;
	}elseif ($btsbwh > 1){
		$res = '<a href="'.$path.'page=1">1</a>'.$res;
	}
	$arrsrc = array(
		' title="Go to page '.$btsats.'" class="active"',
		' title="Go to page '.$btsats.'">'
	);
	$arrrep = array(
		' title="Go to page '.$btsats.'" class="active fullborder"',
		' title="Go to page '.$btsats.'" class="fullborder">'
	);
	if ($btsats < ($jpage-1)){
		$res = str_replace($arrsrc,$arrrep,$res);
		$res .= '<a href="" class="noborder" onclick="return false;">...</a>'.
			'<a href="'.$path.'page='.$jpage.'" class="fullborder">'.$jpage.'</a>';
	}elseif ($btsats < $jpage){
		$res .= '<a href="'.$path.'page='.$jpage.'" class="fullborder">'.$jpage.'</a>';
	}else{
		$res = str_replace($arrsrc,$arrrep,$res);
	}
	return $res;
}

//echo "<pre>";print_r($_SERVER);echo "</pre>";
$errMsg = '';
$sinfil = $sinfil1 = $sinsort = $titlecatfil = $proditm = '';
$urlcat = $path.'details/';
if (isset($_GET['categories']) && $_GET['categories']!=''){
	//cek for valid category
	$errMsg = '';
	$struktur = explode('/',$_GET['categories']);
	if (count($struktur) > 1){
		foreach ($struktur as $index => $catname){
			if ($index == 0){$parent = 0;}else{
				//$parent = $struktur[( $index-1 )];
				$parent = $rescatcek[0];
			}
			$rescatcek = catcek($sqlcon,$parent,$catname);
			if (is_string($rescatcek) && $rescatcek!=''){
				$errMsg = $rescatcek;
				break;
			}
		}
	}else{
		$errMsg = "Forbidden selected categories!";
	}
	if ($errMsg == ''){
		$urlcat .= $_GET['categories'].'/';
		$titlecatfil = $struktur[( count($struktur)-1 )];
		
		$rescatcek = array_merge($rescatcek,catchlcek($sqlcon,$rescatcek[0]));
		if ($sinfil != ''){$sinfil .= ' and ';}
		$sinfil .= "pr_ca_id in (";
		foreach ($rescatcek as $indexcat => $contentcat){
			if ($indexcat > 0){$sinfil .= ',';}
			$sinfil .= $contentcat;
		}
		$sinfil .= ")";
	}
}elseif (isset($_GET['search']) && $_GET['search']!=''){
	$search = "%".str_replace(' ','%',mysqli_real_escape_string($sqlcon,strtolower(trim($_GET['search']))))."%";
	if ($sinfil != ''){$sinfil .= ' and ';}
	$sinfil .= "(lower(pr_name) like '".$search."' or lower(pr_tagline) like '".$search."' or ".
	"lower(pr_desc) like '".$search."' or pr_ca_id in (".
		"select ca_id from category where lower(ca_name) like '".$search."' and ca_status=1".
	"))";
}
if ($errMsg == ''){
	//sort by
	$optsortby = '';
	$arrsortby = array(
		''=>'Choose one','PLH'=>'Price Low to High','PHL'=>'Price High to Low','MP'=>'Most Popular',
		'BS'=>'Best Seller','N'=>'Newest'
	);
	if (isset($_GET['sort']) && $_GET['sort']!=''){
		if ($_GET['sort'] == 'PLH'){
			$sinsort = ' order by pv_price asc';
		}elseif ($_GET['sort'] == 'PHL'){
			$sinsort = ' order by pv_price desc';
		}elseif ($_GET['sort'] == 'N'){
			$sinsort = ' order by pv_update desc';
		}
	}
	foreach ($arrsortby as $kode => $namanya){
		if ($kode == ''){$disabled = ' disabled="disabled"';}else{$disabled = '';}
		if (isset($_GET['sort']) && $kode==$_GET['sort']){
			$selected = ' selected="selected"';
		}elseif ($kode == '' && (!isset($_GET['sort']))){
			$selected = ' selected="selected"';
		}elseif ($kode == '' && isset($_GET['sort']) && !array_key_exists($_GET['sort'], $arrsortby)){
			$selected = ' selected="selected"';
		}else{
			$selected = '';
		}
		$optsortby .= '<option value=""'.$disabled.$selected.' url="'.buildGet($path,'sort',$kode).'">'.$namanya.'</option>';
	}

	//filter categories
	$filtercat = '';
	if (!isset($_GET['categories'])){
		$filtercat = catfilter($sqlcon,$path);
		$choosencat = array();
		if (isset($_GET['cat']) && $_GET['cat']!=''){
			$choosencat = explode(',',$_GET['cat']);
		}
		if (count($choosencat) > 0){
			if ($sinfil != ''){$sinfil .= ' and ';}
			$sinfil .= "pr_ca_id in (";
			foreach ($choosencat as $index => $content){
				if ($index > 0){$sinfil .= ",";}
				$sinfil .= intval($content);
			}
			$sinfil .= ")";
		}
	}
	if ($filtercat != ''){
		$filtercat = '<section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Categories<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<div class="col col-4 filcat">'.$filtercat.'</div>'.
			'</div>'.
		'</section>';
	}
	
	//filter size
	$filtersize = '';
	$choosensize = array();
	if (isset($_GET['size']) && $_GET['size']!=''){
		$choosensize = explode(',',$_GET['size']);
		if ($sinfil1 != ''){$sinfil1 .= ' and ';}
		$sinfil1 .= "pv_sz_id in (";
		foreach ($choosensize as $index => $content){
			if ($index > 0){$sinfil1 .= ",";}
			$sinfil1 .= intval($content);
		}
		$sinfil1 .= ")";
	}
	$qrymstr = mysqli_query($sqlcon,"select * from size order by sz_sequence,sz_name");
	$qrymstr1 = 0;
	if ($qrymstr){$qrymstr1 = mysqli_num_rows($qrymstr);}
	if ($qrymstr1 > 0){while ($qrymstr2 = mysqli_fetch_assoc($qrymstr)){
		$checked = '';
		$kodechoosen = $choosensize;
		if (in_array($qrymstr2['sz_id'],$choosensize)){
			$checked = ' checked="checked"';
			$key = array_search($qrymstr2['sz_id'], $kodechoosen);
			unset($kodechoosen[$key]);
		}else{
			array_push($kodechoosen,$qrymstr2['sz_id']);
		}
		$kode = '';
		if (count($kodechoosen) > 0){foreach ($kodechoosen as $indexid){
			if ($kode != ''){$kode .= ',';}
			$kode .= $indexid;
		}}
		$filtersize .= '<label class="checkbox">'.
			'<input class="filterchecked" type="checkbox"'.$checked.' url="'.buildGet($path,'size',$kode).'">'.
			'<i></i>'.$qrymstr2['sz_name'].
		'</label>';
	}}
	if ($filtersize != ''){
		$filtersize = '<section class="sky-form">'.
			'<h4 class="showhidefiltersection">Shop by Size<i class="fa fa-chevron-down"></i></h4>'.
			'<div class="row1 scroll-pane">'.
				'<div class="col col-4">'.$filtersize.'</div>'.
			'</div>'.
		'</section>';
	}

	//filter style
	$filterstyle = '';
	$choosenstyle = array();
	if (isset($_GET['style']) && $_GET['style']!=''){
		$choosenstyle = explode(',',$_GET['style']);
		if ($sinfil1 != ''){$sinfil1 .= ' and ';}
		$sinfil1 .= "pv_st_id in (";
		foreach ($choosenstyle as $index => $content){
			if ($index > 0){$sinfil1 .= ",";}
			$sinfil1 .= intval($content);
		}
		$sinfil1 .= ")";
	}
	$qrymstr = mysqli_query($sqlcon,"select * from style order by st_sequence,st_name");
	$qrymstr1 = 0;
	if ($qrymstr){$qrymstr1 = mysqli_num_rows($qrymstr);}
	if ($qrymstr1 > 0){while ($qrymstr2 = mysqli_fetch_assoc($qrymstr)){
		$checked = '';
		$kodechoosen = $choosenstyle;
		if (in_array($qrymstr2['st_id'],$choosenstyle)){
			$checked = ' checked="checked"';
			$key = array_search($qrymstr2['st_id'], $kodechoosen);
			unset($kodechoosen[$key]);
		}else{
			array_push($kodechoosen,$qrymstr2['st_id']);
		}
		$kode = '';
		if (count($kodechoosen) > 0){foreach ($kodechoosen as $indexid){
			if ($kode != ''){$kode .= ',';}
			$kode .= $indexid;
		}}
		$filterstyle .= '<label class="checkbox">'.
			'<input class="filterchecked" type="checkbox"'.$checked.' url="'.buildGet($path,'style',$kode).'">'.
			'<i></i>'.$qrymstr2['st_name'].
		'</label>';
	}}
	if ($filterstyle != ''){
		$filterstyle = '<section class="sky-form">'.
			'<h4 class="showhidefiltersection">Shop by Style<i class="fa fa-chevron-down"></i></h4>'.
			'<div class="row1 scroll-pane">'.
				'<div class="col col-4">'.$filterstyle.'</div>'.
			'</div>'.
		'</section>';
	}

	//filter color
	$filtercolor = '';
	$choosencolor = array();
	if (isset($_GET['color']) && $_GET['color']!=''){
		$choosencolor = explode(',',$_GET['color']);
		if ($sinfil1 != ''){$sinfil1 .= ' and ';}
		$sinfil1 .= "pv_ci_id in ( select ci_id from color_item where ci_cl_id in (";
		foreach ($choosencolor as $index => $content){
			if ($index > 0){$sinfil1 .= ",";}
			$sinfil1 .= intval($content);
		}
		$sinfil1 .= "))";
	}
	$qrymstr = mysqli_query($sqlcon,"select * from color order by cl_code");
	$qrymstr1 = 0;
	if ($qrymstr){$qrymstr1 = mysqli_num_rows($qrymstr);}
	if ($qrymstr1 > 0){while ($qrymstr2 = mysqli_fetch_assoc($qrymstr)){
		$checked = '';
		$kodechoosen = $choosencolor;
		if (in_array($qrymstr2['cl_id'],$choosencolor)){
			$checked = '<i class="fa fa-check"></i>';
			$key = array_search($qrymstr2['cl_id'], $kodechoosen);
			unset($kodechoosen[$key]);
		}else{
			array_push($kodechoosen,$qrymstr2['cl_id']);
		}
		$kode = '';
		if (count($kodechoosen) > 0){foreach ($kodechoosen as $indexid){
			if ($kode != ''){$kode .= ',';}
			$kode .= $indexid;
		}}
//		<li><a href="#" style="background-color: #E60D41;"><i class="fa fa-check"></i></a></li>
		$filtercolor .= '<li><a href="'.buildGet($path,'color',$kode).'" '.
			'style="background-color: #'.$qrymstr2['cl_code'].';">'.$checked.'</a></li>';
	}}
	if ($filtercolor != ''){
		$filtercolor = '<section class="sky-form">'.
			'<h4 class="showhidefiltersection">Shop by Colour<i class="fa fa-chevron-down"></i></h4>'.
			'<div class="row1 scroll-pane">'.
				'<ul class="w_nav2 filtercolor">'.$filtercolor.'</ul>'.
			'</div>'.
		'</section>';
	}
	
	//------------------------------------------------------------------------------------------------
	//construct filter query
	if ($sinfil1 != ''){
		if ($sinfil != ''){$sinfil .= ' and ';}
		$sinfil .= "pr_id in (".
			"select pv_pr_id from product_variety where ".$sinfil1." and pv_status=1".
		"))";
	}
	//count product
	$qryjprod = "select * from product where ".$sinfil;
	$jprod = mysqli_query($sqlcon,$qryjprod);
	$jprod1 = 0;
	if ($jprod){$jprod1 = mysqli_num_rows($jprod);}
	if ($jprod1 == 0){
		$errMsg = 'No product found!';
	}else{
		//construct limit
		$limit = 12;$page = 1;
		if (isset($_GET['page'])){$page = intval($_GET['page']);}
		if ($page > ceil($jprod1/$limit)){$page = ceil($jprod1/$limit);}
		$sinlimit = " limit ".(($page-1)*$limit).",".$limit;
		//paging section
		$pagingsection = pagingsection($jprod1,$limit,$page);
		//get product
		$prod = mysqli_query($sqlcon,"select * from product where ".$sinfil.$sinsort.$sinlimit);
		while ($prod2 = mysqli_fetch_assoc($prod)){
			$price = mysqli_fetch_assoc(mysqli_query($sqlcon,
				"select min(pv_price) minprice,max(pv_price) maxprice from product_variety ".
				"where pv_status=1 and pv_pr_id=".$prod2['pr_id']
			));
			if ($price['minprice'] == $price['maxprice']){
				$pricep = 'IDR '.number_format($price['minprice'],0,',','.');
			}else{
				$pricep = 'IDR '.number_format($price['minprice'],0,',','.').' - IDR '.
						number_format($price['maxprice'],0,',','.');
			}
			
			$labeldisc = $classlabeldisc = '';
			if ($prod2['pr_price'] > $price['minprice']){
				$classlabeldisc = ' withlabeldisc';
				$labeldisc = '<div class="labeldiscount">'.
					'<img alt="Special Promo Discount" src="'.$path.'images/icon/label-discount.png">'.
					'<div class="discnumber">'.ceil(
							(($prod2['pr_price']-$price['minprice'])/$prod2['pr_price'])*100
					).'</div>'.
				'</div>';
			}
			
			$mainimg = 'default.png';$optcolor = '';
			$arroptcol = array();
			$img = mysqli_query($sqlcon,
				"select pi_st_id,pi_ci_id,pi_image,ci_image,cl_name from product_image ".
				"inner join color_item on ci_id=pi_ci_id ".
				"inner join color on cl_id=ci_cl_id ".
				"where pi_status=1 and pi_image!='' and ci_image!='' and pi_pr_id=".$prod2['pr_id']." ".
				"order by pi_sequence asc"
			);
			$img1 = 0;
			if ($img){$img1 = mysqli_num_rows($img);}
			if ($img1 > 0){while ($img2 = mysqli_fetch_assoc($img)){
				if (file_exists("images/productitem/".$img2['pi_image'])){
					if ($mainimg=='default.png'){
						$mainimg = "productitem/".$img2['pi_image'];
					}
					
					$strdt = $img2['pi_st_id'].':'.$img2['pi_ci_id'];
					$clrname = '';
					if ($img2['cl_name'] != ''){
						$clrname = ' '.$img2['cl_name'];
					}
					if (!isset($arroptcol[$strdt])){
						$arroptcol[$strdt] = array(
							$prod2['pr_name'].$clrname,$img2['pi_image'],$img2['ci_image']
						);
					}
				}
			}}
			if (count($arroptcol) > 1){
				foreach ($arroptcol as $contentoptcol){
					$optcolor .= '<img class="colortype" alt="'.htmlentities($contentoptcol[0]).'" '.
						'img-src="'.$path.'images/productitem/'.$contentoptcol[1].'" '.
						'src="'.$path.'images/productcolor/'.$contentoptcol[2].'">';
				}
				$optcolor = '<div class="grid_1'.$classlabeldisc.'">'.$optcolor.'</div>';
			}
			
			$proditm .= '<div class="col-sm-4 col-xs-6 proditm">'.
				'<div class="content_box">'.
					'<a href="'.$urlcat.rawurlencode(urlencode($prod2['pr_name'])).'">'.
						'<img src="'.$path.'images/'.$mainimg.'" class="img-responsive" '.
						'alt="'.htmlentities($prod2['pr_name']).'"/>'.$labeldisc.
					'</a>'.
					'<h4 class="'.$classlabeldisc.'">'.
						'<a href="'.$urlcat.rawurlencode(urlencode($prod2['pr_name'])).'">'.
							htmlentities($prod2['pr_name']).
						'</a>'.
					'</h4>'.
					'<div class="grid_1 simpleCart_shelfItem'.$classlabeldisc.'">'.
						'<div class="item_add"><span class="item_price"><h6>'.$pricep.'</h6></span></div>'.
					'</div>'.$optcolor.
				'</div>'.
			'</div>';
		}
	}
}
?>