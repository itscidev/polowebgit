<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
function smail($sqlcon,$contactemail,$contactnama,$subject,$strhtml){
	$result = array();
	$cekpurp = @mysqli_query($sqlcon,
		"select * from mail_smtp where ms_purpose='web'"
	);
	$cekpurp1 = 0;
	if ($cekpurp){$cekpurp1 = mysqli_num_rows($cekpurp);}
	if ($cekpurp1 != 1){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'] .= 'SMTP not found!';
	}else{
		$sender = mysqli_fetch_array($cekpurp);
		$host = $sender['ms_host'];
		$port = $sender['ms_port'];
		$secure = $sender['ms_secure'];
		$emailfr = $sender['ms_sendermail'];
		$emailpwd = $sender['ms_password'];
		$emailname = $sender['ms_sendername'];

		require 'PHPMailer/PHPMailerAutoload.php';

		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		// Set PHPMailer to use the sendmail transport
		$mail->isSMTP();
		//$mail->isSendmail();
		$mail->Host = $host;
		$mail->Port = $port;
		$mail->SMTPSecure = $secure;
		$mail->SMTPAuth = true;
		$mail->Username = $emailfr;
		$mail->Password = $emailpwd;
		//Set who the message is to be sent from
		$mail->setFrom( $emailfr,$emailname );
		//Set an alternative reply-to address
		$mail->addReplyTo( $emailfr,$emailname );
		//Set an alternative BCC address
		$mail->addBCC( $emailfr,$emailname );
		//Set who the message is to be sent to
		$mail->addAddress($contactemail,$contactnama);
		//Set the subject line
		$mail->Subject = $subject;
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$strhtml="<html>".
		"<head></head>".
		"<body>".
			$strhtml.
			"<br><br><br><br><br>Regards,<br>".$emailname.
		"</body>".
		"</html>";
		$mail->msgHTML($strhtml);
		//Replace the plain text body with one created manually
		$mail->AltBody = 'We are sorry, your email client is not support HTML format. Please open with other programs. Thank you.';
		//Attach an image file
		//	$mail->addAttachment('images/phpmailer_mini.png');

		//send the message, check for errors
		if (!$mail->send()) {
			//	    echo "Mailer Error: " . $mail->ErrorInfo;
			$result['Error'] = 'Can not send email. '.$mail->ErrorInfo;
		}else{
			$result['Status'] = 'Email send success!';
		}
	}
	return $result;
}
$result = array();
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/profile/')==0){
	$result['Error']='Ilegal access detected!';
}else{
	include_once 'cms/configuration/connect.php';
	include_once 'cms/model/member.php';
	//1:change name,2:send email verification,3:change password,4:upload foto
	if (isset($_POST['mode'])){$mode=intval($_POST['mode']);}else{$mode=0;}
	if (isset($_POST['name'])){$name=trim($_POST['name']);}else{$name='';}
	if (isset($_POST['passoldlogin'])){$passoldlogin=trim($_POST['passoldlogin']);}else{$passoldlogin='';}
	if (isset($_POST['passlogin'])){$passlogin=trim($_POST['passlogin']);}else{$passlogin='';}
	if (isset($_POST['passretypelogin'])){$passretypelogin=trim($_POST['passretypelogin']);}else{$passretypelogin='';}
	
	if ($mode==0){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='No action detected!';
	}
	if ($name=='' && $mode==1){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Name cannot empty!';
	}
	if ($mode==3){
		if ($passoldlogin==''){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Please fill Old Password field!';
		}elseif (strlen($passoldlogin)<8){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Old Password length can not less then 8 character!';
		}else{
			$passnum = $passlower = $passupper = 0;
			for ($i=0;$i<strlen($passoldlogin);$i++){
				$a = substr($passoldlogin,$i,1);
				if ($passnum==0 && ord($a)>=48 && ord($a)<=57){
					$passnum=1;
				}elseif ($passlower==0 && ord($a)>=97 && ord($a)<=122){
					$passlower = 1;
				}elseif ($passupper==0 && ord($a)>=65 && ord($a)<=90){
					$passupper = 1;
				}
				if ($passnum==1 && $passlower==1 && $passupper==1){
					break;
				}
			}
			if ($passnum==0 || $passlower==0 || $passupper==0){
				if (!isset($result['Error'])){
					$result['Error'] = '';
				}else{
					$result['Error'] .= '<br />';
				}
				$result['Error'].='Old Passwords must contain numbers, lowercase and uppercase letters!';
			}
		}
		if ($passlogin==''){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Please fill New Password field!';
		}elseif (strlen($passlogin)<8){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='New Password length can not less then 8 character!';
		}else{
			$passnum = $passlower = $passupper = 0;
			for ($i=0;$i<strlen($passlogin);$i++){
				$a = substr($passlogin,$i,1);
				if ($passnum==0 && ord($a)>=48 && ord($a)<=57){
					$passnum=1;
				}elseif ($passlower==0 && ord($a)>=97 && ord($a)<=122){
					$passlower = 1;
				}elseif ($passupper==0 && ord($a)>=65 && ord($a)<=90){
					$passupper = 1;
				}
				if ($passnum==1 && $passlower==1 && $passupper==1){
					break;
				}
			}
			if ($passnum==0 || $passlower==0 || $passupper==0){
				if (!isset($result['Error'])){
					$result['Error'] = '';
				}else{
					$result['Error'] .= '<br />';
				}
				$result['Error'].='New Passwords must contain numbers, lowercase and uppercase letters!';
			}
		}
		if ($passretypelogin==''){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Please fill Retype Password field!';
		}elseif ($passretypelogin!=$passlogin){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Retype Password field not same!';
		}
	}
	if ($mode==4){
		if (!isset($_FILES['fotouploader']['name'])){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Can not detected foto field!';
		}elseif (
			strtolower($_FILES['fotouploader']['type'])!='image/jpeg' 
			&& strtolower($_FILES['fotouploader']['type'])!='image/png'
		){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Can not accepted image type!';
		}elseif ($_FILES['fotouploader']['size'] > 1048576){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Max. file size is 1MB!';
		}
	}
}
if (!isset($result['Error'])){
	if (isset($_SERVER['HTTP_HOST']) && (
		$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
		|| $_SERVER['HTTP_HOST']=='172.16.1.19'
	)){
		$localserv = 1;
	}else{
		$localserv = 0;
	}
	session_start();
	if (isset($_SESSION["loginmem"])){
		//visitorID:timestampEnd:rememberMe
		$explvst = explode(':',$_SESSION["loginmem"]);
		$vstid = $explvst[0];
		//cek existing
		$field = 'mm_email,mm_password,mm_foto';
		$join = '';
		$where = "mm_id=".$vstid;
		$exis = member_s($sqlcon,$field,$join,$where);
		if (!is_array($exis)){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].=$exis;
		}else{
			$exis1 = mysqli_fetch_assoc($exis[1]);
		}
	}else{
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Session expired. Please sign in again!';
	}
}
if (!isset($result['Error']) && $mode==1){
	$set = "mm_update='".date("Y-m-d H:i:s")."',mm_name='".mysqli_real_escape_string($sqlcon,$name)."'";
	member_u($sqlcon,$set,$vstid);
	//	    echo "Message sent!";
	$result['Status'] = 'Name saved successfully!';
}
if (!isset($result['Error']) && $mode==2){
	if (isset($_SERVER['HTTP_HOST']) && (
		$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
		|| $_SERVER['HTTP_HOST']=='172.16.1.19'
	)){
		$localserv = 1;
	}else{
		$localserv = 0;
	}
	$emaillogin = $exis1['mm_email'];
	$subject = "Polo Indonesia email verification";
	$strhtml="Dear Valuable Customer";
	$strhtml.="<br><br>Click this link below to verified your email : <br>";
	if ($localserv==1){
		$strhtml.="<a href=\"http://".$_SERVER['HTTP_HOST']."/poloweb/loginpurpose/activate/".md5($emaillogin).
		"\" title=\"activate my email account\">click this link</a>";
	}else{
		$strhtml.="<a href=\"http://".$_SERVER['HTTP_HOST']."/profile/loginpurpose/".md5($emaillogin).
		"\" title=\"activate my email account\">click this link</a>";
	}
	$strhtml.=".<br>After activated you can receive notice from us such as invoice billing, ".
			"payment confirmation, order progres, etc.";
	
	$send = smail($sqlcon,$emaillogin,'',$subject,$strhtml);
	if (isset($send['Error'])){
		$result['Error'] = $send['Error'];
	}else {
		$result['Status'] = 'Email verification sent successfully. Please cek your email!';
	}
}if (!isset($result['Error']) && $mode==3){
	if (md5($passoldlogin)!=$exis1['mm_password']){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Wrong Old Password!';
	}else{
		$set = "mm_update='".date("Y-m-d H:i:s")."',mm_password='".md5($passlogin)."'";
		member_u($sqlcon,$set,$vstid);
		//	    echo "Message sent!";
		$result['Status'] = 'Password reset successfully!';
	}
}if (!isset($result['Error']) && $mode==4){
	if ($exis1['mm_foto']!='' && file_exists("images/foto/".$exis1['mm_foto'])){
		unlink("images/foto/".$exis1['mm_foto']);
	}
	$strrep = array("'",'"',' ','/','\\','#');
	$expfile = explode('.',$_FILES['fotouploader']['name']);
	$exts = ".".$expfile[(count($expfile)-1)];
	array_push($strrep,$exts);
	$imagename = str_replace($strrep,'',$_FILES['fotouploader']['name'])."-".$vstid.$exts;
	move_uploaded_file(
		$_FILES['fotouploader']['tmp_name'],'images/foto/'.$imagename
	);
	$set = "mm_update='".date("Y-m-d H:i:s")."',mm_foto='".mysqli_real_escape_string($sqlcon,$imagename)."'";
	member_u($sqlcon,$set,$vstid);
	//	    echo "Message sent!";
	$result['images'] = $imagename;
	$result['Status'] = 'Foto upload successfully!';
}
if (isset($sqlcon)){mysqli_close($sqlcon);}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>