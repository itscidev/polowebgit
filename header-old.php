<div class="agileheader" id="agileitshome">

<!-- Navigation -->
<nav class="navbar navbar-default w3ls navbar-fixed-top">
	<div class="container">
		<div class="row rownavbar">
			<div class="col-sm-3 hideonmobile">
				<div>
					<a href="#" title="Change language">
						<span>Language</span>
						<img alt="Choose Language" src="<?php echo $path;?>images/icon/globe.jpg">
					</a>
					<a href="stores.html" title="Find nearest POLO store">
						<img alt="Find POLO Store" src="<?php echo $path;?>images/icon/pointer1.jpg">
					</a>
				</div>
			</div>
			<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" 
			data-target="#bs-megadropdown-tabs">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="col-sm-6" align="center">
				<a class="navbar-brand agileinfo" href="<?php echo $path;?>index.html">
					<img src="<?php echo $path;?>images/polo-logo.jpg" title="POLO Indonesia Webstore">
				</a>
			</div>
			<div class="col-sm-3" align="right" id="rightheadermobile">
				<div class="rightheader">
					<input type="text" name="searchpolo" placeholder="Search.."> 
					<!-- <img alt="Search" src="images/icon/magnifier.jpg" style="width: 40px;"> -->
					<img alt="My Account" src="<?php echo $path;?>images/icon/avatar.jpg">
					<img alt="My Order List" src="<?php echo $path;?>images/icon/cart.jpg">
				</div>
			</div>
			
			<!-- <a class="navbar-brand agileinfo" href="index.html"><span>GROOVY</span> APPAREL</a> --> 
		</div>
		<?php /*?>
		<div class="navbar-header wthree nav_2">
			<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" 
			data-target="#bs-megadropdown-tabs">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- <a class="navbar-brand agileinfo" href="index.html"><span>GROOVY</span> APPAREL</a> --> 
			<a class="navbar-brand agileinfo" href="<?php echo $path;?>index.html" style="padding:2px;">
				<img src="<?php echo $path;?>images/polo-logo.jpg" title="POLO Indonesia Webstore">
			</a>
			<ul class="w3header-cart">
				<li class="wthreecartaits">
					<span class="my-cart-icon">
						<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
						<span class="badge badge-notify my-cart-badge"></span>
					</span>
				</li>
			</ul>
		</div>*/?>
		<div id="bs-megadropdown-tabs" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<?php echo $menutree;/*
				<li class="dropdown">
					<a href="#" class="dropdown-toggle w3-agile hyper" data-toggle="dropdown">
						<span> MEN </span>
					</a>
					<ul class="dropdown-menu aits-w3 multi multi1">
						<div class="row">
							<div class="col-sm-9">
								<div class="row">
									<div class="col-sm-3 w3layouts-nav-agile w3layouts-mens-nav-agileits w3layouts-mens-nav-agileits-1">
										<ul class="multi-column-dropdown">
											<li class="heading"><a href="<?php echo $path;?>women.html">FEATURED</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>New Arrivals</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Online Only</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Brands</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Clearance Sale</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Discount Store</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Editor's Pick</a></li>
										</ul>
									</div>

									<div class="col-sm-3 w3layouts-nav-agile w3layouts-mens-nav-agileits w3layouts-mens-nav-agileits-1">
										<ul class="multi-column-dropdown">
											<li class="heading">FEATURED</li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>New Arrivals</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Online Only</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Brands</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Clearance Sale</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Discount Store</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Editor's Pick</a></li>
										</ul>
									</div>

									<div class="col-sm-3 w3layouts-nav-agile w3layouts-mens-nav-agileits w3layouts-mens-nav-agileits-1">
										<ul class="multi-column-dropdown">
											<li class="heading">FEATURED</li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>New Arrivals</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Online Only</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Brands</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Clearance Sale</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Discount Store</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Editor's Pick</a></li>
										</ul>
									</div>
									
									<div class="col-sm-3 w3layouts-nav-agile w3layouts-mens-nav-agileits w3layouts-mens-nav-agileits-1">
										<ul class="multi-column-dropdown">
											<li class="heading">FEATURED</li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>New Arrivals</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Online Only</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Brands</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Clearance Sale</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Discount Store</a></li>
											<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Editor's Pick</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
							
								<div class="col-sm-12 w3layouts-nav-agile w3layouts-mens-nav-agileits w3layouts-mens-nav-agileits-4">
									<p>ACCESSORIES</p>
									<a href="<?php echo $path;?>women.html"><img src="<?php echo $path;?>images/men-nav-3.jpg" alt="Groovy Apparel"></a>
								</div>
							
							
							</div>
							<div class="clearfix"></div>
							<p class="promo">Use Promo Code <span>#CFFGTY56</span> and take 30% off the products. <a href="#">Details</a></p>
						</div>

					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle hyper" data-toggle="dropdown"><span> WOMEN GENDER </span></a>
					<ul class="dropdown-menu multi multi1">
						<div class="row">

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-womens-nav-agileits w3layouts-womens-nav-agileits-1">
								<ul class="multi-column-dropdown">
									<li class="heading">FEATURED</li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>New Arrivals</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Online Only</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Brands</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Clearance Sale</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Discount Store</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Editor's Pick</a></li>
								</ul>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-womens-nav-agileits w3layouts-womens-nav-agileits-2">
								<p>TOP</p>
								<a href="<?php echo $path;?>women.html"><img src="<?php echo $path;?>images/women-nav-1.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-womens-nav-agileits w3layouts-womens-nav-agileits-3">
								<p>LEGS</p>
								<a href="<?php echo $path;?>women.html"><img src="<?php echo $path;?>images/women-nav-2.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-womens-nav-agileits w3layouts-womens-nav-agileits-4">
								<p>ACCESSORIES</p>
								<a href="<?php echo $path;?>women.html"><img src="<?php echo $path;?>images/women-nav-3.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="clearfix"></div>
							<p class="promo">Use Promo Code <span>#CFFGTY56</span> and take 30% off the products. <a href="#">Details</a></p>
						</div>

					</ul>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle hyper" data-toggle="dropdown"><span> KIDS </span></a>
					<ul class="dropdown-menu multi multi1">
						<div class="row">

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-acc-nav-agileits w3layouts-acc-nav-agileits-1">
								<ul class="multi-column-dropdown">
									<li class="heading">FEATURED</li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>New Arrivals</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Online Only</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Brands</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Clearance Sale</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Discount Store</a></li>
									<li><a href="<?php echo $path;?>women.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Editor's Pick</a></li>
								</ul>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-acc-nav-agileits w3layouts-acc-nav-agileits-2">
								<p>BOY</p>
								<a href="<?php echo $path;?>women.html"><img src="<?php echo $path;?>images/other-nav-1.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-acc-nav-agileits w3layouts-acc-nav-agileits-3">
								<p>GIRL</p>
								<a href="<?php echo $path;?>women.html"><img src="<?php echo $path;?>images/other-nav-2.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-acc-nav-agileits w3layouts-acc-nav-agileits-4">
								<p>ACCESSORIES</p>
								<a href="<?php echo $path;?>women.html"><img src="<?php echo $path;?>images/other-nav-3.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="clearfix"></div>
							<p class="promo">Use Promo Code <span>#CFFGTY56</span> and take 30% off the products. <a href="#">Details</a></p>
						</div>

					</ul>
				</li>*/?>
				<li><a href="about.html">EVENT</a></li>
				<li><a href="about.html">GIFTS</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle hyper" data-toggle="dropdown"><span> SALE </span></a>
					<ul class="dropdown-menu multi multi1">
						<div class="row">

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-hot-nav-agileits w3layouts-hot-nav-agileits-1">
								<ul class="multi-column-dropdown">
									<li class="heading">FEATURED</li>
									<li><a href="<?php echo $path;?>"><i class="fa fa-angle-right" aria-hidden="true"></i>New Arrivals</a></li>
									<li><a href="<?php echo $path;?>"><i class="fa fa-angle-right" aria-hidden="true"></i>Online Only</a></li>
									<li><a href="<?php echo $path;?>"><i class="fa fa-angle-right" aria-hidden="true"></i>Brands</a></li>
									<li><a href="<?php echo $path;?>"><i class="fa fa-angle-right" aria-hidden="true"></i>Clearance Sale</a></li>
									<li><a href="<?php echo $path;?>"><i class="fa fa-angle-right" aria-hidden="true"></i>Discount Store</a></li>
									<li><a href="<?php echo $path;?>"><i class="fa fa-angle-right" aria-hidden="true"></i>Editor's Pick</a></li>
								</ul>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-hot-nav-agileits w3layouts-hot-nav-agileits-2">
								<p>MEN</p>
								<a href="<?php echo $path;?>"><img src="<?php echo $path;?>images/other-nav-1.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-hot-nav-agileits w3layouts-hot-nav-agileits-3">
								<p>WOMEN</p>
								<a href="<?php echo $path;?>"><img src="<?php echo $path;?>images/other-nav-2.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="col-sm-3 w3layouts-nav-agile w3layouts-hot-nav-agileits w3layouts-hot-nav-agileits-4">
								<p>KIDS</p>
								<a href="<?php echo $path;?>"><img src="<?php echo $path;?>images/other-nav-3.jpg" alt="Groovy Apparel"></a>
							</div>

							<div class="clearfix"></div>
							<p class="promo">Use Promo Code <span>#CFFGTY56</span> and take 30% off the products. <a href="#">Details</a></p>
						</div>

					</ul>
				</li>
				
				<!-- <li class="wthreesearch">
					<form action="#" method="post">
						<input type="search" name="Search" placeholder="Search for a Product" required="">
						<button type="submit" class="btn btn-default search" aria-label="Left Align">
							<i class="fa fa-search" aria-hidden="true"></i>
						</button>
					</form>
				</li>
				<li class="wthreecartaits wthreecartaits2 cart cart box_1">
					<form action="#" method="post" class="last"> 
						<input type="hidden" name="cmd" value="_cart" />
						<input type="hidden" name="display" value="1" />
						<button class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
					</form>   
				</li> -->
			</ul>
		</div>

	</div>
</nav>
<!-- //Navigation -->

<!-- Header-Top-Bar-(Hidden) -->
<div class="agileheader-topbar">
	<div class="container">
		<?php /*?>
		<div class="col-md-6 agileheader-topbar-grid agileheader-topbar-grid1">
			<p>Free shipping on orders over $150. <a href="<?php echo $path;?>stores.html">Details</a></p>
		</div>
		<div class="col-md-6 agileheader-topbar-grid agileheader-topbar-grid2">
			<ul>
				<li><a href="<?php echo $path;?>stores.html">Store Locator</a></li>
				<li><a href="<?php echo $path;?>faq.html">FAQ</a></li>
				<li><a class="popup-with-zoom-anim" href="#small-dialog1">Login</a></li>
				<li><a class="popup-with-zoom-anim" href="#small-dialog2">Sign Up</a></li>
				<!-- <li><a href="codes.html">Codes</a></li>
				<li><a href="icons.html">Icons</a></li> -->
				<li><a href="<?php echo $path;?>contact.html">Contact</a></li>
			</ul>
		</div>*/?>
		<div class="clearfix"></div>
	</div>

	<!-- Popup-Box -->
	<div id="popup1">
		<div id="small-dialog1" class="mfp-hide agileinfo">
			<div class="pop_up">
			<form action="#" method="post">
				<h3>LOGIN</h3>
				<input type="text" Name="Userame" placeholder="Username" required="">
				<input type="password" Name="Password" placeholder="Password" required="">
				<ul class="tick w3layouts agileinfo">
					<li>
						<input type="checkbox" id="brand1" value="">
						<label for="brand1"><span></span>Remember me</label>
					</li>
					<li>
						<a href="#">Forgot Password?</a>
					</li>
				</ul>
				<div class="send-button wthree agileits">
					<input type="submit" value="LOGIN">
				</div>
			</form>
			</div>
		</div>
		<div id="small-dialog2" class="mfp-hide agileinfo">
			<div class="pop_up">
			<form action="#" method="post">
				<h3>SIGN UP</h3>
				<input type="text" Name="Name" placeholder="Name" required="">
				<input type="text" Name="Email" placeholder="Email" required="">
				<input type="password" Name="Password" placeholder="Password" required="">
				<input type="text" Name="Phone Number" placeholder="Phone Number" required="">
				<div class="send-button wthree agileits">
					<input type="submit" value="SIGN UP">
				</div>
			</form>
			</div>
		</div>
	</div>
	<!-- //Popup-Box -->

</div>
<!-- //Header-Top-Bar-(Hidden) -->
<!-- Header Running Text -->
<div class="row headerrowrunningtext">
	<div id="headerrunningtext" class="col-xs-12">
		<div id="runningtextcontainer" class="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide">Get Special Price for Christmas Moment All Indonesia</div>
			</div>
		</div>
	</div>
</div><!-- 
<div class="row">
	<div class="col-xs-12" style="background-color: #1a4071;" align="center">
		<div id="headerrunningtext" style="height: 3em; color: #FFFFFF; overflow: hidden; line-height: 3em; max-width: 1000px;">
			Get Special Price for Christmas Moment All Indonesia
		</div>
	</div>
</div> -->
<!-- //Header Running Text -->
</div>