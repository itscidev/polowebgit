<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $heads;?>
	
	<!-- Meta-Tags -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- //Meta-Tags -->
	
	<!-- Custom-StyleSheet-Links -->
	<link rel="shortcut icon" type="image/png" href="<?php echo $path;?>images/favicon.png" >
	<!-- Bootstrap-CSS -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/bootstrap.css">
	<!-- Index-Page-CSS -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/mystyle.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style1.css">
	
	<!-- Fonts -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fonts.css">
	<!-- //Fonts -->
	
	<!-- Font-Awesome-File-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/font-awesome.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>fonts/fontawesome-webfont.ttf">
	<!-- //Font-Awesome-File-Links -->
	
	<!-- Swiper -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/swiper.min.css">
	<!-- //Swiper -->
	
</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->
	
<!-- content -->
<div class="container">
<div class="women_main">
	<!-- start sidebar -->
	<div class="col-md-3 s-d">
	  <div class="w_sidebar">
		<div class="w_nav1">
			<h4>All</h4>
			<ul>
				<li><a href="#">women</a></li>
				<li><a href="#">new arrivals</a></li>
				<li><a href="#">trends</a></li>
				<li><a href="#">boys</a></li>
				<li><a href="#">girls</a></li>
				<li><a href="#">sale</a></li>
			</ul>	
		</div>
		<h3>filter by</h3>
		<section  class="sky-form">
					<h4>catogories</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked="checked"><i></i>kutis</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>churidar kurta</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>salwar</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>printed sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>	
							</div>
						</div>
		</section>
		<section  class="sky-form">
					<h4>brand</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
							</div>
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>vishud</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>amari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>																								
							</div>
						</div>
		</section>
		<section class="sky-form">
			<h4>colour</h4>
			<ul class="w_nav2">
				<li><a class="color1" href="#"></a></li>
				<li><a class="color2" href="#"></a></li>
				<li><a class="color3" href="#"></a></li>
				<li><a class="color4" href="#"></a></li>
				<li><a class="color5" href="#"></a></li>
				<li><a class="color6" href="#"></a></li>
				<li><a class="color7" href="#"></a></li>
				<li><a class="color8" href="#"></a></li>
				<li><a class="color9" href="#"></a></li>
				<li><a class="color10" href="#"></a></li>
				<li><a class="color12" href="#"></a></li>
				<li><a class="color13" href="#"></a></li>
				<li><a class="color14" href="#"></a></li>
				<li><a class="color15" href="#"></a></li>
				<li><a class="color5" href="#"></a></li>
				<li><a class="color6" href="#"></a></li>
				<li><a class="color7" href="#"></a></li>
				<li><a class="color8" href="#"></a></li>
				<li><a class="color9" href="#"></a></li>
				<li><a class="color10" href="#"></a></li>
			</ul>
		</section>
		<section class="sky-form">
						<h4>discount</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="radio"><input type="radio" name="radio" checked=""><i></i>60 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>50 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>40 % and above</label>
							</div>
							<div class="col col-4">
								<label class="radio"><input type="radio" name="radio"><i></i>30 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>20 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>10 % and above</label>
							</div>
						</div>						
		</section>
	</div>
   </div>
	<!-- start content -->
	<div class="col-md-9 w_content">
		<div class="women">
			<a href="#"><h4>Enthecwear - <span>4449 itemms</span> </h4></a>
			<ul class="w_nav">
						<li>Sort : </li>
		     			<li><a class="active" href="#">popular</a></li> |
		     			<li><a href="#">new </a></li> |
		     			<li><a href="#">discount</a></li> |
		     			<li><a href="#">price: Low High </a></li> 
		     			<div class="clear"></div>	
		     </ul>
		     <div class="clearfix"></div>	
		</div>
		<!-- grids_of_4 -->
		<div class="grids_of_4">
		  <div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/'.rawurlencode('namaproduknya');?>">
			   	   	 <img src="<?php echo $path;?>images/w1.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/'.rawurlencode('namaproduknya');?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $99.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/'.rawurlencode('namaproduknya');?>">
			   	   	 <img src="<?php echo $path;?>images/w2.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/'.rawurlencode('namaproduknya')?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $76.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/'.rawurlencode('namaproduknya')?>">
			   	   	 <img src="<?php echo $path;?>images/w3.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/'.rawurlencode('namaproduknya')?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $58.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/'.rawurlencode('namaproduknya')?>">
			   	   	 <img src="<?php echo $path;?>images/w4.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/'.rawurlencode('namaproduknya')?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $112.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="clearfix"></div>
		</div>
		
		
		<div class="grids_of_4">
		 <div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>">
			   	   	 <img src="<?php echo $path;?>images/w5.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $109.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>">
			   	   	 <img src="<?php echo $path;?>images/w6.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $95.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>">
			   	   	 <img src="<?php echo $path;?>images/w7.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $68.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>">
			   	   	 <img src="<?php echo $path;?>images/w8.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $74.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="grids_of_4">
		  <div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>">
			   	   	 <img src="<?php echo $path;?>images/w9.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $80.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>">
			   	   	 <img src="<?php echo $path;?>images/w10.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $65.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>">
			   	   	 <img src="<?php echo $path;?>images/w11.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $90.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="grid1_of_4">
				<div class="content_box"><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>">
			   	   	 <img src="<?php echo $path;?>images/w12.jpg" class="img-responsive" alt=""/>
				   	  </a>
				    <h4><a href="<?php echo $path.'details/'.$_GET['id'].'/namaproduknya'?>"> Duis autem</a></h4>
				     <p>It is a long established fact that</p>
					 <div class="grid_1 simpleCart_shelfItem">
				    
					 <div class="item_add"><span class="item_price"><h6>ONLY $75.00</h6></span></div>
					 </div>
			   	</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- end grids_of_4 -->
		
		
	</div>
	<div class="clearfix"></div>
	
	<!-- end content -->
</div>
</div>

	<?php include_once 'footer.php';?>

<!-- Default-JavaScript -->
<script src="<?php echo $path;?>js/jquery-2.2.3.js"></script>
<!-- Supportive-Modernizr-JavaScript -->
<script src="<?php echo $path;?>js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script src="<?php echo $path;?>js/modernizr.custom.js"></script>
<!-- cart-js -->
<script src="<?php echo $path;?>js/minicart.js"></script>
<!-- Pricing-Popup-Box-JavaScript -->
<script src="<?php echo $path;?>js/jquery.magnific-popup.js" type="text/javascript"></script>
<!-- Model-Slider-JavaScript-Files -->
<!-- <script src="js/jquery.film_roll.js"></script> -->
<!-- Bootstrap-JavaScript -->
<script src="<?php echo $path;?>js/bootstrap.js"></script>
<script src="<?php echo $path;?>js/myscript.js"></script>
<script src="<?php echo $path;?>js/swiper.min.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script type="application/x-javascript"> 
	addEventListener("load", function() { 
		setTimeout(hideURLbar, 0); 
	}, false); 
	function hideURLbar(){ window.scrollTo(0,1); } 

	<!-- //cart-js --> 
	<!-- Shopping-Cart-JavaScript -->
	w3l.render();
    w3l.cart.on('w3agile_checkout', function (evt) {
    	var items, len, i;

    	if (this.subtotal() > 0) {
    		items = this.items();

    		for (i = 0, len = items.length; i < len; i++) { 
    		}
    	}
    });
	$(document).ready(function(){
		<!-- Pricing-Popup-Box-JavaScript -->
		$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});
	});
</script>
</body>
</html>