<?php
//path defined
if (isset($_SERVER['HTTP_HOST']) && (
	$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
	|| $_SERVER['HTTP_HOST']=='172.16.1.19'
)){
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/';
	$pathcook = '/poloweb/';
}else{
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/';
	$pathcook = '/';
}

if (substr_count( $_SERVER['SCRIPT_NAME'], '/checkout.php' ) == 0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}elseif (!isset($_SERVER['HTTP_REFERER'])){
	header ( 'location:'.$path.'index.html' );
	exit ();
}elseif (substr_count( $_SERVER['HTTP_REFERER'], '/cart' ) == 0){
	header ( 'location:'.$path.'index.html' );
	exit ();
}elseif (!isset($_GET['type']) || (isset($_GET['type']) && intval($_GET['type'])==0)){
	header ( 'location:'.$path.'index.html' );
	exit ();
}else{
	include_once 'cms/configuration/connect.php';
	//for cart session
	$cartitem = '';
	$cartcount = $cartsubtotal = $totweight = $ongkir = $discount = $total = $grandtotal = 0;
	if (isset($_COOKIE["cartvst"])){
		include_once 'cms/model/productvariety.php';
		include_once 'cms/model/productimage.php';
		//idprodvariety=>qty,idprodvariety=>qty
		$cartvst = json_decode($_COOKIE["cartvst"],true);
		if (count($cartvst) > 0){
			foreach ($cartvst as $idvar => $qty){
				$field = "pv_qty,pv_price,pv_pr_id,pr_name,pr_weight,pv_st_id,st_name,pv_sz_id,sz_name,".
				"pv_ci_id,ci_image,cl_name";
				$join = "inner join product on pr_id=pv_pr_id ".
					"left outer join style on st_id=pv_st_id ".
					"left outer join size on sz_id=pv_sz_id ".
					"left outer join color_item on ci_id=pv_ci_id ".
					"left outer join color on cl_id=ci_cl_id";
				$where = "pv_qty>0 and pv_status=1 and pr_status=1 and pv_id=".$idvar;
				$cekpv = productvariety_s($sqlcon,$field,$join,$where);
				if (is_array($cekpv) && $cekpv[0]>0){
					$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
					$cartcount = $cartcount + $qty;
					
					$stname = '';
					if ($cekpv1['pv_st_id']!=0){
						$stname = ' - ';
						if ($cekpv1['st_name']==''){
							$stname .= 'Undefined!';
						}else{
							$stname .= $cekpv1['st_name'];
						}
					}
					
					$coloritm = '';
					if ($cekpv1['pv_ci_id']!=0 && $cekpv1['ci_image']!='' && file_exists("images/productcolor/".$cekpv1['ci_image'])){
						$coloritm = ', Color : <img alt="'.htmlentities($cekpv1['cl_name']).'" src="'.$path.'images/productcolor/'.$cekpv1['ci_image'].'">';
					}

					$sizeitm = '';
					if ($cekpv1['pv_sz_id']!=0 && $cekpv1['sz_name']!=''){
						$sizeitm .= ', Size : '.$cekpv1['sz_name'];
					}

					$cartitem .= '<tr><td>'.htmlentities($cekpv1['pr_name'].$stname).$coloritm.$sizeitm.'</td><td>'.number_format($qty,0,',','.').'x</td></tr>';
					
					$cartsubtotal = $cartsubtotal + ($qty*$cekpv1['pv_price']);
					$totweight = $totweight + ($qty * $cekpv1['pr_weight']);
				}else{
					unset($cartvst[$idvar]);
				}
			}
		}
		unset($stname,$coloritm);
		setcookie( 'cartvst',json_encode($cartvst),time()+60*60*24*30,$pathcook );
	}
	$cartcountp = number_format($cartcount,0,',','.');
	$cartsubtotalp = 'Rp '.number_format($cartsubtotal,0,',','.');

	$recname = $recphone = $recaddr = $reccourier = $courier = $existingcourier = '-';

	include_once 'cms/model/lokasistep.php';
	include_once 'cms/model/memberaddress.php';
	include_once 'cms/model/ongkir.php';
	include_once 'cms/controller/rajaongkir.php';
	include_once 'cms/model/paymenttype.php';

	$explvst = explode(':',$_COOKIE["loginmem"]);
	$memid = $explvst[0];

	$field = "mma_id,mma_saveas,mma_los_id,prop.lok_name prop,kab.lok_name kab,kec.lok_name kec,mma_zip,mma_name,mma_phone,mma_address";
	$join = "inner join lokasi_step on los_id=mma_los_id ".
		"left outer join lokasi prop on los_prop_lok_id=prop.lok_id ".
		"left outer join lokasi kab on los_kab_lok_id=kab.lok_id ".
		"left outer join lokasi kec on los_kec_lok_id=kec.lok_id";
	$where = "mma_mm_id=".$memid." and mma_deleted=0 and mma_id=".$_COOKIE['cartaddrid'];
	$getaddr = memberaddress_s($sqlcon,$field,$join,$where);
	$destlosid = 0;
	if (is_array($getaddr) && $getaddr[0]>0){
		$getaddr1 = mysqli_fetch_assoc($getaddr[1]);
		$addaddr = '';
		if ($getaddr1['kec']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['kec'];
		}
		if ($getaddr1['kab']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['kab'];
		}
		if ($getaddr1['prop']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['prop'];
		}
		if ($getaddr1['mma_zip']!=0){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['mma_zip'];
		}
		$recname = $getaddr1['mma_name'];
		$recphone = $getaddr1['mma_phone'];
		$recaddr = $getaddr1['mma_address'].' '.$addaddr;
		$destlosid = $getaddr1['mma_los_id'];

		setcookie( 'cartaddrid',$getaddr1['mma_id'],time()+60*60*24*30,$pathcook );
	}
	
	//select courier
	//get default sender location id
	$sendlocid = mysqli_fetch_assoc(mysqli_query($sqlcon,"select co_los_id from contact limit 1"));
	if (isset($sendlocid['co_los_id']) && $sendlocid['co_los_id']!=0){
		$originlosid = $sendlocid['co_los_id'];
	}else{
		$originlosid = 0;
	}
	
	// echo "O:".$originlosid.",D:".$destlosid;
	if ($destlosid!=0 && $originlosid!=0){
		$field = "ong_id,eks_code,et_code,et_kelipatanberat,ong_estimasi,ong_price";
		$join = "inner join ekspedisi_type on et_id=ong_et_id inner join ekspedisi on eks_id=et_eks_id";
		$where = "ong_origin_los_id=".$originlosid." and ong_dest_los_id=".$destlosid." and et_status=1 and ong_id=".$_COOKIE['cartcourier'];
		$getong = ongkir_s($sqlcon,$field,$join,$where);
		if (is_array($getong) && $getong[0]>0){
			$getong1 = mysqli_fetch_assoc($getong[1]);
			$toteksprice = $getong1['ong_price'] * ceil($totweight/$getong1['et_kelipatanberat']);
			$ongkir = $toteksprice;
			$reccourier = $getong1['eks_code']." ".$getong1['et_code']." (".$getong1['ong_estimasi'].")";
			setcookie( 'cartcourier',$getong1['ong_id'],time()+60*60*24*30,$pathcook );
		}
	}

	$voucherused = "-";
	
	// payment type
	$field = "pt_id,pt_name,pt_logo,pt_vendor,pt_status";
	$where = "pt_status in (1,2) and pt_id=".intval($_GET['type']);
	$getpt = paymenttype_s($sqlcon,$field,'',$where);
	$paytype = 0;
	if (is_array($getpt) && $getpt[0]>0){
		$getpt1 = mysqli_fetch_assoc($getpt[1]);
		
		$paymentlogo = $path.'images/paymentlogo/'.$getpt1['pt_logo'];
		$paymentname = $getpt1['pt_name'];
		if ($getpt1['pt_vendor']=='midtrans' && $getpt1['pt_name']=='Credit Card'){
			$paytype = 1;
		}
		if ($getpt1['pt_vendor']=='local' && $getpt1['pt_name']=='Manual Transfer Bank'){
			$paytype = 2;
		}
	}

	$grandtotal = $cartsubtotal + $ongkir - $discount;
	$grandtotalp = number_format($grandtotal,0,',','.');

	$addjs = '';
	if ($paytype == 1){
		include_once 'plugin/midtrans/configuration.php';
		$addjs = '<script id="midtrans-script" type="text/javascript" src="https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js" data-environment="sandbox" data-client-key="'.$confmidtrans['ClientKey'].'"></script>';
	}
}
?>