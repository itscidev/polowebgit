<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/cart.php' ) == 0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}
$addresschooseid = $ongkirchooseid = $paymentchooseid = $vchid = 0;
$paymentlogo = $paymentname = $paymentlist = '';
$ongkir = $discount = $total = $grandtotal = 0;
$checkoutmsg = '';
$citembig = '';
$totweight = 0;
if (count($cartvst) > 0){
	foreach ($cartvst as $idvar => $qty){
		$field = "pv_qty,pv_price,pv_pr_id,pr_name,pr_price,pr_weight,pv_st_id,st_name,pv_sz_id,sz_name,".
				"pv_ci_id,ci_image,cl_name";
		$join = "inner join product on pr_id=pv_pr_id ".
				"left outer join style on st_id=pv_st_id ".
				"left outer join size on sz_id=pv_sz_id ".
				"left outer join color_item on ci_id=pv_ci_id ".
				"left outer join color on cl_id=ci_cl_id";
		$where = "pv_qty>0 and pv_status=1 and pr_status=1 and pv_id=".$idvar;
		$cekpv = productvariety_s($sqlcon,$field,$join,$where);
		if (is_array($cekpv) && $cekpv[0]>0){
			$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
			$citembig .= '<div class="cartitem cartitem'.$idvar.'">';
			$citembig .= '<div class="cartitemimg">';
			$field = "pi_image";
			$where = "pi_pr_id=".$cekpv1['pv_pr_id']." and pi_st_id=".$cekpv1['pv_st_id']." ".
					"and pi_ci_id=".$cekpv1['pv_ci_id']." and pi_status=1";
			$order = "pi_sequence asc";
			$getimg = productimage_s($sqlcon,$field,'',$where,'',$order,1);
			if (is_array($getimg) && $getimg[0]>0){
				$getimg1 = mysqli_fetch_assoc($getimg[1]);
				if ($getimg1['pi_image']!='' && file_exists("images/productitem/".$getimg1['pi_image'])){
					$citembig .= '<img alt="'.htmlentities($cekpv1['pr_name']).'" '.
							'src="'.$path.'images/productitem/'.$getimg1['pi_image'].'">';
				}
			}
			$citembig .= '</div>';
			$citembig .= '<div class="cartitemdesc cartpage">';
			$stname = '';
			if ($cekpv1['pv_st_id']!=0){
				$stname = ' - ';
				if ($cekpv1['st_name']==''){
					$stname .= 'Undefined!';
				}else{
					$stname .= $cekpv1['st_name'];
				}
			}
			$citembig .= '<p>'.htmlentities($cekpv1['pr_name'].$stname).'</p>';
			if ($cekpv1['pv_ci_id'] != 0){
				$coloritm = 'Undefined!';
				if ($cekpv1['ci_image']!='' && file_exists("images/productcolor/".$cekpv1['ci_image'])){
					$coloritm = '<img alt="'.htmlentities($cekpv1['cl_name']).'" '.
							'src="'.$path.'images/productcolor/'.$cekpv1['ci_image'].'">';
				}
				$citembig .= '<p>COLOR : '.$coloritm.'</p>';
			}
			if ($cekpv1['pv_sz_id'] != 0){
				if ($cekpv1['sz_name']==''){
					$citembig .= '<p>SIZE : Undefined!</p>';
				}else{
					$citembig .= '<p>SIZE : '.$cekpv1['sz_name'].'</p>';
				}
			}
// 			$citembig .= '<p>QTY : '.number_format($qty,0,',','.').'</p>';
			$citembig .= '<p class="forprice">';
			if ($cekpv1['pr_price']>$cekpv1['pv_price']){
				$citembig .= '<span class="strightout">Rp '.number_format($cekpv1['pr_price'],0,',','.').'</span>';
			}
			$citembig .= 	'Rp '.number_format($cekpv1['pv_price'],0,',','.');
			$citembig .= '</p>';
			$citembig .= '</div>';
			$citembig .= '<div class="cartitemqty cartpage">';
			$citembig .= 	'<p>QTY</p>';
			$citembig .= 	'<select class="qtybig" onchange="cartedititem('.$idvar.');">';
			for ($i=1;$i<=$cekpv1['pv_qty'];$i++){
				$selected = '';
				if ($i == $qty){$selected = ' selected="selected"';}
				$citembig .= 	'<option value="' . $i . '"' . $selected . '>' . $i . '</option>';
			}
			$citembig .= 	'</select>';
			$citembig .= '</div>';
			$citembig .= '<div class="cartitemdel cartpage">';
			$citembig .= '<i class="fa fa-times pointer" onclick="cartdelitem('.$idvar.');return false;"></i>';
			$citembig .= '</div>';
			$citembig .= '</div>';
			$totweight = $totweight + ($qty * $cekpv1['pr_weight']);
		}
	}
}else{
	$citembig .= '<h2>No product in cart!</h2>';
}
$totweightp = number_format($totweight,0,',','.').' Gram';

$codeuse = '-';
if (isset($_COOKIE['cartvch'])){
	//SAMPLEVOUCHER<i class="fa fa-times"></i>
	$codeuse = $_COOKIE['cartvch'].'<i class="fa fa-times"></i>';
}

$addressuse = $addressdetail = $existingaddr = $optaddrlosid = $courier = $existingcourier = '';
if ($memid == 0){
	$addressuse = '<p>Need login first to set addresses!</p>';
	$checkoutmsg = '<p>Need login first before check out! '.
	'<a href="#" onclick="forceopenloginpanel(); return false;" title="Open login console">'.
	'Click here to login!</a></p>';
}else{
	include_once 'cms/model/lokasistep.php';
	include_once 'cms/model/memberaddress.php';
	include_once 'cms/model/ongkir.php';
	include_once 'cms/controller/rajaongkir.php';
	include_once 'cms/model/paymenttype.php';
	$field = "mma_id,mma_saveas,mma_los_id,prop.lok_name prop,kab.lok_name kab,kec.lok_name kec,mma_zip,mma_name,mma_phone,mma_address";
	$join = "inner join lokasi_step on los_id=mma_los_id ".
		"left outer join lokasi prop on los_prop_lok_id=prop.lok_id ".
		"left outer join lokasi kab on los_kab_lok_id=kab.lok_id ".
		"left outer join lokasi kec on los_kec_lok_id=kec.lok_id";
	$where = "mma_mm_id=".$memid." and mma_deleted=0";
	$order = "mma_id desc";
	$getaddr = memberaddress_s($sqlcon,$field,$join,$where,'',$order);
	$no = 0;$destlosid = 0;
	if (is_array($getaddr) && $getaddr[0]>0){while ($getaddr1 = mysqli_fetch_assoc($getaddr[1])){
		$addaddr = '';
		if ($getaddr1['kec']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['kec'];
		}
		if ($getaddr1['kab']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['kab'];
		}
		if ($getaddr1['prop']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['prop'];
		}
		if ($getaddr1['mma_zip']!=0){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['mma_zip'];
		}
		$existingaddr .= '<tr class="addrrow row'.$getaddr1['mma_id'].'">'.
			'<td class="addrusethis" onclick="addrupd('.$getaddr1['mma_id'].');">'.
				'<strong>Saved as : '.$getaddr1['mma_saveas'].'</strong><br>'.$getaddr1['mma_name'].' ('.$getaddr1['mma_phone'].')<br>'.$getaddr1['mma_address'].' '.$addaddr.'.'.
				'<p class="hidden">'.json_encode($getaddr1).'</p>'.
			'</td>'.
			'<td class="addrdelthis"><i class="fa fa-times" title="Delete this address" '.
			'onclick="addrdel('.$getaddr1['mma_id'].');"></i></td>'.
		'</tr>';
		if ((isset($_COOKIE['cartaddrid']) && $_COOKIE['cartaddrid']==$getaddr1['mma_id']) || $no==0){
			$addressuse = $getaddr1['mma_saveas'];
			$addressdetail = $getaddr1['mma_name'].' ('.$getaddr1['mma_phone'].')<br>'.
				$getaddr1['mma_address'].' '.$addaddr;
			$destlosid = $getaddr1['mma_los_id'];
			$addresschooseid = $getaddr1['mma_id'];
			setcookie( 'cartaddrid',$getaddr1['mma_id'],time()+60*60*24*30,$pathcook );
		}
		$no++;
	}}else{
		$addressuse = 'No saved address found!';
		$addressdetail = 'Please add new address.';
		$existingaddr .= '<tr class="addrrow row0">'.
			'<td class="addrusethis" onclick="addrupd(0);">No saved address found!'.
				'<p class="hidden">'.json_encode(array('mma_id'=>0,'mma_saveas'=>'No saved address found!','mma_los_id'=>0,'prop'=>'','kab'=>'','kec'=>'','mma_zip'=>'','mma_name'=>'','mma_phone'=>'','mma_address'=>'Please add new address.')).'</p>'.
			'</td>'.
		'</tr>';
		// $existingaddr .= '<tr class="addrrow nodata"><td class="addrusethis">No saved address found!</td></tr>';
	}
	
	//select2 kecamatan
	$field = "los_id,los_prop_lok_id,prop.lok_name prop,los_kab_lok_id,kab.lok_name kab,los_kec_lok_id,kec.lok_name kec";
	$join = "left outer join lokasi prop on los_prop_lok_id=prop.lok_id ".
			"left outer join lokasi kab on los_kab_lok_id=kab.lok_id ".
			"left outer join lokasi kec on los_kec_lok_id=kec.lok_id";
	$order = "3,5,7";
	$getaddr = lokasistep_s($sqlcon,$field,$join,'','',$order);
	$no = 0;
	if (is_array($getaddr) && $getaddr[0]>0){while ($getaddr1 = mysqli_fetch_assoc($getaddr[1])){
		$selected = '';
		if ($no == 0){
			$selected = ' selected="selected"';
		}
		$no++;
		$addaddr = '';
		if ($getaddr1['kec']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['kec'];
		}
		if ($getaddr1['kab']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['kab'];
		}
		if ($getaddr1['prop']!=''){
			if ($addaddr != ''){$addaddr .= ', ';}
			$addaddr .= $getaddr1['prop'];
		}
		$optaddrlosid .= '<option value="' . $getaddr1['los_id'] . '"' . $selected . '>' . 
			$addaddr . '</option>';
	}}
	
	//select courier
	//get default sender location id
	$sendlocid = mysqli_fetch_assoc(mysqli_query($sqlcon,"select co_los_id from contact limit 1"));
	if (isset($sendlocid['co_los_id']) && $sendlocid['co_los_id']!=0){
		$originlosid = $sendlocid['co_los_id'];
	}else{
		$originlosid = 0;
	}
	
	// echo "O:".$originlosid.",D:".$destlosid;
	// if (isset($_COOKIE['cartcourier'])){
	// 	echo "cour:".$_COOKIE['cartcourier'];
	// }
	if ($destlosid!=0 && $originlosid!=0){
		$field = "ong_id,eks_code,et_code,et_kelipatanberat,ong_estimasi,ong_price";
		$join = "inner join ekspedisi_type on et_id=ong_et_id inner join ekspedisi on eks_id=et_eks_id";
		$where = "ong_origin_los_id=".$originlosid." and ong_dest_los_id=".$destlosid." and et_status=1";
		$order = "2,5";
		$getong = ongkir_s($sqlcon,$field,$join,$where,'',$order);
		if (is_array($getong) && $getong[0]>0){while ($getong1 = mysqli_fetch_assoc($getong[1])){
			$toteksprice = $getong1['ong_price'] * ceil($totweight/$getong1['et_kelipatanberat']);
			if (isset($_COOKIE['cartcourier']) && $_COOKIE['cartcourier']==$getong1['ong_id']){
				$courier = $getong1['eks_code']." ".$getong1['et_code']." (".$getong1['ong_estimasi'].
					") Rp ".number_format($toteksprice,0,',','.');
				$ongkir = $toteksprice;
				$ongkirchooseid = $getong1['ong_id'];
				setcookie( 'cartcourier',$getong1['ong_id'],time()+60*60*24*30,$pathcook );
				$couridfromcok = 1;
			}elseif (!isset($couridfromcok) && ($ongkir==0 || $ongkir>$toteksprice)){
				$courier = $getong1['eks_code']." ".$getong1['et_code']." (".$getong1['ong_estimasi'].
					") Rp ".number_format($toteksprice,0,',','.');
				$ongkir = $toteksprice;
				$ongkirchooseid = $getong1['ong_id'];
				setcookie( 'cartcourier',$getong1['ong_id'],time()+60*60*24*30,$pathcook );
			}
			$existingcourier .= '<tr class="crrrow row'.$getong1['ong_id'].'" onclick="crrusethis('.$getong1['ong_id'].',1,1);">'.
			'<td class="crrusethis">'.
				$getong1['eks_code'].' '.$getong1['et_code'].' ('.$getong1['ong_estimasi'].') Rp '.number_format($getong1['ong_price'],0,',','.').' / '.$getong1['et_kelipatanberat'].'Gr'.
				'<p class="hidden">'.json_encode(array(
					'toteksprice'=>$toteksprice,
					'show'=>$getong1['eks_code'].' '.$getong1['et_code'].' ('.$getong1['ong_estimasi'].') Rp '.number_format($toteksprice,0,',','.')
				)).'</p>'.
			'</td>'.
			'<td class="crrestweight">'.number_format($totweight,0,',','.').' Gr</td>'.
			'<td class="crrcost">Rp '.number_format($toteksprice,0,',','.').'</td>'.
		'</tr>';
		}}else{
			$courier = 'No expedition available!';
			$existingcourier = '<tr class="crrrow nodata row0" onclick="crrusethis(0,1);">'.
				'<td class="crrusethis">No expedition available!'.
					'<p class="hidden">'.json_encode(array(
						'toteksprice'=>0,
						'show'=>'No expedition available!'
					)).'</p>'.
				'</td>'.
			'</tr>';
			// $existingcourier .= '<tr class="crrrow nodata"><td class="crrusethis">No expedition available!</td></tr>';
		}
	}else{
		$courier = 'Destination / origin not available!';
		$existingcourier = '<tr class="crrrow nodata row0" onclick="crrusethis(0,1);">'.
			'<td class="crrusethis">No expedition available!'.
				'<p class="hidden">'.json_encode(array(
					'toteksprice'=>0,
					'show'=>'No expedition available!'
				)).'</p>'.
			'</td>'.
		'</tr>';
		// $existingcourier .= '<tr class="crrrow nodata"><td class="crrusethis">Destination / origin not available!</td></tr>';
	}

	// payment type
	$field = "pt_id,pt_name,pt_logo,pt_status";
	$where = "pt_status in (1,2)";
	$getpt = paymenttype_s($sqlcon,$field,'',$where);
	if (is_array($getpt) && $getpt[0]>0){while ($getpt1 = mysqli_fetch_assoc($getpt[1])){
		if ($paymentchooseid == 0){
			$paymentchooseid = $getpt1['pt_id'];
			$paymentlogo = $path.'images/paymentlogo/'.$getpt1['pt_logo'];
			$paymentname = $getpt1['pt_name'];
		}
		$paymentlist .= '<p title="'.$getpt1['pt_name'].'" class="pay'.$getpt1['pt_id'].'" onclick="paymentusethis('.$getpt1['pt_id'].',1);">'.
			'<img src="'.$path.'images/paymentlogo/'.$getpt1['pt_logo'].'">'.
			'<span>'.$getpt1['pt_name'].'</span>'.
		'</p>';
	}}

}

$total = $cartsubtotal + $ongkir;
$totalp = 'Rp '.number_format($total,0,',','.');
$discountp = 'Rp '.number_format($discount,0,',','.');
$grandtotal = $total - $discount;
$grandtotalp = 'Rp '.number_format($grandtotal,0,',','.');
?>