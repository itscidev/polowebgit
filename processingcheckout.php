<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
//path defined
if (isset($_SERVER['HTTP_HOST']) && (
	$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
	|| $_SERVER['HTTP_HOST']=='172.16.1.19'
)){
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/';
	$pathcook = '/poloweb/';
}else{
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/';
	$pathcook = '/';
}

$result = array();
include_once 'cms/configuration/connect.php';
include_once 'cms/model/productvariety.php';
include_once 'cms/model/ongkir.php';
include_once 'cms/model/cart.php';
include_once 'cms/model/cartdetail.php';

if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/checkout.html')==0){
	$result['Error']='Ilegal access detected!';
}elseif (!isset($_COOKIE['loginmem'])){
	$result['Error']='Login session expired!';
}elseif (!isset($_COOKIE['cartaddrid'])){
	$result['Error']='Address session expired!';
}elseif (!isset($_COOKIE['cartcourier'])){
	$result['Error']='Courier session expired!';
}elseif (!isset($_COOKIE['cartvst'])){
	$result['Error']='Cart session expired!';
}elseif (!isset($_POST['paytype']) || (isset($_POST['paytype']) && intval($_POST['paytype'])==0)){
	$result['Error']='Payment Type not detected!';
}else{
	$explvst = explode(':',$_COOKIE['loginmem']);
	$memid = $explvst[0];
	$ptid = intval($_POST['paytype']);
	$mmaid = $_COOKIE['cartaddrid'];
	$ongid = $_COOKIE['cartcourier'];
	$ongweight = 0;
	$ongprice = 0;
	$discount = 0;
	$vchid = 0;
	$status = 0;
	$comment = '';
	if (isset($_POST['comment']) && trim($_POST['comment'])!=''){
		$comment = trim($_POST['comment']);
	}if (isset($_POST['cardnumber']) && trim($_POST['cardnumber'])!=''){
		$cardnumber = trim($_POST['cardnumber']);
	}if (isset($_POST['cardexpired']) && trim($_POST['cardexpired'])!=''){
		$cardexpired = trim($_POST['cardexpired']);
	}if (isset($_POST['cardcvv']) && trim($_POST['cardcvv'])!=''){
		$cardcvv = trim($_POST['cardcvv']);
	}
	$idcart = cart_i($sqlcon,$mmaid,$ongid,$ongweight,$ongprice,$discount,$vchid,$ptid,$comment,$status);
	if (is_array($idcart) && $idcart[0]>0){
		$crid = $idcart[0];
		$cartvst = json_decode($_COOKIE["cartvst"],true);
		if (count($cartvst)>0){
			foreach ($cartvst as $idpv => $qty){
				$field = "pv_price,pr_weight,pv_qty";
				$join = "inner join product on pr_id=pv_pr_id";
				$where = "pv_id=".$idpv;
				$cekpv = productvariety_s($sqlcon,$field,$join,$where);
				if (is_array($cekpv) && $cekpv[0]>0){
					$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
					$pvid = $idpv;
					$price = $cekpv1['pv_price'];
					cartdetail_i($sqlcon,$crid,$pvid,$price,$qty);

					//kurangi pv_qty
					$id = $idpv;
					$set = "pv_qty=(pv_qty-".$qty.")";
					productvariety_u($sqlcon,$set,$id);

					$ongweight = $ongweight + ($qty*$cekpv1['pr_weight']);
				}
			}
			$set = "cr_ong_weight=".$ongweight.",cr_updateby=".$memid;

			//get ongkir price
			$field = "et_kelipatanberat,ong_price";
			$join = "inner join ekspedisi_type on et_id=ong_et_id";
			$where = "ong_id=".$ongid;
			$getong = ongkir_s($sqlcon,$field,$join,$where);
			if (is_array($getong) && $getong[0]>0){
				$getong1 = mysqli_fetch_assoc($getong[1]);
				$set .= ",cr_ong_price=".(ceil($ongweight/$getong1['et_kelipatanberat'])*$getong1['ong_price']);
			}
			cart_u($sqlcon,$set,$crid);
			$result['Status']='Data Saved!';
			$result['idcart']=$crid;
		}else{
			$result['Error']='Cart session expired!';
		}
	}else{
		$result['Error']=$idcart;
	}

	if (isset($_COOKIE["cartvst"])){
		unset($_COOKIE["cartvst"]);
		setcookie( 'cartvst',null,-1,$pathcook );
	}
	if (isset($_COOKIE["cartaddrid"])){
		unset($_COOKIE["cartaddrid"]);
		setcookie( 'cartaddrid',null,-1,$pathcook );
	}
	if (isset($_COOKIE["cartcourier"])){
		unset($_COOKIE["cartcourier"]);
		setcookie( 'cartcourier',null,-1,$pathcook );
	}
}

if ($sqlcon){mysqli_close($sqlcon);}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>