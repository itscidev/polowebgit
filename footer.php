<!-- Footer -->
<div class="col-xs-12 myfooter">
	<div class="row">
		<div class="col-sm-2 col-xs-5 withpaddingtop">
			<span>ABOUT</span>
			<img alt="About POLO Indonesia" src="<?php echo $path;?>images/icon/polo.jpg">
			<ul>
				<li><a href="<?php echo $path;?>about.html">About Us</a></li>
				<li><a href="<?php echo $path;?>regulations/privacy">Privacy</a></li>
				<li><a href="<?php echo $path;?>regulations/term">Term of Use</a></li>
			</ul>
		</div>
		<div class="col-sm-3 col-xs-7 withpaddingtop">
			<span>CUSTOMER ASSISTANCE</span>
			<img alt="Customer Assistance POLO Indonesia" src="<?php echo $path;?>images/icon/email.jpg">
			<ul>
				<li><a href="<?php echo $path;?>regulations/faq">FAQ</a></li>
				<li><a href="<?php echo $path;?>regulations/contact">Contact Us</a></li>
			</ul>
		</div>
		<div class="col-sm-2 col-xs-7 withpaddingtop">
			<span>STORE LOCATOR</span>
			<img alt="Store Location POLO Indonesia" src="<?php echo $path;?>images/icon/pointer.jpg">
			<ul>
				<li><a href="<?php echo $path;?>stores.html" title="Find nearest POLO store">
					Find a Store
				</a></li>
			</ul>
		</div>
		<div class="col-sm-1 col-xs-12 hideonmobile">
			<div style="border-left: 1px solid #231f20; height: 300px; margin: 10px; float: left;"></div>
			<div style="border-left: 1px solid #231f20; height: 200px; margin: 60px 10px; float: left;"></div>
		</div>
		<div class="col-sm-4 col-xs-12 footerother">
			<span class="titlefollow">FOLLOW</span>
			<div class="agilesocialwthree">
				<ul class="social-icons">
					<li><a href="#" class="facebook w3ls" title="Go to Our Facebook Page"><i class="fa w3ls fa-facebook-square" aria-hidden="true"></i></a></li>
					<li><a href="#" class="twitter w3l" title="Go to Our Twitter Account"><i class="fa w3l fa-twitter-square" aria-hidden="true"></i></a></li>
					<li><a href="#" class="instagram wthree" title="Go to Our Instagram Account"><i class="fa wthree fa-instagram" aria-hidden="true"></i></a></li>
					<!-- <li><a href="#" class="googleplus w3" title="Go to Our Google Plus Account"><i class="fa w3 fa-google-plus-square" aria-hidden="true"></i></a></li>
					<li><a href="#" class="youtube w3layouts" title="Go to Our Youtube Channel"><i class="fa w3layouts fa-youtube-square" aria-hidden="true"></i></a></li> -->
				</ul>
			</div>
			<div style="height: 30px;"></div>
			<style type="text/css">
			.newsletter::placeholder {
				color: #a7a7a7;
			}
	        </style>
			<span class="titlenewsletter">SIGN UP FOR NEWS LETTER</span>
			<input name="registernewsletter" class="newsletter" type="text" placeholder="Enter Email Address">
			<p style="color: #000000; margin-top: 30px;">
				Click here to read POLO INDONESIA 
				<a href="<?php echo $path;?>regulations/privacy">Privacy Notice</a>. Or 
				<a href="<?php echo $path;?>regulations/contact">Contact Us</a>.
			</p>
		</div>
		<div class="col-xs-12" style="text-align: center; padding: 20px 20px 10px 20px;">
			&copy; POLOINDONESIA 2017. ALL RIGHTS RESERVED.
		</div>
	</div>
</div>
<?php /*
<div class="agileinfofooter">
	<div class="agileinfofooter-grids">

		<div class="col-xs-4 agileinfofooter-grid agileinfofooter-grid1">
			<ul>
				<li><a href="about.html">ABOUT</a></li>
				<li><a href="<?php echo $path;?>women.html">MEN'S</a></li>
				<li><a href="<?php echo $path;?>women.html">MEN'S ACCESSORIES</a></li>
				<li><a href="<?php echo $path;?>women.html">WOMEN'S</a></li>
				<li><a href="<?php echo $path;?>women.html">WOMEN'S ACCESSORIES</a></li>
			</ul>
		</div>

		<div class="col-xs-4 agileinfofooter-grid agileinfofooter-grid1">
			<ul>
				<li><a href="<?php echo $path;?>stores.html">STORE LOCATOR</a></li>
				<li><a href="<?php echo $path;?>faq.html">FAQs</a></li>
				<li><a href="<?php echo $path;?>contact.html">CONTACT</a></li>
			</ul>
		</div>

		<div class="col-xs-4 agileinfofooter-grid agileinfofooter-grid1">
			<address>
				<ul>
					<li>40019 Parma Via Modena</li>
					<li>Sant'Agata Bolognese</li>
					<li>BO, Italy</li>
					<li>+1 (734) 123-4567</li>
					<li><a class="mail" href="mailto:mail@example.com">info@example.com</a></li>
				</ul>
			</address>
		</div>
		<div class="clearfix"></div>

	</div>
</div>
<!-- //Footer -->

<!-- Copyright -->
<div class="w3lscopyrightaits">
	<div class="col-md-8 w3lscopyrightaitsgrid w3lscopyrightaitsgrid1">
		<p>© 2017 Groovy Apparel. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="=_blank"> W3layouts </a></p>
	</div>
	<div class="col-md-4 w3lscopyrightaitsgrid w3lscopyrightaitsgrid2">
		<div class="agilesocialwthree">
			<ul class="social-icons">
				<li><a href="#" class="facebook w3ls" title="Go to Our Facebook Page"><i class="fa w3ls fa-facebook-square" aria-hidden="true"></i></a></li>
				<li><a href="#" class="twitter w3l" title="Go to Our Twitter Account"><i class="fa w3l fa-twitter-square" aria-hidden="true"></i></a></li>
				<li><a href="#" class="googleplus w3" title="Go to Our Google Plus Account"><i class="fa w3 fa-google-plus-square" aria-hidden="true"></i></a></li>
				<li><a href="#" class="instagram wthree" title="Go to Our Instagram Account"><i class="fa wthree fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href="#" class="youtube w3layouts" title="Go to Our Youtube Channel"><i class="fa w3layouts fa-youtube-square" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<!-- //Copyright -->
*/?>