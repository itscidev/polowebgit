<!DOCTYPE html>
<html lang="en">
<head>
	<title>Polo Indonesia</title>
	
	<!-- Meta-Tags -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- //Meta-Tags -->
	
	<!-- Custom-StyleSheet-Links -->
	<link rel="shortcut icon" type="image/png" href="images/favicon.png" >

	<!-- Fonts -->
	<link type="text/css" media="all" rel="stylesheet" href="css/fonts.css">
	<!-- //Fonts -->
	<style type="text/css">
	html,body{
		background-color: #1a4071;
		margin: 0px;
		padding: 0px;
	}
	div.container{
		width: 60%;
		padding: 15% 20%;
	}
	div.container > h3{
		font-size: 16px;
		font-family: cambria;
		color: #ffffff;
		text-align: center;
	}
	div.container > div.browserimg{
		text-align: center;
	}
	div.container > div.browserimg > div{
		display: inline-block;
		margin: 10px;
	}
	div.container > div.browserimg > div > p{
		font-family: cambria;
		color: #ffffff;
	}
	div.container > div.browserimg img{
		height: 100px;
		width: auto;
	}
	</style>
	
</head>
<!-- //Head -->
<!-- Body -->
<body>
	<div class="container">
		<h3>Untuk kelancaran berselancar, mohon perbaharui browser Anda terlebih dahulu.</h3>
		<div class="browserimg">
			<div>
				<img src="images/icon/browser-chrome.png" alt="chrome">
				<p>Ver. 56 ++</p>
			</div>
			<div>
				<img src="images/icon/browser-firefox.png" alt="firefox">
				<p>Ver. 51 ++</p>
			</div>
			<div>
				<img src="images/icon/browser-opera.png" alt="opera">
				<p>Ver. 30 ++</p>
			</div>
			<div>
				<img src="images/icon/browser-safari.png" alt="safari">
				<p>Ver. 10 ++</p>
			</div>
			<div>
				<img src="images/icon/browser-ie.png" alt="internet explorer">
				<p>Edge Ver. 38 ++</p>
			</div>
		</div>
	</div>
</body><!-- //Body -->
</html>