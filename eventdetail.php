<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/o-eventdetail.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<div class="col-sm-8 col-xs-12 descpromo">
		<h3>POLO PROMO</h3>
		<img alt="promo detail" src="<?php echo $path;?>images/banner/deletethis-event-ladies.jpg">
		<div class="description">
			<p>Promo Kilat Ramadhan Ekstra Edisi Rumah Tangga</p><br />
			<p><b>Deskripsi</b></p>
			<p>Nikmati promo kilat cashback 20% hingga Rp 50.000 tanpa minimum pembelian!</p><br />
			<p><b>Syarat dan Ketentuan</b></p>
			<ol>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
			</ol><br />
		</div>
		<p class="readmore">Read More<i class="fa fa-angle-down" aria-hidden="true"></i></p>
	</div>
	<div class="col-sm-4 col-xs-12 infopromo">
		<div>INFO PROMO</div>
		<div>
			<img alt="periode promo" src="<?php echo $path;?>images/icon/clock.jpg">
			<p>Periode Promo</p>
			<p>10-31 Mei 2019</p>
		</div>
		<div>
			<div>
				<img alt="periode promo" src="<?php echo $path;?>images/icon/coupon.jpg">
				Kode Promo
				<i class="fa fa-info-circle" aria-hidden="true"></i>
			</div>
			<div class="row">
				<div>JUMATSERU</div>
				<div>Salin Kode</div>
			</div>
		</div>
		<div class="cekbutton">Cek Sekarang</div>
	</div>

<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
</body><!-- //Body -->
</html>