<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/stores.php' ) == 0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}
function haversineGreatCircleDistance(
	$latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
	// convert from degrees to radians
	$latFrom = deg2rad($latitudeFrom);
	$lonFrom = deg2rad($longitudeFrom);
	$latTo = deg2rad($latitudeTo);
	$lonTo = deg2rad($longitudeTo);

	$latDelta = $latTo - $latFrom;
	$lonDelta = $lonTo - $lonFrom;

	$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
			cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
	return $angle * $earthRadius;
}

$searchstore = $result = '';
if (isset($_POST['searchstore']) && $_POST['searchstore']!=''){
	$searchstore = $_POST['searchstore'];
	$tokensec = md5('/Stores.Php:'.date("d"));
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$path."processinggpscoordinates.html");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query(array(
		'st_address'=>$searchstore, 'tokensec'=>$tokensec
	)));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($ch);
	curl_close($ch);
	$res = json_decode(json_decode($output),true);
	//echo "<pre>";print_r($res);echo "</pre>";
	if (isset($res['Error'])){
		if (isset($res['Error']['st_address'])){
			$result = "Please fill the field!";
		}else{
			$result = $res['Error'];
		}
	}elseif (isset($res['error'])){
		$result = $res['error'];
	}elseif (isset($res[0]['lat'])){
		$lat = $res[0]['lat'];
		$long = $res[0]['lon'];
		$limitout = 8;
		$limitdist = 30000;//meter
		
		$resout = array();
		
		$outlet = mysqli_query($sqlcon,"select * from store where st_status=1");
		$outlet1 = mysqli_num_rows($outlet);
		if ($outlet1>0){while ($outlet2 = mysqli_fetch_assoc($outlet)){
			$distance = haversineGreatCircleDistance($lat,$long,$outlet2['st_lat'],$outlet2['st_long']);
			$distance = intval($distance);
			if ($distance <= $limitdist){
				while (isset($resout[$distance])){
					$distance = $distance + 1;
				}
				$resout[$distance] = array(
					$outlet2['st_id'],
					$outlet2['st_dispname'],
					$outlet2['st_address'],
					$outlet2['st_zip']
				);
				if (count($resout) > $limitout){
					$delar = array_pop($resout);
					unset($delar);
				}
			}
		}}
		if (count($resout) > 0){foreach ($resout as $distance => $content){
			$kpos = '';
			if ($content[3]!='' && $content[3]!=0){
				$kpos = ' ('.$content[3].')';
			}
			$result .= '<div class="col-md-3 col-sm-6 col-xs-12">'.
				'<div>'.
					'<h3>POLO</h3>'.
					'<span>'.$content[1].'<br />'.$content[2].$kpos.'</span>'.
				'</div>'.
			'</div>';
		}}else{
			$result = 'No closest outlet found at your location!';
		}
	}else{
		$result = 'Invalid field search!';
	}
	if ($result != ''){
		$result = '<div class="col-xs-12 resultsearch">'.$result.'</div>';
	}
	
	/* $result = '<div class="col-xs-12 resultsearch">'.
	'<div class="col-md-3 col-sm-6 col-xs-12">'.
	'<div>'.
// 	'<img src="images/polo-logo.png" title="POLO Store Indonesia" alt="POLO Store Indonesia">'.
	'<h3>POLO</h3>'.
	'<span>St. Moritz Mall<br />Jl. Puri Raya Boulevard 1.<br />Jakarta Barat - Indonesia (11610)</span>'.
	'</div>'.
	'</div>'.
	'<div class="col-md-3 col-sm-6 col-xs-12">'.
	'<div>'.
// 	'<img src="images/polo-logo.png" title="POLO Store Indonesia" alt="POLO Store Indonesia">'.
	'<h3>POLO</h3>'.
	'<span>Puri Indah Mall<br />Jl. Puri Agung, Puri Indah, Kembangan Selatan.<br />'.
	'Jakarta Barat - Indonesia (11610)</span>'.
	'</div>'.
	'</div>'.
	'<div class="col-md-3 col-sm-6 col-xs-12">'.
	'<div>'.
// 	'<img src="images/polo-logo.png" title="POLO Store Indonesia" alt="POLO Store Indonesia">'.
	'<h3>POLO</h3>'.
	'<span>Central Park Mall<br />Jl. Let. Jend. S. Parman, Kav. 28.<br />'.
	'Jakarta Barat - Indonesia (11470)</span>'.
	'</div>'.
	'</div>'.
	'<div class="col-md-3 col-sm-6 col-xs-12">'.
	'<div>'.
// 	'<img src="images/polo-logo.png" title="POLO Store Indonesia" alt="POLO Store Indonesia">'.
	'<h3>POLO</h3>'.
	'<span>Taman Anggrek Mall<br />Jl. Let. Jend. S. Parman, Kav. 21.<br />'.
	'Jakarta Barat - Indonesia (11470)</span>'.
	'</div>'.
	'</div>'.
	'</div>'; */
}
?>