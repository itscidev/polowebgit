<?php include_once 'kalkulasiorderhistorydetail.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Meta-Tags -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- //Meta-Tags -->

	<!-- Bootstrap-CSS -->	   
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/bootstrap.css">
	<!-- Fonts -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fonts.css">
	<!-- //Fonts -->
	<!-- Font-Awesome-File-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/font-awesome.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>fonts/fontawesome-webfont.ttf">
	<!-- //Font-Awesome-File-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/orderhistorydetail.css">
</head>
<!-- //Head -->
<body>
<div class="modal-content">
  <div class="modal-header">
    <h4 class="modal-title">Detail Order</h4>
  </div>
  <div class="modal-body">
  	<table width="100%">
  		<tr>
  			<td width="50%"><label>Invoice Number</label><?php echo $invno;?></td>
  			<td width="50%"><label>Status</label><?php echo $invstatus;?></td>
  		</tr>
  		<tr><td colspan="2" class="batasan"></td></tr>
  		<tr>
  			<td><label>Date Order</label><?php echo $invdate;?></td>
  			<td><a href="orderhistorydetail.php" title="Print out order" onclick="return false;">Print Out</a></td>
  		</tr>
  	</table>
  	<div class="batasan"></div>
  	<b>List of Ordered Product</b>
  	<div class="spacetop">
		<table width="100%" class="listproduct"><!-- 
			<tr>
				<td width="60%"><img src="images/productitem/4-WANGKY-orange.jpg" alt="Wangky - Custom Fit">Wangky - Custom Fit, Color , Size M</td>
				<td width="20%">1 pcs<br>Rp 500.000</td>
				<td width="20%"><label>Total</label>Rp 500.000</td>
			</tr>
		 --><?php echo $trlist;?></table>
	</div>
  	
	<div class="batasan"></div>
	<b>Comment</b>
	<div class="spacetop">
		<p><?php echo $invcomment;?></p>
	</div>
	<div class="batasan"></div>
	<b>Delivery</b>
	<div class="spacetop">
		<p><!-- JNE Reguler<br>No Resi : 298354655 --><?php echo $ekstype;?></p>
		<p><!-- alamat lengkap --><?php echo $address;?></p>
	</div>
	<div class="batasan"></div>
	<b>Payment</b>
	<div class="spacetop payment">
		<table width="100%" class="payment">
		<tr>
			<td width="80%">Total Product</td>
			<td width="20%">Rp <!-- 500.000 --><?php echo $tprodp;?></td>
		</tr>
		<tr>
			<td>Total Delivery</td>
			<td>Rp <?php echo $tongp;?></td>
		</tr>
		<tr>
			<td>Total Discount</td>
			<td>Rp <?php echo $tdiscp;?></td>
		</tr>
		<tr>
			<td>Total Payment</td>
			<td class="colorred">Rp <?php echo $totalp;?></td>
		</tr>
		<tr>
			<td>Payment Type</td>
			<td><?php echo $paytype;?><!-- Manual Transfer Bank --></td>
		</tr>
	</table>
	</div><?php
if ($delprog==1){?>
	<div class="batasan"></div>
	<b>Delivery Progress</b>
	<div class="spacetop">
		<table width="100%"><!-- 
		<tr>
			<td width="30%">Sabtu, 9 Mei 2020 14:03 WIB</td>
			<td width="70%">Transaksi selesai.
Dana akan diteruskan ke penjual.</td>
		</tr>
		<tr>
			<td>Sabtu, 9 Mei 2020 14:03 WIB</td>
			<td>Transaksi dikonfirmasi.
Transaksi telah dikonfirmasi pembeli dan menunggu review Tokopedia.</td>
		</tr>
	 --><?php echo $delprogp;?></table>
	</div><?php 
}?>
  </div>
</div>
	<!-- Default-JavaScript -->
	<script src="<?php echo $path;?>js/jquery-2.2.3.js"></script>
	<!-- Bootstrap-JavaScript -->
	<script src="<?php echo $path;?>js/bootstrap.js"></script>
</body><!-- //Body -->
</html>