<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/orderhistory.css">
	<!-- //Own Custom-StyleSheet-Links -->

    <!-- Fancybox simple pop up --> 
	<link type="text/css" media="screen" rel="stylesheet" href="<?php echo $path;?>plugin/fancybox/jquery.fancybox.css?v=2.1.5" />

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<div class="container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-12">
				<h3>My Order History</h3>
				<!-- <div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-4"><label>Purchase Date</label>5 Mei 2020</div>
							<div class="col-xs-4"><label>Total Charge</label>Rp 500.000</div>
							<div class="col-xs-4"><label>Payment Status</label>Waiting Payment</div>
						</div>
					</div>
					<div class="panel-body">
						<table>
							<tr>
								<td><img src="images/productitem/4-WANGKY-orange.jpg" alt="Wangky - Custom Fit">Wangky - Custom Fit, Color , Size M</td>
								<td>1 pcs<br>Rp 500.000</td>
								<td><label>Total</label>Rp 500.000</td>
							</tr>
						</table>
					</div>
					<div class="panel-footer">
						<div class="row">
							<div class="col-xs-6">Invoice : 1</div>
							<div class="col-xs-6"><span class="floatright"><i class="fa fa-info marginright"></i>See Detail</span></div>
						</div>
					</div>
				</div> -->

				<?php echo $listbuy;
if ($no>0){?>
				<nav>
					<ul class="pagination">
						<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
						<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
					    <li><a href="#">2</a></li>
					    <li><a href="#">3</a></li>
					    <li><a href="#">4</a></li>
					    <li><a href="#">5</a></li>
					    <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
					</ul>
				</nav><?php
}?>
			</div>
		</div>
	</div><?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
	<!-- Add fancyBox main JS and CSS files -->
	<script src="<?php echo $path;?>plugin/fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<script src="<?php echo $path;?>js/orderhistory.js"></script>
</body><!-- //Body -->
</html>