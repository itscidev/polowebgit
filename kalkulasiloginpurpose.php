<?php
$error = $sukses = '';
$reset = 0;
if (substr_count( $_SERVER['SCRIPT_NAME'], '/loginpurpose.php' )==0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}elseif (isset($_GET['id']) && $_GET['id']!=''){
	$allowtype = array('activate','reset','banned');
	$exid = explode('/',$_GET['id']);
	if (!in_array($exid[0],$allowtype)){
		$error = "No action required!";
	}
	include_once 'cms/configuration/connect.php';
	include_once 'cms/model/visitor.php';
	if ($exid[0]=='activate'){
		$field = "vis_id";
		$where = "MD5(vis_email)='".mysqli_real_escape_string($sqlcon,$exid[1])."'";
		$exist =  visitor_s($sqlcon,$field,'',$where);
		if (is_array($exist) && $exist[0]>0){
			$exist1 = mysqli_fetch_assoc($exist[1]);
			$set = "vis_emailstatus=1,vis_update='".date("Y-m-d H:i:s")."'";
			visitor_u($sqlcon, $set, $exist1['vis_id']);
			$sukses = 'Email has been activated!';
		}elseif (is_array($exist) && $exist[0]==0){
			$error = "Account not found!";
		}else{
			$error = $exist;
		}
	}elseif ($exid[0]=='banned'){
		$field = "vis_id";
		$where = "vis_passwordreset='".mysqli_real_escape_string($sqlcon,$exid[1])."'";
		$exist =  visitor_s($sqlcon,$field,'',$where);
		if (is_array($exist) && $exist[0]>0){
			$exist1 = mysqli_fetch_assoc($exist[1]);
			$set = "vis_banned=0,vis_bannedreason='',vis_passwordreset='',".
				"vis_update='".date("Y-m-d H:i:s")."'";
			visitor_u($sqlcon, $set, $exist1['vis_id']);
			$sukses = 'Your account ban has been released! Now you can sign in normaly again.';
		}elseif (is_array($exist) && $exist[0]==0){
			$error = "Account not found!";
		}else{
			$error = $exist;
		}
	}elseif ($exid[0]=='reset'){
		$tokenR = $exid[1];
		$field = "vis_id,vis_email";
		$where = "vis_passwordreset='".mysqli_real_escape_string($sqlcon,$tokenR)."'";
		$exist =  visitor_s($sqlcon,$field,'',$where);
		if (is_array($exist) && $exist[0]>0){
			$exist1 = mysqli_fetch_assoc($exist[1]);
			$emailR = $exist1['vis_email'];
			$reset = 1;
		}elseif (is_array($exist) && $exist[0]==0){
			$error = "Account not found!";
		}else{
			$error = $exist;
		}
	}
}
//|| !isset($vstid) || (isset($vstid) && $vstid==0)
if ($error != ''){
	$error = '<div class="alert alert-danger alert-dismissible" role="alert">'.
		'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
		'<span aria-hidden="true">&times;</span></button>'.
		'<strong>Warning!</strong> '.$error.
	'</div>';
}
if ($sukses != ''){
	$sukses = '<div class="alert alert-success alert-dismissible" role="alert">'.
		'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
		'<span aria-hidden="true">&times;</span></button>'.
		'<strong>Well done!</strong> '.$sukses.
	'</div>';
}
?>