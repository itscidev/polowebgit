var cardData = {
	"card_number": 4811111111111114,
	"card_exp_month": 02,
	"card_exp_year": 2025,
	"card_cvv": 123,
};

// callback functions
var options = {
	onSuccess: function(response){
		// Success to get card token_id, implement as you wish here
		console.log('Success to get card token_id, response:', response);
		var token_id = response.token_id;
		console.log('This is the card token_id:', token_id);
		$("#procPayment").show();
	},
	onFailure: function(response){
		// Fail to get card token_id, implement as you wish here
		console.log('Fail to get card token_id, response:', response);
		$("span.procMsg").html(response['status_message']);
		$("#procPayment").show();
	}
};

function getCardToken(){
	var splitexp = $("#cardexpired").val().split("/");
	cardData['card_number'] = $("#cardnumber").val();
	cardData['card_exp_month'] = splitexp[0];
	cardData['card_exp_year'] = splitexp[1];
	cardData['card_cvv'] = $("#cardcvv").val();
	//MidtransNew3ds.getCardToken(cardData, options);
}

$(document).ready(function(){
	if (parent.$("textarea#comment").val() != ''){
		$("#tabnotes span").html(
			parent.$("textarea#comment").val().replace(/\r?\n/g, '<br />')
		);
		$("#comment").val(	parent.$("textarea#comment").val() );
	}
	$(".closefancy").click(function (){
		parent.closefancy();
	});
	$("#procPayment").click(function (){
		$("#procPayment").hide();
		$("span.procMsg").html('Please wait...');
		if ($("#paytype").val()=='1'){
			getCardToken();
		}
		$.post( parent.$("input[name='searchpolo']").attr('rolepath') + "processingcheckout.html", 
		$( "#paymentform" ).serialize(), function( out ) {
			if (out.hasOwnProperty('Error')){
				// alert(out['Error']);
				$("span.procMsg").html(out['Error']);
				$("#procPayment").show();
			}else if (out.hasOwnProperty('Status')){
				$("span.procMsg").html(out['Status'] + ' Please wait, page is redirecting.');
				location.href=parent.$("input[name='searchpolo']").attr('rolepath') + "paymentstatus.html?idcart=".out['idcart'];
			}
		}, "json")/*.always(function(out) {
			console.log(out);
		})*/;
	});
});