function updateUserNotif(){
	var jpending = parseInt($("div.orhis > span").html());
	var totnotif = 0;
	totnotif = totnotif + jpending;
	if (totnotif == 0){
		if ($("div.badgeuser").hasClass('active')==true){
			$("div.badgeuser").removeClass('active');
		}
	}else{
		if ($("div.badgeuser").hasClass('active')==false){
			$("div.badgeuser").addClass('active');
		}
	}
	$("div.badgeuser").html(totnotif);
}
function updateUserPending(jpending){
	if (parseInt(jpending)==0){
		if ($("div.orhis > span").hasClass('hide')==false){
			$("div.orhis > span").addClass('hide');
		}
	}else{
		if ($("div.orhis > span").hasClass('hide')==true){
			$("div.orhis > span").removeClass('hide');
		}
	}
	$("div.orhis > span").html(parseInt(jpending));
	updateUserNotif();
}
function switchsignout(){
	$("div.signoutmsg").html('Processing...');
	$("div.accountsignout").hide();
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingsignout.html", { 
		signout:1
	}, function( out ) {
		if (out.hasOwnProperty('Error')){
			$("div.signoutmsg").html(out['Error']);
		}else if (out.hasOwnProperty('Status')){
			$("div.signoutmsg").html('Sign out success!<br >Please wait for window auto reloading.');
			window.location.reload(true);	
		}
		$("div.signoutmsg").show();
	}, "json")/*.always(function(out) {
		console.log( out );
	})*/;
}
var posLogin = 0;
function signinupforgot(){
	$("div.loginaccmsg").html('Processing...');
	$("div.loginaccbutton").hide();
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processinglogin.html", { 
		poslogin:posLogin, 
		emaillogin: $("#emaillogin").val(), 
		passlogin: $("#passlogin").val(),
		passretypelogin: $("#passretypelogin").val(),
		rememberlogin: $('#rememberlogin').is(":checked")
	}, function( out ) {
		if (out.hasOwnProperty('Error')){
			$("div.loginaccmsg").html(out['Error']);
			if (out['Error']=='Account has been banned!'){
				$("div.loginaccmsg").append(
					' <a href="index.html" title="Self release banned" '+
					'onclick="switchreleasebanned(); return false;">Click here</a> to self release ban!'
				);
			}
		}else if (out.hasOwnProperty('Status')){
			if (posLogin==0){
				$("div.loginaccmsg").html('Login success!<br >Please wait for window auto reloading.');
				window.location.reload(true);	
			}else if (posLogin==1){
				$("#passlogin").val('');
				$("div.loginaccmsg").html('Registration success!<br >We will redirect you to login panel.');
				setTimeout(function(){
					switchsignin();
				}, 2000);
			}else if(posLogin==2){
				$("div.loginaccmsg").html('Please check your email for the instruction to reset password.');
			}else if(posLogin==3){
				$("div.loginaccmsg").html('Please check your email for the instruction to release ban.');
			}
		}
		$("div.loginaccbutton").show();
	}, "json")/*.always(function(out) {
		console.log( out );
	})*/;
}
function switchsignin(){
	posLogin = 0;
	$("div.loginaccmsg").html('');
	$("div.logintitle").html('Sign In Panel');
	$("div.logintext,div.loginsignup,input[name='passlogin'],div.loginor,div.loginother").show();
	$("div.loginbackpage,input[name='passretypelogin']").hide();
	$("div.loginaccbutton").html('Sign In');
//	$("#emaillogin").focus();
}
function switchsignup(){
	posLogin = 1;
	$("div.loginaccmsg").html('');
	$("div.logintitle").html('Sign Up Panel');
	$("div.logintext,div.loginsignup,div.loginor,div.loginother").hide();
	$("div.loginbackpage,input[name='passretypelogin'],input[name='passlogin']").show();
	$("div.loginaccbutton").html('Sign Up');
//	$("#emaillogin").focus();
}
function switchforgotpass(){
	posLogin = 2;
	$("div.loginaccmsg").html('');
	$("div.logintitle").html('Forgot Password Panel');
	$("div.logintext,div.loginsignup,input[name='passretypelogin'],input[name='passlogin'],div.loginor,div.loginother").hide();
	$("div.loginbackpage").show();
	$("div.loginaccbutton").html('Send to Email');
//	$("#emaillogin").focus();
}
function switchreleasebanned(){
	posLogin = 3;
	$("div.loginaccmsg").html('');
	$("div.logintitle").html('Self Release Banned Panel');
	$("div.logintext,div.loginsignup,input[name='passretypelogin'],input[name='passlogin'],div.loginor,div.loginother").hide();
	$("div.loginbackpage").show();
	$("div.loginaccbutton").html('Send to Email');
//	$("#emaillogin").focus();
}
function smartmenu(urltogo){
	if ($(".hideonmobile").css('display')!='none'){
		//desktop version
		window.location.href = urltogo;
	}
}
var childclick = 0;
var timetocloselang = 0;
var timetocloselogin = 0;
var timetoclosecart = 0;
$(document).ready(function(){
	/*after login,when visitor click thumb image / name*/
	$("div.accountimage").click(function() {
		window.location.href = $("input[name='searchpolo']").attr('rolepath') + "profile/";
	});
	/*enter on login,forgot,register panel*/
	$("#emaillogin,#passlogin,#passretypelogin").on('keyup', function (e) {
	    if (e.keyCode==13) {
	    	signinupforgot();
	    }
	});
	window.scrollTo(0,1);
	/*language action*/
	$("ul.languageopt > li").click(function (){
		if ($(this).hasClass("active")==false){
			$.post( $("input[name='searchpolo']").attr('rolepath') + "processinglanguage.html", 
			{ lang: $(this).attr('attr') }, 
			function (data){
//				console.log(data);
				if (data.hasOwnProperty('Error')){
					if ( typeof(data['Error']) != 'object' ){
						alert(data['Error']);
					}
				}else{
					window.location.reload(false);
				}
			}, "json")/*.always(function (data){
				console.log(data);
			})*/;
		}
	});
	/*search on enter*/
	$("input[name='searchpolo']").on('keyup', function (e) {
	    if (e.keyCode==13 && $(this).val()!='') {
	        window.location.href = $(this).attr('rolepath') + "search/"+$(this).val();
	    }
	});
	/*cart show/hide @ header*/
	if ($("nav.navbar").find("div.hideonmobile").css("display")=='none'){
		$("img#cart-icon,div.badgecart.active").click(function() {
			if ($("div.sectioncart").hasClass("active")==false && $("div.badgecart").hasClass("active")){
				$("div.sectioncart").addClass("active");
			}else{
				$("div.sectioncart").removeClass("active");
			}
		});
	}else{
		$("img#cart-icon,div.badgecart.active,div.sectioncart").mouseenter(function() {
			if ($("div.sectioncart").hasClass("active")==false && $("div.badgecart").hasClass("active")){
				$("div.sectioncart").addClass("active");
			}
			if (timetoclosecart != 0){
				clearTimeout(timetoclosecart);
			}
		}).mouseleave(function() {
			timetoclosecart = setTimeout(function(){
				if ($("div.sectioncart").hasClass("active")==true){
					$("div.sectioncart").removeClass("active");
				}	
			}, 500);
		});	
	}
	/*login show/hide @ header*/
	if ($("nav.navbar").find("div.hideonmobile").css("display")=='none'){
		$("img#login-icon").click(function() {
			if ($("div.sectionlogin").hasClass("active")==false){
//				$("#emaillogin").focus();
				$("div.sectionlogin").addClass("active");
			}else{
				$("div.sectionlogin").removeClass("active");
				switchsignin();
			}
		});
	}else{
		$("img#login-icon,div.sectionlogin").mouseenter(function() {
			if ($("div.sectionlogin").hasClass("active")==false){
//				$("#emaillogin").focus();
				$("div.sectionlogin").addClass("active");
			}
			if (timetocloselogin != 0){
				clearTimeout(timetocloselogin);
			}
		}).mouseleave(function() {
			timetocloselogin = setTimeout(function(){
				if ($("div.sectionlogin").hasClass("active")==true){
					$("div.sectionlogin").removeClass("active");
					switchsignin();
				}	
			}, 500);
		});
	}
	/*langage show/hide @ header*/
	$("a.language,div.sectionlanguage").mouseenter(function() {
		if ($("div.sectionlanguage").hasClass("active")==false){
			$("div.sectionlanguage").addClass("active");
		}
		if (timetocloselang != 0){
			clearTimeout(timetocloselang);
		}
	}).mouseleave(function() {
		timetocloselang = setTimeout(function(){
			if ($("div.sectionlanguage").hasClass("active")==true){
				$("div.sectionlanguage").removeClass("active");
			}	
		}, 500);
	});
	$( "ul.languageopt li" ).hover(
		function() {
			$( this ).addClass('hovering');
		}, function() {
			$( this ).removeClass('hovering');
		}
	);
	/*Auto Adjust header padding because of fixed nav*/
	var totpadheader = $("nav.navbar").find('.container').outerHeight(true) + 5;
	$(".paddingbecausenav").css('padding-top',totpadheader+'px');
	
	/* Header Running Text */
	var runningtext = new Swiper('#runningtextcontainer', {
		speed: 400,
		grabCursor: true,
		autoplay: {
			delay: 5000
		},
		loop: true
	});
	$("#runningtextcontainer").hover(function(){
		runningtext.autoplay.stop();
	}, function(){
		runningtext.autoplay.start();
	});
	
	/*-- Dropdown-Menu-JavaScript --*/
	//for non mobile
	$("nav.navbar").find(".dropdown").hover(
		function() {
			//console.log('1:'+$(".hideonmobile").css('display'));
			if ($(".hideonmobile").css('display')!='none'){
				//$('.dropdown-menu', this).stop( true, true ).slideDown("fast");
				if (!$(this).hasClass('open')){
					$(this).addClass('open');
				}
			}
		},
		function() {
			//console.log('2:'+$(".hideonmobile").css('display'));
			if ($(".hideonmobile").css('display')!='none'){
				//$('.dropdown-menu', this).stop( true, true ).slideUp("fast");
				if ($(this).hasClass('open')){
					$(this).removeClass('open');
				}
			}
		}
	);
	//for mobile
	$("button.navbar-toggle").click(function() {
		if ($(".hideonmobile").css('display')=='none' && $(this).hasClass('collapsed')){
			$("li.dropdown,li.nodropdown").removeClass('.open').removeClass('closed');
			$("a.dropdown-toggle").removeClass('closed');
			$("ul.multi-column-dropdown").removeClass('closed').removeClass('open');
			/*$("li.dropdown").find('a.dropdown-toggle').css(
				'background-image','url("images/menu/background-right-part.jpg")'
			);
			$("li.dropdown").find('a.dropdown-toggle').find('i').hide('fast');
			$("li.dropdown,li.nodropdown").show('fast');
			$("li.dropdown").find('a.dropdown-toggle').show('fast');*/
		}
		/*if ($(".hideonmobile").css('display')=='none' && !$(this).hasClass('collapsed')){
			if ($("ul.multi-column-dropdown.open").length > 0){
				//$("ul.multi-column-dropdown.open,li.dropdown.open").trigger('click');
				$("li.dropdown.open").trigger('click');
			}else{}
		}*/
	});
	$("nav.navbar").find("li.dropdown").click(function() {
		childclick = 0;
		if ($(".hideonmobile").css('display')=='none'){
			var thisID = $(this);
			setTimeout(function(){
				//console.log(thisID.hasClass('open'));
				if (thisID.hasClass('open') && childclick==0){
					$("li.dropdown,li.nodropdown").not('.open').addClass('closed');
					/*thisID.find('a.dropdown-toggle').css('background-image','none');
					thisID.find('a.dropdown-toggle').find('i').show('fast');
					$("li.dropdown,li.nodropdown").not(".open").hide('fast');
					thisID.find('.multi-column-dropdown').removeClass('open');
					thisID.find('.multi-column-dropdown').show('fast');*/
				}else if (!thisID.hasClass('open') && childclick==0){
					$("li.dropdown.closed,li.nodropdown.closed").removeClass('closed');					
					/*thisID.find('a.dropdown-toggle').css(
						'background-image','url("images/menu/background-right-part.jpg")'
					);
					thisID.find('a.dropdown-toggle').find('i').hide('fast');
					$("li.dropdown,li.nodropdown").show('fast');*/
					
				}else if (!thisID.hasClass('open') && childclick==1){
					//protect top parent still have open class (bubble click effect)
					thisID.addClass('open');
				}
			}, 2);
		}
	});
	$("nav.navbar").find("ul.multi-column-dropdown").click(function() {
		setTimeout(function(){
			childclick = 1;
		}, 1);
		if ($(this).hasClass('open')){
			$(this).removeClass('open');
			$("ul.multi-column-dropdown.closed").removeClass('closed');
			$(this).closest("li.dropdown").find('a.dropdown-toggle').removeClass('closed');
			//$(this).closest("li.dropdown.closed").removeClass('closed').addClass('open');
			/*$("li.dropdown.open").find("a.dropdown-toggle").stop( true, true ).slideDown("fast");
			$("ul.multi-column-dropdown").stop( true, true ).slideDown("fast");
			$(this).find('li.headingchild').stop( true, true ).slideUp("fast");
			$(this).find('li.heading').find('i').stop( true, true ).hide("fast");
			$(this).find('li.heading').stop( true, true ).css(
				'background-image','url("images/menu/background-right-part.jpg")'
			);*/
		}else if ($(".hideonmobile").css('display')=='none'){
			$(this).addClass('open');
			$("li.dropdown.open").find("ul.multi-column-dropdown").not('.open').addClass('closed');
			$(this).closest("li.dropdown").find('a.dropdown-toggle').addClass('closed');
			//$("li.dropdown.open").removeClass('open').addClass('closed');
			/*$("li.dropdown.open").find("a.dropdown-toggle").stop( true, true ).slideUp("fast");
			$("ul.multi-column-dropdown").not('.open').stop( true, true ).slideUp("fast");
			$(this).find('li.headingchild').stop( true, true ).slideDown("fast");
			$(this).find('li.heading').find('i').stop( true, true ).show("fast");
			$(this).find('li.heading').stop( true, true ).css('background-image','none');*/
		}
	});
	/*$("ul.multi-column-dropdown").hover(
		function() {
			//console.log('1a:'+$(".hideonmobile").css('display'));
			if ($(".hideonmobile").css('display')=='none'){
				$(this).find('li.headingchild').stop( true, true ).slideDown("fast");
			}
		},
		function() {
			//console.log('2a:'+$(".hideonmobile").css('display'));
			if ($(".hideonmobile").css('display')=='none'){
				$(this).find('li.headingchild').stop( true, true ).slideUp("fast");
			}
		}
	);*/
});