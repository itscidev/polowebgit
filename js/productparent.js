$(document).ready(function(){
	/* slide promo */
	var slidepromo = new Swiper('#slidepromo', {
		speed: 400,
		grabCursor: true,
		lazy: true,
		autoplay: {
			delay: 8000
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		loop: false
	});
	$("#slidepromo").hover(function(){
		slidepromo.autoplay.stop();
	}, function(){
		slidepromo.autoplay.start();
	});
});