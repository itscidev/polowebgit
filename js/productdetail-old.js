var Thumbs,Gallery;
function thumbSwap (){
	var Thumbs = new Swiper(".Thumbs", {
		direction: "vertical",
		spaceBetween: 10,
		loop: true,
		/*loopedSlides: 4,*/
		centeredSlides: true,
		slidesPerView: 'auto',
		touchRatio: 0.2,
		slideToClickedSlide: true
	});
	return Thumbs;
}
function gallerySwap (){
	var Gallery = new Swiper(".Gallery", {
		spaceBetween: 10,
		loop: true,
		loopedSlides: 4,
		centeredSlides: true,
		autoplay: {
			delay: 5000
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev'
		},
	});
	return Gallery;
}
function changeContentInStore(){
	/*image src*/
	var imgnya = $( "div.swiper-container.Thumbs" ).find("div.swiper-wrapper").find("div.swiper-slide");
	if (imgnya.first().length > 0){
		$("div.popup-contentleft").find("img").attr("src", imgnya.find("img").attr("src"));
	}
	/*size default*/
	var sizenya = $("div.det_nav1").find("div.multiopt.active").html();
	$("div.popup-contentright").find("select").val(sizenya);
}
function addtocart(){
	if ($("div.sectioncart").hasClass("active")==false){
		$("div.sectioncart").addClass("active");
		setTimeout(function(){
			$("div.sectioncart").removeClass("active");
		}, 2000);
	}
}
function togglePopUp(){
	if ($("div.popup-outer").css("display")=='block'){
		$("div.popup-outer").hide("fast");	
	}else{
		firstshowpopup();
		$("div.popup-outer").show("fast");
	}
}
function firstshowpopup(){
	$("div.resultsearchinstore").html('');
	$("div.popup-contentleft").show();
	$("div.popup-contentright p").show();
	if ($("div.popup-contentright").hasClass('col-sm-12')){
		$("div.popup-contentright").removeClass('col-sm-12').addClass('col-sm-7');
	}
	if ($("input#sistext").hasClass('secondstep')){
		$("input#sistext,select#sissize,button#sisbutton").removeClass('secondstep');
	}
}
function secondshowpopup(){
	$("div.resultsearchinstore").html('');
	$("div.popup-contentleft").hide();
	$("div.popup-contentright p").hide();
	if ($("div.popup-contentright").hasClass('col-sm-7')){
		$("div.popup-contentright").removeClass('col-sm-7').addClass('col-sm-12');
		$("input#sistext,select#sissize,button#sisbutton").addClass('secondstep');
	}
}
$(document).ready(function(){
	Thumbs = thumbSwap();
	Gallery = gallerySwap();
	/*var Thumbs = new Swiper(".Thumbs", {
		direction: "vertical",
		spaceBetween: 10,
		loop: true,
		loopedSlides: 4,
		centeredSlides: true,
		slidesPerView: 'auto',
		touchRatio: 0.2,
		slideToClickedSlide: true
	});
	var Gallery = new Swiper(".Gallery", {
		spaceBetween: 10,
		loop: true,
		loopedSlides: 4,
		centeredSlides: true,
		autoplay: {
			delay: 5000
		},
		navigation: {
	        nextEl: '.swiper-button-next',
	        prevEl: '.swiper-button-prev',
	      },
	});*/
	Gallery.controller.control = Thumbs;
	Thumbs.controller.control = Gallery;
	$(".Gallery,.Thumbs").hover(function(){
		Gallery.autoplay.stop();
	}, function(){
		Gallery.autoplay.start();
	});
	
	$(".zoombutton").click(function(){
		if ($(".zoombutton").hasClass('fa-search-plus')){
			$(".zoombutton").removeClass('fa-search-plus').addClass('fa-search-minus');
			$(".productmagnifier").show('slow');
			Gallery.autoplay.stop();
			$(".productmagnifier").css(
				"background-image", 
				"url('" + $(".swiper-slide-duplicate-active").find('img').attr('src') + "')"
			);
		}else{
			$(".zoombutton").removeClass('fa-search-minus').addClass('fa-search-plus');
			$(".productmagnifier").hide('fast');
			Gallery.autoplay.start();
			$(".productmagnifier").css("background-image","none");
		}
	});
	
	var firstload = 0, offset = 0, divWidth = 0, divHeight = 0;
	$(".productmagnifier").mousemove(function( event ) {
		if (firstload == 0){
			offset = $(".productmagnifier").offset();
			divWidth = $( ".productmagnifier" ).width();
			divHeight = $( ".productmagnifier" ).height();
			firstload = 1;
		}
		var posX = ((event.pageX-offset.left)/divWidth)*100;
		var posY = ((event.pageY-offset.top)/divHeight)*100;
		/*var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
		var clientCoords = "( " + event.clientX + ", " + event.clientY + " )";
		$( ".productmagnifier" ).html( 
			"( event.pageX, event.pageY ) : " + pageCoords + 
			"<br />( event.clientX, event.clientY ) : " + clientCoords + 
			"<br />left: " + offset.left + ", top: " + offset.top + 
			"<br />width: " + divWidth + ", height: " + divHeight + 
			"<br />posX: " + posX + "%, posY: " + posY + "%"
		);*/
		$( ".productmagnifier" ).css("background-position", posX + "% " + posY + "%");
	});
	//change gallery view
	$("img.multiopt").click(function (){
		if ($(this).hasClass('active')==false){
			$("img.multiopt.active").removeClass('active');
			$(this).addClass('active');
			Thumbs.removeAllSlides();
			Gallery.removeAllSlides();
			var datanow = $(this).attr('dataprod');
			if (optprod.hasOwnProperty(datanow)){
				$.each(optprod[datanow], function( index1, value1 ) {
					Thumbs.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
					Gallery.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
				});
				changeContentInStore();
			}
//			$.each(optprod, function( index, value ) {
//				if (index==datanow){
//					$.each(value, function( index1, value1 ) {
//						Thumbs.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
//						Gallery.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
//					});
//					changeContentInStore();
//				}
//				//  alert( index + ": " + value );
//			});
			Thumbs.destroy( false,true );
			Gallery.destroy( false,true );
			Thumbs = thumbSwap();
			Gallery = gallerySwap();
			Gallery.controller.control = Thumbs;
			Thumbs.controller.control = Gallery;
		}
	});
	//change size
	$("div.multiopt").click(function (){
		if ($(this).hasClass('active')==false){
			$("div.multiopt.active").removeClass('active');
			$(this).addClass('active');
		}
	});
	
	/*search on store*/
	changeContentInStore();
	
});
function pi_tab(idtarget){
	$("div.tab-pane.fade.active.in").removeClass('in').removeClass('active');
	$("div#"+idtarget).addClass('active').addClass('in');
}
function searchstore(urlpar){
	$.ajax({
		url: urlpar + 'processingsearchinstore.html',
		method: "POST",
		data: { sistext: $("#sistext").val(), sissize: $("#sissize").val() },
		dataType: "json",
		beforeSend: function() {
			$("div.loading").css('display','block');
			secondshowpopup();
		}
	}).done(function( out ) {
		//console.log(out);
		if (out.hasOwnProperty('Error')){
			if ( typeof(out['Error']) != 'object' ){
				$("div.resultsearchinstore").html(out['Error']);
			}
		}else{
			$.each( out.Result,function( index, value ) {
				$("div.resultsearchinstore").append(
					'<div class="col-sm-6 col-xs-12 outletlist"><div>' + value + '</div></div>'
				);
			});
		}
		$("div.loading").css('display','none');
	});
	
	/*popup-contentleft*/
}