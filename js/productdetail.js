var Thumbs,Gallery;
var choosenopt = [0,0,0,0];//style,size,color,idvariant

function thumbSwap (){
	var Thumbs = new Swiper(".Thumbs", {
		direction: "vertical",
		spaceBetween: 10,
		loop: true,
		/*loopedSlides: 4,*/
		centeredSlides: true,
		slidesPerView: 'auto',
		touchRatio: 0.2,
		slideToClickedSlide: true
	});
	return Thumbs;
}
function gallerySwap (){
	var Gallery = new Swiper(".Gallery", {
		spaceBetween: 10,
		loop: true,
		loopedSlides: 4,
		centeredSlides: true,
		autoplay: {
			delay: 5000
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev'
		},
	});
	return Gallery;
}
function changeContentInStore(){
	/*image src*/
	var imgnya = $( "div.swiper-container.Thumbs" ).find("div.swiper-wrapper").find("div.swiper-slide");
	if (imgnya.first().length > 0){
		$("div.popup-contentleft").find("img").attr("src", imgnya.find("img").attr("src"));
	}
	/*size default*/
	var sizenya = $("div.sizeopt.multiopt.active").html();
	$("span.sissize").html('Size : ' + sizenya);
	/*style default*/
	var stylenya = $("div.styleopt.multiopt.active").html();
	$("span.sisstyle").html('Style : ' + stylenya + ', ');
}

function togglePopUp(){
	if ($("div.popup-outer").css("display")=='block'){
		$("div.popup-outer").hide("fast");	
	}else{
		firstshowpopup();
		$("div.popup-outer").show("fast");
	}
//	console.log(choosenopt);
	//data id,qty & price
//	console.log(proddet);
	//data img
//	console.log(optprod);
}
function firstshowpopup(){
	$("div.resultsearchinstore").html('');
	$("div.popup-contentleft").show();
	$("div.popup-contentright p").show();
	if ($("div.popup-contentright").hasClass('col-sm-12')){
		$("div.popup-contentright").removeClass('col-sm-12').addClass('col-sm-7');
	}
	if ($("input#sistext").hasClass('secondstep')){
		$("input#sistext,select#sissize,button#sisbutton").removeClass('secondstep');
	}
}
function secondshowpopup(){
	$("div.resultsearchinstore").html('');
	$("div.popup-contentleft").hide();
	$("div.popup-contentright p").hide();
	if ($("div.popup-contentright").hasClass('col-sm-7')){
		$("div.popup-contentright").removeClass('col-sm-7').addClass('col-sm-12');
		$("input#sistext,select#sissize,button#sisbutton").addClass('secondstep');
	}
}
function changeImgSlide(){
	var idxImgProd0 = choosenopt[0];
	var idxImgProd1 = choosenopt[2];
	if (optprod.hasOwnProperty(idxImgProd0)){
		if (optprod[idxImgProd0].hasOwnProperty(idxImgProd1)){
			Thumbs.removeAllSlides();
			Gallery.removeAllSlides();
			$.each(optprod[idxImgProd0][idxImgProd1], function( index1, value1 ) {
				Thumbs.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
				Gallery.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
			});
			changeContentInStore();
			Thumbs.destroy( false,true );
			Gallery.destroy( false,true );
			Thumbs = thumbSwap();
			Gallery = gallerySwap();
			Gallery.controller.control = Thumbs;
			Thumbs.controller.control = Gallery;
		}
	}
	//console.log(optprod[idxImgProd0][idxImgProd1]);
}
function checkstockprice(){
	//console.log(choosenopt);
	//data id,qty & price
	//console.log(proddet);
	var cStyle = choosenopt[0];
	var cSize = choosenopt[1];
	var cColor = choosenopt[2];
	var qty = 0;
	var price = 0;
	var pricep = '';
	if (proddet.hasOwnProperty(cStyle)){
		if (proddet[cStyle].hasOwnProperty(cSize)){
			if (proddet[cStyle][cSize].hasOwnProperty(cColor)){
				qty = proddet[cStyle][cSize][cColor]['Q'];
				price = proddet[cStyle][cSize][cColor]['P'];
				pricep = proddet[cStyle][cSize][cColor]['PP'];
				choosenopt[3] = proddet[cStyle][cSize][cColor]['I'];
			}
		}
	}
	if (qty!=0 && price!=0){
		$("p.price").html('IDR ' + pricep + ',-');
		$("a.detbutton.sis").show();
		$("div.errMsg").html('');
	}else{
		$("p.price").html('');
		$("a.detbutton.sis").hide();
		$("div.errMsg").html('Sorry, this variant is not available!');
	}
}
$(document).ready(function(){
	if ($(".styleopt.multiopt.active").length > 0){
		choosenopt[0] = $(".styleopt.multiopt.active").attr('dataattr');
	}
	$(".styleopt.multiopt").click(function (){
		if (choosenopt[0] != $(this).attr('dataattr')){
			choosenopt[0] = $(this).attr('dataattr');
			changeImgSlide();
			checkstockprice();
		}
	});
	if ($(".sizeopt.multiopt.active").length > 0){
		choosenopt[1] = $(".sizeopt.multiopt.active").attr('dataattr');
	}
	$(".sizeopt.multiopt").click(function (){
		if (choosenopt[1] != $(this).attr('dataattr')){
			choosenopt[1] = $(this).attr('dataattr');
			checkstockprice();
		}
	});
	if ($(".coloropt.multiopt.active").length > 0){
		choosenopt[2] = $(".coloropt.multiopt.active").attr('dataattr');
	}
	$(".coloropt.multiopt").click(function (){
		if (choosenopt[2] != $(this).attr('dataattr')){
			choosenopt[2] = $(this).attr('dataattr');
			changeImgSlide();
			checkstockprice();
		}
	});
	checkstockprice();
	Thumbs = thumbSwap();
	Gallery = gallerySwap();
	/*var Thumbs = new Swiper(".Thumbs", {
		direction: "vertical",
		spaceBetween: 10,
		loop: true,
		loopedSlides: 4,
		centeredSlides: true,
		slidesPerView: 'auto',
		touchRatio: 0.2,
		slideToClickedSlide: true
	});
	var Gallery = new Swiper(".Gallery", {
		spaceBetween: 10,
		loop: true,
		loopedSlides: 4,
		centeredSlides: true,
		autoplay: {
			delay: 5000
		},
		navigation: {
	        nextEl: '.swiper-button-next',
	        prevEl: '.swiper-button-prev',
	      },
	});*/
	Gallery.controller.control = Thumbs;
	Thumbs.controller.control = Gallery;
	$(".Gallery,.Thumbs").hover(function(){
		Gallery.autoplay.stop();
	}, function(){
		Gallery.autoplay.start();
	});
	
	$(".zoombutton").click(function(){
		if ($(".zoombutton").hasClass('fa-search-plus')){
			$(".zoombutton").removeClass('fa-search-plus').addClass('fa-search-minus');
			$(".productmagnifier").show('slow');
			Gallery.autoplay.stop();
			$(".productmagnifier").css(
				"background-image", 
				"url('" + $(".swiper-slide-duplicate-active").find('img').attr('src') + "')"
			);
		}else{
			$(".zoombutton").removeClass('fa-search-minus').addClass('fa-search-plus');
			$(".productmagnifier").hide('fast');
			Gallery.autoplay.start();
			$(".productmagnifier").css("background-image","none");
		}
	});
	
	var firstload = 0, offset = 0, divWidth = 0, divHeight = 0;
	$(".productmagnifier").mousemove(function( event ) {
		if (firstload == 0){
			offset = $(".productmagnifier").offset();
			divWidth = $( ".productmagnifier" ).width();
			divHeight = $( ".productmagnifier" ).height();
			firstload = 1;
		}
		var posX = ((event.pageX-offset.left)/divWidth)*100;
		var posY = ((event.pageY-offset.top)/divHeight)*100;
		/*var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
		var clientCoords = "( " + event.clientX + ", " + event.clientY + " )";
		$( ".productmagnifier" ).html( 
			"( event.pageX, event.pageY ) : " + pageCoords + 
			"<br />( event.clientX, event.clientY ) : " + clientCoords + 
			"<br />left: " + offset.left + ", top: " + offset.top + 
			"<br />width: " + divWidth + ", height: " + divHeight + 
			"<br />posX: " + posX + "%, posY: " + posY + "%"
		);*/
		$( ".productmagnifier" ).css("background-position", posX + "% " + posY + "%");
	});
	
	//change gallery view
	$(".multiopt").click(function (){
		if ($(this).hasClass('active')==false){
			if ($(this).parent().find(".multiopt.active").length > 0){
				$(this).parent().find(".multiopt.active").removeClass('active');	
			}
			$(this).addClass('active');
		}
	});
/*	$(".multiopt").click(function (){
		if ($(this).hasClass('active')==false){
			console.log(choosenopt);
			if ($(this).parent().find(".multiopt.active").length > 0){
				$(this).parent().find(".multiopt.active").removeClass('active');	
			}
			$(this).addClass('active');
			Thumbs.removeAllSlides();
			Gallery.removeAllSlides();
//			var datanow = $(this).attr('dataprod');
			if (optprod.hasOwnProperty(datanow)){
				$.each(optprod[datanow], function( index1, value1 ) {
					Thumbs.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
					Gallery.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
				});
				changeContentInStore();
			}
//			$.each(optprod, function( index, value ) {
//				if (index==datanow){
//					$.each(value, function( index1, value1 ) {
//						Thumbs.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
//						Gallery.appendSlide('<div class="swiper-slide"><img src="' + pathurl + 'images/productitem/' + value1 + '"/></div>');
//					});
//					changeContentInStore();
//				}
//				//  alert( index + ": " + value );
//			});
			Thumbs.destroy( false,true );
			Gallery.destroy( false,true );
			Thumbs = thumbSwap();
			Gallery = gallerySwap();
			Gallery.controller.control = Thumbs;
			Thumbs.controller.control = Gallery;
		}
	});*/
	//change size
//	$("div.multiopt").click(function (){
//		if ($(this).hasClass('active') == false){
//			if ($(this).parent().find("div.multiopt.active").length > 0){
//				$(this).parent().find("div.multiopt.active").removeClass('active');	
//			}
//			$(this).addClass('active');
//		}
//	});
	
	/*search on store*/
	changeContentInStore();
	//first time load,clear my location textfield
	$("input#sistext").val('');
	
});
function pi_tab(idtarget){
	$("div.tab-pane.fade.active.in").removeClass('in').removeClass('active');
	$("div#"+idtarget).addClass('active').addClass('in');
}
function searchstore(urlpar){
	$.ajax({
		url: urlpar + 'processingsearchinstore.html',
		method: "POST",
		data: { sistext: $("#sistext").val(), sisidprod: choosenopt[3] },
		dataType: "json",
		beforeSend: function() {
			$("div.loading").css('display','block');
			secondshowpopup();
		}
	}).done(function( out ) {
//		console.log(out);
		if (out.hasOwnProperty('Error')){
			if ( typeof(out['Error']) != 'object' ){
				$("div.resultsearchinstore").html(out['Error']);
			}
		}else{
			$.each( out.Result,function( index, value ) {
				$("div.resultsearchinstore").append(
					'<div class="col-sm-6 col-xs-12 outletlist"><div>' + value + '</div></div>'
				);
			});
		}
		$("div.loading").css('display','none');
	});
	
	/*popup-contentleft*/
}