function resetPassword(){
	if ($("div.alert.alert-danger").length > 0){
		$("div.alert.alert-danger").remove();
	}
	$("div.resetpanel > div.panel-footer").html("Please wait...");
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingloginpurpose.html", {
		tokenlogin: $("#tokenR").html(), passlogin: $("#passwordR").val(), passretypelogin: $("#passwordRR").val() 
	}, function( out ) {
		if (out.hasOwnProperty('Error')){
			$("div.logpcontent").prepend(
				'<div class="alert alert-danger alert-dismissible" role="alert">'+
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
					'<span aria-hidden="true">&times;</span></button>'+
					'<strong>Warning!</strong> '+out['Error']+
				'</div>'
			);
			$("div.resetpanel > div.panel-footer").html(
				'<button type="submit" class="btn btn-default" '+
				'onclick="resetPassword(); return false;">Submit</button>'
			);
		}else if (out.hasOwnProperty('Status')){
			$("div.resetpanel").hide();
			$("div.logpcontent").prepend(
				'<div class="alert alert-success alert-dismissible" role="alert">'+
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
					'<span aria-hidden="true">&times;</span></button>'+
					'<strong>Well done!</strong> '+out['Status']+
				'</div>'
			);
		}
	}, "json")/*.always(function(out) {
		console.log( out );
	})*/;
}
$(document).ready(function(){
	
});