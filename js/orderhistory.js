function openfancy(urlsrc){
	var fancywidth = 50;
	var widthScreen = $( window ).width();
	if (widthScreen < 900){
		fancywidth = 80;
	}
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: fancywidth + '%',
		height		: '80%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true,
		helpers   	: {
			overlay : {closeClick: true} // prevents closing when clicking OUTSIDE fancybox 
		},
		closeBtn	: true/*,
		beforeShow: function(){
			this.height = $('.fancybox-iframe').contents().find('html').height();
		}*/
	});
}
// Close 3DSecure dialog box
function closefancy() {
    $.fancybox.close();
    $("div.paybutton").show();
}
$(document).ready(function(){
	
});