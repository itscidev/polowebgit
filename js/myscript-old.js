function smartmenu(urltogo){
	if ($(".hideonmobile").css('display')!='none'){
		//desktop version
		window.location.href = urltogo;
	}
}
$(document).ready(function(){
	/*Auto Adjust header padding because of fixed nav*/
	var totpadheader = $("nav.navbar").find('.container').outerHeight(true) + 5;
	$(".paddingbecausenav").css('padding-top',totpadheader+'px');
	
	/* Header Running Text */
	var runningtext = new Swiper('#runningtextcontainer', {
	    speed: 400,
	    grabCursor: true,
	    autoplay: {
	        delay: 5000
	      },
	    loop: true
	});
	$("#runningtextcontainer").hover(function(){
		runningtext.autoplay.stop();
	}, function(){
		runningtext.autoplay.start();
	});
	
	/*-- Dropdown-Menu-JavaScript --*/
	$(".dropdown").hover(
		function() {
			$('.dropdown-menu', this).stop( true, true ).slideDown("fast");
			$(this).toggleClass('open');
		},
		function() {
			$('.dropdown-menu', this).stop( true, true ).slideUp("fast");
			$(this).toggleClass('open');
		}
	);
	$(".dropdown").click(function() {
		//console.log("dropdown");
		var thisID = $(this);
		setTimeout(function(){ thisID.addClass("open") }, 2);
	});
	$("ul.multi-column-dropdown").hover(
		function() {
			if ($(".hideonmobile").css('display')=='none'){
				$(this).find('li.headingchild').stop( true, true ).slideDown("fast");
			}
		},
		function() {
			if ($(".hideonmobile").css('display')=='none'){
				$(this).find('li.headingchild').stop( true, true ).slideUp("fast");
			}
		}
	);
});