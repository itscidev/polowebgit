var cartsubtotal = 0;
function updateSubTotalCart(out){
	//qtycart
	if (out.hasOwnProperty('cartqty')){
		$("div.badgecart").html(out['cartqty']);
		$("span.cartcount").html(out['cartqty']);
		if (out['cartqty']!='0' && $("div.badgecart").hasClass('active')==false){
			$("div.badgecart").addClass('active');
			setTimeout(function(){
				$("div.sectioncart").removeClass("active");
			}, 2000);
		}else if (out['cartqty']=='0' && $("div.badgecart").hasClass('active')==true){
			$("div.sectioncart").removeClass("active");
			$("div.badgecart").removeClass('active');
		}
	}else{
		$("span.cartcount").html('0');
	}
	//subtotal
	if (out.hasOwnProperty('cartsubtotal')){
		$("div.cartfooter > div.subtotal > span").html(out['cartsubtotal']);
		$("span.cartsubtotal").html(out['cartsubtotal']);
		cartsubtotal = out['cartsubtotalnom'];
	}else{
		$("span.cartsubtotal").html('Rp 0');
	}
	//est weight
	if (out.hasOwnProperty('cartweight')){
		$("span.cartestweight").html(out['cartweight']);
	}else{
		$("span.cartestweight").html('0 Gram');
	}
}
function cartdelitem(cartitemid){
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingcart.html", { 
		cartmode:'cartdel', 
		idvariety: cartitemid 
	}, function( out ) {
		if (out.hasOwnProperty('Error')){
			alert(out['Error']);
		}else if (out.hasOwnProperty('Status')){
			
			$("div.cartitem.cartitem" + cartitemid).remove();
			
			updateSubTotalCart(out);
			
		}
		if ($("#cartSubTotalNum").length > 0) {
			$("#cartSubTotalNum").html(cartsubtotal); 
		    updateTotalCart();
		}
		/*$("div.btn_form > form").prepend(
			'<a href="#" class="detbutton addtocartbut" onclick="addtocart(); return false;">Add to Cart</a>'
		);*/
	}, "json")/*.always(function(out) {
		console.log(out);
	})*/;
}
function cartadditem(){
	if ($("div.sectioncart").hasClass("active")==false){
		$("div.errMsg").html("Please wait...");
		$("a.detbutton.addtocartbut").hide();
		$.post( $("input[name='searchpolo']").attr('rolepath') + "processingcart.html", { 
			cartmode:'cartadd', 
			idvariety: choosenopt[3] 
		}, function( out ) {
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				$("div.errMsg").html(out['Error']);
			}else if (out.hasOwnProperty('Status')){
				$("div.errMsg").html(out['Status']);
				$("div.sectioncart").addClass("active");
				var addstr = '<div class="cartitem cartitem' + choosenopt[3] + '"><div class="cartitemimg">';
				if (out.hasOwnProperty('image')){
					addstr += '<img alt="' + out['name'] + '" src="' + 
						$("input[name='searchpolo']").attr('rolepath') + 
						'images/productitem/' + out['image'] + '">';
				}
				addstr += '</div><div class="cartitemdesc">';
				addstr += '<p>' + out['name'] + '</p>';
				if (out.hasOwnProperty('color')){
					if (out['color']=='Undefined!'){
						addstr += '<p>COLOR : Undefined!</p>';	
					}else{
						addstr += '<p>COLOR : <img alt="' + out['colorname'] + '" src="' + 
							$("input[name='searchpolo']").attr('rolepath') + 
							'images/productcolor/' + out['color'] + '"></p>';
					}
				}
				if (out.hasOwnProperty('size')){
					if (out['size']=='Undefined!'){
						addstr += '<p>SIZE : Undefined!</p>';	
					}else{
						addstr += '<p>SIZE : ' + out['size'] + '</p>';
					}
				}
				addstr += '<p>QTY : 1</p>';
				addstr += '<p>Rp ' + out['price'] + '</p>';
				addstr += '</div><div class="cartitemdel">';
				addstr += '<i class="fa fa-times pointer" onclick="cartdelitem(' + choosenopt[3] + ');return false;"></i>';
				addstr += '</div></div>';
				$("div.sectioncart > div.cartopt").append(addstr);
				
				updateSubTotalCart(out);
				
				setTimeout(function(){
					$("div.sectioncart").removeClass("active");
				}, 2000);
			}

			$("a.detbutton.addtocartbut").show();
			
			if ($("#cartSubTotalNum").length > 0) {
				$("#cartSubTotalNum").html(cartsubtotal); 
			    updateTotalCart();
			}
		}, "json")/*.always(function(out) {
			console.log(out);
		})*/;
	}
}
function cartedititem(cartitemid){
	$("div.alert.alert-danger.alert-dismissible").remove();
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingcart.html", { 
		cartmode:'cartupd', 
		idvariety: cartitemid, 
		qty:$("div.cartitem.cartitem" + cartitemid).find("select").val() 
	},function( out ) {
		if (out.hasOwnProperty('Error')){
			$("div.cartitem.cartitem" + cartitemid).prepend(
				'<div class="alert alert-danger alert-dismissible" role="alert">' + 
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' + 
						'<span aria-hidden="true">&times;</span>' + 
					'</button>' + 
					'<strong>Warning!</strong> ' + out['Error'] + 
				'</div>'
			);
		}else if (out.hasOwnProperty('Status')){

			updateSubTotalCart(out);
			
		}
		if ($("#cartSubTotalNum").length > 0) {
			$("#cartSubTotalNum").html(cartsubtotal); 
		    updateTotalCart();
		}
	}, "json")/*.always(function(out) {
		console.log(out);
	})*/;
}