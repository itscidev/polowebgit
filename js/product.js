addEventListener("load", function() {
	setTimeout(hideURLbar, 0); 
}, false); 
function hideURLbar(){ window.scrollTo(0,1); }
$(document).ready(function(){
	/* 
	<!-- Pricing-Popup-Box-JavaScript -->
	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',
		fixedContentPos: false,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	}); */
	
	//Initialize Select2 Elements
//	$(".select2").select2({
//		placeholder: "Sort by :"
//	});
//	$('#sortby').on('select2:select', function (e) {
//		console.log('1');
//	});
	$("#sortby").on('change', function() {
		window.location = $("#sortby option:selected").attr('url');
	});
	$(".filterchecked").on('change', function() {
		window.location = $(this).attr('url');
	});
	
	$(".colortype").mouseover(function() {
		$(this).closest("div.content_box").find("img.img-responsive").attr("src", $(this).attr("img-src") );
	});
	$("span.togcat").click(function(){
		$(this).parent().children('div').toggleClass("active");
		$(this).find('i').toggleClass("active");
	});

	$("h4.showhidefiltersection").click(function() {
		if ($(this).find("i").hasClass('up')){
			$(this).find("i").removeClass('up');
			$(this).parent().find("div.row1.scroll-pane").removeClass('up');
		}else{
			$(this).find("i").addClass('up');
			$(this).parent().find("div.row1.scroll-pane").addClass('up');
		}
	});

	$(".hidefilter").click(function() {
		if ($(".hidestring").html() == 'Hide'){
			$(".hidestring").html('Show');
			$("div.col-sm-3.s-d").hide('slow');
			$("div.col-sm-9.w_content").addClass('col-sm-12').removeClass('col-sm-9');
		}else{
			$(".hidestring").html('Hide');
			$("div.col-sm-3.s-d").show('slow');
			$("div.col-sm-12.w_content").addClass('col-sm-9').removeClass('col-sm-12');
		}
	});
	
	//set active/in-active necessary componen on filterCategory
	if ($('input.chkboxcat:checked').length > 0){
		$.each($('input.chkboxcat:checked'), function(index,value) {
			var stop = 0;
			if (stop == 0){
				$(value).parents().map(function() {
					if ($(this).hasClass('filcat')){stop = 1;}
//					//return this.tagName;
					if (stop == 0){
						if (this.tagName=='DIV'){
							//console.log($(this));
							if ($(this).hasClass('active')==false){
								$(this).addClass('active');	
							}
							if ($(this).children("span").children("i").hasClass('active')==false){
								$(this).children("span").children("i").addClass('active');	
							}
							$.each($(this).children("div"), function(index1,value1) {
								if ($(value1).hasClass('active')==false){
									$(value1).addClass('active');
								}	
							});
						}
					}
					
				});
			}
		});
	}
	
	/*hide filter on load, khusus mobile*/
	setTimeout(function(){
		if ($(".hideonmobile").css('display')=='none' && $(".hidestring").html()=='Hide'){
			$(".hidefilter").trigger( "click" );
		}
	}, 2000);
});