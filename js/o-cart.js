function number_format(number, decimals, decPoint, thousandsSep){
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
	var n = !isFinite(+number) ? 0 : +number
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
	var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
	var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
	var s = ''

	var toFixedFix = function(n, prec){
		if (('' + n).indexOf('e') === -1) {
			return +(Math.round(n + 'e+' + prec) + 'e-' + prec)
		} else {
			var arr = ('' + n).split('e')
			var sig = ''
			if (+arr[1] + prec > 0) {
				sig = '+'
			}
			return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec)
		}
	}

	// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.')
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || ''
		s[1] += new Array(prec - s[1].length + 1).join('0')
	}
	return s.join(dec)
}
function updateTotalCart(){
	var pfsub = 0;
	var pfdisc = 0;
	var pfongkir = 0;
	if ($("#cartSubTotalNum").length > 0){
		pfsub = parseFloat($("#cartSubTotalNum").html());
	}
	if ($("#cartDiscTotalNum").length > 0){
		pfdisc = parseFloat($("#cartDiscTotalNum").html());
	}
	if ($("#cartOngkirNum").length > 0){
		pfongkir = parseFloat($("#cartOngkirNum").html());
	}
	$("#cartTotal").html('Rp ' + number_format(( pfsub + pfongkir ),0,',','.'));
	$("#cartDiscTotal").html('Rp ' + number_format(pfdisc,0,',','.'));
	$("#cartGrandTotal").html('Rp ' + number_format((pfsub + pfongkir - pfdisc),0,',','.'));
}
function forceopenloginpanel(){
	if ($("div.sectionlogin").hasClass("active")==false){
		$("div.sectionlogin").addClass("active");
	}
}
function addrusethis(idaddr,forceclose){
	$("#addresschooseid").html(idaddr);
	if (idaddr != 0){
		var parsejson = JSON.parse($("tr.addrrow.row" + idaddr + " > td.addrusethis > p.hidden").html());
		// console.log('masuk');
		// console.log(parsejson);
		$("div.shipmentinfo > span").html(parsejson['mma_saveas']);
		var addaddr = '';
		if (parsejson['kec'] != null){
			if (addaddr != ''){addaddr += ', ';}
			addaddr += getaddr1['kec'];
		}
		if (parsejson['kab'] != null){
			if (addaddr != ''){addaddr += ', ';}
			addaddr += parsejson['kab'];
		}
		if (parsejson['prop'] != null){
			if (addaddr != ''){addaddr += ', ';}
			addaddr += parsejson['prop'];
		}
		if (parsejson['mma_zip'] != '0'){
			if (addaddr != ''){addaddr += ', ';}
			addaddr += parsejson['mma_zip'];
		}
		$("div.shipmentinfo > div").html(
			parsejson['mma_name'] + ' (' + parsejson['mma_phone'] + ')<br>' + parsejson['mma_address'] + addaddr
		);
	}else{
		$("div.shipmentinfo > span").html('No saved address found!');
		$("div.shipmentinfo > div").html('Please add new address.');
	}
	if (forceclose==1){
		$("div.popupwin.popupaddress,div.newaddress,div.existingaddress").removeClass("active");	
	}
}
function preparenewaddr(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$("div.addrmsg").html('');
	$('#cartmode').val('addradd');
	$('#addrid').val('');
	$('#addrsaveas').val('');
	$('#addrnama').val('');
	$('#addrhp').val('');
	$('#addrzip').val('');
	$('#addraddress').val('');
}
function paymentusethis(idpay,forceclose){
	// console.log(idpay);
	$("#paymentchooseid").html(idpay);
	
	$("div.payment-result > img").attr('src',$("p.pay"+idpay+" > img").attr('src'));
	$("div.payment-result > span").html($("p.pay"+idpay+" > span").html());

	if (forceclose==1){
		$("div.payment,div.paymentform").removeClass("active");	
	}
	updateTotalCart();
}
function crraction(idcourier,forceclose){
	$("ongkirchooseid").html(idcourier);
	if (idcourier==0){
		$("div.courierinfo > span").html('No expedition available!');
		$("#cartOngkirNum").html(0);
	}else{
		var objcrr = JSON.parse($("tr.crrrow.row" + idcourier + " > td.crrusethis > p.hidden").html());
		// console.log(objcrr);
		$("div.courierinfo > span").html(objcrr['show']);
		$("#cartOngkirNum").html(objcrr['toteksprice']);
	}
	if (forceclose==1){
		$("div.popupwin.popupcourier").removeClass("active");
	}
	updateTotalCart();
}
function crrusethis(idcourier,forceclose,updcookie){
	// console.log(idcourier);
	if (updcookie!=0){
		$.post( $("input[name='searchpolo']").attr('rolepath') + "processingcart.html", 
		{cartmode:'courierupd',courier:idcourier}, 
		function( out ) {
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					if (out['Error']=='Login session expired!'){
						setTimeout(function(){
							window.location.reload(true);
						}, 2000);
					}
				}else{
					
				}
			}else if (out.hasOwnProperty('Status')){
				crraction(idcourier,forceclose);
			}
		}, "json")/*.always(function(out) {
			console.log(out);
		})*/;
	}else{
		crraction(idcourier,forceclose);
	}
}
function saveAddress(){
	$("div.addrmsg").html('Please wait...');
	$("div.addrsave").hide();
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingcart.html", 
	$( "#addressform" ).serialize(), 
	function( out ) {
		if (out.hasOwnProperty('Error')){
			if (typeof out['Error'] == 'string'){
				$("div.addrmsg").html(out['Error']);
				if (out['Error']=='Login session expired!'){
					setTimeout(function(){
						window.location.reload(true);
					}, 2000);
				}
			}else{
				$("div.addrmsg").html('Error found!');
				if (out['Error'].hasOwnProperty('addrid')){
					$('#addrid').parent().addClass( "has-error" );
				}
				if (out['Error'].hasOwnProperty('addrnama')){
					$('#addrnama').parent().addClass( "has-error" );
				}
				if (out['Error'].hasOwnProperty('addrhp')){
					$('#addrhp').parent().addClass( "has-error" );
				}
				if (out['Error'].hasOwnProperty('addrlosid')){
					$('#addrlosid').parent().addClass( "has-error" );
				}
				if (out['Error'].hasOwnProperty('addraddress')){
					$('#addraddress').parent().addClass( "has-error" );
				}
			}
		}else if (out.hasOwnProperty('Status')){
			if (out['Status']=='addradd'){
				if ($("tr.addrrow.nodata").length>0){
					$("tr.addrrow.nodata").remove();
				}
				$("div.existingaddress > table > tbody").prepend(out['newaddr']);
				preparenewaddr();
				$("div.addrmsg").html('Success add new data!');
				addrusethis(out['addrslctid'],0);
				if (out.hasOwnProperty('courier')){
					$("div.popupcourier tbody").html(out['existcourier']);
					crrusethis(out['courier'],0,0);
				}
			}
			updateSubTotalCart(out);
		}
		$("div.addrsave").show();
	}, "json")/*.always(function(out) {
		console.log(out);
	})*/;
}
function addrupd(idattr){
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingcart.html", 
	{cartmode:'addrupd',addrid:idattr}, 
	function( out ) {
		if (out.hasOwnProperty('Error')){
			if (typeof out['Error'] == 'string'){
				$("div.addrmsg").html(out['Error']);
				if (out['Error']=='Login session expired!'){
					window.location.reload(true);
				}
			}else{
				
			}
		}else if (out.hasOwnProperty('Status')){
			addrusethis(out['addrslctid'],1);
			if (out.hasOwnProperty('courier')){
				$("div.popupcourier tbody").html(out['existcourier']);
				crrusethis(out['courier'],0,0);
			}
		}
	}, "json")/*.always(function(out) {
		console.log(out);
	})*/;
}
function addrdel(idattr){
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingcart.html", 
	{cartmode:'addrdel',addrid:idattr}, 
	function( out ) {
		if (out.hasOwnProperty('Error')){
			if (typeof out['Error'] == 'string'){
				$("div.addrmsg").html(out['Error']);
				if (out['Error']=='Login session expired!'){
					window.location.reload(true);
				}
			}else{
				
			}
		}else if (out.hasOwnProperty('Status')){
			$("tr.addrrow.row" + idattr).remove();
			if ($("div.existingaddress > table > tbody > tr").length==0){
				$("div.existingaddress > table > tbody").prepend(
					'<tr class="addrrow nodata"><td class="addrusethis">No saved address found!</td></tr>'
				);	
			}

			addrusethis(out['addrslctid'],0);
			if (out.hasOwnProperty('courier')){
				$("div.popupcourier tbody").html(out['existcourier']);
				crrusethis(out['courier'],0,0);
			}
		}
	}, "json")/*.always(function(out) {
		console.log(out);
	})*/;
}
function lastCheck(){
	$("div.checkoutmsg").html('Please wait...');
	$("div.paybutton").hide();
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingcart.html", {
		cartmode:'lastcheck',
		addresschooseid:$("#addresschooseid").html(),
		ongkirchooseid:$("#ongkirchooseid").html(),
		vchid:$("#vchid").html(),
		paytypeid:$("#paymentchooseid").html()
	}, function( out ) {
		if (out.hasOwnProperty('Error')){
			if (typeof out['Error'] == 'string'){
				$("div.checkoutmsg").html(out['Error']);
				if (out['Error']=='Login session expired!' || 
				out['Error']=='Changing qty availability! Please wait for page reloading.'){
					setTimeout(function(){
						window.location.reload(true);
					}, 2000);
				}
			}else{
				
			}
			$("div.paybutton").show();
		}else if (out.hasOwnProperty('Status')){
			$("div.checkoutmsg").html('');
			openfancy($("input[name='searchpolo']").attr('rolepath') + "checkout.html?type=" + $("#paymentchooseid").html());
			
			// if (out.hasOwnProperty('totalpending')){
			// 	updateUserPending(out['totalpending']);
			// }
			// window.location.href = $("input[name='searchpolo']").attr('rolepath') + "checkout.html?id=" + 
			// 	out['cartid'];
		}
	}, "json")/*.always(function(out) {
		console.log(out);
	})*/;
}
function openfancy(urlsrc){
	var fancywidth = 40;
	var widthScreen = $( window ).width();
	if (widthScreen < 900){
		fancywidth = 80;
	}
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: fancywidth + '%',
		height		: '95%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: false,
		helpers   	: {
			overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox 
		},
		closeBtn	: false/*,
		beforeShow: function(){
			this.height = $('.fancybox-iframe').contents().find('html').height();
		}*/
	});
}
// Close 3DSecure dialog box
function closefancy() {
    $.fancybox.close();
    $("div.paybutton").show();
}
$(document).ready(function(){
	$("div.co-head").click(function (){
		$("div.co-promo,div.codeform").toggleClass( "active" );
	});
	/*select2*/
	$("select#addrlosid").select2({ width: '100%' });
	/*popupwin address*/
	$("div.addaddressbutton").find("span").click(function(){
		preparenewaddr();
		$("div.popupwin.popupaddress").addClass("active");
		$("div.newaddress").addClass("active");
	});
	$("div.changeaddressbutton").find("span").click(function(){
		$("div.popupwin.popupaddress").addClass("active");
		$("div.existingaddress").addClass("active");
	});
	$("div.popupwin.popupaddress").find("i.popupclose,div.addrcancel").click(function(){
		$("div.popupwin.popupaddress,div.newaddress,div.existingaddress").removeClass("active");
	});
	$("div.popupwin.popupcourier").find("i.popupclose,div.crrcancel").click(function(){
		$("div.popupwin.popupcourier").removeClass("active");
	});
	/*popupwin courier*/
	$("div.changecourierbutton").find("span").click(function(){
		$("div.popupwin.popupcourier").addClass("active");
	});
	$("div.popupwin.popupcourier").find("i.popupclose,div.addrcancel,td.addrusethis,td.courierestweight,td.couriercost").click(function(){
		$("div.popupwin.popupcourier").removeClass("active");
	});
	/*payment*/
	$("div.payment").click(function (){
		$("div.payment,div.paymentform").toggleClass( "active" );
	});
	
	$("div.paybutton").click(function (){
		
	});
});