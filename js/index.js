$(document).ready(function(){
	/* slide promo */
	var slidepromo = new Swiper('#slidepromo', {
		speed: 400,
		grabCursor: true,
		lazy: true,
		autoplay: {
			delay: 8000
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		loop: true
	});
	$("#slidepromo").hover(function(){
		slidepromo.autoplay.stop();
	}, function(){
		slidepromo.autoplay.start();
	});
//	window popup center
	if ($("div.pwcenter").length > 0){
		$("div.pwcancel,div.pwagree").click(function (){
			$("div.pwcenter").hide("fast");
		});
	}
});