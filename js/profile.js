function submitwinpassword(){
	$("div.msgwinpassword").html('Please wait...');
	$("button.submitwinpassword").remove();
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingprofile.html", {
		passoldlogin: $("#ppassword").val(),passlogin: $("#pnpassword").val(),
		passretypelogin: $("#pnrpassword").val(),mode:3
	}, function( out ) {
		if (out.hasOwnProperty('Error')){
			$("div.msgwinpassword").html(out['Error']);
			if (out['Error']=='Session expired. Please sign in again!'){
				setTimeout(window.location.reload(true),3000);
			}
		}else if (out.hasOwnProperty('Status')){
			$("div.msgwinpassword").html(out['Status']);
			window.location.reload(true);
		}
		$("div#winpassword > div.panel-footer").append(
			'<button type="submit" class="btn btn-warning submitwinpassword" '+
			'onclick="submitwinpassword(); return false;">Submit</button>'
		);
	}, "json")/*.always(function(out) {
		console.log( out );
	})*/;
}
function sendActivation(){
	if ($("div.alert.alert-danger").length > 0){
		$("div.alert.alert-danger").remove();
	}
	$("divp.emailver").html('Please wait...');
	$.post( $("input[name='searchpolo']").attr('rolepath') + "processingprofile.html", {
		mode:2
	}, function( out ) {
		if (out.hasOwnProperty('Error')){
			$("div.profInfo").prepend(
				'<div class="alert alert-danger alert-dismissible" role="alert">'+
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
					'<span aria-hidden="true">&times;</span></button>'+
					'<strong>Warning!</strong> '+out['Error']+
				'</div>'
			);
			if (out['Error']=='Session expired. Please sign in again!'){
				setTimeout(window.location.reload(true),3000);
			}
		}else if (out.hasOwnProperty('Status')){
			$("div.profInfo").prepend(
				'<div class="alert alert-success alert-dismissible" role="alert">'+
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
					'<span aria-hidden="true">&times;</span></button>'+
					'<strong>Well done!</strong> '+out['Status']+
				'</div>'
			);
		}
		$("divp.emailver").html(
			'<span class="label label-danger pointer" title="Send link to my email" '+
			'onclick="sendActivation();">Not Verified!</span>'
		);
	}, "json")/*.always(function(out) {
		console.log( out );
	})*/;
}
$(document).ready(function(){
	//auto upload foto
	$("#fotouploader").change(function(){
		$("div.msguploadfoto").html('Please wait...');
		$("input.btnfoto").remove();
		$.ajax({
			type: "POST",
			url: $("input[name='searchpolo']").attr('rolepath') + "processingprofile.html",
			contentType: false,
			cache: false,
			processData: false,
			data: new FormData($( "#khususfoto" )[0]),
			success: function( out ){
				if (out.hasOwnProperty('Error')){
					$("div.msguploadfoto").html(out['Error']);
					if (out['Error']=='Session expired. Please sign in again!'){
						setTimeout(window.location.reload(true),3000);
					}
				}else if (out.hasOwnProperty('Status')){
					$("div.paddingfoto > img,div.accountimage > img").css('background-image','url('+
						$("input[name='searchpolo']").attr('rolepath') +'images/foto/'+out['images']+
					')');
					$("div.msguploadfoto").html(out['Status']);
					setTimeout(function (){$("div.msguploadfoto").html('');},3000);
				}
				$("#fotouploader").val('');
				$("form#khususfoto").append(
					'<input class="btn btn-default btn-block btnfoto" type="button" value="Foto Uploader" '+
					'onclick="$(\'#fotouploader\').click();" />'
				);
			},
			error: function(jqXHR, textStatus, errorThrown){
				$("div.msguploadfoto").html(jqXHR);
				$("#fotouploader").val('');
				$("form#khususfoto").append(
					'<input class="btn btn-default btn-block btnfoto" type="button" value="Foto Uploader" '+
					'onclick="$(\'#fotouploader\').click();" />'
				);
			}
		});
		
	});
	$('.fancybox').fancybox({
//		width		: 'auto',
//		height		: 'auto',
		padding		: 0,
		autoSize	: true
	});
});