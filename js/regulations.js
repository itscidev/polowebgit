function openContent(idnya){
	if ($("div[roleid='" + idnya + "']").hasClass('active')==false){
		$("ol.regulationbreadcrumb").find('li.active').html(
			$("div[roleid='" + idnya + "']").html()
		);
		if ($("div.catChild.active").length > 0){
			$("div.catChild.active").removeClass('active');
		}
		$("div[roleid='" + idnya + "']").addClass('active');
		if ($("div.childregcont.active").length > 0){
			$("div.childregcont.finish").removeClass('finish');
			setTimeout(function(){
				$("div.childregcont.active").removeClass('active');
			}, 50);
			setTimeout(function(){
				$("div.c" + idnya).addClass('active');
				setTimeout(function(){
					$("div.c" + idnya).addClass('finish');
				}, 700);
			}, 1000);
		}else{
			$("div.c" + idnya).addClass('active');
			setTimeout(function(){
				$("div.c" + idnya).addClass('finish');
			}, 700);
		}
		var scrolltocrumb = $( "ol.regulationbreadcrumb" ).offset().top - 
			$( "nav.navbar.navbar-fixed-top" ).find('div.container').outerHeight() - 10;
		//console.log(scrolltocrumb);
		$("html, body").stop().animate({scrollTop:scrolltocrumb}, 500, 'swing');
	}
}
function clearContactSend(){
	$("#contactnama").val('');
	$("#contactemail").val('');
	$("#contactphone").val('');
	$("#contactinvoice").val('');
	$("#contactmessage").val('');
	$(".secprint").val('');
}
function contactSend(){
	//console.log('masuk');
	$("div.divcontactsend").html('Please wait..');
	$.post($("form#contactus").attr('action'),$("#contactus").serialize(),function( data ) {
		$(".secprint").attr("placeholder",data['secprint']);
		$(".sectoken").val(data['sectoken']);
		//console.log(data);
		$("div.divcontactsend").html(
			'<div class="contactsend" id="contactsend" onclick="contactSend(); return false;">SEND</div>'
		);
		if (data.hasOwnProperty('Error')){
			if ( typeof(data['Error']) != 'object' ){
				$("div.divcontactsend").prepend(data['Error']);
			}
			$(".secprint").val('');
		}else{
			$("div.divcontactsend").prepend('Thank for your inquiry. We will respond your email in 2x24 hours!');
			clearContactSend();
		}
	}, "json");
}
$(document).ready(function(){
	$("div.catChild").click(function (){
		if ($("ol.regulationbreadcrumb").css('cursor')=='pointer'){
			$("ol.regulationbreadcrumb").find("li.showonmobile").toggleClass("open");
			$("div.regulationcategory.hideonmobile").toggleClass("open");
		}
		var idchild = $(this).attr('roleid');
		openContent(idchild);
		/*if ($(this).hasClass('active')==false){
			if ($("div.catChild.active").length > 0){
				$("div.catChild.active").removeClass('active');
			}
			$(this).addClass('active');
			if ($("div.childregcont.active").length > 0){
				$("div.childregcont.active").removeClass('active');
				setTimeout(function(){
					$("div.c" + idchild).addClass('active');	
				}, 2000);
			}else{
				$("div.c" + idchild).addClass('active');
			}	
		}*/
	});
	/*select2*/
	$("select#contactsubject").select2({ width: '100%' });
	
	$("ol.regulationbreadcrumb").click(function(){
		if ($(this).css('cursor')=='pointer'){
			$("ol.regulationbreadcrumb").find("li.showonmobile").toggleClass("open");
			$("div.regulationcategory.hideonmobile").toggleClass("open");
		}
	});
	/*clear contact send*/
	clearContactSend();
	/* shop by brand */
	/*var slidepromo = new Swiper('#slidepromo', {
		speed: 400,
		grabCursor: true,
		lazy: true,
		autoplay: {
			delay: 8000
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		loop: true
	});*/
});