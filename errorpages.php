<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<style type="text/css">
	div.errortext{
		min-height: 50vh;
		font-size: 2em;
		padding-top: 3em;
		font-family: Cambria;
	}
	div.errortext > span{
		 font-size: 2.5em;
	}
	div.errortext1{
		font-size: 2em;
		text-align: center;
		font-family: Cambria;
	}
	div.errortext1 > span{
		 font-size: 2.5em;
	}
	@media (max-width: 680px) {
		div.errortext{
			min-height: 0vh;
			font-size: 1em;
			padding-top: 0;
			text-align: center;
		}
		div.errortext > span{
			 font-size: 1.5em;
		}
		div.errortext1{
			font-size: 1em;
		}
		div.errortext1 > span{
			 font-size: 1.5em;
		}
	}
	</style>
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->
	
	<?php echo $resultdisp;?>

<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
</body><!-- //Body -->
</html>