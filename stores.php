<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/o-stores.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->
<div class="row maincontent">
	<div class="col-xs-12 titlepage">Find A Store Near You</div>
	<div class="col-xs-12 subtitlepage">SEARCH IN INDONESIA</div>
	<div class="col-xs-12">
		<div class="col-md-5 col-sm-7 col-xs-12 divform">
			<form name="form1" id="form1" role="form" target="_self" method="post" 
			enctype="multipart/form-data" autocomplete="off">
			<input name="searchstore" type="text" value="<?php echo $searchstore;?>" 
			placeholder="Street Name/Town/Interest Places" class="inputsearch">
			<input name="submitsearch" type="submit" value="SEARCH" class="submitsearch">
			</form>
		</div>
	</div>
	<?php echo $result;?>
	<div class="col-xs-12">NEED HELP?</div>
	<div class="col-xs-12">Contact (Store and Company)</div>
</div>
	
<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
</body><!-- //Body -->
</html>