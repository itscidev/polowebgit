<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
$result = array();
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/regulations/')==0){
	$result['Error']='Ilegal access detected!';
}elseif (isset($_POST['contacttoken']) && $_POST['contacttoken']!=''){
	$tok1 = rand(0,99);
	$tok2 = rand(0,9);
	$result['secprint'] = $tok1.' + '.$tok2.' = ?';
	$result['sectoken'] = md5(($tok1+$tok2).'&T'.date("ymda"));
	
	if (isset($_POST['contactnama'])){$contactnama=trim($_POST['contactnama']);}else{$contactnama='';}
	if (isset($_POST['contactemail'])){$contactemail=trim($_POST['contactemail']);}else{$contactemail='';}
	if (isset($_POST['contactphone'])){$contactphone=trim($_POST['contactphone']);}else{$contactphone='';}
	if (isset($_POST['contactsubject'])){$contactsubject=intval($_POST['contactsubject']);}else{$contactsubject=0;}
	if (isset($_POST['contactinvoice'])){$contactinvoice=trim($_POST['contactinvoice']);}else{$contactinvoice='';}
	if (isset($_POST['contactmessage'])){$contactmessage=trim($_POST['contactmessage']);}else{$contactmessage='';}
	if (isset($_POST['contactsec'])){$contactsec=trim($_POST['contactsec']);}else{$contactsec='';}
	if (isset($_POST['contacttoken'])){$contacttoken=$_POST['contacttoken'];}else{$contacttoken='';}
	
	if ($contactnama==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill name field!';
	}
	if ($contactemail==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill email field!';
	}elseif (filter_var($contactemail, FILTER_VALIDATE_EMAIL)==false){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'] .= 'Wrong email field!';
	}
	if ($contactsubject==0){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please choose subject field!';
	}
	if ($contactmessage==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill message field!';
	}
	if ($contacttoken==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Token key not found!';
	}elseif ($contactsec==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill security field!';
	}elseif ($contacttoken!=md5($contactsec.'&T'.date("ymda"))){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Wrong security fill!';
	}
	if (!isset($result['Error'])){
		include_once 'cms/configuration/connect.php';
		$cekpurp = @mysqli_query($sqlcon,
			"select * from mail_smtp where ms_purpose='web'"
		);
		$cekpurp1 = 0;
		if ($cekpurp){$cekpurp1 = mysqli_num_rows($cekpurp);}
		if ($cekpurp1 != 1){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'] .= 'SMTP not found!';
		}
		$sender = mysqli_fetch_array($cekpurp);
		$host = $sender['ms_host'];
		$port = $sender['ms_port'];
		$secure = $sender['ms_secure'];
		$emailfr = $sender['ms_sendermail'];
		$emailpwd = $sender['ms_password'];
		$emailname = $sender['ms_sendername'];
		$arrsubject = array(
			1=>'Order Status',
			2=>'Order Changes',
			3=>'Returns',
			4=>'My Account Information',
			5=>'Shipping',
			6=>'Placing an Order',
			7=>'Payment and Billing',
			8=>'Promotions',
			9=>'Gift Certificates',
			10=>'Product Questions',
			11=>'Privacy Notice Questions',
			12=>'Product Warranties',
			13=>'International Inquiries',
			14=>'Other'
		);
		$subject = $arrsubject[$contactsubject];
		if (isset($sqlcon)){mysqli_close($sqlcon);}
	
		require 'PHPMailer/PHPMailerAutoload.php';
	
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		// Set PHPMailer to use the sendmail transport
		$mail->isSMTP();
		//$mail->isSendmail();
		$mail->Host = $host;
		$mail->Port = $port;
		$mail->SMTPSecure = $secure;
		$mail->SMTPAuth = true;
		$mail->Username = $emailfr;
		$mail->Password = $emailpwd;
		//Set who the message is to be sent from
		$mail->setFrom( $emailfr,$emailname );
		//Set an alternative reply-to address
		$mail->addReplyTo( $emailfr,$emailname );
		//Set an alternative BCC address
		$mail->addBCC( $emailfr,$emailname );
		//Set who the message is to be sent to
		$mail->addAddress($contactemail,$contactnama);
		//Set the subject line
		$mail->Subject = 'Polo website inquiry for "'.$subject.'" at '.date("d-m-Y H:i:s");
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$strhtml="<html>";
		$strhtml.="<head>";
		$strhtml.="</head>";
		$strhtml.="</body>";
		$strhtml.="Dear ".$contactnama."<br><br>";
		$strhtml.="Thank you for your inquiry. If needed, we will respon at least 2 x 24 hour.<br>";
		$strhtml.="Here is your data,<br><br>";
		$strhtml.="Name : ".$contactnama;
		$strhtml.="<br />Email : ".$contactemail;
		if ($contactphone!=''){$strhtml.="<br />Phone : ".$contactphone;}
		if ($contactinvoice!=''){$strhtml.="<br />Invoice No. : ".$contactinvoice;}
		if ($contactmessage!=''){$strhtml.="<br />Message : ".htmlentities($contactmessage);}
		$strhtml.="<br><br><br><br><br>Regards,<br>".$emailname;
		$strhtml.="</body>";
		$strhtml.="</html>";
		$mail->msgHTML($strhtml);
		//Replace the plain text body with one created manually
		$mail->AltBody = 'We are sorry, your email client is not support HTML format. Please open with other programs. Thank you.';
		//Attach an image file
		//	$mail->addAttachment('images/phpmailer_mini.png');
	
		//send the message, check for errors
		if (!$mail->send()) {
			//	    echo "Mailer Error: " . $mail->ErrorInfo;
			$result['Error'] = 'Can not send inquiry. '.$mail->ErrorInfo;
		} else {
			//	    echo "Message sent!";
			$result['Status'] = 'Thank for your inquiry. We will respond your email in 2x24 hours!.';
		}
		if (isset($sqlcon)){mysqli_close($sqlcon);}
	}
}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>