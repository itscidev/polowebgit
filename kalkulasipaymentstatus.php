<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/paymentstatus.php' )==0 || $memid==0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}

include_once 'cms/model/cart.php';

$errmsg = '';
$arrstatus = array('Waiting Payment','Canceled','Paid','Payment Failed','On Packaging','On Delivery','Delivered','Delivery Failed');
if (isset($_GET['idcart'])){
	$field = "cr_id,cr_pt_id,pt_name,cr_status,cr_create";
	$join = "inner join paymenttype on pt_id=cr_pt_id inner join member_address on mma_id=cr_mma_id";
	$where = "cr_id=".intval($_GET['idcart'])." and mma_mm_id=".$memid;
	$cart = cart_s($sqlcon,$field,$join,$where);
	if (is_array($cart) && $cart[0]>0){
		$cart1 = mysqli_fetch_assoc($cart[1]);
		if ($cart1['pt_name']=='Manual Transfer Bank' && $cart1['cr_status']==0){
			$paystat = 1;
		}else{
			$paystat = 2;
			$invoicestatus = $arrstatus[( $cart1['cr_status'] )];
		}
		if ($cart1['cr_status'] == 0){
			$exp = explode(' ',$cart1['cr_create']);
			$expd = explode('-',$exp[0]);
			$expt = explode(':',$exp[1]);
			$autoterminate = date("d M Y H:i:s",mktime($expt[0]+15,$expt[1],$expt[2],$expd[1],$expd[2],$expd[0]));
		}
	}else{
		$errmsg = $cart.' ID not found!';
	}
}else{
	$errmsg = 'ID not found!';
}

?>