<?php include_once 'kalkulasicheckout.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Meta-Tags -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- //Meta-Tags -->

	<!-- Bootstrap-CSS -->	   
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/bootstrap.css">
	<!-- Fonts -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fonts.css">
	<!-- //Fonts -->
	<!-- Font-Awesome-File-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/font-awesome.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>fonts/fontawesome-webfont.ttf">
	<!-- //Font-Awesome-File-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/checkout.css">
</head>
<!-- //Head -->
<body>
<div class="modal-content">
  <div id="modal-header" class="modal-header">
    <button type="button" class="close closefancy" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    <h4 class="modal-title">Order Confirmation</h4>
  </div>
  <div id="modal-body" class="modal-body">
  	<div class="well grandtotal"><span>Total</span>Rp <?php echo $grandtotalp;?></div>
    <div>
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#tabitem" aria-controls="tabitem" role="tab" data-toggle="tab">Item</a></li>
	    <li role="presentation"><a href="#tabreceiver" aria-controls="tabreceiver" role="tab" data-toggle="tab">Receiver</a></li>
	    <li role="presentation"><a href="#tabvoucher" aria-controls="tabvoucher" role="tab" data-toggle="tab">Voucher</a></li>
	    <li role="presentation"><a href="#tabnotes" aria-controls="tabnotes" role="tab" data-toggle="tab">Notes</a></li>
	  </ul>
	  <!-- Tab panes -->
	  <div class="tab-content panelitem">
	    <div role="tabpanel" class="tab-pane active" id="tabitem">
	    	<table class="table">
	    		<thead>
	    			<tr>
	    				<th>Item</th>
	    				<th>Qty</th>
	    			</tr>
	    		</thead>
	    		<tbody><?php echo $cartitem;?></tbody>
	    	</table>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="tabreceiver">
	    	<div class="row">
			<div class="col-xs-6">
				<span>Name</span><?php echo $recname;?>
				<span>Phone</span><?php echo $recphone;?>
			</div>
			<div class="col-xs-6">
				<span>Address</span><?php echo $recaddr;?>
			</div>
			<div class="col-xs-12"><span>Choosen Expedisi</span><?php echo $reccourier;?></div>
			</div>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="tabvoucher">
	    	<div class="row">
			<div class="col-xs-12"><span>Choosen Voucher</span><?php echo $voucherused;?></div>
			</div>
	    </div>
	    <div role="tabnotes" class="tab-pane" id="tabnotes">
	    	<div class="row">
			<div class="col-xs-12"><span>-</span></div>
			</div>
	    </div>
	  </div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading paytype"><img src="<?php echo $paymentlogo;?>"><span class="paytypename"><?php echo $paymentname;?></span></div>
		<div class="panel-body paymentpanel">
		<form id="paymentform">
			<input type="hidden" name="paytype" id="paytype" value="<?php echo intval($_GET['type']);?>">
			<textarea name="comment" id="comment" class="hide"></textarea><?php
if ($paytype == 1){?>
			<div class="creditcard">
				<p><i class="fa fa-lock"></i>Pembayaran aman dengan Midtrans</p>
				<div class="securedby"><img src="images/paymentlogo/secamex.png"><img src="images/paymentlogo/secjcb.png"><img src="images/paymentlogo/secvisa.png"><img src="images/paymentlogo/secmaster.png"><img src="images/paymentlogo/secnorton.png"></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label class="control-label" for="cardnumber">Card Number</label>
						<input name="cardnumber" type="text" class="form-control" id="cardnumber" placeholder="811 1111 1111 1114">
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group">
						<label class="control-label" for="cardexpired">Expired to</label>
						<input name="cardexpired" type="text" class="form-control" id="cardexpired" placeholder="MM / YYYY">
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group">
						<label class="control-label" for="cardcvv">CVV Number</label>
						<input name="cardcvv" type="text" class="form-control" id="cardcvv" placeholder="...">
					</div>
				</div>
			</div><?php
}elseif ($paytype == 2){?>
			<div class="manualtransfer">
				This payment method needs you to do manual transfer to our bank account within 15 hour or your invoice will automatic terminated.<br>Bank account information will display after you click "Process Payment" button.<br><br>Thank you.
			</div><?php
}?>
		</form>
		</div>
	</div>
	<br><br><br><br>
  </div>
  <div id="modal-footer" class="modal-footer">
  	<span class="procMsg"></span>
    <button type="button" class="btn btn-default closefancy" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" id="procPayment">Process Payment</button>
  </div>
</div>
	<!-- Default-JavaScript -->
	<script src="<?php echo $path;?>js/jquery-2.2.3.js"></script>
	<!-- Bootstrap-JavaScript -->
	<script src="<?php echo $path;?>js/bootstrap.js"></script>
	<script src="<?php echo $path;?>js/checkout.js"></script>
	<?php echo $addjs;?>
</body><!-- //Body -->
</html>