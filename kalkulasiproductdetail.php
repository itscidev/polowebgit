<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/productdetail.php' ) == 0) {
	header ( 'location:'.$pathcook.'index.html' );
	exit ();
}

$errMsg = '';
$mapcolor = $mapstyle = $mapsize = array();
$arrimg = $arrvari = array();
$prname = $prtagline = $prdesc = $prprice = $prinfo = $prcare = $relprod = '';
if (isset($_GET['id']) && $_GET['id']!=''){
	$expid = explode('/', $_GET['id']);
	$id = $expid[( count($expid)-1 )];
	$strqry = "select pr.pr_id,pr.pr_ca_id,pr.pr_name,pr.pr_tagline,pr.pr_desc,pr.pr_info,pr.pr_care,".
		"pr.pr_price ".
	"from product pr ".
	"inner join product_variety on pv_pr_id=pr_id ".
	"where pr_status=1 and pv_status=1 and ".
		"lower(pr_name)='".mysqli_real_escape_string($sqlcon,strtolower($id))."' ".
	"group by pr_id limit 1";
	$prod = mysqli_fetch_assoc(mysqli_query($sqlcon,$strqry));
	if (isset($prod['pr_id']) && $prod['pr_id']>0){
		$prname = $prod['pr_name'];
		$prtagline = $prod['pr_tagline'];
		if ($prtagline != ''){
			$prtagline = '<span class="brand">'.$prtagline.'</span>';
		}
		$prdesc = $prod['pr_desc'];
		$prinfo = $prod['pr_info'];
		$prcare = $prod['pr_care'];
		//get product image
		$striqry = "select pi_image,st_id,ci_id ".
			"from product_image pi ".
			"left outer join style on st_id=pi_st_id ".
			"left outer join color_item on ci_id=pi_ci_id ".
			"where pi_status=1 and pi_pr_id=".$prod['pr_id']." ".
			"order by pi_sequence,st_sequence,ci_sequence";
		//echo $striqry."<br />";
		$prodimg = mysqli_query($sqlcon,$striqry);
		$prodimg1 = 0;
		if ($prodimg){$prodimg1 = mysqli_num_rows($prodimg);}
		if ($prodimg1 > 0){while ($prodimg1 = mysqli_fetch_assoc($prodimg)){
			if (file_exists("images/productitem/".$prodimg1['pi_image'])){
				$idst = intval($prodimg1['st_id']);
				$idci = intval($prodimg1['ci_id']);
				if (!isset($arrimg[$idst][$idci])){$arrimg[$idst][$idci] = array();}
				array_push($arrimg[$idst][$idci],$prodimg1['pi_image']);
			}
			
		}}
		//get variety
		$vari = mysqli_query($sqlcon,
			"select pv.pv_id,pv.pv_name,pv.pv_qty,pv.pv_price,st_id,st_name,sz_id,sz_name,ci_id,ci_image,cl_name ".
			"from product_variety pv ".
			"left outer join style on st_id=pv_st_id ".
			"left outer join size on sz_id=pv_sz_id ".
			"left outer join color_item on ci_id=pv_ci_id ".
			"left outer join color on cl_id=ci_cl_id ".
			"where pv_status=1 and pv_pr_id=".$prod['pr_id']." ".
			"order by st_sequence,sz_sequence,ci_sequence"
		);
		while ($vari1 = mysqli_fetch_assoc($vari)){
			$idst = intval($vari1['st_id']);
			if ($idst!=0 && !(isset($mapstyle[$idst]))){
				$mapstyle[$idst] = $vari1['st_name'];
			}
			$idsz = intval($vari1['sz_id']);
			if ($idsz!=0 && !(isset($mapsize[$idsz]))){
				$mapsize[$idsz] = $vari1['sz_name'];
			}
			$idci = intval($vari1['ci_id']);
			if ($idci!=0 && !(isset($mapcolor[$idci]))){
				$mapcolor[$idci] = array($vari1['ci_image'],$vari1['cl_name']);
			}
			$arrvari[$idst][$idsz][$idci]['I'] = intval($vari1['pv_id']);
			$arrvari[$idst][$idsz][$idci]['Q'] = intval($vari1['pv_qty']);
			$arrvari[$idst][$idsz][$idci]['P'] = intval($vari1['pv_price']);
			$arrvari[$idst][$idsz][$idci]['PP'] = number_format($vari1['pv_price'],0,',','.');
			if ($prprice == ''){$prprice = 'IDR '.number_format($vari1['pv_price'],0,',','.').',-';}
		}
		//get related product
		$rltd = mysqli_query($sqlcon,
			"select pr_id,pr_name,pi_image,min(pv_price) pv_price ".
			"from product_related ".
			"inner join product on pr_id=pl_rel_pr_id ".
			"inner join product_image on pi_id=pr_main_pi_id ".
			"inner join product_variety on pv_pr_id=pr_id and pv_qty>0 and pv_status=1 ".
			"where pl_pr_id=".$prod['pr_id']." and pr_status=1 and pi_status=1 ".
			"group by 1,2,3 limit 3"
		);
		$rltd1 = mysqli_num_rows($rltd);
		
		if ($rltd1 > 0){while ($rltd2 = mysqli_fetch_assoc($rltd)){
			if ($rltd1 == 3){
				$colclass = 'col-md-offset-1 col-md-3 ';
			}elseif ($rltd1 == 2){
				$colclass = 'col-md-offset-2 col-md-4 ';
			}else{
				$colclass = 'col-md-offset-3 col-md-3 ';
			}
			$relprod .= '<div class="'.$colclass.'col-xs-12">'.
				'<div>'.
 					'<img src="'.$path.'images/productitem/'.$rltd2['pi_image'].'" class="img-responsive" '.
 					'alt="'.htmlentities($rltd2['pr_name']).'"/>'.
 				'</div>'.
 				'<p class="title">'.htmlentities($rltd2['pr_name']).'</p>'.
 				'<p class="price">IDR '.number_format($rltd2['pv_price'],0,',','.').',-</p>'.
 			'</div>';
		}}
		if ($relprod != ''){
			$relprod = '<div class="containerdiff relatedproduct">'.
				'<div class="row">'.
					'<h5>Related Products</h5>'.$relprod.
				'</div>'.
			'</div>';
		}
	}else{
		$errMsg = 'Product not found!';
	}
}else{
	$errMsg = 'Are you lost or something?<br />Click here to '.
		'<a href="'.$path.'" title="Polo Indonesia Home Page">home page</a>';
}
//echo "<pre>";print_r($arrimg);echo "</pre>";
//echo "<pre>";print_r($arrvari);echo "</pre>";
//temp DB
// if (isset($_GET['id']) && $_GET['id']!=''){
// 	$expid = explode('/', $_GET['id']);
// 	$id = $expid[( count($expid)-1 )];
// }else{
// 	$id = 'Wangky';
// }
/* var optprod = {
	blue:["d1.jpg", "d2.jpg", "d3.jpg", "d4.jpg"],
	red:["e1.jpg", "e2.jpg", "e3.jpg", "e4.jpg"]
};
//thumbnail image
<div class="swiper-slide"><img src="<?php echo $path;?>images/productitem/d1.jpg"/></div>
//large image
<div class="swiper-slide"><img src="<?php echo $path;?>images/productitem/d1.jpg"/></div>
//color option
<div class="det_nav1">
	<h4>Select colour :</h4>
	<div class="sky-form col col-4">
		<img dataprod="blue" class="multiopt active" alt="blue" src="<?php echo $path;?>images/productcolor/c-blue-orange.jpg">
		<img dataprod="red" class="multiopt" alt="black" src="<?php echo $path;?>images/productcolor/c-grey-purple.jpg">
		<img dataprod="blue" class="multiopt" alt="red" src="<?php echo $path;?>images/productcolor/c-pink-blue.jpg">
		<img dataprod="red" class="multiopt" alt="white" src="<?php echo $path;?>images/productcolor/c-yellow-blue.jpg">
	</div>
</div>*/

// $arrdb = array(
// 	'Wangky' => array(
// 		'opt' => 'var optprod = {yellow:["WANGKY-yellow.jpg"],white:["WANGKY-white.jpg"],tosca:["WANGKY-toscamuda.jpg"],orange:["WANGKY-orange.jpg"],merah:["WANGKY-merahstg.jpg"],birumuda:["WANGKY-birumuda.jpg"],navy:["WANGKY-birunavy.jpg"],biru:["WANGKY-biru.jpg"]};',
// 		'img' => '<div class="swiper-slide"><img src="'.$path.'images/productitem/WANGKY-yellow.jpg"/></div>',
// 		'color' => '<div class="det_nav1">'.
// 	'<h4>Select colour :</h4>'.
// 	'<div class="sky-form col col-4">'.
// 		'<img dataprod="yellow" class="multiopt active" alt="blue" src="'.$path.'images/productcolor/c-yellow-blue.jpg">'.
// 		'<img dataprod="white" class="multiopt" alt="black" src="'.$path.'images/productcolor/c-grey-purple.jpg">'.
// 		'<img dataprod="tosca" class="multiopt" alt="red" src="'.$path.'images/productcolor/c-blue-grey.jpg">'.
// 		'<img dataprod="orange" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-orange-blue.jpg">'.
// 		'<img dataprod="merah" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-pink-black.jpg">'.
// 		'<img dataprod="birumuda" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-blue-purple.jpg">'.
// 		'<img dataprod="navy" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-blue-red.jpg">'.
// 		'<img dataprod="biru" class="multiopt" alt="white" src="'.$path.'images/productcolor/c-blue-yellow.jpg">'.
// 	'</div>'.
// '</div>'
// 	),
// 	'Flag Edition Brasil' => array(
// 		'opt' => 'var optprod = {brasil:["FlagEdition-Brasil.jpg"]};',
// 		'img' => '<div class="swiper-slide"><img src="'.$path.'images/productitem/FlagEdition-Brasil.jpg"/></div>',
// 		'color' => ''
// 	),
// 	'Flag Edition England' => array(
// 		'opt' => 'var optprod = {england:["FlagEdition-England.jpg"]};',
// 		'img' => '<div class="swiper-slide"><img src="'.$path.'images/productitem/FlagEdition-England.jpg"/></div>',
// 		'color' => ''
// 	),
// 	'Flag Edition France' => array(
// 		'opt' => 'var optprod = {france:["FlagEdition-France.jpg"]};',
// 		'img' => '<div class="swiper-slide"><img src="'.$path.'images/productitem/FlagEdition-France.jpg"/></div>',
// 		'color' => ''
// 	)
// );

$styleoption = $sizeoption = $coloroption = '';
$urutan = $idst = 0;
if (count($mapstyle) > 0){foreach ($mapstyle as $index => $content){
	$active = '';
	if ($urutan == 0){$active = ' active';$idst = $index;}
	$styleoption .= '<div class="styleopt multiopt'.$active.'" dataattr="'.$index.'">'.$content.'</div>';
	$urutan++;
}}
if ($styleoption != ''){
	$styleoption = '<div class="det_nav1">'.
		'<h4>Select Style :</h4>'.
		'<div class="sky-form col col-4">'.$styleoption.'</div>'.
	'</div>';
}
$urutan = 0;
if (count($mapsize) > 0){foreach ($mapsize as $index => $content){
	$active = '';
	if ($urutan == 0){$active = ' active';}
	$sizeoption .= '<div class="sizeopt multiopt'.$active.'" dataattr="'.$index.'">'.$content.'</div>';
	$urutan++;
}}
if ($sizeoption != ''){
	$sizeoption = '<div class="det_nav1">'.
		'<h4>Select Size :</h4>'.
		'<div class="sky-form col col-4">'.$sizeoption.'</div>'.
	'</div>';
}
$urutan = $idci = 0;
if (count($mapcolor) > 0){foreach ($mapcolor as $index => $content){
	$active = '';
	if ($urutan == 0){$active = ' active';$idci = $index;}
	$coloroption .= '<img class="coloropt multiopt'.$active.'" alt="'.$content[1].'" '.
		'src="'.$path.'images/productcolor/'.$content[0].'" dataattr="'.$index.'">';
	$urutan++;
}}
if ($coloroption != ''){
	$coloroption = '<div class="det_nav1">'.
		'<h4>Select Colour :</h4>'.
		'<div class="sky-form col col-4">'.$coloroption.'</div>'.
	'</div>';
}

$thumbimg = '';
//$optprod = $arrdb[$id]['opt'];
//$thumbimg = $arrdb[$id]['img'];
// $coloroption = $arrdb[$id]['color'];
if (isset($arrimg[$idst][$idci]) && count($arrimg[$idst][$idci])>0){foreach ($arrimg[$idst][$idci] as $imgsrc){
	$thumbimg .= '<div class="swiper-slide"><img src="'.$path.'images/productitem/'.$imgsrc.'"/></div>';
}}
$normalimg = $thumbimg;

$optprod = 'var optprod = '.json_encode($arrimg).';var proddet='.json_encode($arrvari).';';
?>