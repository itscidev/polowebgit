<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/o-index.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<!-- popup window center -->
	<div class="pwcenter" style="display: none;">
		<div>
			<img alt="potongan harga" src="<?php echo $path;?>images/icon/polo.jpg">
			<p>Jadilah yang pertama tahu penawaran terbaik POLO dan dapatkan</p>
			<p>POTONGAN Rp 75.000</p>
			<p>untuk order berikutnya</p>
		</div>
		<div>
			<div class="pwcancel">
				<div>Tidak,<br />Saya tidak tertarik<i class="fa fa-angle-right" aria-hidden="true"></i></div>
			</div>
			<div class="pwagree">
				<div>Ya,<br />Beri tahu saya<i class="fa fa-angle-right" aria-hidden="true"></i></div>
			</div>
		</div>
	</div>
	<!-- //popup window center -->
	
	<?php echo $headerslide;/*?>
	<!-- Header-Slider -->
	<div class="w3slideraits">
		<div class="fluid_dg_wrap fluid_dg_emboss pattern_1 fluid_dg_white_skin" id="fluid_dg_wrap_4">
			<div data-thumb="<?php echo $path;?>images/WEB-IMAGE0-small.jpg" data-src="<?php echo $path;?>images/WEB-IMAGE0.jpg"></div>
			<div data-thumb="<?php echo $path;?>images/WEB-IMAGE1-small.jpg" data-src="<?php echo $path;?>images/WEB-IMAGE1.jpg"></div>
			<div data-thumb="<?php echo $path;?>images/WEB-IMAGE3-small.jpg" data-src="<?php echo $path;?>images/WEB-IMAGE2.jpg"></div>
		</div>
	</div>
	<!-- //Header-Slider -->*/?>
	

	<!-- Model-Slider -->
	<!-- <div class="agilemodel-slider">
		<div id='film_roll_1'>
			<div><img src="images/model_slide-1.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-2.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-3.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-4.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-5.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-6.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-7.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-8.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-9.jpg" alt="Groovy Apparel"></div>
			<div><img src="images/model_slide-10.jpg" alt="Groovy Apparel"></div>
		</div>
	</div> -->
	<!-- //Model-Slider -->



	<!-- Latest-Arrivals -->
	<!-- <div class="wthreehome-latest">
		<div class="container">

			<div class="wthreehome-latest-grids">
				<div class="col-md-6 wthreehome-latest-grid wthreehome-latest-grid1">
					<div class="grid">
						<figure class="effect-apollo">
							<img src="images/home-latest-1.jpg" alt="Groovy Apparel">
							<figcaption></figcaption>
						</figure>
					</div>
					<h4>DENIM TOPS</h4>
					<h5>Lorem Ipsum Dolor Site Amet</h5>
					<h6><a href="womens.html">Shop Now</a></h6>
				</div>
				<div class="col-md-6 wthreehome-latest-grid wthreehome-latest-grid2">
					<div class="grid">
						<figure class="effect-apollo">
							<img src="images/home-latest-2.jpg" alt="Groovy Apparel">
							<figcaption></figcaption>
						</figure>
					</div>
					<h4>LEATHER JACKETS</h4>
					<h5>Lorem Ipsum Dolor Site Amet</h5>
					<h6><a href="womens.html">Shop Now</a></h6>
				</div>
				<div class="col-md-6 wthreehome-latest-grid wthreehome-latest-grid3">
					<div class="grid">
						<figure class="effect-apollo">
							<img src="images/home-latest-3.jpg" alt="Groovy Apparel">
							<figcaption></figcaption>
						</figure>
					</div>
					<h4>WATCHES</h4>
					<h5>Lorem Ipsum Dolor Site Amet</h5>
					<h6><a href="womens_accessories.html">Shop Now</a></h6>
				</div>
				<div class="col-md-6 wthreehome-latest-grid wthreehome-latest-grid4">
					<div class="grid">
						<figure class="effect-apollo">
							<img src="images/home-latest-4.jpg" alt="Groovy Apparel">
							<figcaption></figcaption>
						</figure>
					</div>
					<h4>BEACH WEAR</h4>
					<h5>Lorem Ipsum Dolor Site Amet</h5>
					<h6><a href="mens.html">Shop Now</a></h6>
				</div>
			</div>
			<div class="clearfix"></div>

		</div>
	</div> -->
	<!-- //Latest-Arrivals -->

	<!-- Winter-Collection -->
	<!-- <div class="wthreewinter-coll">
		<div class="container">
			<h1>BRACE YOURSELVES! WINTER IS COMING...</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			<div class="wthreeshop-a">
				<a href="womens.html">SHOP WINTER COLLECTION</a>
			</div>
		</div>
	</div> -->
	<!-- //Winter-Collection -->

	<!-- Denim-Collection -->
	<!-- <div class="wthreedenim-coll">
		<div class="style-grids">
			<div class="col-md-6 style-grid style-grid-1">
				<img src="images/style-1.jpg" alt="Groovy Apparel">
				<div class="style-grid-1-text">
					<h3>DENIM JEANS</h3>
					<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur.</p>
					<div class="wthreeshop-a">
						<a href="womens.html">SHOP DENIM COLLECTION</a>
					</div>
				</div>
			</div>
			<div class="col-md-6 style-grid style-grid-2">
				<div class="style-image-1">
					<img src="images/style-2.jpg" alt="Groovy Apparel">
					<div class="style-grid-2-text">
						<h3>WATER REPELLENT</h3>
						<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.</p>
					</div>
				</div>
				<div class="style-image-2">
					<img src="images/style-3.jpg" alt="Groovy Apparel">
					<div class="style-grid-2-text">
						<h3>STITCHED TO PERFECTION</h3>
						<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div> -->
	<!-- //Denim-Collection -->

	<!-- Clearance-Sale -->
	<!-- <div class="aitsclearance-sales">
		<div class="aitsclearance-sales-text">
			<h2>ALL CLEARANCE ITEMS</h2>
			<h4>BUY ONE AND GET ONE FREE</h4>
			<h5>50% OFF <small>On selected products<sup>*</sup></small></h5>
			<div class="wthreeshop-a">
				<a href="mens.html">SHOP NOW</a>
			</div>
		</div>
	</div> -->
	<!-- //Clearance-Sale -->

	<!-- Video -->
	<?php echo $videosection;/*?>
	<div class="row videosection">
		<div class="col-sm-offset-1 col-sm-7 col-xs-12">
			<video width="90%" height="auto" controls>
				<source src="<?php echo $path;?>images/polo.mp4" type="video/mp4">
				Your browser does not support the video tag.
			</video> 
		</div>
		<div class="col-sm-3 col-xs-12">
			<a href="<?php echo $path;?>details/Women/Clothing/Polo Shirts/Wangky" title="New In">
				<img alt="new in" src="<?php echo $path;?>images/d1.jpg">
				<p><i class="fa fa-angle-double-right" aria-hidden="true"></i>NEW IN</p>
			</a>
		</div>
	</div>*/?>
	<!-- //Video -->

	<!-- Quick Menu -->
	<?php echo $quickmenu;/*?>
	<div class="agileshopping">
		<div class="container">

			<div class="agileshoppinggrids">
				<div class="col-md-12 agileshoppinggrid agileshoppinggrid1">
					<h3>CHOOSE THE BEST FOR YOU</h3>
					<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a 
					piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard 
					McClintock.</p>
				</div>
				<div class="col-md-4 agileshoppinggrid agileshoppinggrid1">
					<div class="grid">
						<figure class="effect-apollo">
							<img src="<?php echo $path;?>images/polo_men.jpg" alt="Groovy Apparel">
							<figcaption></figcaption>
						</figure>
					</div>
					<!-- <div class="wthreeshop-a">
						<a href="mens.html">SHOP MEN</a>
					</div> -->
					<!-- <h4>WATCHES</h4>
					<h5>Lorem Ipsum Dolor Site Amet</h5>
					<h6><a href="womens_accessories.html">Shop Now</a></h6> -->
				</div>
				<div class="col-md-4 agileshoppinggrid agileshoppinggrid2">
					<div class="grid">
						<figure class="effect-apollo">
							<img src="<?php echo $path;?>images/polo_women.jpg" alt="Groovy Apparel">
							<figcaption></figcaption>
						</figure>
					</div>
					<!-- <div class="wthreeshop-a">
						<a href="mens.html">SHOP MEN</a>
					</div> -->
					<!-- <h4>WATCHES</h4>
					<h5>Lorem Ipsum Dolor Site Amet</h5>
					<h6><a href="womens_accessories.html">Shop Now</a></h6> -->
				</div>
				<div class="col-md-4 agileshoppinggrid agileshoppinggrid2">
					<div class="grid">
						<figure class="effect-apollo">
							<img src="<?php echo $path;?>images/polo_kid.jpg" alt="Groovy Apparel">
							<figcaption></figcaption>
						</figure>
					</div>
					<!-- <div class="wthreeshop-a">
						<a href="mens.html">SHOP MEN</a>
					</div> -->
					<!-- <h4>WATCHES</h4>
					<h5>Lorem Ipsum Dolor Site Amet</h5>
					<h6><a href="womens_accessories.html">Shop Now</a></h6> -->
				</div>
				<!-- <div class="clearfix"></div> -->
			</div>

			<div class="agileshoppinggrids-bottom">
				<!-- <h3>CHOOSE THE BEST FOR YOU</h3>
				<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock.</p> -->
				<div class="agileshoppinggrids-bottom-grids">
					<div class="col-md-4 agileshoppinggrids-bottom-grid agileshoppinggrids-bottom-grid3">
						<div class="wthreeshop-a">
							<a href="<?php echo $path;?>women.html">SHOP MENS</a>
						</div>
					</div>
					<div class="col-md-4 agileshoppinggrids-bottom-grid agileshoppinggrids-bottom-grid3">
						<div class="wthreeshop-a">
							<a href="<?php echo $path;?>women.html">SHOP WOMENS</a>
						</div>
					</div>
					<div class="col-md-4 agileshoppinggrids-bottom-grid agileshoppinggrids-bottom-grid3">
						<div class="wthreeshop-a">
							<a href="<?php echo $path;?>women.html">SHOP KIDS</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

		</div>
	</div>*/?>
	<!-- //Quick Menu -->

	<!-- Seasons Trend -->
	<?php echo $seasonsstyle;?><!-- 
	<div class="col-xs-12 seasonstrend">
		<h2>
			<i>Office Style from POLO</i>
		</h2>
		<div class="col-md-12 agileshopping">
			<div class="agileshoppinggrid agileshoppinggrid1">
				<div class="grid">
					<figure class="effect-apollo" style="border:none;">
						<img src="<?php //echo $path;?>images/styleseason1.jpg" alt="Current trend season style"/>
					</figure>
				</div>
			</div>
		</div>
		<div id="explorenow">
			<b><i class="fa fa-angle-double-right"></i> EXPLORE NOW</b>
		</div>
	</div> -->
	<!-- //Seasons Trend -->

	<!-- Newsletter -->
	<!-- <div class="agileinfonewsl">

		<h3>SIGN UP FOR NEWSLETTER</h3>
		<p>Subscribe to us to get highest-level access to our new styles and trends</p>
		<div class="wthreeshop-a">
			<a class="popup-with-zoom-anim" href="#small-dialog3">SUBSCRIBE</a>
		</div>

			Popup-Box
			<div id="popup2">
				<div id="small-dialog3" class="mfp-hide agileinfo">
					<div class="pop_up">
						<h4>SUBSCRIBE</h4>
						<form action="#" method="post">
							<input class="email aitsw3ls" type="email" placeholder="Email" required="">
							<input type="submit" class="submit" value="SUBSCRIBE">
						</form>
					</div>
				</div>
			</div>
			//Popup-Box

	</div> -->
	<!-- //Newsletter -->

<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
	<script src="<?php echo $path;?>js/index.js"></script>
</body><!-- //Body -->
</html>