<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/mystyleproduct.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style1.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/etalage.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/o-productdetail.css">
</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header --><?php 
if ($errMsg == ''){?>
<!-- content -->
<div class="container">
<div class="women_main">
	<!-- start content -->
	<div class="row single">
		<div class="col-md-12 det">
			<div class="single_left">
				<div class="col-md-5">
					<div class="swiper-container Thumbs">
						<div class="swiper-wrapper">
							<?php echo $thumbimg;?><!--  
							<div class="swiper-slide"><img src="<?php //echo $path;?>images/productitem/d1.jpg"/></div>
							<div class="swiper-slide"><img src="<?php //echo $path;?>images/productitem/d2.jpg"/></div>
							<div class="swiper-slide"><img src="<?php //echo $path;?>images/productitem/d3.jpg"/></div>
							<div class="swiper-slide"><img src="<?php //echo $path;?>images/productitem/d4.jpg"/></div> -->
						</div>
					</div>
					<div class="swiper-container Gallery">
						<div class="swiper-wrapper">
							<?php echo $normalimg;?><!--  
							<div class="swiper-slide"><img src="<?php //echo $path;?>images/productitem/d1.jpg"/></div>
							<div class="swiper-slide"><img src="<?php //echo $path;?>images/productitem/d2.jpg"/></div>
							<div class="swiper-slide"><img src="<?php //echo $path;?>images/productitem/d3.jpg"/></div>
							<div class="swiper-slide"><img src="<?php //echo $path;?>images/productitem/d4.jpg"/></div> -->
						</div>
						<!-- Add Arrows -->
					    <div class="swiper-button-next"></div>
					    <div class="swiper-button-prev"></div>
					</div>
					<i class="fa fa-search-plus zoombutton"></i>
					<div class="productmagnifier"></div>
					<div class="clearfix"></div>		
				</div>
				<div class="col-md-7">
					<h3><?php echo $prname;?></h3>
					<?php echo $prtagline;?>
					<p><?php echo $prdesc;?></p>
					<p class="price"><?php echo $prprice;?></p>
					<!-- <div class="det_nav1">
						<h4>Select style :</h4>
						<div class="sky-form col col-4">
							<div class="multiopt active">Custom Fit</div>
							<div class="multiopt">Regular Fit</div>
							<div class="multiopt">Slim Fit</div>
						</div>
					</div>
					<div class="det_nav1">
						<h4>Select size :</h4>
						<div class="sky-form col col-4">
							<div class="multiopt active">S</div>
							<div class="multiopt">M</div>
							<div class="multiopt">L</div>
							<div class="multiopt">XL</div>
						</div>
					</div> -->
					<?php echo $styleoption.$sizeoption.$coloroption;?>
					<!--  
					<div class="det_nav1">
						<h4>Select colour :</h4>
						<div class="sky-form col col-4">
							<img dataprod="blue" class="multiopt active" alt="blue" src="<?php //echo $path;?>images/productcolor/c-blue-orange.jpg">
							<img dataprod="red" class="multiopt" alt="black" src="<?php //echo $path;?>images/productcolor/c-grey-purple.jpg">
							<img dataprod="blue" class="multiopt" alt="red" src="<?php //echo $path;?>images/productcolor/c-pink-blue.jpg">
							<img dataprod="red" class="multiopt" alt="white" src="<?php //echo $path;?>images/productcolor/c-yellow-blue.jpg">
						</div>
					</div> -->
					<div class="btn_form">
						<div class="errMsg"></div>
						<form action="#" method="post"> 
						<a href="#" class="detbutton addtocartbut" onclick="cartadditem(); return false;">Add to Cart</a>
						<a href="#" class="detbutton sis" onclick="togglePopUp(); return false;">Search in Store</a>
						</form>
					</div>
					<!-- <form action="#" method="post">
						<input type="hidden" name="cmd" value="_cart">
						<input type="hidden" name="add" value="1"> 
						<input type="hidden" name="w3l_item" value="Striped Top "> 
						<input type="hidden" name="amount" value="110.00"> 
						<button type="submit" class="w3l-cart pw3l-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
						<span class="w3-agile-line"> </span>
						<a href="#" data-toggle="modal" data-target="#myModal1"></a>
					</form>
					<a href="#"><span>Search in Store</span></a> -->
					
				</div>
				<div class="clearfix"></div>
			</div>
			
		</div><!-- 	
	<div class="col-md-3">
	  <div class="w_sidebar">
		<div class="w_nav1">
			<h4>All</h4>
			<ul>
				<li><a href="women.html">women</a></li>
				<li><a href="#">new arrivals</a></li>
				<li><a href="#">trends</a></li>
				<li><a href="#">boys</a></li>
				<li><a href="#">girls</a></li>
				<li><a href="#">sale</a></li>
			</ul>	
		</div>
		<h3>filter by</h3>
		<section  class="sky-form">
					<h4>catogories</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>kurtas</label>
							</div>
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>kutis</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>churidar kurta</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>salwar</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>printed sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>	
							</div>
						</div>
		</section>
		<section  class="sky-form">
					<h4>brand</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
							</div>
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>vishud</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>amari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>																								
							</div>
						</div>
		</section>
		<section class="sky-form">
			<h4>colour</h4>
			<ul class="w_nav2">
				<li><a class="color1" href="#"></a></li>
				<li><a class="color2" href="#"></a></li>
				<li><a class="color3" href="#"></a></li>
				<li><a class="color4" href="#"></a></li>
				<li><a class="color5" href="#"></a></li>
				<li><a class="color6" href="#"></a></li>
				<li><a class="color7" href="#"></a></li>
				<li><a class="color8" href="#"></a></li>
				<li><a class="color9" href="#"></a></li>
				<li><a class="color10" href="#"></a></li>
				<li><a class="color12" href="#"></a></li>
				<li><a class="color13" href="#"></a></li>
				<li><a class="color14" href="#"></a></li>
				<li><a class="color15" href="#"></a></li>
				<li><a class="color5" href="#"></a></li>
				<li><a class="color6" href="#"></a></li>
				<li><a class="color7" href="#"></a></li>
				<li><a class="color8" href="#"></a></li>
				<li><a class="color9" href="#"></a></li>
				<li><a class="color10" href="#"></a></li>
			</ul>
		</section>
		<section class="sky-form">
						<h4>discount</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="radio"><input type="radio" name="radio" checked=""><i></i>60 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>50 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>40 % and above</label>
							</div>
							<div class="col col-4">
								<label class="radio"><input type="radio" name="radio"><i></i>30 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>20 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>10 % and above</label>
							</div>
						</div>						
		</section>
	</div>
   </div> -->
		<div class="clearfix"></div>		
	</div>
	


	
</div>
</div>

<div class="containerdiff productinformation">
	<div class="row">
		<div class="col-sm-5 col-xs-12">
			<h4>Product Information</h4>
			<?php echo $prinfo;?><!-- 
			<p>COMPOSITION<br />EXTERIOR<br />62% Viscose<br />35% Polyamide<br />3% Elastane</p> -->
		</div>
		<div class="col-sm-7 col-xs-12">
			<h4>Care</h4>
			<?php echo $prcare;?><!-- 	
			<ul style="list-style-type: none;">
			<li><img class="prodcare" alt="" src="<?php //echo $path;?>images/productcare/icon-info-product-01.jpg">Machine wash up to 30C/86F gantle cycle</li>
			<li><img class="prodcare" alt="" src="<?php //echo $path;?>images/productcare/icon-info-product-02.jpg">Iron up to 110C/230F</li>
			<li><img class="prodcare" alt="" src="<?php //echo $path;?>images/productcare/icon-info-product-03.jpg">Do not tuble dry</li>
			<li><img class="prodcare" alt="" src="<?php //echo $path;?>images/productcare/icon-info-product-04.jpg">Do not bleach, dry clean tetrachlorethylene</li>
			</ul> -->
		</div>
	</div><!-- 
	<ul class="nav nav-tabs">
	<li class="hideonmobile active">
		<a data-toggle="tab" href="#productinformation">Product Information</a>
	</li>
	<li class="hideonmobile"><a data-toggle="tab" href="#care">Care</a></li>
	<li class="showonmobile dropdown active">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			Detail Information<span class="caret"></span>
		</a>
		<ul class="dropdown-menu">
			<li><a href="#" onclick="pi_tab('productinformation'); return false;">
				Product Information
			</a></li>
			<li><a href="#" onclick="pi_tab('care'); return false;">Care</a></li>
		</ul>
	</li>
	</ul>
	<div class="tab-content">
		<div id="productinformation" class="tab-pane fade active in">
			<p>COMPOSITION<br />EXTERIOR<br />62% Viscose<br />35% Polyamide<br />3% Elastane</p>
		</div>
		<div id="care" class="tab-pane fade">
			<ul style="list-style-type: none;">
			<li><img class="prodcare" alt="" src="<?php //echo $path;?>images/productcare/icon-info-product-01.jpg">Machine wash up to 30C/86F gantle cycle</li>
			<li><img class="prodcare" alt="" src="<?php //echo $path;?>images/productcare/icon-info-product-02.jpg">Iron up to 110C/230F</li>
			<li><img class="prodcare" alt="" src="<?php //echo $path;?>images/productcare/icon-info-product-03.jpg">Do not tuble dry</li>
			<li><img class="prodcare" alt="" src="<?php //echo $path;?>images/productcare/icon-info-product-04.jpg">Do not bleach, dry clean tetrachlorethylene</li>
			</ul>
		</div>
	</div> -->
</div><?php 
}else{?>
<div class="row rowErrMsg">
	<div class="col-xs-12 s-d errMsg"><?php echo $errMsg;?></div>
	<div class="clearfix"></div>
</div><?php 
}
echo $relprod;?>
<!-- <div class="containerdiff relatedproduct">
	<div class="row">
		<h5>Related Products</h5>
		<div class="col-md-offset-1 col-md-3 col-xs-12">
			<div>
				<img src="<?php //echo $path;?>images/RelatedProduct-01.jpg" class="img-responsive" alt=""/>
			</div>
			<p class="title">Men's Polo Shirt</p>
			<p class="price">IDR 899.000,-</p>
		</div>
		<div class="col-md-3 col-xs-12">
			<div>
				<img src="<?php //echo $path;?>images/RelatedProduct-02.jpg" class="img-responsive" alt=""/>
			</div>
			<p class="title">Bag's</p>
			<p class="price">IDR 899.000,-</p>
		</div>
		<div class="col-md-3 col-xs-12">
			<div>
				<img src="<?php //echo $path;?>images/RelatedProduct-03.jpg" class="img-responsive" alt=""/>
			</div>
			<p class="title">Accesories</p>
			<p class="price">IDR 899.000,-</p>
		</div>
	</div>
</div> -->
<!-- open search in store -->
<div class="popup-outer">
	<div class="loading">
		<div>Processing...</div>
	</div>
	<div class="popup-inner">
		<div class="popup-row">
			<div class="col-sm-5 col-xs-12 popup-contentleft">
				<img alt="<?php echo htmlentities($prname);?>" src="">
			</div>
			<div class="col-sm-7 col-xs-12 popup-contentright">
				<p class="sisinfo">
					<span class="sisname"><?php echo $prname;?></span><br>
					<span class="sisstyle"></span><span class="sissize"></span>
				</p>
				<p>WOULD YOU LIKE TO KNOW IF THIS ITEM IS AVAILABLE IN STORE?</p>
				<p>To check in store availability, enter a city or post code. And we recommend that you call 
				the store to make sure they have the item you are interested in.</p>
				<input id="sistext" type="text" placeholder="Enter street name/town/interest place" >
				<button value="search" id="sisbutton" onclick="searchstore('<?php echo $path;?>'); return false;">
					SEARCH FOR STORES
				</button>
				<div class="resultsearchinstore">
					<!-- <div class="col-sm-6 col-xs-12">
						<span>St. Moritz Mall<br />Jl. Puri Raya Boulevard 1.<br />
						Jakarta Barat - Indonesia (11610)</span>
					</div>
					<div class="col-sm-6 col-xs-12">
						<span>Puri Indah Mall<br />Jl. Puri Agung, Puri Indah, Kembangan Selatan.<br />
						Jakarta Barat - Indonesia (11610)</span>
					</div>
					<div class="col-sm-6 col-xs-12">
						<span>Central Park Mall<br />Jl. Let. Jend. S. Parman, Kav. 28.<br />
						Jakarta Barat - Indonesia (11470)</span>
					</div>
					<div class="col-sm-6 col-xs-12">
						<span>Taman Anggrek Mall<br />Jl. Let. Jend. S. Parman, Kav. 21.<br />
						Jakarta Barat - Indonesia (11470)</span>
					</div> -->
				</div>
			</div>
		</div>
		<span onclick="togglePopUp();">Close</span>
	</div>
</div>

<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
<script src="<?php echo $path;?>js/productdetail.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script type="application/x-javascript"> 
	var pathurl = '<?php echo $path;?>';
	<?php echo $optprod;?>
	/* var optprod = {
		blue:["d1.jpg", "d2.jpg", "d3.jpg", "d4.jpg"],
		red:["e1.jpg", "e2.jpg", "e3.jpg", "e4.jpg"]
	}; */
</script>
</body>
</html>