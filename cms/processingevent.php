<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/event.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select ev_image from event where ev_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['ev_image']!='' && file_exists("../images/event/".$cekimg2['ev_image'])){
				unlink("../images/event/".$cekimg2['ev_image']);
			}
			mysqli_query( $sqlcon,"delete from event where ev_id=".$_POST['delid'] );
			$result['Status']='Delete Success!';		
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}else{
		$result['Error']='Data already deleted!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['ev_title'])){$ev_title=$_POST['ev_title'];}else{$ev_title='';}
	if (isset($_POST['ev_code'])){$ev_code=$_POST['ev_code'];}else{$ev_code='';}
	if (isset($_POST['ev_status'])){$ev_status=intval($_POST['ev_status']);}else{$ev_status=0;}
	if (isset($_POST['ev_draft'])){$ev_draft=intval($_POST['ev_draft']);}else{$ev_draft=0;}
	if (isset($_POST['ev_desc'])){$ev_desc=$_POST['ev_desc'];}else{$ev_desc='';}
	if (isset($_POST['ev_start']) && substr_count($_POST['ev_start'],'/')==2){
		$expd = explode(' ',$_POST['ev_start']);
		$xpd = explode('/',$expd[0]);
		$ev_start=$xpd[2].'-'.$xpd[0].'-'.$xpd[1].' '.$expd[1];
	}else{$ev_start=date("Y-m-d H:i:s");}
	if (isset($_POST['ev_end']) && substr_count($_POST['ev_end'],'/')==2){
		$expd = explode(' ',$_POST['ev_end']);
		$xpd = explode('/',$expd[0]);
		$ev_end=$xpd[2].'-'.$xpd[0].'-'.$xpd[1].' '.$expd[1];
	}else{$ev_end=date("Y-m-d H:i:s");}
	
	if ($ev_title==''){
		$result['Error']['ev_title']=1;
	}else{
		$addcekex = '';
		if ($_POST['formid']!='new' && $_POST['formid']!='' && $_POST['formid']>0){
			$addcekex = ' and ev_id!='.intval($_POST['formid']);
		}
		$cekex = mysqli_num_rows(mysqli_query($sqlcon,
			"select ev_id from event where ev_title='".mysqli_real_escape_string($sqlcon,$ev_title)."'".
			$addcekex
		));
		if ($cekex > 0){
			$result['Error']['ev_title']=1;
		}
	}
	if (strtotime($ev_end) < strtotime($ev_start)){$result['Error']['ev_end']=1;}
	if (isset($_FILES['ev_image']['name']) && !(
		strtolower($_FILES['ev_image']['type'])=='image/jpeg' || strtolower($_FILES['ev_image']['type'])=='image/png'
	)){
		$result['Error']['ev_image']=1;
	}
	
	$strrep = array("'",'"',' ','/','\\','#');
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into event (".
			"ev_status,ev_draft,ev_start,ev_end,ev_image,".
			"ev_title,".
			"ev_code,".
			"ev_desc,".
			"ev_createby,ev_create,ev_updateby,ev_update".
		") values (".
			$ev_status.",".$ev_draft.",'".$ev_start."','".$ev_end."','',".
			"'".mysqli_real_escape_string( $sqlcon,$ev_title )."',".
			"'".mysqli_real_escape_string( $sqlcon,$ev_code )."',".
			"'".mysqli_real_escape_string( $sqlcon,$ev_desc )."',".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);
		if (isset($_FILES['ev_image']['name']) && $_FILES['ev_image']['name']!=''){
			$insertid = mysqli_insert_id( $sqlcon );
			$strrep = array("'",'"',' ','/','\\');
			$ev_image = str_replace($strrep,'', $insertid.'-'.$_FILES['ev_image']['name']);
			move_uploaded_file(
				$_FILES['ev_image']['tmp_name'],'../images/event/'.$ev_image
			);
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['ev_image']['size']);
				
			$result['ev_image']='<img src="../images/event/'.$ev_image.'" class="itemimg"><br />' . $fz;
			mysqli_query( $sqlcon,
				"update event set ev_image='".mysqli_real_escape_string( $sqlcon,$ev_image )."' ".
				"where ev_id=".$insertid
			);
		}else{
			$ev_image = '';
			$result['ev_image']='<img src="../images/default.png" class="itemimg">';
		}
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		if (isset($_FILES['ev_image']['name']) && $_FILES['ev_image']['name']!=''){
			$cekimg = mysqli_query( $sqlcon,
				"select ev_image from event where ev_id=".intval($_POST['formid'])
			);
			$cekimg1 = mysqli_num_rows($cekimg);
			if ($cekimg1 > 0){
				$cekimg2 = mysqli_fetch_array($cekimg);
				if ($cekimg2['ev_image']!='' && file_exists("../images/event/".$cekimg2['ev_image'])){
					unlink("../images/event/".$cekimg2['ev_image']);
				}
			}
			$ev_image = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['ev_image']['name']);
			move_uploaded_file(
				$_FILES['ev_image']['tmp_name'],'../images/event/'.$ev_image
			);
			$addsql = ",ev_image='".mysqli_real_escape_string( $sqlcon,$ev_image )."'";
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['ev_image']['size']);
		
			$result['ev_image']='<img src="../images/event/'.$ev_image.'" class="itemimg"><br />' . $fz;
		}else{
			$addsql = '';
		}
		mysqli_query( $sqlcon,
		"update event set ".
			"ev_title='".mysqli_real_escape_string( $sqlcon,$ev_title )."',".
			"ev_code='".mysqli_real_escape_string( $sqlcon,$ev_code )."',".
			"ev_start='".$ev_start."',".
			"ev_end='".$ev_end."',".
			"ev_status=".$ev_status.",".
			"ev_draft=".$ev_draft.",".
			"ev_desc='".mysqli_real_escape_string( $sqlcon,$ev_desc )."',".
			"ev_update='".date("Y-m-d H:i:s")."',".
			"ev_updateby=".$cokidusr.$addsql." ".
		"where ev_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>