<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/bannerhomeslider.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$buttondraft = draftStatus();

include_once 'kalkulasibannerhomesliderfilter.php';

$resview = numrowdb($sqlcon,"select bn_id from banner".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select banner.*,GROUP_CONCAT(bns.bns_ca_id) groupcaid ".
		"from banner " .
		"left outer join banner_showon bns on bns_bn_id=bn_id" .
		$sinfil . " group by bn_id" . $sinlimit
	);
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
	if ($getdt1 ['bn_status']==1){
		$status = '<span class="label label-success">Active</span>';
	}else{
		$status = '<span class="label label-danger">Pasif</span>';
	}
	if ($getdt1 ['bn_draft']==1){
		$draft = '<span class="label label-warning">Draft</span>';
	}else{
		$draft = '';
	}
	if ($getdt1['bn_image']!='' && file_exists("../images/banner/".$getdt1['bn_image'])){
		$image =  '<img src="'.$path.'/images/banner/'.$getdt1['bn_image'].'" style="height:50px;">' . 
			'<br />' . formatBytes(filesize("../images/banner/".$getdt1['bn_image']));
	}else{
		$image = '<img src="'.$path.'/images/default.png" style="width:50px;">';
	}
	if ($getdt1['bn_image1']!='' && file_exists("../images/banner/".$getdt1['bn_image1'])){
		$image1 =  '<img src="'.$path.'/images/banner/'.$getdt1['bn_image1'].'" style="height:50px;">' .
				'<br />' . formatBytes(filesize("../images/banner/".$getdt1['bn_image1']));
	}else{
		$image1 = '<img src="'.$path.'/images/default.png" style="width:50px;">';
	}
	$strfrom = date("m/d/Y H:i:s",strtotime($getdt1['bn_from']));
	$strto = date("m/d/Y H:i:s",strtotime($getdt1['bn_to']));
	$list .= "<tr id=\"tr" . $getdt1 ['bn_id'] . "\">" . 
	"<td id=\"fbn_from" . $getdt1 ['bn_id'] . "\">" . $strfrom . "</td>" .
	"<td id=\"fbn_to" . $getdt1 ['bn_id'] . "\">" . $strto . "</td>" .
	"<td id=\"fbn_title" . $getdt1 ['bn_id'] . "\">" . htmlentities($getdt1 ['bn_title']) . "</td>" .
	"<td id=\"fbn_image" . $getdt1 ['bn_id'] . "\">" . $image . "</td>" .
	"<td id=\"fbn_image1" . $getdt1 ['bn_id'] . "\">" . $image1 . "</td>" .
	"<td>".
		"<span id=\"fstatus" . $getdt1 ['bn_id'] . "\">" . $status . "</span>".
		"<span id=\"fdraft" . $getdt1 ['bn_id'] . "\">" . $draft . "</span>".
	"</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fbn_status".$getdt1['bn_id']."\" class=\"hide\">".$getdt1['bn_status']."</p>".
		"<p id=\"fbn_draft".$getdt1['bn_id']."\" class=\"hide\">".$getdt1['bn_draft']."</p>".
		"<p id=\"fbns_ca_id".$getdt1['bn_id']."\" class=\"hide\">".$getdt1['groupcaid']."</p>".
		"<p id=\"fbn_sequence".$getdt1['bn_id']."\" class=\"hide\">".$getdt1['bn_sequence']."</p>".
		"<p id=\"fbn_subtitle".$getdt1['bn_id']."\" class=\"hide\">".$getdt1['bn_subtitle']."</p>".
		"<p id=\"fbn_link".$getdt1['bn_id']."\" class=\"hide\">".$getdt1['bn_link']."</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['bn_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data ".htmlentities($getdt1['bn_title'])."\" ".
		"onClick=\"prepareedit('".$getdt1['bn_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['bn_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data ".htmlentities($getdt1['bn_title'])."\" ".
		"onClick=\"preparedelete('".$getdt1['bn_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"7\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

$bn_from = date("m/d/Y H:i:s");
$bn_to = date("m/d/Y H:i:s",mktime(date("H"),date("i"),date("s"),date("m"),date("d")+15,date("Y")));

//list page show on
$showonpage = '';
$data = @mysqli_query($sqlcon,"select * from category where ca_ca_id=0 order by ca_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
	if ($data2['ca_status']==0){
		$disable = ' disabled="disabled"';
	}
	$idcat = $data2['ca_id'];
	$showonpage .= '<div class="col-md-3 col-sm-6 col-xs-12 form-group">'.
		'<div class="checkbox">'.
			'<label>'.
				'<input name="bns_ca_id['.$idcat.']" id="bns_ca_id'.$idcat.'" type="checkbox" '.
				'value="1" class="bns_ca_id"'.$disable.'>'.$data2['ca_name'].
			'</label>'.
		'</div>'.
	'</div>';
}}
?>