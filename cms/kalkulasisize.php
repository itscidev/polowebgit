<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/size.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasisizefilter.php';

$resview = numrowdb($sqlcon,"select sz_id from size".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = @mysqli_query( $sqlcon,"select * from size".$sinfil.$sinlimit );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	$list .= "<tr id=\"tr" . $getdt1['sz_id'] . "\">" .
		"<td id=\"fsz_sequence" . $getdt1['sz_id'] . "\" align=\"left\">" . $getdt1['sz_sequence'] . "</td>" .
		"<td id=\"fsz_name" . $getdt1['sz_id'] . "\" align=\"left\">" . htmlentities($getdt1['sz_name']) . "</td>" .
		"<td style=\"font-size:17px;\">" .
			"<a href=\"#windata\" id=\"edit".$getdt1['sz_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data " . htmlentities($getdt1['sz_name']) . "\" " .
			"onClick=\"prepareedit('" . $getdt1['sz_id'] . "');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['sz_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data " . htmlentities($getdt1['sz_name']) . "\" " .
			"onClick=\"preparedelete('" . $getdt1['sz_id'] . "');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>" .
		"</td>" .
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"3\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}
?>