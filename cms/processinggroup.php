<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/group.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (!isset($_POST['delete']) || (isset($_POST['delete']) && $_POST['delete']!=1)){
		$result['Error']='Confirmation box not checked!';
	}else{
		mysqli_query( $sqlcon,"delete from group_access where ga_gr_id=".$_POST['delid'] );
		mysqli_query( $sqlcon,"delete from `group` where gr_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['gr_name'])){$gr_name=$_POST['gr_name'];}else{$gr_name='';}
	if (isset($_POST['gr_desc'])){$gr_desc=$_POST['gr_desc'];}else{$gr_desc='';}
	if (isset($_POST['ac'])){$ac=$_POST['ac'];}else{$ac=array();}
	
	if ($gr_name == ''){$result['Error']['gr_name'] = 1;}
	if (count($ac) == 0){
		$result['Error']['ac'] = 1;
	}else{
		$adaisi = 0;
		foreach ($ac as $content){
			if ($content == 1){$adaisi = 1;}
		}
		if ($adaisi == 0){$result['Error']['ac'] = 1;}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into `group` (".
		"gr_name,".
		"gr_desc,".
		"gr_createby,gr_create,gr_updateby,gr_update".
	") values (".
		"'".mysqli_real_escape_string( $sqlcon,$gr_name )."',".
		"'".mysqli_real_escape_string( $sqlcon,$gr_desc )."',".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	$lastid = mysqli_insert_id($sqlcon);
	//query insert access
	foreach ($ac as $index => $content){
		if ($content == 1){
			mysqli_query( $sqlcon,
				"insert into group_access (".
					"ga_gr_id,ga_ac_id,".
					"ga_createby,ga_create,ga_updateby,ga_update".
				") values (".
					$lastid.",".$index.",".
					$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
				")" 
			);
		}
	}
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	mysqli_query( $sqlcon,
	"update `group` set ".
	"gr_name='".mysqli_real_escape_string( $sqlcon,$gr_name )."',".
	"gr_desc='".mysqli_real_escape_string( $sqlcon,$gr_desc )."',".
	"gr_updateby=".$cokidusr.",gr_update='".date("Y-m-d H:i:s")."' ".
	"where gr_id=".$_POST['formid']
	);
	
	//processing group access
	$exarr = array();
	$cekexist = @mysqli_query( $sqlcon,"select ga_id from group_access where ga_gr_id=".$_POST['formid'] );
	$cekexist1 = 0;
	if ($cekexist){$cekexist1 = mysqli_num_rows($cekexist);}
	if ($cekexist1 > 0){while ($cekexist2 = mysqli_fetch_array($cekexist)){
		$idnow = $cekexist2['ga_id'];
		if (!isset($ac[$idnow]) || (isset($ac[$idnow]) && $ac[$idnow]==0)){
			mysqli_query( $sqlcon,"delete from group_access where ga_id=".$idnow );
		}else{
			array_push( $exarr,$cekexist2['ga_id'] );
		}
	}}
	$accnow = '';
	foreach ($ac as $index => $content){
		if ($accnow != ''){$accnow .= ',';}
		$accnow .= $index;
		if ($content==1 && !in_array($index,$exarr)){
			mysqli_query( $sqlcon,
				"insert into group_access (".
				"ga_gr_id,ga_ac_id,".
				"ga_createby,ga_create,ga_updateby,ga_update".
			") values (".
				$_POST['formid'].",".$index.",".
				$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
			")"
			);
		}
	}
	$result['accnow']=$accnow;
	$result['Status']='Update';
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>