<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/size.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (!isset($_POST['delete']) || (isset($_POST['delete']) && $_POST['delete']!=1)){
		$result['Error']='Confirmation box not checked!';
	}else{
		mysqli_query( $sqlcon,"delete from size where sz_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['sz_name'])){$sz_name=$_POST['sz_name'];}else{$sz_name='';}
	if (isset($_POST['sz_sequence'])){$sz_sequence=intval($_POST['sz_sequence']);}else{$sz_sequence=0;}
	
	if ($sz_name == ''){
		$result['Error']['sz_name'] = 1;
	}else{
		$addexist = '';
		if ($_POST['formid']!='new'){$addexist = ' and sz_id!='.intval($_POST['formid']);}
		$exist = mysqli_num_rows(mysqli_query($sqlcon,
			"select sz_id from size ".
			"where sz_name='".mysqli_real_escape_string($sqlcon,$sz_name)."'".$addexist
		));
		if ($exist > 0){$result['Error']['sz_name'] = 1;}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into size (".
		"sz_name,sz_sequence,".
		"sz_createby,sz_create,sz_updateby,sz_update".
	") values (".
		"'".mysqli_real_escape_string( $sqlcon,$sz_name )."',".$sz_sequence.",".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	mysqli_query( $sqlcon,
	"update size set ".
	"sz_name='".mysqli_real_escape_string( $sqlcon,$sz_name )."',".
	"sz_sequence=".$sz_sequence.",".
	"sz_updateby=".$cokidusr.",sz_update='".date("Y-m-d H:i:s")."' ".
	"where sz_id=".$_POST['formid']
	);
	$result['Status']='Update';
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>