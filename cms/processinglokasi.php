<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/lokasi.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from lokasi where lok_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';		
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['lok_name'])){$lok_name=trim($_POST['lok_name']);}else{$lok_name='';}
	if (isset($_POST['lok_rajaongkir_id'])){$lok_rajaongkir_id=intval($_POST['lok_rajaongkir_id']);}else{$lok_rajaongkir_id=0;}
	
	if ($lok_name==''){$result['Error']['lok_name']=1;}
	if (!isset($result['Error'])){
		$addcek = '';
		if ($_POST['formid']!='new'){
			$addcek = ' and lok_id!='.intval($_POST['formid']);
		}
		$cekexist = mysqli_num_rows(mysqli_query($sqlcon,
			"select lok_id from lokasi ".
			"where lower(lok_name)='".mysqli_real_escape_string($sqlcon,strtolower($lok_name))."'".$addcek
		));
		if ($cekexist!=0){
			$result['Error']['lok_name']=1;
		}
	}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,"insert into lokasi (".
			"lok_name,".
			"lok_rajaongkir_id,".
			"lok_createby,lok_create,lok_updateby,lok_update".
		") values (".
			"'".mysqli_real_escape_string( $sqlcon,$lok_name )."',".
			$lok_rajaongkir_id.",".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")");
		//$result['QryErr']=mysqli_error($sqlcon);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update lokasi set ".
			"lok_name='".mysqli_real_escape_string( $sqlcon,$lok_name )."',".
			"lok_rajaongkir_id=".$lok_rajaongkir_id.",".
			"lok_update='".date("Y-m-d H:i:s")."',".
			"lok_updateby=".$cokidusr." ".
		"where lok_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>