<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/preorderconfirmation.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasipreorderconfirmationfilter.php';

$urltarget = "getajaxpreorder.html";

$resview = numrowdb($sqlcon,"select pc_id from preorder_confirmation".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$strsin = "select pc.*,po_id,po_nama,po_hp,po_pesanan ".
		"from preorder_confirmation pc left outer join preorder po on po_id=pc_po_id".
		$sinfil . " group by pc_id" . $sinlimit;
	$getdt = mysqli_query( $sqlcon,$strsin ) ;
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
	if ($getdt1 ['pc_status']==1){
		$status = '<span class="label label-success">Approved</span>';
	}else{
		$status = '<span class="label label-danger">Not</span>';
	}
	$confdate = date("m/d/Y H:i:s",strtotime($getdt1['pc_create']));
	if ($getdt1['pc_bukti']!='' && file_exists("../preorder/bukti/".$getdt1['pc_bukti'])){
		$image =  '<img src="'.$path.'/preorder/bukti/'.$getdt1['pc_bukti'].'" style="height:50px;">' . 
			'<br />' . formatBytes(filesize("../preorder/bukti/".$getdt1['pc_bukti']));
	}else{
		$image = '<img src="'.$path.'/images/default.png" style="width:50px;">';
	}
	$linkedpo = '';
	if ($getdt1['pc_po_id']!=0 && isset($getdt1['po_id'])){
		$jsonorder = json_decode($getdt1['po_pesanan'],true);
		$pesanan = '';
		foreach ($jsonorder as $key => $value) {
			if ($pesanan != ''){$pesanan .= ", ";}
			$pesanan .= $key.' '.$value;
		}
		if ($pesanan!=''){$pesanan = ' ('.$pesanan.')';}
		$linkedpo = $getdt1['po_nama'].', '.$getdt1['po_hp'].$pesanan;
	}
	$list .= "<tr id=\"tr" . $getdt1 ['pc_id'] . "\">" . 
	"<td>" . $confdate . "</td>" .
	"<td id=\"fpc_nama" . $getdt1 ['pc_id'] . "\">" . htmlentities($getdt1 ['pc_nama']) . "</td>" .
	"<td id=\"fpc_hp" . $getdt1 ['pc_id'] . "\">" . htmlentities($getdt1 ['pc_hp']) . "</td>" .
	"<td id=\"fpc_bukti" . $getdt1 ['pc_id'] . "\">" . $image . "</td>" .
	"<td>".
		"<span id=\"fstatus" . $getdt1 ['pc_id'] . "\">" . $status . "</span>".
	"</td>" .
	"<td id=\"fpc_po_name" . $getdt1 ['pc_id'] . "\">" . $linkedpo . "</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fpc_po_id".$getdt1['pc_id']."\" class=\"hide\">".$getdt1['pc_po_id']."</p>".
		"<p id=\"fpc_status".$getdt1['pc_id']."\" class=\"hide\">".$getdt1['pc_status']."</p>".
		"<p id=\"fpc_email".$getdt1['pc_id']."\" class=\"hide\">".$getdt1['pc_email']."</p>".		
		"<a href=\"#windata\" id=\"edit".$getdt1['pc_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data ".htmlentities($getdt1['pc_nama'])."\" ".
		"onClick=\"prepareedit('".$getdt1['pc_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['pc_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data ".htmlentities($getdt1['pc_nama'])."\" ".
		"onClick=\"preparedelete('".$getdt1['pc_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"7\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

$optpc_po_id = '';
?>