<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/pagesoverwrite.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$buttondraft = draftStatus();

include_once 'kalkulasipagesoverwritefilter.php';

$resview = numrowdb($sqlcon,"select pg_id from pages".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$qrysel = "select * from pages".$sinfil.$sinlimit;
	//	echo $qrysel;
	$getdt = @mysqli_query( $sqlcon,$qrysel );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	if ($getdt1['pg_status']==1){
		$status = '<span class="label label-success">Active</span>';
	}else{
		$status = '<span class="label label-danger">Pasif</span>';
	}
	if ($getdt1 ['pg_draft']==1){
		$draft = '<span class="label label-warning">Draft</span>';
	}else{
		$draft = '';
	}
	$list .= "<tr id=\"tr" . $getdt1['pg_id'] . "\">" .
		"<td id=\"fpg_name" . $getdt1['pg_id'] . "\" align=\"left\">" . htmlentities($getdt1['pg_name']) . "</td>" .
		"<td id=\"fpg_title" . $getdt1 ['pg_id'] . "\">" . htmlentities($getdt1['pg_title']) . "</td>" . 
		"<td>".
			"<span id=\"fstatus" . $getdt1 ['pg_id'] . "\">" . $status . "</span>".
			"<span id=\"fdraft" . $getdt1 ['pg_id'] . "\">" . $draft . "</span>".
		"</td>" .
		"<td style=\"font-size:17px;\">" .
			"<p id=\"fpg_keyword".$getdt1['pg_id']."\" class=\"hide\">".htmlentities($getdt1['pg_keyword'])."</p>".
			"<p id=\"fpg_desc".$getdt1['pg_id']."\" class=\"hide\">".htmlentities($getdt1['pg_desc'])."</p>".
			"<p id=\"fpg_status".$getdt1['pg_id']."\" class=\"hide\">".$getdt1['pg_status']."</p>".
			"<p id=\"fpg_draft".$getdt1['pg_id']."\" class=\"hide\">".$getdt1['pg_draft']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['pg_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data " . htmlentities($getdt1['pg_name']) . "\" " .
			"onClick=\"prepareedit('" . $getdt1['pg_id'] . "');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['pg_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data " . htmlentities($getdt1['pg_name']) . "\" " .
			"onClick=\"preparedelete('" . $getdt1['pg_id'] . "');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>" .
		"</td>" .
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"4\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td></tr>";
}
?>