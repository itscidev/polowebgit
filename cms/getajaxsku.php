<?php
/* return result like this
{
  "results": [
    {
      "id": 1,
      "text": "Option 1"
    },
    {
      "id": 2,
      "text": "Option 2",
      "selected": true
    },
    {
      "id": 3,
      "text": "Option 3",
      "disabled": true
    }
  ]
} */
$result['results'] = array();
$data1 = 0;
include_once ("configuration/connect.php");

if (isset($_GET['search']) && $_GET['search']!=''){
	$keysearch = $_GET['search'];
}else{$keysearch = '';}
if ($keysearch != ''){
	$qrystr = "select pr_id,pr_name,".
		"ci_id,ci_image,cl_name, ".
		"st_id,st_name ".
	"from product_variety ".
	"inner join product on pr_id=pv_pr_id ".
	"inner join color_item on ci_id=pv_ci_id ".
	"inner join color on cl_id=ci_cl_id ".
	"left outer join style on st_id=pv_st_id ".
	"where concat(lower(pr_name),'/',lower(st_name),'/',lower(cl_name)) like '%".mysqli_real_escape_string($sqlcon,strtolower($keysearch))."%' ".
	"group by pr_id,ci_id,st_id ".
	"order by pr_name,st_name,cl_name"; 
	$data = @mysqli_query( $sqlcon,$qrystr );
	if ($data){$data1 = mysqli_num_rows($data);}
	
	if ($data1 > 0){while ($data2 = mysqli_fetch_assoc($data)){
		if ($data2['st_id']==null){
			$data2['st_id']=0;
		}
		array_push($result['results'],array(
			'id' => $data2['pr_id'].":".$data2['st_id'].":".$data2['ci_id'],
			"text" => $data2['pr_name']."/".$data2['st_name']."/".$data2['cl_name'].
				"<img src=\"../images/productcolor/".$data2['ci_image']."\" class=\"colorimg\">"
		));
	}}
}

if ($sqlcon){
	mysqli_close( $sqlcon );
}

/* $get = $post = $server = $body = '';
if (isset ( $_GET )) {$get = ", Get : " . json_encode ( $_GET );}
if (isset ( $_POST )) {$post = ", Post : " . json_encode ( $_POST );}
if (isset ( $_SERVER )) {$server = ", Server : " . json_encode ( $_SERVER );}
$body = ", Body : ".file_get_contents('php://input');
$forlog = date ( "d-m-Y H:i:s" ) . ", result : " . json_encode ( $result ) . $body . $get . $post . 
		$server . PHP_EOL;
foreach ( file ( 'testing/log.txt' ) as $nline => $line ) {
	if ($nline < 150) {
		$forlog .= $line;
	}
}
$myfile = fopen ( "testing/log.txt", 'w' );
fwrite ( $myfile, $forlog );
fclose ( $myfile ); */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>