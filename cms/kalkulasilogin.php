<?php
//special case for favicon
$HTTP_HOST = array(
	'cms.poloindonesia.com'
);

if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://poloindonesia.com';
}else{
	$path = '..';
}
$favicon = '<link rel="shortcut icon" type="image/png" href="'.$path.'/images/favicon.png">';
unset($HTTP_HOST);

//special case for title head
$headtitle = '<title>CMS POLO Indonesia</title>';

if (isset($_COOKIE['login']) && $_COOKIE['login']!=''){
	header("location:index.html");
	exit();
}else{
	$pesan='';$rememberme='';
	if (isset($_POST['uname']) && isset($_POST['password'])){
		if ($_POST['uname']=='' || $_POST['password']==''){
			$pesan = 'No User / Password detected!';
		}else{
			include_once 'configuration/connect.php';
			$getusr = mysqli_query( $sqlcon,
				"select usr_id,usr_blacklist,usr_password,usr_accessip from user where ".
				"usr_name='".mysqli_real_escape_string( $sqlcon,$_POST['uname'] )."'"
			);
			$getusr1 = mysqli_num_rows($getusr);
			if ($getusr1 == 0){
				$pesan = 'User not found!';
			}else{
				$getusr2 = mysqli_fetch_array($getusr);
				if ($getusr2['usr_blacklist'] > 3){
					$pesan = 'User in blacklist! Cek your email with subject '.
					'<b>POLO Account Blacklist</b> to un-lock or contact Web Master!';
				}elseif ($getusr2['usr_accessip'] != ''){
					if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) ){
						$clientip = $_SERVER['HTTP_X_FORWARDED_FOR'];
					}elseif( isset($_SERVER['REMOTE_ADDR']) ){
						$clientip = $_SERVER['REMOTE_ADDR'];
					}
					if ($getusr2['usr_accessip'] != $clientip){
						$pesan = 'IP not allow to access!';
					}
				}elseif ($getusr2['usr_password'] != md5($_POST['password'])){
					$sqlunlock = '';
					if ($getusr2['usr_blacklist'] == 3){
						$pesan = 'User in blacklist! Cek your email to un-lock or contact Web Master!';
						
						$torand = array(
							'0','1','2','3','4','5','6','7','8','9',
							'a','b','c','d','e','f','g','h','i','j',
							'k','l','m','n','o','p','q','r','s','t',
							'u','v','w','x','y','z'
						);
						$codeunlock='';
						for ($i=0;$i<18;$i++){
							$codeunlock .= $torand[ rand(0,35) ];
						}
						$sqlunlock = ",usr_blacklistunlock='".$codeunlock."'";

						if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) ){
							$clientip = $_SERVER['HTTP_X_FORWARDED_FOR'];
						}elseif( isset($_SERVER['REMOTE_ADDR']) ){
							$clientip = $_SERVER['REMOTE_ADDR'];
						}else{
							$clientip = 'Un-known';
						}
						
						$httpref = explode('/cms/',$_SERVER['HTTP_REFERER']);

						//execute sendmail
						$Subject	= 'POLO Indonesia Account Blacklist';
						$Message	= 'Dear Admin<br /><br />'.
							'Your account under blacklist because of incorrect login trying.<br />'.
							'Last tried login at '.date("d-m-Y H:i:s").', from IP : '.$clientip.'<br /><br />'.
							'You can unlock this account by clicking at this link : '.
							'<a href="'.$httpref[0].'/cms/unlockblacklist.html?reqid='.$codeunlock.'" '.
							'title="Unlock my account"><b>Un-lock my account</b></a><br />'.
							'Or contact web master.<br /><br />Thank You';
						$AltMessage	= 'Dear Admin\n\n'.
							'Your account under blacklist because of incorrect login trying.\n'.
							'Last tried login at '.date("d-m-Y H:i:s").', from IP : '.$clientip.'\n\n'.
							'You can unlock this account by open this link at your browser : '.
							$httpref[0].'/cms/unlockblacklist.html?reqid='.$codeunlock.' \n'.
							'Or contact web master.\n\nThank You';
						$ToEmail	= $_POST['uname'];
						mysqli_query( $sqlcon,
							"update user set usr_blacklist=".($getusr2['usr_blacklist']+1).
							$sqlunlock." where usr_id='".$getusr2['usr_id']."'"
						);
						include_once 'configuration/function.php';
						$semail		= sendEmail($Subject,$Message,$AltMessage,$ToEmail,'cms','','',$sqlcon);
					}else{
						mysqli_query( $sqlcon,
							"update user set usr_blacklist=".($getusr2['usr_blacklist']+1)." ".
							"where usr_id='".$getusr2['usr_id']."'"
						);
						$pesan = 'Password not right!';
					}
				}else{
					mysqli_query( $sqlcon,
						"update user set usr_lastlogin='".date("Y-m-d H:i:s")."',usr_blacklist=0 ".
						"where usr_id='".$getusr2['usr_id']."'"
					);
					if (isset($_POST['rememberme']) && $_POST['rememberme']==1){
						$expiredtime = 60 * 24 * 60 * 60;//60 hari
					}else{
						$expiredtime = 60 * 60;//60 menit
					}
					//[usrid];[expiredtime]
					setcookie('login',base64_encode($getusr2['usr_id'].';'.$expiredtime),time() + $expiredtime);
					header("location:index.html");
					exit();					
				}
			}
			if ($sqlcon){mysqli_close($sqlcon);}
		}
	}
}
?>