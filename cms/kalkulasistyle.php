<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/style.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasistylefilter.php';

$resview = numrowdb($sqlcon,"select st_id from style".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,"select * from style".$sinfil.$sinlimit );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	$list .= "<tr id=\"tr" . $getdt1['st_id'] . "\">" .
		"<td id=\"fst_name" . $getdt1['st_id'] . "\" align=\"left\">" . htmlentities($getdt1['st_name']) . "</td>" .
		"<td style=\"font-size:17px;\">" .
			"<a href=\"#windata\" id=\"edit".$getdt1['st_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data " . htmlentities($getdt1['st_name']) . "\" " .
			"onClick=\"prepareedit('" . $getdt1['st_id'] . "');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['st_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data " . htmlentities($getdt1['st_name']) . "\" " .
			"onClick=\"preparedelete('" . $getdt1['st_id'] . "');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>" .
		"</td>" .
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"2\">No Data!</td><td class=\"hide\"></td></tr>";
}
?>