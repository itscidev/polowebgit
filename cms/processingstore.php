<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/store.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from store where st_id=".$_POST['delid'] );
		$resultdel = 'Delete Success!';
	}
	if ($resultdel != ''){
		$result['Status'] = $resultdel;
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['st_code'])){$st_code=strtoupper(trim($_POST['st_code']));}else{$st_code='';}
	if (isset($_POST['st_name'])){$st_name=trim($_POST['st_name']);}else{$st_name='';}
	if (isset($_POST['st_dispname'])){$st_dispname=trim($_POST['st_dispname']);}else{$st_dispname='';}
	if (isset($_POST['st_zip'])){$st_zip=intval($_POST['st_zip']);}else{$st_zip=0;}
	if (isset($_POST['st_lat'])){$st_lat=floatval($_POST['st_lat']);}else{$st_lat=0;}
	if (isset($_POST['st_long'])){$st_long=floatval($_POST['st_long']);}else{$st_long=0;}
	if (isset($_POST['st_address'])){$st_address=trim($_POST['st_address']);}else{$st_address='';}
	if (isset($_POST['st_status'])){$st_status=intval($_POST['st_status']);}else{$st_status=0;}
	
	if ($st_code==''){$result['Error']['st_code']=1;}
	if ($st_name==''){$result['Error']['st_name']=1;}
	if ($st_dispname==''){$result['Error']['st_dispname']=1;}
	if ($st_address==''){$result['Error']['st_address']=1;}
	if (!isset($result['Error'])){
		//cek existing code
		$addcek = '';
		if ($_POST['formid']!='new'){
			$addcek = ' and st_id!='.intval($_POST['formid']);
		}
		$cek = mysqli_num_rows(mysqli_query($sqlcon,
			"select st_id from store ".
			"where st_code='".mysqli_real_escape_string( $sqlcon,$st_code )."'".$addcek
		));
		if ($cek!=0){$result['Error']['st_code']=1;}
	}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,"insert into store (".
			"st_status,st_zip,st_lat,st_long,".
			"st_code,".
			"st_name,".
			"st_dispname,".
			"st_address,".
			"st_createby,st_create,st_updateby,st_update".
		") values (".
			$st_status.",".$st_zip.",".$st_lat.",".$st_long.",".
			"'".mysqli_real_escape_string( $sqlcon,$st_code )."',".
			"'".mysqli_real_escape_string( $sqlcon,$st_name )."',".
			"'".mysqli_real_escape_string( $sqlcon,$st_dispname )."',".
			"'".mysqli_real_escape_string( $sqlcon,$st_address )."',".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")");
		$result['QryErr']=mysqli_error($sqlcon);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update store set ".
			"st_code='".mysqli_real_escape_string( $sqlcon,$st_code )."',".
			"st_name='".mysqli_real_escape_string( $sqlcon,$st_name )."',".
			"st_dispname='".mysqli_real_escape_string( $sqlcon,$st_dispname )."',".
			"st_address='".mysqli_real_escape_string( $sqlcon,$st_address )."',".
			"st_status=".$st_status.",".
			"st_zip=".$st_zip.",".
			"st_lat=".$st_lat.",".
			"st_long=".$st_long.",".
			"st_update='".date("Y-m-d H:i:s")."',".
			"st_updateby=".$cokidusr." ".
		"where st_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>