<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/event.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$buttondraft = draftStatus();

include_once 'kalkulasieventfilter.php';

$resview = numrowdb($sqlcon,"select ev_id from event".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select * ".
		"from event " .
		$sinfil . " group by ev_id" . $sinlimit
	);
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
	if ($getdt1 ['ev_status']==1){
		$status = '<span class="label label-success">Active</span>';
	}else{
		$status = '<span class="label label-danger">Pasif</span>';
	}
	if ($getdt1 ['ev_draft']==1){
		$draft = '<span class="label label-warning">Draft</span>';
	}else{
		$draft = '';
	}
	if ($getdt1['ev_image']!='' && file_exists("../images/event/".$getdt1['ev_image'])){
		$image = '<img src="'.$path.'/images/event/'.$getdt1['ev_image'].'" class="itemimg">' .
			'<br />' . formatBytes(filesize("../images/event/".$getdt1['ev_image']));
	}else{
		$image = '<img src="'.$path.'/images/default.png" class="itemimg">';
	}
		
	$list .= "<tr id=\"tr" . $getdt1 ['ev_id'] . "\">" . 
	"<td id=\"fev_title" . $getdt1 ['ev_id'] . "\">" . htmlentities($getdt1['ev_title']) . "</td>" .
	"<td id=\"fev_code" . $getdt1 ['ev_id'] . "\">" . htmlentities($getdt1['ev_code']) . "</td>" .
	"<td id=\"fev_start" . $getdt1 ['ev_id'] . "\">" . date("m/d/Y H:i:s",strtotime($getdt1['ev_start'])) . "</td>" .
	"<td id=\"fev_end" . $getdt1 ['ev_id'] . "\">" . date("m/d/Y H:i:s",strtotime($getdt1['ev_end'])) . "</td>" .
	"<td id=\"fev_image" . $getdt1 ['ev_id'] . "\">" . $image . "</td>" .
	"<td>".
		"<span id=\"fstatus" . $getdt1 ['ev_id'] . "\">" . $status . "</span>".
		"<span id=\"fdraft" . $getdt1 ['ev_id'] . "\">" . $draft . "</span>".
	"</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fev_status".$getdt1['ev_id']."\" class=\"hide\">".$getdt1['ev_status']."</p>".
		"<p id=\"fev_draft".$getdt1['ev_id']."\" class=\"hide\">".$getdt1['ev_draft']."</p>".
		"<p id=\"fev_desc".$getdt1['ev_id']."\" class=\"hide\">".
			htmlentities($getdt1['ev_desc']).
		"</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['ev_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data ".htmlentities($getdt1['ev_title'])."\" ".
		"onClick=\"prepareedit('".$getdt1['ev_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['ev_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data ".htmlentities($getdt1['ev_title'])."\" ".
		"onClick=\"preparedelete('".$getdt1['ev_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"7\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}
?>