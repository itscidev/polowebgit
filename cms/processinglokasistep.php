<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/lokasistep.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from lokasi_step where los_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';		
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['los_zip'])){$los_zip=trim($_POST['los_zip']);}else{$los_zip='';}
	if (isset($_POST['los_prop_lok_id'])){$los_prop_lok_id=intval($_POST['los_prop_lok_id']);}else{$los_prop_lok_id=0;}
	if (isset($_POST['los_kab_lok_id'])){$los_kab_lok_id=intval($_POST['los_kab_lok_id']);}else{$los_kab_lok_id=0;}
	if (isset($_POST['los_kec_lok_id'])){$los_kec_lok_id=intval($_POST['los_kec_lok_id']);}else{$los_kec_lok_id=0;}
	
	if ($los_prop_lok_id==0){
		$result['Error']['los_prop_lok_id']=1;
	}
	if ($los_kab_lok_id==0){
		$result['Error']['los_kab_lok_id']=1;
	}
// 	if ($los_kec_lok_id==0){
// 		$result['Error']['los_kec_lok_id']=1;
// 	}
	if (!isset($result['Error'])){
		$addcek = '';
		if ($_POST['formid']!='new'){
			$addcek = ' and los_id!='.intval($_POST['formid']);
		}
		$cekexist = mysqli_num_rows(mysqli_query($sqlcon,
			"select los_id from lokasi_step ".
			"where los_prop_lok_id=".$los_prop_lok_id." and los_kab_lok_id=".$los_kab_lok_id." ".
				"and los_kec_lok_id=".$los_kec_lok_id.$addcek
		));
		if ($cekexist!=0){
			$result['Error']['los_prop_lok_id']=1;
			$result['Error']['los_kab_lok_id']=1;
			$result['Error']['los_kec_lok_id']=1;
		}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into lokasi_step (".
		"los_prop_lok_id,los_kab_lok_id,los_kec_lok_id,".
		"los_zip,".
		"los_createby,los_create,los_updateby,los_update".
	") values (".
		$los_prop_lok_id.",".$los_kab_lok_id.",".$los_kec_lok_id.",".
		"'".mysqli_real_escape_string($sqlcon,$los_zip)."',".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$qryupd = "update lokasi_step set ".
		"los_prop_lok_id=".$los_prop_lok_id.",".
		"los_kab_lok_id=".$los_kab_lok_id.",".
		"los_kec_lok_id=".$los_kec_lok_id.",".
		"los_zip='".mysqli_real_escape_string($sqlcon,$los_zip)."',".
		"los_updateby=".$cokidusr.",los_update='".date("Y-m-d H:i:s")."' ".
	"where los_id=".$_POST['formid'];
	mysqli_query( $sqlcon,$qryupd );
	$result['Status']='Update';
//	$result['QryUpd']=$qryupd;
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>