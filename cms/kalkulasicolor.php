<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/color.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasicolorfilter.php';

$resview = numrowdb($sqlcon,"select cl_id from color".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,"select * from color".$sinfil.$sinlimit );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	$list .= "<tr id=\"tr" . $getdt1['cl_id'] . "\">" .
		"<td id=\"fcl_name" . $getdt1['cl_id'] . "\" align=\"left\">" . htmlentities($getdt1['cl_name']) . "</td>" .
		"<td id=\"fcl_code_display" . $getdt1['cl_id'] . "\" align=\"left\">".
			"<i class=\"fa fa-square\" style=\"color:#".$getdt1['cl_code'].";\"></i>".
		"</td>" .
		"<td style=\"font-size:17px;\">" .
			"<p id=\"fcl_code".$getdt1['cl_id']."\" class=\"hide\">".$getdt1['cl_code']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['cl_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data " . htmlentities($getdt1['cl_name']) . "\" " .
			"onClick=\"prepareedit('" . $getdt1['cl_id'] . "');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['cl_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data " . htmlentities($getdt1['cl_name']) . "\" " .
			"onClick=\"preparedelete('" . $getdt1['cl_id'] . "');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>" .
		"</td>" .
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"3\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}
?>