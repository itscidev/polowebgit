<?php
//special case for favicon
$HTTP_HOST = array(
		'cms.rotio.id'
);

if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://rotio.id';
}else{
	$path = '..';
}
$favicon = '<link rel="shortcut icon" type="image/png" href="'.$path.'/images/favicon.png">';
unset($HTTP_HOST);

//special case for title head
$headtitle = '<title>CMS Polo Indonesia</title>';

if (isset($_COOKIE['login']) && $_COOKIE['login']!=''){
	header("location:index.html");
	exit();
}else{
	$pesan='';
	if (!isset($_GET['reqid']) || $_GET['reqid']=='' || strlen($_GET['reqid'])!=18){
		$pesan='Un-known parameter detected!';
	}else{
		include_once 'configuration/connect.php';
		$getusr = @mysqli_query( $sqlcon,
			"select usr_id from user ".
			"where usr_blacklistunlock='".mysqli_real_escape_string($sqlcon,$_GET['reqid'])."'"
		);
		$getusr1 = 0;
		if ($getusr){$getusr1 = mysqli_num_rows($getusr);}
		if ($getusr1 == 0){
			$pesan='Un-known parameter detected!';
		}else{
			mysqli_query( $sqlcon,
				"update user set usr_blacklistunlock='',usr_blacklist=0 ".
				"where usr_blacklistunlock='".mysqli_real_escape_string($sqlcon,$_GET['reqid'])."'"
			);
			$pesan='Un-lock blacklist success!';
		}
		if ($sqlcon){mysqli_close($sqlcon);}
	}
}
?>