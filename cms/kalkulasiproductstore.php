<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/productstore.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}


include_once 'kalkulasiproductstorefilter.php';

$resview = numrowdb($sqlcon,"select ps_id from product_store".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$strsql = "select ps.*,pv_name,pv_artikel,st_code,st_name ".
		"from product_store ps ".
		"left outer join product_variety on pv_id=ps_pv_id ".
		"left outer join store on st_id=ps_st_id ".
		$sinfil . " group by ps_id" . $sinlimit;
// 	echo $strsql;
	$getdt = mysqli_query( $sqlcon,$strsql );
	while ( $getdt1 = mysqli_fetch_assoc ( $getdt ) ) {
		$varname = $getdt1['pv_name'];
		if ($getdt1['pv_artikel']!=''){
			$varname .= '-'.$getdt1['pv_artikel'];
		}
		$list .= "<tr id=\"tr" . $getdt1 ['ps_id'] . "\">" . 
		"<td id=\"fps_pv_name" . $getdt1 ['ps_id'] . "\">" . htmlentities($varname) . "</td>" .
		"<td id=\"fps_st_name" . $getdt1 ['ps_id'] . "\">" . htmlentities($getdt1['st_code'].'-'.$getdt1['st_name']) . "</td>" .
		"<td style=\"font-size:17px;\">" . 
			"<p id=\"fps_pv_id".$getdt1['ps_id']."\" class=\"hide\">".$getdt1['ps_pv_id']."</p>".
			"<p id=\"fps_st_id".$getdt1['ps_id']."\" class=\"hide\">".$getdt1['ps_st_id']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['ps_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data\" onClick=\"prepareedit('".$getdt1['ps_id']."');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
			"<a href=\"#windelete\" id=\"delete".$getdt1['ps_id']."\" class=\"fancybox\" ".
			"title=\"Delete Data\" onClick=\"preparedelete('".$getdt1['ps_id']."');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
		"</td>" . 
		"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"3\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

//construct options parent
$optps_pv_id = '';
$data = @mysqli_query($sqlcon,"select * from product_variety order by pv_name,pv_artikel asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
 	if ($data2['pv_status']==0 && $data2['pv_draft']==0){
 		$disable = ' disabled="disabled"';
 	}
 	$varname = $data2['pv_name'];
 	if ($data2['pv_artikel']!=''){
 		$varname .= '-'.$data2['pv_artikel'];
 	}
	$optps_pv_id .= '<option value="' . $data2['pv_id'] . '"'.$disable.'>'.
		htmlentities( $varname ) .
	'</option>';
}}
//construct options store
$optps_st_id = '';
$data = @mysqli_query($sqlcon,"select * from store order by st_code,st_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
	if ($data2['st_status']==0){
		$disable = ' disabled="disabled"';
	}
	$optps_st_id .= '<option value="' . $data2['st_id'] . '"'.$disable.'>'.
		htmlentities( $data2['st_code'].'-'.$data2['st_name'] ) .
	'</option>';
}}
?>