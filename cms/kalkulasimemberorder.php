<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/memberorder.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$arrstatus = array('Waiting Payment','Canceled','Paid','Payment Failed','On Packaging','On Delivery','Delivered','Delivery Failed');

include_once 'kalkulasimemberorderfilter.php';

$resview = numrowdb($sqlcon,"select cr_id from cart".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$strsin = "select cr.*,mm.mm_email,mma.mma_name ".
		"from cart cr left outer join member_address mma on mma_id=cr_mma_id left outer join member mm on mm_id=mma_mm_id".
		$sinfil . " group by cr_id" . $sinlimit;
	// echo $strsin;
	$getdt = mysqli_query( $sqlcon,$strsin ) ;
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
	$status = $arrstatus[( $getdt1['cr_status'] )];
	$orderdate = date("m/d/Y H:i:s",strtotime($getdt1['cr_create']));
	$list .= "<tr id=\"tr" . $getdt1['cr_id'] . "\">" . 
	"<td>" . $orderdate . "</td>" .
	"<td id=\"fcr_invoice" . $getdt1['cr_id'] . "\">" . htmlentities($getdt1['cr_id']) . "</td>" .
	"<td>" . htmlentities($getdt1['mm_email']) . "</td>" .
	"<td>" . htmlentities($getdt1['mma_name']) . "</td>" .
	"<td>".
		"<span id=\"fstatus" . $getdt1['cr_id'] . "\">" . $status . "</span>".
	"</td>" .
	"<td id=\"fcr_awb" . $getdt1['cr_id'] . "\">" . htmlentities($getdt1['cr_awb']) . "</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fcr_status".$getdt1['cr_id']."\" class=\"hide\">".$getdt1['cr_status']."</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['cr_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data ".htmlentities($getdt1['cr_id'])."\" ".
		"onClick=\"prepareedit('".$getdt1['cr_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"memberorder.html\" title=\"See detail order ".htmlentities($getdt1['cr_id'])."\" ".
		"onClick=\"openfancy('".$path."/orderhistorydetail.html?cartid=".$getdt1['cr_id']."'); return false;\"><i class=\"fa fa-info-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"7\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

$optcr_status = '';
foreach ($arrstatus as $index => $content){
	$disabled = '';
	// if ($content[1]==0 && $content[2]==0){
	// 	$disabled = ' disabled="disabled"';
	// }
	$optcr_status .= '<option value="' . $index . '"'.$disabled.'>' . $content . '</option>';
}
?>