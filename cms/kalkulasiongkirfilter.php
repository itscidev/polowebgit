<?php
$filter = array();
if (isset( $_COOKIE['filter'] )) {
	setcookie( "filter", $_COOKIE['filter'], time() + 7200 );
	$filter = json_decode( $_COOKIE['filter'], true );
}

$sinfil = '';
if (isset( $filter['ON']['isianteks'] )) {
	$isianteks = $filter['ON']['isianteks'];
}else{
	$isianteks = '';
}
if ($isianteks != ''){
	$getidpar = mysqli_query($sqlcon,
		"select et_id from ekspedisi_type ".
		"left outer join ekspedisi on eks_id=et_eks_id ".
		"where et_code like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%' ".
			"or et_name like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%' ".
			"or eks_code like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%' ".
			"or eks_name like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%'"
	);
	$getidpar1 = 0;
	$addsinfil = '';
	if ($getidpar){$getidpar1 = mysqli_num_rows($getidpar);}
	if ($getidpar1 > 0){
		while ($getidpar2 = mysqli_fetch_assoc($getidpar)){
			if ($addsinfil != ''){$addsinfil .= ',';}
			$addsinfil .= $getidpar2['lok_id'];
		}
		$sinfil .= "ong_et_id in (".$addsinfil.")";
	}else{
		$sinfil .= "0=1";
	}
}

if (isset( $filter['ON']['isianteks1'] )) {
	$isianteks1 = $filter['ON']['isianteks1'];
}else{
	$isianteks1 = '';
}
if ($isianteks1 != ''){
	if ($sinfil != ''){$sinfil .= ' and ';}
	$getidpar = mysqli_query($sqlcon,
		"select los_id ".
		"from lokasi_step ".
		"left outer join lokasi prop on prop.lok_id=los_prop_lok_id ".
		"left outer join lokasi kab on kab.lok_id=los_kab_lok_id ".
		"left outer join lokasi kec on kec.lok_id=los_kec_lok_id ".
		"where prop.lok_name like '%" . mysqli_real_escape_string( $sqlcon,$isianteks1 ) . "%' ".
			"or kab.lok_name like '%" . mysqli_real_escape_string( $sqlcon,$isianteks1 ) . "%' ".
			"or kec.lok_name like '%" . mysqli_real_escape_string( $sqlcon,$isianteks1 ) . "%'"
	);
	$getidpar1 = 0;
	$addsinfil = '';
	if ($getidpar){$getidpar1 = mysqli_num_rows($getidpar);}
	if ($getidpar1 > 0){
		while ($getidpar2 = mysqli_fetch_assoc($getidpar)){
			if ($addsinfil != ''){$addsinfil .= ',';}
			$addsinfil .= $getidpar2['los_id'];
		}
		$sinfil .= "(ong_origin_los_id in (".$addsinfil.") or ong_dest_los_id in (".$addsinfil."))";
	}else{
		$sinfil .= "0=1";
	}
}

if ($sinfil != '') {
	$sinfil = " where " . $sinfil;
} else {
	$sinfil = '';
}

if (isset( $filter['ON']['batasanjumlah'] )) {
	$batasanjumlah = intval( $filter['ON']['batasanjumlah'] );
}else{
	$batasanjumlah = 200;
}
$sinlimit = ' limit '.$batasanjumlah;
?>