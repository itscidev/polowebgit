<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/lokasi.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasilokasifilter.php';

$resview = numrowdb($sqlcon,"select lok_id from lokasi".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select * ".
		"from lokasi " .
		$sinfil . " group by lok_id" . $sinlimit
	);
	while ( $getdt1 = mysqli_fetch_assoc ( $getdt ) ) {
		
		$list .= "<tr id=\"tr" . $getdt1 ['lok_id'] . "\">" .
		"<td id=\"flok_name" . $getdt1 ['lok_id'] . "\">" . htmlentities($getdt1['lok_name']) . "</td>" .
		"<td id=\"flok_rajaongkir_id" . $getdt1 ['lok_id'] . "\">" . htmlentities($getdt1['lok_rajaongkir_id']) . "</td>" .
		"<td style=\"font-size:17px;\">" . 
			"<a href=\"#windata\" id=\"edit".$getdt1['lok_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data\" ".
			"onClick=\"prepareedit('".$getdt1['lok_id']."');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
			"<a href=\"#windelete\" id=\"delete".$getdt1['lok_id']."\" class=\"fancybox\" ".
			"title=\"Delete Data\" ".
			"onClick=\"preparedelete('".$getdt1['lok_id']."');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
		"</td>" . 
		"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"3\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}
?>