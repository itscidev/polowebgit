<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/preorder.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$arrstatus = array('Waiting','In Line','Canceled','Paid','In Delivery','Delivered');

include_once 'kalkulasipreorderfilter.php';

$resview = numrowdb($sqlcon,"select po_id from preorder".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$strsin = "select * ".
		"from preorder ".
		$sinfil . " group by po_id" . $sinlimit;
	$getdt = mysqli_query( $sqlcon,$strsin ) ;
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
	$status = $arrstatus[( $getdt1['po_status'] )];
	$orderdate = date("m/d/Y H:i:s",strtotime($getdt1['po_create']));
	$jsonorder = json_decode($getdt1['po_pesanan'],true);
	$pesanan = '';
	foreach ($jsonorder as $key => $value) {
		if ($pesanan != ''){$pesanan .= "<br>";}
		$pesanan .= $key.' ('.$value.' pack)';
	}
	$list .= "<tr id=\"tr" . $getdt1 ['po_id'] . "\">" . 
	"<td>" . $orderdate . "</td>" .
	"<td id=\"fpo_nama" . $getdt1 ['po_id'] . "\">" . htmlentities($getdt1 ['po_nama']) . "</td>" .
	"<td id=\"fpo_hp" . $getdt1 ['po_id'] . "\">" . htmlentities($getdt1 ['po_hp']) . "</td>" .
	"<td id=\"fpo_pesanan" . $getdt1 ['po_id'] . "\">" . $pesanan . "</td>" .
	"<td>".
		"<span id=\"fstatus" . $getdt1 ['po_id'] . "\">" . $status . "</span>".
	"</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fpo_status".$getdt1['po_id']."\" class=\"hide\">".$getdt1['po_status']."</p>".
		"<p id=\"fpo_email".$getdt1['po_id']."\" class=\"hide\">".$getdt1['po_email']."</p>".
		"<p id=\"fpo_keterangan".$getdt1['po_id']."\" class=\"hide\">".$getdt1['po_keterangan']."</p>".
		"<p id=\"fpo_alamat".$getdt1['po_id']."\" class=\"hide\">".$getdt1['po_alamat']."</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['po_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data ".htmlentities($getdt1['po_nama'])."\" ".
		"onClick=\"prepareedit('".$getdt1['po_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['po_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data ".htmlentities($getdt1['po_nama'])."\" ".
		"onClick=\"preparedelete('".$getdt1['po_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"6\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td></tr>";
}

$optpo_status = '';
foreach ($arrstatus as $index => $content){
	$disabled = '';
	// if ($content[1]==0 && $content[2]==0){
	// 	$disabled = ' disabled="disabled"';
	// }
	$optpo_status .= '<option value="' . $index . '"'.$disabled.'>' . $content . '</option>';
}
?>