<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/user.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (!isset($_POST['delete']) || $_POST['delete']!=1){
		$result['Error']='Confirmation box not checked!';
	}else{
		mysqli_query( $sqlcon,"delete from user where usr_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';
	}
}
if (isset($_POST['unlockid']) && $_POST['unlockid']>0 && !isset($result['Error'])){
	if (!isset($_POST['unlock']) || $_POST['unlock']!=1){
		$result['Error']='Confirmation box not checked!';
	}else{
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update user set ".
			"usr_blacklist=0,usr_blacklistunlock='',".
			"usr_update='".date("Y-m-d H:i:s")."',usr_updateby=".$cokidusr." ".
		"where usr_id=".$_POST['unlockid']
		);
		$result['Status']='Un-lock Success!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['usr_name'])){$usr_name=$_POST['usr_name'];}else{$usr_name='';}
	if (isset($_POST['usr_password'])){$usr_password=$_POST['usr_password'];}else{$usr_password='';}
	if (isset($_POST['usr_blacklist'])){
		$usr_blacklist=intval($_POST['usr_blacklist']);
	}else{$usr_blacklist=0;}
	if (isset($_POST['usr_accessip'])){$usr_accessip=$_POST['usr_accessip'];}else{$usr_accessip='';}
	if (isset($_POST['usr_gr_id'])){$usr_gr_id=intval($_POST['usr_gr_id']);}else{$usr_gr_id=0;}
	
	if ($usr_name==''){
		$result['Error']['userlogin']=1;
	}elseif (!filter_var($usr_name, FILTER_VALIDATE_EMAIL)){
		$result['Error']['userlogin']=1;
	}
	if ($usr_gr_id==0){$result['Error']['usr_gr_id']=1;}
	if ($_POST['formid']=='new'){
		if ($usr_password==''){$result['Error']['password']=1;}
		if (!isset($result['Error'])){
			$ceku = mysqli_num_rows(mysqli_query( $sqlcon,
				"select usr_id from user ".
				"where usr_name='".mysqli_real_escape_string( $sqlcon,$usr_name )."'"
			));
			if ($ceku>0){$result['Error']['userlogin']=1;}
		}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into user (".
		"usr_name,usr_password,".
		"usr_accessip,usr_blacklist,".
		"usr_createby,usr_create,usr_updateby,usr_update,".
		"usr_gr_id".
	") values (".
		"'".mysqli_real_escape_string( $sqlcon,$usr_name )."','".md5($usr_password)."',".
		"'".mysqli_real_escape_string( $sqlcon,$usr_accessip )."',".$usr_blacklist.",".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."',".
		$usr_gr_id.
	")";
	mysqli_query( $sqlcon,$strquery);
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	if ($usr_password!=''){
		$addsql=",usr_password='".md5($usr_password)."'";
	}else{
		$addsql='';
	}
	include 'configuration/cookie.php';
	mysqli_query( $sqlcon,
	"update user set ".
	"usr_name='".mysqli_real_escape_string( $sqlcon,$usr_name )."',usr_blacklist=".$usr_blacklist.
	",usr_accessip='".$usr_accessip."',usr_updateby=".$cokidusr.",usr_update='".date("Y-m-d H:i:s")."'".
	",usr_gr_id=".$usr_gr_id.$addsql." ".
	"where usr_id=".$_POST['formid']
	);
	$result['Status']='Update';
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>