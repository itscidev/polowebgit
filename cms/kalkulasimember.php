<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/member.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasimemberfilter.php';

$resview = numrowdb($sqlcon,"select mm_id from member".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$strsin = "select * ".
		"from member ".
		$sinfil . " group by mm_id" . $sinlimit;
	$getdt = mysqli_query( $sqlcon,$strsin ) ;
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
	$regdate = date("m/d/Y H:i:s",strtotime($getdt1['mm_create']));
	$lastdate = date("m/d/Y H:i:s",strtotime($getdt1['mm_lastlogin']));
	if ($getdt1['mm_foto']!='' && file_exists("../images/foto/".$getdt1['mm_foto'])){
		$image =  '<img src="'.$path.'/images/foto/'.$getdt1['mm_foto'].'" style="height:50px;">' . 
			'<br />' . formatBytes(filesize("../images/foto/".$getdt1['mm_foto']));
	}else{
		$image = '<img src="'.$path.'/images/default.png" style="width:50px;">';
	}
	if ($getdt1 ['mm_emailstatus']==1){
		$emailstatus = '<span class="label label-success">Email</span>';
	}else{
		$emailstatus = '<span class="label label-danger">Email</span>';
	}
	$bannedstatus = '';
	if ($getdt1 ['mm_banned']>7){
		$bannedstatus = '<span class="label label-danger">Banned</span>';
	}
	$list .= "<tr id=\"tr" . $getdt1 ['mm_id'] . "\">" . 
	"<td>" . $regdate . "</td>" .
	"<td>" . $lastdate . "</td>" .
	"<td id=\"fmm_email" . $getdt1 ['mm_id'] . "\">" . htmlentities($getdt1 ['mm_email']) . "</td>" .
	"<td id=\"fmm_name" . $getdt1 ['mm_id'] . "\">" . htmlentities($getdt1 ['mm_name']) . "</td>" .
	"<td id=\"fmm_image" . $getdt1 ['mm_id'] . "\">" . $image . "</td>" .
	"<td>".
		"<span id=\"femailstatus" . $getdt1 ['mm_id'] . "\">" . $emailstatus . "</span>".
		"<span id=\"fbannedstatus" . $getdt1 ['mm_id'] . "\">" . $bannedstatus . "</span>".
	"</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fmm_emailstatus".$getdt1['mm_id']."\" class=\"hide\">".$getdt1['mm_emailstatus']."</p>".
		"<p id=\"fmm_banned".$getdt1['mm_id']."\" class=\"hide\">".$getdt1['mm_banned']."</p>".
		"<p id=\"fmm_bannedreason".$getdt1['mm_id']."\" class=\"hide\">".htmlentities($getdt1['mm_bannedreason'])."</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['mm_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data ".htmlentities($getdt1['mm_name'])."\" ".
		"onClick=\"prepareedit('".$getdt1['mm_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['mm_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data ".htmlentities($getdt1['mm_name'])."\" ".
		"onClick=\"preparedelete('".$getdt1['mm_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"7\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}
?>