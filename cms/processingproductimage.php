<?php
include_once("configuration/connect.php");

function setMainImg($sqlcon,$parId,$chlId){
	$parId = intval($parId);
	$chlId = intval($chlId);
	$cekmain = mysqli_fetch_assoc(mysqli_query($sqlcon,
		"select pr_main_pi_id from product where pr_id=".$parId
	));
	if (isset($cekmain['pr_main_pi_id']) && $cekmain['pr_main_pi_id']==0){
		mysqli_query($sqlcon,
			"update product set pr_main_pi_id=".$chlId." where pr_id=".$parId
		);
	}
}

$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/productimage.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['setmainid']) && $_POST['setmainid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select pi_pr_id from product_image where pi_id=".$_POST['setmainid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		mysqli_query( $sqlcon,
			"update product set pr_main_pi_id=".$_POST['setmainid']." where pr_id=".$cekimg2['pi_pr_id']
		);
		$result['Status']='Update Success!';
	}else{
		$result['Error']='Image ID not exist!';
	}
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select pi_image from product_image where pi_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['pi_image']!='' && file_exists("../images/productitem/".$cekimg2['pi_image'])){
				unlink("../images/productitem/".$cekimg2['pi_image']);
			}
			mysqli_query( $sqlcon,"delete from product_image where pi_id=".$_POST['delid'] );
			$result['Status']='Delete Success!';
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['pi_status'])){$pi_status=intval($_POST['pi_status']);}else{$pi_status=0;}
	if (isset($_POST['pi_draft'])){$pi_draft=intval($_POST['pi_draft']);}else{$pi_draft=0;}
	if (isset($_POST['pi_sequence'])){$pi_sequence=intval($_POST['pi_sequence']);}else{$pi_sequence=0;}
	if (isset($_POST['pi_sku_id'])){$pi_sku_id=$_POST['pi_sku_id'];}else{$pi_sku_id=0;}
	
	if ($pi_sku_id==0 || $pi_sku_id=='0:0:0'){
		$result['Error']['pi_sku_id']=1;
	}elseif (substr_count($pi_sku_id,':') != 2){
		$result['Error']['pi_sku_id']=1;
	}else{
		$expsku = explode(":",$pi_sku_id);
		$pi_pr_id = $expsku[0];
		$pi_st_id = $expsku[1];
		$pi_ci_id = $expsku[2];
	}
	if ($_POST['formid']=='new' && (!isset($_FILES['pi_image']['name'])) ){
		$result['Error']['pi_image']=1;
	}elseif (count($_FILES['pi_image']['name'])==0){
		$result['Error']['pi_image']=1;
	}else{
		$lolos = 1;
		foreach ($_FILES['pi_image']['type'] as $type){
			if (strtolower($type)!='image/jpeg' && strtolower($type)!='image/png'){
				$lolos = 0;
			}
		}
		if ($lolos == 0){
			$result['Error']['pi_image']=1;
		}
	}
	
	$strrep = array("'",'"',' ','/','\\','#');
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		$strrep = array("'",'"',' ','/','\\');
		$urutan = 0;
		foreach ($_FILES['pi_image']['name'] as $index => $imgname){
			if ($pi_sequence == 0){$sequence = $index;}else{$sequence = $pi_sequence;}
			mysqli_query( $sqlcon,
			"insert into product_image (".
				"pi_status,pi_draft,pi_sequence,pi_pr_id,pi_st_id,pi_ci_id,".
				"pi_image,".
				"pi_createby,pi_create,pi_updateby,pi_update".
			") values (".
				$pi_status.",".$pi_draft.",".$sequence.",".$pi_pr_id.",".$pi_st_id.",".$pi_ci_id.",'',".
				$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
			")"
			);
			$insertid = mysqli_insert_id( $sqlcon );
			
			$pi_image = str_replace($strrep,'', $insertid.'-'.$imgname);
			move_uploaded_file(
				$_FILES['pi_image']['tmp_name'][$index],'../images/productitem/'.$pi_image
			);
			
			mysqli_query( $sqlcon,
				"update product_image set pi_image='".mysqli_real_escape_string( $sqlcon,$pi_image )."' ".
				"where pi_id=".$insertid
			);
			if ($urutan==0){setMainImg($sqlcon,$pi_pr_id,$insertid);}
			$urutan++;
		}
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		if (isset($_FILES['pi_image']['name'][0]) && $_FILES['pi_image']['name'][0]!=''){
			$cekimg = mysqli_query( $sqlcon,
				"select pi_image from product_image where pi_id=".intval($_POST['formid'])
			);
			$cekimg1 = mysqli_num_rows($cekimg);
			if ($cekimg1 > 0){
				$cekimg2 = mysqli_fetch_array($cekimg);
				if ($cekimg2['pi_image']!='' && file_exists("../images/productitem/".$cekimg2['pi_image'])){
					unlink("../images/productitem/".$cekimg2['pi_image']);
				}
			}
			$pi_image = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['pi_image']['name'][0]);
			move_uploaded_file(
				$_FILES['pi_image']['tmp_name'][0],'../images/productitem/'.$pi_image
			);
			$addsql = ",pi_image='".mysqli_real_escape_string( $sqlcon,$pi_image )."'";
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['pi_image']['size'][0]);
		
			$result['pi_image']='<img src="../images/productitem/'.$pi_image.'" class="itemimg"><br />' . $fz;
		}else{
			$addsql = '';
		}
		mysqli_query( $sqlcon,
		"update product_image set ".
			"pi_status=".$pi_status.",".
			"pi_draft=".$pi_draft.",".
			"pi_sequence=".$pi_sequence.",".
			"pi_pr_id=".$pi_pr_id.",".
			"pi_st_id=".$pi_st_id.",".
			"pi_ci_id=".$pi_ci_id.",".
			"pi_update='".date("Y-m-d H:i:s")."',".
			"pi_updateby=".$cokidusr.$addsql." ".
		"where pi_id=".intval($_POST['formid'])
		);
		setMainImg($sqlcon,$pi_pr_id,intval($_POST['formid']));
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>