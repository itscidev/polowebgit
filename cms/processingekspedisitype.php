<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/ekspedisitype.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from ekspedisi_type where et_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';		
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['et_code'])){$et_code=trim($_POST['et_code']);}else{$et_code='';}
	if (isset($_POST['et_name'])){$et_name=trim($_POST['et_name']);}else{$et_name='';}
	if (isset($_POST['et_eks_id'])){$et_eks_id=intval($_POST['et_eks_id']);}else{$et_eks_id=0;}
	if (isset($_POST['et_kelipatanberat'])){$et_kelipatanberat=intval($_POST['et_kelipatanberat']);}else{$et_kelipatanberat=0;}
	if (isset($_POST['et_status'])){$et_status=intval($_POST['et_status']);}else{$et_status=0;}
	
	if ($et_code==''){
		$result['Error']['et_code']=1;
	}
	if ($et_name==''){
		$result['Error']['et_name']=1;
	}
	if ($et_eks_id==0){
		$result['Error']['et_eks_id']=1;
	}
	if ($et_kelipatanberat==0){
		$result['Error']['et_kelipatanberat']=1;
	}
	if (!isset($result['Error'])){
		$addcek = '';
		if ($_POST['formid']!='new'){
			$addcek = ' and et_id!='.intval($_POST['formid']);
		}
		$cekexist = mysqli_num_rows(mysqli_query($sqlcon,
			"select et_id from ekspedisi_type ".
			"where lower(et_code)='".mysqli_real_escape_string($sqlcon,strtolower($et_code))."' ".
				"and et_eks_id=".$et_eks_id.$addcek
		));
		if ($cekexist!=0){
			$result['Error']['et_code']=1;
		}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into ekspedisi_type (".
		"et_status,et_eks_id,et_kelipatanberat,".
		"et_code,et_name,".
		"et_createby,et_create,et_updateby,et_update".
	") values (".
		$et_status.",".$et_eks_id.",".$et_kelipatanberat.",".
		"'".mysqli_real_escape_string($sqlcon,$et_code)."','".mysqli_real_escape_string($sqlcon,$et_name)."',".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$qryupd = "update ekspedisi_type set ".
		"et_status=".$et_status.",et_eks_id=".$et_eks_id.",et_kelipatanberat=".$et_kelipatanberat.",".
		"et_code='".mysqli_real_escape_string($sqlcon,$et_code)."',".
		"et_name='".mysqli_real_escape_string($sqlcon,$et_name)."',".
		"et_updateby=".$cokidusr.",et_update='".date("Y-m-d H:i:s")."' ".
	"where et_id=".$_POST['formid'];
	mysqli_query( $sqlcon,$qryupd );
	$result['Status']='Update';
//	$result['QryUpd']=$qryupd;
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>