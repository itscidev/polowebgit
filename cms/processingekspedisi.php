<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/ekspedisi.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select eks_logo from ekspedisi where eks_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['eks_logo']!='' && file_exists("../images/ekspedisi/".$cekimg2['eks_logo'])){
				unlink("../images/ekspedisi/".$cekimg2['eks_logo']);
			}
			mysqli_query( $sqlcon,"delete from ekspedisi where eks_id=".$_POST['delid'] );
			$result['Status']='Delete Success!';		
		}elseif (isset($_POST['deleteimage']) && $_POST['deleteimage']==1){
			if ($cekimg2['eks_logo']!='' && file_exists("../images/ekspedisi/".$cekimg2['eks_logo'])){
				unlink("../images/ekspedisi/".$cekimg2['eks_logo']);
			}
			mysqli_query( $sqlcon,"update ekspedisi set eks_logo='' where eks_id=".$_POST['delid'] );
			$result['eks_logo']='<img src="../images/default.png" style="height:50px;">';
			$result['Status']='Delete Image Success!';
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}else{
		$result['Error']='Data already deleted!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['eks_name'])){$eks_name=trim($_POST['eks_name']);}else{$eks_name='';}
	if (isset($_POST['eks_code'])){$eks_code=trim($_POST['eks_code']);}else{$eks_code='';}
	if (isset($_FILES['eks_logo']['name']) && !(
		strtolower($_FILES['eks_logo']['type'])=='image/jpeg' || strtolower($_FILES['eks_logo']['type'])=='image/png'
	)){
		$result['Error']['eks_logo']=1;
	}
	
	if ($eks_code==''){$result['Error']['eks_code']=1;}
	if ($eks_name==''){$result['Error']['eks_name']=1;}
	if (!isset($result['Error'])){
		$addcek = '';
		if ($_POST['formid']!='new'){
			$addcek = ' and eks_id!='.intval($_POST['formid']);
		}
		$cekexist = mysqli_num_rows(mysqli_query($sqlcon,
			"select eks_id from ekspedisi ".
			"where lower(eks_code)='".mysqli_real_escape_string($sqlcon,strtolower($eks_code))."'".$addcek
		));
		if ($cekexist!=0){
			$result['Error']['eks_code']=1;
		}
	}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,"insert into ekspedisi (".
			"eks_name,eks_logo,".
			"eks_code,".
			"eks_createby,eks_create,eks_updateby,eks_update".
		") values (".
			"'".mysqli_real_escape_string( $sqlcon,$eks_name )."','',".
			"'".mysqli_real_escape_string( $sqlcon,$eks_code )."',".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")");
		if (isset($_FILES['eks_logo']['name']) && $_FILES['eks_logo']['name']!=''){
			$insertid = mysqli_insert_id( $sqlcon );
			$strrep = array("'",'"',' ','/','\\');
			$eks_logo = str_replace($strrep,'', $insertid.'-'.$_FILES['eks_logo']['name']);
			move_uploaded_file(
				$_FILES['eks_logo']['tmp_name'],'../images/ekspedisi/'.$eks_logo
			);
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['eks_logo']['size']);
				
			$result['eks_logo']='<img src="../images/ekspedisi/'.$eks_logo.'" style="height:24px;"><br />' . $fz;
			mysqli_query( $sqlcon,
				"update ekspedisi set eks_logo='".mysqli_real_escape_string( $sqlcon,$eks_logo )."' ".
				"where eks_id=".$insertid
			);
		}else{
			$eks_logo = '';
			$result['eks_logo']='<img src="../images/default.png" style="height:24px;">';
		}
		//$result['QryErr']=mysqli_error($sqlcon);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		if (isset($_FILES['eks_logo']['name']) && $_FILES['eks_logo']['name']!=''){
			$cekimg = mysqli_query( $sqlcon,
				"select eks_logo from ekspedisi where eks_id=".intval($_POST['formid'])
			);
			$cekimg1 = mysqli_num_rows($cekimg);
			if ($cekimg1 > 0){
				$cekimg2 = mysqli_fetch_array($cekimg);
				if ($cekimg2['eks_logo']!='' && file_exists("../images/ekspedisi/".$cekimg2['eks_logo'])){
					unlink("../images/ekspedisi/".$cekimg2['eks_logo']);
				}
			}
			$strrep = array("'",'"',' ','/','\\');
			$eks_logo = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['eks_logo']['name']);
			move_uploaded_file(
				$_FILES['eks_logo']['tmp_name'],'../images/ekspedisi/'.$eks_logo
			);
			$addsql = ",eks_logo='".mysqli_real_escape_string( $sqlcon,$eks_logo )."'";
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['eks_logo']['size']);
		
			$result['eks_logo']='<img src="../images/ekspedisi/'.$eks_logo.'" style="height:30px;"><br />' . $fz;
		}else{
			$addsql = '';
		}
		mysqli_query( $sqlcon,
		"update ekspedisi set ".
			"eks_name='".mysqli_real_escape_string( $sqlcon,$eks_name )."',".
			"eks_code='".mysqli_real_escape_string( $sqlcon,$eks_code )."',".
			"eks_update='".date("Y-m-d H:i:s")."',".
			"eks_updateby=".$cokidusr.$addsql." ".
		"where eks_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>