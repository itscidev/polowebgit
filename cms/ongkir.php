<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo $headtitle;?>
    <!-- Favicons -->
    <?php echo $favicon;?>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Ionicons 
    <link rel="stylesheet" href="css/ionicons.min.css">
	<!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.css">
    <!-- Theme style -->
	<link rel="stylesheet" href="dist/css/AdminLTE.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	         folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/jquery.dataTables.min.css">
    <link rel="stylesheet" href="plugins/datatables/dataTables.fixedColumns.min.css">
	
	<!-- Fancybox simple pop up -->
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/ajaxpost.css" />
	<link rel="stylesheet" type="text/css" href="css/mycss.css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

	<?php include_once 'mainheader.php';?>
	<!-- =============================================== -->
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Price & Destination Controller
				<small>Set shipment origin-destination, estimation and price data</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li class="active">Ongkir</li>
			</ol>
		</section>

		<!-- Main content -->
        <section class="content">
        	<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="box box-primary">
		                <div class="box-header">
		                  <h3 class="box-title"><?php echo $notifview;?></h3>
		                  <div class="box-tools">
		                  	<a href="#winsync" class="fancybox" title="Syncronize data with Raja Ongkir" onclick="preparesync();">
		                  	<button type="button" class="btn btn-warning">Sync Loc</button>
		                  	</a>
		                  	<a href="#windata" class="fancybox" title="Add New Data" onclick="preparenew();">
		                  	<button type="button" class="btn btn-warning">New Data</button>
		                  	</a>
		                  	<button type="button" class="btn btn-warning" title="Open Filter Data"
							onclick="openfancy('filter.php?page=ON');">Filter</button>
		                  </div>
		                </div><!-- /.box-header -->
		                <div class="box-body">
		                  <table id="datalist" class="table table-bordered table-striped" style="width: 100%;">
		                  	<thead>
		                    <tr>
		                      <th>Expedition Type</th>
		                      <th>Origin</th>
		                      <th>Destination</th>
		                      <th>Estimation</th>
		                      <th>Price</th>
		                      <th></th>
		                    </tr>
		                    </thead>
		                    <tbody><?php echo $list;?></tbody>
		                  </table>
		                </div><!-- /.box-body -->
					</div><!-- /.box -->
	            </div>
        	</div>
        </section><!-- /.content -->
		
		</div>
		<!-- /.content-wrapper -->

		<?php include_once 'footer.php';?>
		<div id="windata" class="box box-warning" style="display: none;">
			<div class="box-body">
				<form name="form1" id="form1" role="form" action="processingongkir.html" method="post" 
				enctype="multipart/form-data">
					<!-- Select2 -->
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Expedition Type</label>
							<select name="ong_et_id" id="ong_et_id" class="form-control select2" style="width: 100%;">
								<?php echo $optong_et_id;?>
							</select>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Origin</label>
							<select name="ong_origin_los_id" id="ong_kab_lok_id" class="form-control select2" style="width: 100%;">
								<?php echo $optong_los_id;?>
							</select>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Destination</label>
							<select name="ong_dest_los_id" id="ong_dest_los_id" class="form-control select2" style="width: 100%;">
								<?php echo $optong_los_id;?>
							</select>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Estimation</label>
							<input name="ong_estimasi" id="ong_estimasi" type="text" class="form-control" placeholder="Enter...">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Price</label>
							<input name="ong_price" id="ong_price" type="text" class="form-control" placeholder="Enter...">
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 box-footer">
						<div class="progress">
							<div class="bar"></div>
							<div class="percent">0%</div>
						</div>
						<input name="formid" id="formid" type="hidden" value="new">
						<button type="submit" class="btn btn-warning">Submit</button>
					</div>
				</form>
			</div>
		</div>

		<div id="windelete" class="box box-danger" style="display: none;">
			<div class="box-body">
				<!-- general form elements disabled -->
				<form name="form2" id="form2" role="form" action="processingongkir.html" method="post" 
				enctype="multipart/form-data">
					<!-- checkbox -->
					<div class="form-group" align="center">
						<div class="checkbox">
							<label> 
							<input name="delete" id="delete" type="checkbox" value="1" /> 
							Are you sure want to delete this data?
							</label><!-- 
							<p id="deldesc"></p> -->
						</div>
					</div>
					<div class="form-group" align="center">
						<input name="delid" id="delid" type="hidden" value="" />
						<div class="progress">
							<div class="bar"></div>
							<div class="percent">0%</div>
						</div>
						<button class="btn btn-danger" type="submit">Delete</button>
					</div>
				</form>
			</div>
		</div>
		
		<div id="winsync" class="box box-warning" style="display: none;">
			<div class="box-body">
				<!-- general form elements disabled -->
				<form name="form3" id="form3" role="form" action="processingongkir.html" method="post" 
				enctype="multipart/form-data">
					<!-- checkbox -->
					<div class="form-group" align="center">
						<div class="checkbox">
							<label> 
							<input name="syncronize" id="syncronize" type="checkbox" value="1" /> 
							Are you sure want to syncronize with Raja Ongkir?
							</label><!-- 
							<p id="deldesc"></p> -->
						</div>
					</div>
					<div class="form-group" align="center">
						<input name="syncid" id="syncid" type="hidden" value="new" />
						<div class="progress">
							<div class="bar"></div>
							<div class="percent">0%</div>
						</div>
						<button class="btn btn-warning" type="submit">Sync Now</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="plugins/select2/select2.full.min.js"></script>
	<!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="dist/js/demo.js"></script>
	<script src="js/ajaxpost.js"></script>
	<script src="js/ongkir.js"></script>
	
</body>
</html>