<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/bannertext.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$buttondraft = draftStatus();

include_once 'kalkulasibannertextfilter.php';

$resview = numrowdb($sqlcon,"select bt_id from banner_text".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$strsin = "select bt.*,GROUP_CONCAT(bts.bts_ca_id) groupcaid ".
		"from banner_text bt ".
		"left outer join banner_text_showon bts on bts_bt_id=bt_id" .
		$sinfil . " group by bt_id" . $sinlimit;
	$getdt = mysqli_query( $sqlcon,$strsin ) ;
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
	if ($getdt1 ['bt_status']==1){
		$status = '<span class="label label-success">Active</span>';
	}else{
		$status = '<span class="label label-danger">Pasif</span>';
	}
	if ($getdt1 ['bt_draft']==1){
		$draft = '<span class="label label-warning">Draft</span>';
	}else{
		$draft = '';
	}
	$strfrom = date("m/d/Y H:i:s",strtotime($getdt1['bt_from']));
	$strto = date("m/d/Y H:i:s",strtotime($getdt1['bt_to']));
	$list .= "<tr id=\"tr" . $getdt1 ['bt_id'] . "\">" . 
	"<td id=\"fbt_from" . $getdt1 ['bt_id'] . "\">" . $strfrom . "</td>" .
	"<td id=\"fbt_to" . $getdt1 ['bt_id'] . "\">" . $strto . "</td>" .
	"<td id=\"fbt_title" . $getdt1 ['bt_id'] . "\">" . htmlentities($getdt1 ['bt_title']) . "</td>" .
	"<td id=\"fbt_anchor" . $getdt1 ['bt_id'] . "\">" . htmlentities($getdt1 ['bt_anchor']) . "</td>" .
	"<td>".
		"<span id=\"fstatus" . $getdt1 ['bt_id'] . "\">" . $status . "</span>".
		"<span id=\"fdraft" . $getdt1 ['bt_id'] . "\">" . $draft . "</span>".
	"</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fbt_status".$getdt1['bt_id']."\" class=\"hide\">".$getdt1['bt_status']."</p>".
		"<p id=\"fbt_draft".$getdt1['bt_id']."\" class=\"hide\">".$getdt1['bt_draft']."</p>".
		"<p id=\"fbts_ca_id".$getdt1['bt_id']."\" class=\"hide\">".$getdt1['groupcaid']."</p>".
		"<p id=\"fbt_sequence".$getdt1['bt_id']."\" class=\"hide\">".$getdt1['bt_sequence']."</p>".
		"<p id=\"fbt_link".$getdt1['bt_id']."\" class=\"hide\">".$getdt1['bt_link']."</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['bt_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data ".htmlentities($getdt1['bt_title'])."\" ".
		"onClick=\"prepareedit('".$getdt1['bt_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['bt_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data ".htmlentities($getdt1['bt_title'])."\" ".
		"onClick=\"preparedelete('".$getdt1['bt_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"6\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td></tr>";
}

$bt_from = date("m/d/Y H:i:s");
$bt_to = date("m/d/Y H:i:s",mktime(date("H"),date("i"),date("s"),date("m"),date("d")+15,date("Y")));

//list page show on
$showonpage = '';
$data = @mysqli_query($sqlcon,"select * from category where ca_ca_id=0 order by ca_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
	if ($data2['ca_status']==0){
		$disable = ' disabled="disabled"';
	}
	$idcat = $data2['ca_id'];
	$showonpage .= '<div class="col-md-3 col-sm-6 col-xs-12 form-group">'.
		'<div class="checkbox">'.
			'<label>'.
				'<input name="bts_ca_id['.$idcat.']" id="bts_ca_id'.$idcat.'" type="checkbox" '.
				'value="1" class="bts_ca_id"'.$disable.'>'.$data2['ca_name'].
			'</label>'.
		'</div>'.
	'</div>';
}}
?>