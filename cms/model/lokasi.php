<?php
function lokasi_autoiu($sqlcon,$name,$rajaongkirid=0){
	$name = trim($name);
	$rajaongkirid = intval($rajaongkirid);
	
	$field = "lok_id,lok_rajaongkir_id";
	$join = '';
	$where = "lower(lok_name)='".mysqli_real_escape_string($sqlcon,strtolower($name))."'";
	$cekex = lokasi_s($sqlcon,$field,$join,$where);
	if (is_array($cekex) && $cekex[0]==0){
		$ins = lokasi_i($sqlcon,$name,$rajaongkirid);
		return $ins;
	}elseif (is_array($cekex) && $cekex[0]>0){
		$cekex1 = mysqli_fetch_assoc($cekex[1]);
		if ($cekex1['lok_rajaongkir_id'] != $rajaongkirid){
			$set = "lok_rajaongkir_id=".$rajaongkirid;
			lokasi_u($sqlcon,$set,$cekex1['lok_id']);
		}
		return array($cekex1['lok_id']);
	}elseif (gettype($cekex) == 'string'){
		return mysqli_error($sqlcon);
	}
}
function lokasi_i($sqlcon,$name,$rajaongkirid=0){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$name = trim($name);
	$rajaongkirid = intval($rajaongkirid);
	
	$qry = @mysqli_query($sqlcon,
		"insert into lokasi (".
			"lok_name,lok_rajaongkir_id,".
			"lok_create,lok_createby,lok_update,lok_updateby".
		") values (".
			"'".mysqli_real_escape_string($sqlcon,$name)."',".$rajaongkirid.",".
			"'".date("Y-m-d H:i:s")."',".$idupdater.",'".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function lokasi_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update lokasi set lok_update='".date("Y-m-d H:i:s")."',lok_updateby=".$idupdater.",".
			$set." where lok_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function lokasi_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from lokasi where lok_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function lokasi_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from lokasi".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>