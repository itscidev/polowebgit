<?php
function ekspedisi_autoiu($sqlcon,$code,$name='',$logo=''){
	$code = trim($code);
	$name = trim($name);
	
	$field = "eks_id,eks_name";
	$join = '';
	$where = "lower(eks_code)='".mysqli_real_escape_string($sqlcon,strtolower($code))."'";
	$cekex = ekspedisi_s($sqlcon,$field,$join,$where);
	if (is_array($cekex) && $cekex[0]==0){
		$ins = ekspedisi_i($sqlcon,$code,$name,$logo);
		return $ins;
	}elseif (is_array($cekex) && $cekex[0]>0){
		$cekex1 = mysqli_fetch_assoc($cekex[1]);
		if ($cekex1['eks_name'] != $name){
			$set = "eks_name='".mysqli_real_escape_string($sqlcon,$name)."'";
			ekspedisi_u($sqlcon,$set,$cekex1['eks_id']);
		}
		return array($cekex1['eks_id']);
	}elseif (gettype($cekex) == 'string'){
		return mysqli_error($sqlcon);
	}
}
function ekspedisi_i($sqlcon,$code,$name='',$logo=''){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$code = trim($code);
	$name = trim($name);
	
	$qry = @mysqli_query($sqlcon,
		"insert into ekspedisi (".
			"eks_logo,eks_code,".
			"eks_name,".
			"eks_create,eks_createby,eks_update,eks_updateby".
		") values (".
			"'".mysqli_real_escape_string($sqlcon,$logo)."','".mysqli_real_escape_string($sqlcon,$code)."',".
			"'".mysqli_real_escape_string($sqlcon,$name)."',".
			"'".date("Y-m-d H:i:s")."',".$idupdater.",'".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ekspedisi_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update ekspedisi set eks_update='".date("Y-m-d H:i:s")."',eks_updateby=".$idupdater.",".
			$set." where eks_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ekspedisi_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from ekspedisi where eks_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ekspedisi_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from ekspedisi".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>