<?php
function ekspedisitype_autoiu($sqlcon,$parid,$code,$name='',$status=0,$kelipatanberat=1000){
	$name = trim($name);
	$code = trim($code);
	$status = intval($status);
	$parid = intval($parid);
	$kelipatanberat = intval($kelipatanberat);
	
	$field = "et_id,et_name";
	$join = '';
	$where = "lower(et_code)='".mysqli_real_escape_string($sqlcon,strtolower($code))."' and et_eks_id=".$parid;
	$cekex = ekspedisitype_s($sqlcon,$field,$join,$where);
	if (is_array($cekex) && $cekex[0]==0){
		$ins = ekspedisitype_i($sqlcon,$parid,$code,$name,$status,$kelipatanberat);
		return $ins;
	}elseif (is_array($cekex) && $cekex[0]>0){
		$cekex1 = mysqli_fetch_assoc($cekex[1]);
		if ($cekex1['et_name']!=$name){
			$set = "et_name='".mysqli_real_escape_string($sqlcon,$name)."'";
			ekspedisitype_u($sqlcon,$set,$cekex1['et_id']);
		}
		return array($cekex1['et_id']);
	}elseif (gettype($cekex) == 'string'){
		return mysqli_error($sqlcon);
	}
}
function ekspedisitype_i($sqlcon,$parid,$code,$name='',$status=0,$kelipatanberat=1000){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$name = trim($name);
	$code = trim($code);
	$status = intval($status);
	$parid = intval($parid);
	$kelipatanberat = intval($kelipatanberat);
	
	$qry = @mysqli_query($sqlcon,
		"insert into ekspedisi_type (".
			"et_eks_id,et_status,".
			"et_name,et_code,et_kelipatanberat,".
			"et_create,et_createby,et_update,et_updateby".
		") values (".
			$parid.",".$status.",".
			"'".mysqli_real_escape_string($sqlcon,$name)."','".mysqli_real_escape_string($sqlcon,$code)."',".$kelipatanberat.",".
			"'".date("Y-m-d H:i:s")."',".$idupdater.",'".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ekspedisitype_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update ekspedisi_type set et_update='".date("Y-m-d H:i:s")."',et_updateby=".$idupdater.",".
			$set." where et_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ekspedisitype_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from ekspedisi_type where et_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ekspedisitype_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from ekspedisi_type".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>