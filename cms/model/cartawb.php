<?php
function cartawb_autoiu($sqlcon,$crid=0,$result=''){
	$crid = intval($crid);
	$result = trim($result);
	
	$field = "cw_id,cw_result";
	$join = '';
	$where = "cw_cr_id=".$crid;
	$cekex = cartawb_s($sqlcon,$field,$join,$where);
	if (is_array($cekex) && $cekex[0]==0){
		$ins = cartawb_i($sqlcon,$crid,$result);
		return $ins;
	}elseif (is_array($cekex) && $cekex[0]>0){
		$cekex1 = mysqli_fetch_assoc($cekex[1]);
		if ($cekex1['cw_result']!=$result){
			$set = "cw_result='".mysqli_real_escape_string($sqlcon,$result)."'";
			cartawb_u($sqlcon,$set,$cekex1['cw_id']);
		}
		return array($cekex1['cw_id']);
	}elseif (gettype($cekex) == 'string'){
		return mysqli_error($sqlcon);
	}
}
function cartawb_i($sqlcon,$crid=0,$result=''){
	$crid = intval($crid);
	$result = trim($result);
	
	$qry = @mysqli_query($sqlcon,
		"insert into cart_awb (".
			"cw_result,cw_cr_id,".
			"cw_create,cw_update".
		") values (".
			"'".mysqli_real_escape_string($sqlcon,$result)."',".$crid.",".
			"'".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."'".
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cartawb_u($sqlcon,$set,$id){
	$qry = @mysqli_query($sqlcon,
		"update cart_awb set cw_update='".date("Y-m-d H:i:s")."'".
			$set." where cw_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cartawb_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from cart_awb where cw_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cartawb_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from cart_awb".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>