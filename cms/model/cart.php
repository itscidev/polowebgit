<?php
function cart_i($sqlcon,$mmaid=0,$ongid=0,$ongweight=0,$ongprice=0,$discount=0,$vchid=0,$ptid=0,$comment='',$status=0){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$mmaid = intval($mmaid);
	$ongid = intval($ongid);
	$ongweight = intval($ongweight);
	$ongprice = intval($ongprice);
	$discount = intval($discount);
	$vchid = intval($vchid);
	$ptid = intval($ptid);
	$comment = trim($comment);
	//0='Waiting',1='Canceled',2='errorPayment',3='successPayment',4='inDelivery',5='delivered'
	$status = intval($status);
	
	$qry = @mysqli_query($sqlcon,
		"insert into cart (".
			"cr_comment,cr_mma_id,cr_ong_id,cr_ong_weight,cr_ong_price,cr_discount,cr_vch_id,cr_pt_id,cr_status,".
			"cr_create,cr_update,cr_updateby".
		") values (".
			"'".mysqli_real_escape_string($sqlcon,$comment)."',".$mmaid.",".$ongid.",".$ongweight.",".$ongprice.",".$discount.",".$vchid.",".$ptid.",".$status.",".
			"'".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cart_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update cart set cr_update='".date("Y-m-d H:i:s")."',cr_updateby=".$idupdater.",".
			$set." where cr_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cart_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from cart where cr_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cart_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from cart".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>