<?php
function paymenttype_i($sqlcon,$vendor,$name='',$logo='',$status=0){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_devendor($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$vendor = trim($vendor);
	$name = trim($name);
	$logo = trim($logo);
	$status = intval($status);
	
	$qry = @mysqli_query($sqlcon,
		"insert into paymenttype (".
			"pt_logo,pt_vendor,".
			"pt_name,pt_status,".
			"pt_create,pt_createby,pt_update,pt_updateby".
		") values (".
			"'".mysqli_real_escape_string($sqlcon,$logo)."','".mysqli_real_escape_string($sqlcon,$vendor)."',".
			"'".mysqli_real_escape_string($sqlcon,$name)."',".$status.",".
			"'".date("Y-m-d H:i:s")."',".$idupdater.",'".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function paymenttype_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_devendor($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update paymenttype set pt_update='".date("Y-m-d H:i:s")."',pt_updateby=".$idupdater.",".
			$set." where pt_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function paymenttype_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from paymenttype where pt_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function paymenttype_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from paymenttype".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>