<?php
function cartdetail_i($sqlcon,$crid=0,$pvid=0,$price=0,$qty=0){
	$crid = intval($crid);
	$pvid = intval($pvid);
	$price = intval($price);
	$qty = intval($qty);
	
	$qry = @mysqli_query($sqlcon,
		"insert into cart_detail (".
			"crd_cr_id,crd_pv_id,crd_price,crd_qty".
		") values (".
			$crid.",".$pvid.",".$price.",".$qty.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cartdetail_u($sqlcon,$set,$id){
	$qry = @mysqli_query($sqlcon,
		"update cart_detail set ".$set." where crd_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cartdetail_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from cart_detail where crd_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function cartdetail_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from cart_detail".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>