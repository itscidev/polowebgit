<?php
function productimage_i(
	$sqlcon,$prid,$stid,$ciid,$image='',$status=0,$draft=0,$sequence=0
){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$image = trim($image);
	$status = intval($status);
	$draft = intval($draft);
	$prid = intval($prid);
	$stid = intval($stid);
	$ciid = intval($ciid);
	$sequence = intval($sequence);
	
	$qry = @mysqli_query($sqlcon,
		"insert into product_image (".
			"pi_pr_id,pi_st_id,pi_ci_id,pi_status,pi_draft,pi_sequence,".
			"pi_image,".
			"pi_create,pi_createby,pi_update,pi_updateby".
		") values (".
			$prid.",".$stid.",".$ciid.",".$status.",".$draft.",".$sequence.",".
			"'".mysqli_real_escape_string($sqlcon,$image)."',".
			"'".date("Y-m-d H:i:s")."',".$idupdater.",'".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function productimage_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update product_image set pi_update='".date("Y-m-d H:i:s")."',pi_updateby=".$idupdater.",".
			$set." where pi_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function productimage_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from product_image where pi_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function productimage_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	//echo "select ".$field." from product_image".$addsql; 
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from product_image".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>