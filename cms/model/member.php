<?php
function member_i(
	$sqlcon,$email,$password,$banned=0,$bannedreason='',$name='',$foto='',$lastlogin='0000-00-00 00:00:00',
	$idupdater=0,$emailstatus=0
){
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$email = strtolower(trim($email));
	$emailstatus = intval($emailstatus);
	$password = trim($password);
	$banned = intval($banned);
	$bannedreason = trim($bannedreason);
	$name = trim($name);
	$foto = trim($foto);
	$lastlogin = trim($lastlogin);
	$idupdater = intval($idupdater);
	
	$qry = @mysqli_query($sqlcon,
		"insert into member (".
			"mm_email,mm_password,".
			"mm_banned,mm_bannedreason,".
			"mm_name,mm_foto,".
			"mm_lastlogin,mm_emailstatus,".
			"mm_create,mm_update,mm_updateby".
		") values (".
			"'".mysqli_real_escape_string($sqlcon,$email)."','".mysqli_real_escape_string($sqlcon,$password)."',".
			$banned.",'".mysqli_real_escape_string($sqlcon,$bannedreason)."',".
			"'".mysqli_real_escape_string($sqlcon,$name)."','".mysqli_real_escape_string($sqlcon,$foto)."',".
			"'".mysqli_real_escape_string($sqlcon,$lastlogin)."',".$emailstatus.",".
			"'".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function member_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update member set mm_update='".date("Y-m-d H:i:s")."',mm_updateby=".$idupdater.",".
			$set." where mm_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function member_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from member where mm_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function member_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from member".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>