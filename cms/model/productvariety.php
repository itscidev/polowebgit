<?php
function productvariety_i(
	$sqlcon,$prid,$stid,$szid,$ciid,$name='',$artikel='',$qty=0,$price=0,$status=0,$draft=0
){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$name = trim($name);
	$artikel = trim($artikel);
	$status = intval($status);
	$prid = intval($prid);
	
	$qry = @mysqli_query($sqlcon,
		"insert into product_variety (".
			"pv_pr_id,pv_st_id,pv_sz_id,pv_ci_id,pv_qty,pv_price,pv_status,pv_draft,".
			"pv_name,pv_artikel,".
			"pv_create,pv_createby,pv_update,pv_updateby".
		") values (".
			$prid.",".$stid.",".$szid.",".$ciid.",".$qty.",".$price.",".$status.",".$draft.",".
			"'".mysqli_real_escape_string($sqlcon,$name)."','".mysqli_real_escape_string($sqlcon,$artikel)."',".
			"'".date("Y-m-d H:i:s")."',".$idupdater.",'".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function productvariety_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update product_variety set pv_update='".date("Y-m-d H:i:s")."',pv_updateby=".$idupdater.",".
			$set." where pv_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function productvariety_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from product_variety where pv_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function productvariety_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from product_variety".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>