<?php
function lokasistep_autoiu($sqlcon,$propid=0,$kabid=0,$kecid=0,$zip=''){
	$propid = intval($propid);
	$kabid = intval($kabid);
	$kecid = intval($kecid);
	$zip = trim($zip);
	
	$field = "los_id,los_zip";
	$join = '';
	$where = "los_prop_lok_id=".$propid." and los_kab_lok_id=".$kabid." and los_kec_lok_id=".$kecid;
	$cekex = lokasistep_s($sqlcon,$field,$join,$where);
	if (is_array($cekex) && $cekex[0]==0){
		$ins = lokasistep_i($sqlcon,$propid,$kabid,$kecid,$zip);
		return $ins;
	}elseif (is_array($cekex) && $cekex[0]>0){
		$cekex1 = mysqli_fetch_assoc($cekex[1]);
		if ($cekex1['los_zip']!=$zip){
			$set = "los_zip='".mysqli_real_escape_string($sqlcon,$zip)."'";
			lokasistep_u($sqlcon,$set,$cekex1['los_id']);
		}
		return array($cekex1['los_id']);
	}elseif (gettype($cekex) == 'string'){
		return mysqli_error($sqlcon);
	}
}
function lokasistep_i($sqlcon,$propid=0,$kabid=0,$kecid=0,$zip=''){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$propid = intval($propid);
	$kabid = intval($kabid);
	$kecid = intval($kecid);
	$zip = trim($zip);
	
	$qry = @mysqli_query($sqlcon,
		"insert into lokasi_step (".
			"los_prop_lok_id,los_kab_lok_id,los_kec_lok_id,los_zip,".
			"los_create,los_createby,los_update,los_updateby".
		") values (".
			$propid.",".$kabid.",".$kecid.",'".mysqli_real_escape_string($sqlcon,$zip)."',".
			"'".date("Y-m-d H:i:s")."',".$idupdater.",'".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function lokasistep_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update lokasi_step set los_update='".date("Y-m-d H:i:s")."',los_updateby=".$idupdater.",".
			$set." where los_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function lokasistep_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from lokasi_step where los_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function lokasistep_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from lokasi_step".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>