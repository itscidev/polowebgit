<?php
function ongkir_autoiu($sqlcon,$etid=0,$originid=0,$destid=0,$estimasi='',$price=0){
	$etid = intval($etid);
	$originid = intval($originid);
	$destid = intval($destid);
	$estimasi = trim($estimasi);
	$price = intval($price);
	
	$field = "ong_id,ong_estimasi,ong_price,ong_update";
	$join = '';
	$where = "ong_et_id=".$etid." and ong_origin_los_id=".$originid." and ong_dest_los_id=".$destid;
	$cekex = ongkir_s($sqlcon,$field,$join,$where);
	if (is_array($cekex) && $cekex[0]==0){
		$ins = ongkir_i($sqlcon,$etid,$originid,$destid,$estimasi,$price);
		return $ins;
	}elseif (is_array($cekex) && $cekex[0]>0){
		$cekex1 = mysqli_fetch_assoc($cekex[1]);
		if ($cekex1['ong_estimasi']!=$estimasi || $cekex1['ong_price']!=$price || strtotime($cekex1['ong_update'])<=strtotime(date("Y-m-d")." 00:00:00")){
			$set = "ong_estimasi='".mysqli_real_escape_string($sqlcon,$estimasi)."',ong_price=".$price;
			ongkir_u($sqlcon,$set,$cekex1['ong_id']);
		}
		return array($cekex1['ong_id']);
	}elseif (gettype($cekex) == 'string'){
		return mysqli_error($sqlcon);
	}
}
function ongkir_i($sqlcon,$etid=0,$originid=0,$destid=0,$estimasi='',$price=0){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	$etid = intval($etid);
	$originid = intval($originid);
	$destid = intval($destid);
	$estimasi = trim($estimasi);
	$price = intval($price);
	
	$qry = @mysqli_query($sqlcon,
		"insert into ongkir (".
			"ong_et_id,ong_origin_los_id,ong_dest_los_id,ong_estimasi,ong_price,".
			"ong_create,ong_update,ong_updateby".
		") values (".
			$etid.",".$originid.",".$destid.",'".mysqli_real_escape_string($sqlcon,$estimasi)."',".$price.",".
			"'".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ongkir_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update ongkir set ong_update='".date("Y-m-d H:i:s")."',ong_updateby=".$idupdater.",".
			$set." where ong_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ongkir_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from ongkir where ong_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function ongkir_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from ongkir".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>