<?php
function memberaddress_i($sqlcon,$visid=0,$saveas,$name='',$phone='',$losid=0,$zip='',$address='',$deleted=0){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}elseif (isset($_COOKIE['loginvst'])){
		$expcok = explode(':',$_COOKIE["loginvst"]);
		$idupdater = $expcok[0];
	}
	$visid = intval($visid);
	$saveas = trim($saveas);
	$name = trim($name);
	$phone = trim($phone);
	$losid = intval($losid);
	$zip = intval($zip);
	$address = trim($address);
	$deleted = intval($deleted);
	
	$qry = @mysqli_query($sqlcon,
		"insert into member_address (".
			"mma_mm_id,mma_saveas,mma_los_id,mma_zip,".
			"mma_name,mma_phone,".
			"mma_address,mma_deleted,".
			"mma_create,mma_createby,mma_update,mma_updateby".
		") values (".
			$visid.",'".mysqli_real_escape_string($sqlcon,$saveas)."',".$losid.",".$zip.",".
			"'".mysqli_real_escape_string($sqlcon,$name)."','".mysqli_real_escape_string($sqlcon,$phone)."',".
			"'".mysqli_real_escape_string($sqlcon,$address)."',".$deleted.",".
			"'".date("Y-m-d H:i:s")."',".$idupdater.",'".date("Y-m-d H:i:s")."',".$idupdater.
		")"
	);
	if ($qry){
		$qry1 = mysqli_insert_id($sqlcon);
		return array($qry1);
	}else{
		return mysqli_error($sqlcon);
	}
}
function memberaddress_u($sqlcon,$set,$id){
	$idupdater = 0;
	if (isset($_COOKIE['login'])){
		$expcok = explode(';',base64_decode($_COOKIE['login']));
		$idupdater = $expcok[0];
	}elseif (isset($_COOKIE['loginvst'])){
		$expcok = explode(':',$_COOKIE["loginvst"]);
		$idupdater = $expcok[0];
	}
	
	$qry = @mysqli_query($sqlcon,
		"update member_address set mma_update='".date("Y-m-d H:i:s")."',mma_updateby=".$idupdater.",".
			$set." where mma_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function memberaddress_d($sqlcon,$id){
	$qry = @mysqli_query($sqlcon,
		"delete from member_address where mma_id=".$id
	);
	if ($qry){
		return array($id);
	}else{
		return mysqli_error($sqlcon);
	}
}
function memberaddress_s($sqlcon,$field,$join='',$where='',$group='',$order='',$limit='',$offset=''){
	$addsql = '';
	if ($join != ''){$addsql .= ' '.$join;}
	if ($where != ''){$addsql .= ' where '.$where;}
	if ($group != ''){$addsql .= ' group by '.$group;}
	if ($order != ''){$addsql .= ' order by '.$order;}
	if ($limit != ''){$addsql .= ' limit '.$limit;}
	if ($offset != ''){$addsql .= ' offset '.$offset;}
	$qry = @mysqli_query($sqlcon,
		"select ".$field." from member_address".$addsql
	);
	if ($qry){
		$qry1 = mysqli_num_rows($qry);
		return array($qry1,$qry);
	}else{
		return mysqli_error($sqlcon);
	}
}
?>