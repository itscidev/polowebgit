<?php
if (!isset($_COOKIE['login']) || $_COOKIE['login'] == ''){
	header("location:login.html");
	exit();
}else{
	include_once 'configuration/connect.php';
	include_once 'configuration/function.php';
	include_once 'configuration/cookie.php';
	
	//special case for favicon
	$HTTP_HOST = array(
		'cms.poloindonesia.com'
	);
	
	if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
		$path = 'http://poloindonesia.com';
	}else{
		$path = '..';
	}
	$favicon = '<link rel="shortcut icon" type="image/png" href="'.$path.'/images/favicon.png">';
	unset($HTTP_HOST);
	//special case for title head
	$headtitle = '<title>CMS POLO Indonesia</title>';
	
	$getaks = mysqli_fetch_array(mysqli_query(
		$sqlcon,"select * from user	where usr_id = ".$cokidusr
	));
	$ucreate = explode('-',substr($getaks['usr_create'],0,10));
	$ucreate = date("j M Y",mktime(0,0,0,$ucreate[1],$ucreate[2],$ucreate[0]));
	$upicture = $getaks['usr_foto'];
	if ($upicture==null || $upicture==''){
		$upicture = 'default.jpg';
	}
	$dlastlog = explode(' ',$getaks['usr_lastlogin']);
	$dlastlog1 = explode('-',$dlastlog[0]);
	$lastlog = date("j M Y ".$dlastlog[1],mktime(0,0,0,$dlastlog1[1],$dlastlog1[2],$dlastlog1[0]));
	
	//get all access name
	$aksstr = "select ac_name,CASE ".
		"WHEN gr_id is null THEN 0 ".
		"ELSE 1 ".
		"END akses ".
	"from access ".
	"left outer join group_access on ga_ac_id=ac_id ".
	"left outer join `group` on gr_id=ga_gr_id ".
		"and gr_id=" . $getaks['usr_gr_id'] ." ".
	"group by 1,2";
	$getaddaks = @mysqli_query( $sqlcon,$aksstr );
	$getaddaks1 = 0;
	if ($getaddaks){$getaddaks1 = mysqli_num_rows ( $getaddaks );}
	if ($getaddaks1 > 0) {while ( $getaddaks2 = mysqli_fetch_array( $getaddaks ) ) {
		$getaks[( $getaddaks2['ac_name'] )] = $getaddaks2['akses'];
	}}
	
	// construct menu listing
	$posnow = $_SERVER['SCRIPT_NAME'];
	$exppos = explode ( '/', $posnow );
	$exppos = $exppos[ (count($exppos)-1) ];

	$mClass = json_decode(classMenu(array( 
		'index.php' 
	)),true);
	$mainnav = '<li'.$mClass['index.php'].'>'.
		'<a href="index.html"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>'.
	'</li>';
	
	//menu User Account
	if ($getaks['User']==1 || $getaks['Access']==1 || $getaks['Group']==1){
		
		$classparent = '';
		$mClass = json_decode(classMenu(array( 
			'user.php','access.php','group.php' 
		)),true);
		if ($mClass['parent'] != '') {
			$classparent = ' active';
		}
		
		$mainnav .= '<li class="treeview' . $classparent . '">' .
			'<a href="#">' .
				'<i class="fa fa-lock"></i> <span>Account</span> '.
				'<i class="fa fa-angle-left pull-right"></i>'.
			'</a>' .
			'<ul class="treeview-menu">';
		if ($getaks['User'] == 1){
			$mainnav.= '<li' . $mClass['user.php'] . '>'.
				'<a href="user.html"><i class="fa fa-circle-o"></i> <span>User Account</span></a>'.
				'</li>';
		}
		if ($getaks['Access'] == 1){
			$mainnav .= '<li' . $mClass['access.php'] . '>' .
				'<a href="access.html"><i class="fa fa-circle-o"></i> <span>Access</span></a>' .
				'</li>';
		}
		if ($getaks['Group'] == 1){
			$mainnav .= '<li' . $mClass['group.php'] . '>' .
				'<a href="group.html"><i class="fa fa-circle-o"></i> <span>Group</span></a>' .
				'</li>';
		}
		$mainnav .= '</ul></li>';
	}
	
	//General Pages, title, keyword & description
	if ($getaks['GeneralPages']==1){
	
		$classparent = '';
		$mClass = json_decode(classMenu(array( 
			'pagesgeneral.php','pagesoverwrite.php' 
		)),true);
		if ($mClass['parent'] != '') {
			$classparent = ' active';
		}
	
		$mainnav .= '<li class="treeview' . $classparent . '">' .
			'<a href="#">' .
				'<i class="fa fa-clone"></i> <span>Pages</span> '.
				'<i class="fa fa-angle-left pull-right"></i>'.
			'</a>' .
			'<ul class="treeview-menu">';
		if ($getaks['GeneralPages'] == 1){
			$mainnav.= '<li' . $mClass['pagesgeneral.php'] . '>'.
				'<a href="pagesgeneral.html"><i class="fa fa-circle-o"></i> <span>General</span></a>'.
				'</li>';
			$mainnav.= '<li' . $mClass['pagesoverwrite.php'] . '>'.
				'<a href="pagesoverwrite.html"><i class="fa fa-circle-o"></i> <span>Overwrite</span></a>'.
				'</li>';
		}
		$mainnav .= '</ul></li>';
	}
	
	//Banner
	if ($getaks['Banner']==1){

		$classparent = '';
		$mClass = json_decode(classMenu(array(
			'bannerhomeslider.php','bannerhomequickmenu.php','bannervideopromo.php','bannerseasons.php',
			'bannertext.php'
		)),true);
		if ($mClass['parent'] != '') {
			$classparent = ' active';
		}
		
		$mainnav .= '<li class="treeview' . $classparent . '">' .
			'<a href="#">' .
				'<i class="fa fa-picture-o"></i> <span>Banner</span> '.
				'<i class="fa fa-angle-left pull-right"></i>'.
			'</a>' .
			'<ul class="treeview-menu">';
		if ($getaks['Banner'] == 1){
			$mainnav.= '<li' . $mClass['bannerhomeslider.php'] . '>'.
				'<a href="bannerhomeslider.html"><i class="fa fa-circle-o"></i> <span>Top Slider</span></a>'.
				'</li>';
			$mainnav.= '<li' . $mClass['bannervideopromo.php'] . '>'.
				'<a href="bannervideopromo.html"><i class="fa fa-circle-o"></i> <span>Video Promo</span></a>'.
				'</li>';
			$mainnav.= '<li' . $mClass['bannerhomequickmenu.php'] . '>'.
				'<a href="bannerhomequickmenu.html"><i class="fa fa-circle-o"></i> <span>Quick Menu</span></a>'.
				'</li>';
			$mainnav.= '<li' . $mClass['bannerseasons.php'] . '>'.
				'<a href="bannerseasons.html"><i class="fa fa-circle-o"></i> <span>Seasons Style</span></a>'.
				'</li>';
			$mainnav.= '<li' . $mClass['bannertext.php'] . '>'.
				'<a href="bannertext.html"><i class="fa fa-circle-o"></i> <span>Text Caraosel</span></a>'.
				'</li>';
		}
		$mainnav .= '</ul></li>';
	}

	//Auxiliary
	if ($getaks['Auxiliary']==1){
	
		$classparent = '';
		$mClass = json_decode(classMenu(array(
			'store.php'
		)),true);
		if ($mClass['parent'] != '') {
			$classparent = ' active';
		}
	
		$mainnav .= '<li class="treeview' . $classparent . '">' .
			'<a href="#">' .
				'<i class="fa fa-archive"></i> <span>Auxiliary</span> '.
				'<i class="fa fa-angle-left pull-right"></i>'.
			'</a>' .
			'<ul class="treeview-menu">';
		if ($getaks['Auxiliary'] == 1){
			$mainnav.= '<li' . $mClass['store.php'] . '>'.
				'<a href="store.html"><i class="fa fa-circle-o"></i> <span>Store</span></a>'.
			'</li>';
		}
		$mainnav .= '</ul></li>';
	}
	
	//menu Category Trees & Product
	if ($getaks['ProductCategory']==1 || $getaks['Product']==1){
	
		$classparent = '';
		$mClass = json_decode(classMenu(array( 
			'category.php','categorytree.php' 
		)),true);
		if ($mClass['parent'] != '') {
			$classparent = ' active';
		}
		$mClass1 = json_decode(classMenu(array(
			'style.php','size.php'
		)),true);
		if ($mClass1['parent'] != '') {
			$classparent = ' active';
		}
		$mClass2 = json_decode(classMenu(array(
			'color.php','coloritem.php'
		)),true);
		if ($mClass2['parent'] != '') {
			$classparent = ' active';
		}
		$mClass3 = json_decode(classMenu(array(
			'product.php','productvariety.php',
			'productimage.php','productrelated.php','productstore.php'
		)),true);
		if ($mClass3['parent'] != '') {
			$classparent = ' active';
		}
	
		$mainnav .= '<li class="treeview' . $classparent . '">' .
			'<a href="#">' .
				'<i class="fa fa-tags"></i> <span>Catalog</span> '.
				'<i class="fa fa-angle-left pull-right"></i>'.
			'</a>' .
			'<ul class="treeview-menu">';
		if ($getaks['ProductCategory'] == 1){
			$mainnav .= '<li class="' . $mClass['parent'] . '">' .
				'<a href="#">' .
					'<i class="fa fa-circle-o"></i> <span>Category</span> ' .
					'<i class="fa fa-angle-left pull-right"></i>' .
				'</a>' .
				'<ul class="treeview-menu">' .
				'<li' . $mClass['category.php'] . '>' .
					'<a href="category.html"><i class="fa fa-circle-o"></i> <span>Tree Setting</span></a>'.
				'</li>' .
				'<li' . $mClass['categorytree.php'] . '>' .
					'<a href="categorytree.html"><i class="fa fa-circle-o"></i> <span>View Tree</span></a>'.
				'</li>' . 
				'</ul>' .
			'</li>';
		}
		if ($getaks['Product'] == 1){
// 			$classproduct = '';
// 			if ($mClass1['product.php'] != ''){
// 				$classproduct = $mClass1['product.php'];
// 			}elseif ($mClass1['productvariety.php'] != ''){
// 				$classproduct = $mClass1['productvariety.php'];
// 			}elseif ($mClass1['productimage.php'] != ''){
// 				$classproduct = $mClass1['productimage.php'];
// 			}
			$mainnav .= '<li' . $mClass1['style.php'] . '><a href="style.html">' .
				'<i class="fa fa-circle-o"></i> <span>Style</span>' .
			'</a></li>' .
			'<li' . $mClass1['size.php'] . '><a href="size.html">' .
				'<i class="fa fa-circle-o"></i> <span>Size</span>' .
			'</a></li>';
			$mainnav .= '<li class="' . $mClass2['parent'] . '">' .
				'<a href="#">' .
					'<i class="fa fa-circle-o"></i> <span>Color</span> ' .
					'<i class="fa fa-angle-left pull-right"></i>' .
				'</a>' .
				'<ul class="treeview-menu">' .
				'<li' . $mClass2['color.php'] . '><a href="color.html">' .
					'<i class="fa fa-circle-o"></i> <span>Basic</span>' .
				'</a></li>' .
				'<li' . $mClass2['coloritem.php'] . '><a href="coloritem.html">' .
					'<i class="fa fa-circle-o"></i> <span>Specified</span>' .
				'</a></li>' .
				'</ul>' .
			'</li>';
			$mainnav .= '<li class="' . $mClass3['parent'] . '">' .
				'<a href="#">' .
					'<i class="fa fa-circle-o"></i> <span>Product</span> ' .
					'<i class="fa fa-angle-left pull-right"></i>' .
				'</a>' .
				'<ul class="treeview-menu">' .
				'<li' . $mClass3['product.php'] . '><a href="product.html">' .
					'<i class="fa fa-circle-o"></i> <span>Item</span>' .
				'</a></li>' .
				'<li' . $mClass3['productvariety.php'] . '><a href="productvariety.html">' .
					'<i class="fa fa-circle-o"></i> <span>Variety</span>' .
				'</a></li>' .
				'<li' . $mClass3['productimage.php'] . '><a href="productimage.html">' .
					'<i class="fa fa-circle-o"></i> <span>Image</span>' .
				'</a></li>' .
				'<li' . $mClass3['productrelated.php'] . '><a href="productrelated.html">' .
					'<i class="fa fa-circle-o"></i> <span>Related</span>' .
				'</a></li>' .
				'<li' . $mClass3['productstore.php'] . '><a href="productstore.html">' .
					'<i class="fa fa-circle-o"></i> <span>Store Located</span>' .
				'</a></li>' .
				'</ul>' .
			'</li>';
		}
		$mainnav .= '</ul></li>';
	}

	//Event
	if ($getaks['Event']==1){
	
		$classparent = '';
		$mClass = json_decode(classMenu(array(
			'event.php'
		)),true);
		if ($mClass['parent'] != '') {
			$classparent = ' active';
		}
	
		$mainnav .= '<li class="treeview' . $classparent . '">' .
			'<a href="#">' .
				'<i class="fa fa-calendar-o"></i> <span>Event</span> '.
				'<i class="fa fa-angle-left pull-right"></i>'.
			'</a>' .
			'<ul class="treeview-menu">';
		if ($getaks['Event'] == 1){
			$mainnav.= '<li' . $mClass['event.php'] . '>'.
					'<a href="event.html"><i class="fa fa-circle-o"></i> <span>Event</span></a>'.
				'</li>';
		}
		$mainnav .= '</ul></li>';
	}
	
	//menu Contact
	if ($getaks['Contact'] == 1){
		$classparent = '';
		$mClass = json_decode(classMenu(array( 
			'contact.php','contactsmtp.php' 
		)),true);
		
		if ($mClass['parent'] != '') {
			$classparent = ' active';
		}
		$mainnav .= '<li class="treeview' . $classparent . '">' .
			'<a href="#">' .
				'<i class="fa fa-map-marker"></i> <span>Contact</span> '.
				'<i class="fa fa-angle-left pull-right"></i>'.
			'</a>' .
			'<ul class="treeview-menu">';
		$mainnav .= '<li' . $mClass['contact.php'] . '>' .
			'<a href="contact.html"><i class="fa fa-circle-o"></i> <span>Company Info</span></a>' .
		'</li>';
		$mainnav .= '<li' . $mClass['contactsmtp.php'] . '>' .
			'<a href="contactsmtp.html"><i class="fa fa-circle-o"></i> <span>Mail SMTP</span></a>' .
		'</li>';
		$mainnav .= '</ul></li>';
	}
	
	//overwrite access right
	$aksfileuploader = 0;
	if ($getaks['Product']==1 || $getaks['Event']==1){
		$aksfileuploader = 1;
	}
	//protect user random access
	//list accessable file :
	$listpagear = array(
		'login.php' => array('',1),
		'index.php' => array('',1),
		'filter.php' => array('kalkulasifilter.php',1),
		'profile.php' => array('kalkulasiprofile.php',1),
		'user.php' => array('kalkulasiuser.php',$getaks['User']),
		'access.php' => array ('kalkulasiaccess.php',$getaks['Access']),
		'group.php' => array ('kalkulasigroup.php',$getaks['Group']),
		'pagesgeneral.php' => array('kalkulasipagesgeneral.php',$getaks['GeneralPages']),
		'pagesoverwrite.php' => array('kalkulasipagesoverwrite.php',$getaks['GeneralPages']),
		'contact.php' => array('kalkulasicontact.php',$getaks['Contact']),
		'contactsmtp.php' => array('kalkulasicontactsmtp.php',$getaks['Contact']),
		'category.php' => array('kalkulasicategory.php',$getaks['ProductCategory']),
		'categorytree.php' => array('kalkulasicategorytree.php',$getaks['ProductCategory']),
		'size.php' => array('kalkulasisize.php',$getaks['Product']),
		'style.php' => array('kalkulasistyle.php',$getaks['Product']),
		'color.php' => array('kalkulasicolor.php',$getaks['Product']),
		'coloritem.php' => array('kalkulasicoloritem.php',$getaks['Product']),
		'product.php' => array('kalkulasiproduct.php',$getaks['Product']),
		'productvariety.php' => array('kalkulasiproductvariety.php',$getaks['Product']),
		'productimage.php' => array('kalkulasiproductimage.php',$getaks['Product']),
		'productrelated.php' => array('kalkulasiproductrelated.php',$getaks['Product']),
		'productstore.php' => array('kalkulasiproductstore.php',$getaks['Product']),
		'fileuploader.php' => array('kalkulasifileuploader.php',$aksfileuploader),
		'bannerhomeslider.php' => array('kalkulasibannerhomeslider.php',$getaks['Banner']),
		'bannerhomequickmenu.php' => array('kalkulasibannerhomequickmenu.php',$getaks['Banner']),
		'bannervideopromo.php' => array('kalkulasibannervideopromo.php',$getaks['Banner']),
		'bannerseasons.php' => array('kalkulasibannerseasons.php',$getaks['Banner']),
		'bannertext.php' => array('kalkulasibannertext.php',$getaks['Banner']),
		'event.php' => array('kalkulasievent.php',$getaks['Event']),
		'store.php' => array('kalkulasistore.php',$getaks['Auxiliary'])
	);
	if (array_key_exists ( $exppos, $listpagear )) {
		if ($listpagear[$exppos][1] == 0) {
			header ( 'location:index.html' );
			exit ();
		} elseif ($listpagear[$exppos][0] != '') {
			include_once $listpagear[$exppos][0];
		}
	} else {
		include 'signout.php';
	}
	if (isset($sqlcon)){mysqli_close($sqlcon);}
}
?>