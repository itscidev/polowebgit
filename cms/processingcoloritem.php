<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/coloritem.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select ci_image from color_item where ci_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['ci_image']!='' && file_exists("../images/productcolor/".$cekimg2['ci_image'])){
				unlink("../images/productcolor/".$cekimg2['ci_image']);
			}
			mysqli_query( $sqlcon,"delete from color_item where ci_id=".$_POST['delid'] );
			$result['Status']='Delete Success!';		
		}elseif (isset($_POST['deleteimage']) && $_POST['deleteimage']==1){
			if ($cekimg2['ci_image']!='' && file_exists("../images/productcolor/".$cekimg2['ci_image'])){
				unlink("../images/productcolor/".$cekimg2['ci_image']);
			}
			mysqli_query( $sqlcon,"update color_item set ci_image='' where ci_id=".$_POST['delid'] );
			$result['ci_image']='<img src="../images/default.png" style="height:50px;">';
			$result['Status']='Delete Image Success!';
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}else{
		$result['Error']='Data already deleted!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['ci_sequence'])){$ci_sequence=intval($_POST['ci_sequence']);}else{$ci_sequence=0;}
	if (isset($_POST['ci_cl_id'])){$ci_cl_id=intval($_POST['ci_cl_id']);}else{$ci_cl_id=0;}
	
	if (isset($_FILES['ci_image']['name']) && !(
		strtolower($_FILES['ci_image']['type'])=='image/jpeg' || strtolower($_FILES['ci_image']['type'])=='image/png'
	)){
		$result['Error']['ci_image']=1;
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into color_item (".
		"ci_cl_id,".
		"ci_image,ci_sequence,".
		"ci_createby,ci_create,ci_updateby,ci_update".
	") values (".
		$ci_cl_id.",".
		"'',".$ci_sequence.",".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	if (isset($_FILES['ci_image']['name']) && $_FILES['ci_image']['name']!=''){
		$insertid = mysqli_insert_id( $sqlcon );
		$strrep = array("'",'"',' ','/','\\');
		$ci_image = str_replace($strrep,'', $insertid.'-'.$_FILES['ci_image']['name']);
		move_uploaded_file(
			$_FILES['ci_image']['tmp_name'],'../images/productcolor/'.$ci_image
		);
			
		include_once 'configuration/function.php';
		$fz = formatBytes($_FILES['ci_image']['size']);
			
		$result['ci_image']='<img src="../images/productcolor/'.$ci_image.'" style="height:30px;"><br />' . $fz;
		mysqli_query( $sqlcon,
			"update color_item set ci_image='".mysqli_real_escape_string( $sqlcon,$ci_image )."' ".
			"where ci_id=".$insertid
		);
	}else{
		$ci_image = '';
		$result['ci_image']='<img src="../images/default.png" style="height:30px;">';
	}
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	if (isset($_FILES['ci_image']['name']) && $_FILES['ci_image']['name']!=''){
		$cekimg = mysqli_query( $sqlcon,
			"select ci_image from color_item where ci_id=".intval($_POST['formid'])
		);
		$cekimg1 = mysqli_num_rows($cekimg);
		if ($cekimg1 > 0){
			$cekimg2 = mysqli_fetch_array($cekimg);
			if ($cekimg2['ci_image']!='' && file_exists("../images/productcolor/".$cekimg2['ci_image'])){
				unlink("../images/productcolor/".$cekimg2['ci_image']);
			}
		}
		$strrep = array("'",'"',' ','/','\\');
		$ci_image = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['ci_image']['name']);
		move_uploaded_file(
			$_FILES['ci_image']['tmp_name'],'../images/productcolor/'.$ci_image
		);
		$addsql = ",ci_image='".mysqli_real_escape_string( $sqlcon,$ci_image )."'";
			
		include_once 'configuration/function.php';
		$fz = formatBytes($_FILES['ci_image']['size']);
	
		$result['ci_image']='<img src="../images/productcolor/'.$ci_image.'" style="height:30px;"><br />' . $fz;
	}else{
		$addsql = '';
	}
	$qryupd = "update color_item set ".
	"ci_cl_id=".$ci_cl_id.",ci_sequence=".$ci_sequence.",".
	"ci_updateby=".$cokidusr.",ci_update='".date("Y-m-d H:i:s")."'".$addsql." ".
	"where ci_id=".$_POST['formid'];
	mysqli_query( $sqlcon,$qryupd );
	$result['Status']='Update';
//	$result['QryUpd']=$qryupd;
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>