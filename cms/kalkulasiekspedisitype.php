<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/ekspedisitype.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasiekspedisitypefilter.php';

$resview = numrowdb($sqlcon,"select et_id from ekspedisi_type".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$qrysel = "select ekspedisi_type.*,eks_name,eks_code from ekspedisi_type ".
			"left outer join ekspedisi on eks_id=et_eks_id".$sinfil.$sinlimit;
	//echo $qrysel;
	$getdt = mysqli_query( $sqlcon,$qrysel );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
		if ($getdt1 ['et_status']==1){
			$status = '<span class="label label-success">Active</span>';
		}else{
			$status = '<span class="label label-danger">Pasif</span>';
		}	
	$list .= "<tr id=\"tr" . $getdt1['et_id'] . "\">" .
		"<td id=\"fet_eks_name" . $getdt1 ['et_id'] . "\">".htmlentities($getdt1['eks_code'].'-'.$getdt1['eks_name']) ."</td>" .
		"<td id=\"fet_code" . $getdt1 ['et_id'] . "\">" . htmlentities($getdt1['et_code']) . "</td>" . 
		"<td id=\"fet_name" . $getdt1 ['et_id'] . "\">" . htmlentities($getdt1['et_name']) . "</td>" .
		"<td id=\"fet_kelipatanberat" . $getdt1 ['et_id'] . "\">" . $getdt1['et_kelipatanberat'] . "</td>" .
		"<td>".
			"<span id=\"fstatus" . $getdt1 ['et_id'] . "\">" . $status . "</span>".
		"</td>" .
		"<td style=\"font-size:17px;\">" .
			"<p id=\"fet_status".$getdt1['et_id']."\" class=\"hide\">".$getdt1['et_status']."</p>".
			"<p id=\"fet_eks_id".$getdt1['et_id']."\" class=\"hide\">".$getdt1['et_eks_id']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['et_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data\" " .
			"onClick=\"prepareedit('" . $getdt1['et_id'] . "');\"><i class=\"fa fa-edit\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['et_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data\" " .
			"onClick=\"preparedelete('" . $getdt1['et_id'] . "');\"><i class=\"fa fa-times-circle\"></i></a>" .
		"</td>" .
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"6\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

//list ekspedisi
$optet_eks_id = '';
$data = @mysqli_query($sqlcon,"select * from ekspedisi order by eks_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
// 	if ($data2['et_status']==0){
// 		$disable = ' disabled="disabled"';
// 	}
	$optet_eks_id .= '<option value="' . $data2['eks_id'] . '"'.$disable.'>'.
		htmlentities( $data2['eks_code'].'-'.$data2['eks_name'] ) . 
	'</option>';
}}
?>