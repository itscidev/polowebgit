<?php
if (substr_count($_SERVER['SCRIPT_NAME'], '/filter.php') == 0){
	header('location:index.html');
	exit();
}

//format cookie filter (json) : 
// array('page'=>array($_POST));
$filter=array(); 
if (isset($_COOKIE['filter'])){
	setcookie("filter", $_COOKIE['filter'], time()+18000);
	$filter = json_decode($_COOKIE['filter'],true);
}

$asal = '';
if (isset($_GET['page']) && $_GET['page']!=''){
	$asal = $_GET['page'];
}
$titlepage = '';
$isiantekslabel = $isianteks = '';
$isianteks1label = $isianteks1 = '';
$pilihangandalabel = $pilihangandaopt = '';
$batasanjumlah = 200;

//Payment Type
if (isset($_GET['page']) && $_GET['page'] == 'PT'){
	$titlepage = 'Filter Page Payment Type';

	$isiantekslabel = 'Name / Vendor';
	if ( isset($filter['PT']['isianteks']) && $filter['PT']['isianteks']!='' ){
		$isianteks = $filter['PT']['isianteks'];
	}
	
	if ( isset($filter['PT']['batasanjumlah']) && $filter['PT']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['PT']['batasanjumlah'];
	}
}
//Member Order
if (isset($_GET['page']) && $_GET['page'] == 'MOR'){
	$titlepage = 'Filter Page Member Order';

	$isiantekslabel = 'Name';
	if ( isset($filter['MOR']['isianteks']) && $filter['MOR']['isianteks']!='' ){
		$isianteks = $filter['MOR']['isianteks'];
	}
	
	if ( isset($filter['MOR']['batasanjumlah']) && $filter['MOR']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['MOR']['batasanjumlah'];
	}
}
//Member
if (isset($_GET['page']) && $_GET['page'] == 'MM'){
	$titlepage = 'Filter Page Member Confirmation';

	$isiantekslabel = 'Name / Email';
	if ( isset($filter['MM']['isianteks']) && $filter['MM']['isianteks']!='' ){
		$isianteks = $filter['MM']['isianteks'];
	}
	
	if ( isset($filter['MM']['batasanjumlah']) && $filter['MM']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['MM']['batasanjumlah'];
	}
}
//Pre-order Confirmation
if (isset($_GET['page']) && $_GET['page'] == 'PRC'){
	$titlepage = 'Filter Page Pre-order Confirmation';

	$isiantekslabel = 'Name / Email / Hp';
	if ( isset($filter['PRC']['isianteks']) && $filter['PRC']['isianteks']!='' ){
		$isianteks = $filter['PRC']['isianteks'];
	}
	
	if ( isset($filter['PRC']['batasanjumlah']) && $filter['PRC']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['PRC']['batasanjumlah'];
	}
}
//Pre-order
if (isset($_GET['page']) && $_GET['page'] == 'PRO'){
	$titlepage = 'Filter Page Pre-order';

	$isiantekslabel = 'Name / Email / Hp';
	if ( isset($filter['PRO']['isianteks']) && $filter['PRO']['isianteks']!='' ){
		$isianteks = $filter['PRO']['isianteks'];
	}
	
	if ( isset($filter['PRO']['batasanjumlah']) && $filter['PRO']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['PRO']['batasanjumlah'];
	}
}
//Ongkir
if (isset($_GET['page']) && $_GET['page'] == 'ON'){
	$titlepage = 'Filter Page Ongkir';

	$isiantekslabel = 'Expedition Type';
	if ( isset($filter['ON']['isianteks']) && $filter['ON']['isianteks']!='' ){
		$isianteks = $filter['ON']['isianteks'];
	}
	
	$isianteks1label = 'Origins / Destination';
	if ( isset($filter['ON']['isianteks1']) && $filter['ON']['isianteks1']!='' ){
		$isianteks1 = $filter['ON']['isianteks'];
	}

	if ( isset($filter['ON']['batasanjumlah']) && $filter['ON']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['ON']['batasanjumlah'];
	}
}
//Lokasi Step
if (isset($_GET['page']) && $_GET['page'] == 'LS'){
	$titlepage = 'Filter Page Location Step';

	$isiantekslabel = 'Propinsi / Kabupaten / Kecamatan';
	if ( isset($filter['LS']['isianteks']) && $filter['LS']['isianteks']!='' ){
		$isianteks = $filter['LS']['isianteks'];
	}

	if ( isset($filter['LS']['batasanjumlah']) && $filter['LS']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['LS']['batasanjumlah'];
	}
}
//Lokasi
if (isset($_GET['page']) && $_GET['page'] == 'LO'){
	$titlepage = 'Filter Page Master Location';

	$isiantekslabel = 'Name';
	if ( isset($filter['LO']['isianteks']) && $filter['LO']['isianteks']!='' ){
		$isianteks = $filter['LO']['isianteks'];
	}

	if ( isset($filter['LO']['batasanjumlah']) && $filter['LO']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['LO']['batasanjumlah'];
	}
}
//Product Store Located
if (isset($_GET['page']) && $_GET['page'] == 'PS'){
	$titlepage = 'Filter Page Product Store Located';

	$isiantekslabel = 'Variant Name / Variant Artikel / Store Code / Store Name';
	if ( isset($filter['PS']['isianteks']) && $filter['PS']['isianteks']!='' ){
		$isianteks = $filter['PS']['isianteks'];
	}

	if ( isset($filter['PS']['batasanjumlah']) && $filter['PS']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['PS']['batasanjumlah'];
	}
}
//Product Related
if (isset($_GET['page']) && $_GET['page'] == 'PL'){
	$titlepage = 'Filter Page Product Related';

	$isiantekslabel = 'Product Name / Related Product Name';
	if ( isset($filter['PL']['isianteks']) && $filter['PL']['isianteks']!='' ){
		$isianteks = $filter['PL']['isianteks'];
	}

	if ( isset($filter['PL']['batasanjumlah']) && $filter['PL']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['PL']['batasanjumlah'];
	}
}
//Product Variety
if (isset($_GET['page']) && $_GET['page'] == 'PV'){
	$titlepage = 'Filter Page Product Variety';

	$isiantekslabel = 'Name';
	if ( isset($filter['PV']['isianteks']) && $filter['PV']['isianteks']!='' ){
		$isianteks = $filter['PV']['isianteks'];
	}

	if ( isset($filter['PV']['batasanjumlah']) && $filter['PV']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['PV']['batasanjumlah'];
	}
}
//Product Item
if (isset($_GET['page']) && $_GET['page'] == 'PR'){
	$titlepage = 'Filter Page Product Item';

	$isiantekslabel = 'Name';
	if ( isset($filter['PR']['isianteks']) && $filter['PR']['isianteks']!='' ){
		$isianteks = $filter['PR']['isianteks'];
	}

	if ( isset($filter['PR']['batasanjumlah']) && $filter['PR']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['PR']['batasanjumlah'];
	}
}
//Store
if (isset($_GET['page']) && $_GET['page'] == 'ST'){
	$titlepage = 'Filter Page Master Store';

	$isiantekslabel = 'Code/Name/Display Name';
	if ( isset($filter['ST']['isianteks']) && $filter['ST']['isianteks']!='' ){
		$isianteks = $filter['ST']['isianteks'];
	}

	if ( isset($filter['ST']['batasanjumlah']) && $filter['ST']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['ST']['batasanjumlah'];
	}
}
//Size
if (isset($_GET['page']) && $_GET['page'] == 'SZ'){
	$titlepage = 'Filter Page Master Size';

	$isiantekslabel = 'Name';
	if ( isset($filter['SZ']['isianteks']) && $filter['SZ']['isianteks']!='' ){
		$isianteks = $filter['SZ']['isianteks'];
	}

	if ( isset($filter['SZ']['batasanjumlah']) && $filter['SZ']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['SZ']['batasanjumlah'];
	}
}
//Color
if (isset($_GET['page']) && $_GET['page'] == 'CL'){
	$titlepage = 'Filter Page Master Color';

	$isiantekslabel = 'Name / Code';
	if ( isset($filter['CL']['isianteks']) && $filter['CL']['isianteks']!='' ){
		$isianteks = $filter['CL']['isianteks'];
	}

	if ( isset($filter['CL']['batasanjumlah']) && $filter['CL']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['CL']['batasanjumlah'];
	}
}
//Color Item
if (isset($_GET['page']) && $_GET['page'] == 'CI'){
	$titlepage = 'Filter Page Master Color Item';

	$isiantekslabel = 'Category';
	if ( isset($filter['CI']['isianteks']) && $filter['CI']['isianteks']!='' ){
		$isianteks = $filter['CI']['isianteks'];
	}

	if ( isset($filter['CI']['batasanjumlah']) && $filter['CI']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['CI']['batasanjumlah'];
	}
}
//Master User
if (isset($_GET['page']) && $_GET['page'] == 'US'){
	$titlepage = 'Filter Page Master User Account';

	$isiantekslabel = 'User Name';
	if ( isset($filter['US']['isianteks']) && $filter['US']['isianteks']!='' ){
		$isianteks = $filter['US']['isianteks'];
	}
	
	$pilihangandalabel = 'Group Access Name';
	$getdt = @mysqli_query( $sqlcon,
		"select gr_id,gr_name from `group` order by gr_name asc"
	);
	if ($getdt){while ($getdt1 = mysqli_fetch_array($getdt)){
		if (isset($filter['US']['pilihanganda'])
		&& in_array($getdt1['gr_id'],$filter['US']['pilihanganda'])){
			$selected = ' selected="selected"';
		}else{
			$selected = '';
		}
		$pilihangandaopt.= '<option value="'.$getdt1['gr_id'].'"'.$selected.'>'.$getdt1['gr_name'].'</option>';
	}}
	
	if ( isset($filter['US']['batasanjumlah']) && $filter['US']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['US']['batasanjumlah'];
	}
}
//Access
if (isset($_GET['page']) && $_GET['page'] == 'AC'){
	$titlepage = 'Filter Page Access Name';

	$isiantekslabel = 'Name / Description';
	if ( isset($filter['AC']['isianteks']) && $filter['AC']['isianteks']!='' ){
		$isianteks = $filter['AC']['isianteks'];
	}

	if ( isset($filter['AC']['batasanjumlah']) && $filter['AC']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['AC']['batasanjumlah'];
	}
}
//Group
if (isset($_GET['page']) && $_GET['page'] == 'GR'){
	$titlepage = 'Filter Page Group Access';

	$isiantekslabel = 'Name / Description';
	if ( isset($filter['GR']['isianteks']) && $filter['GR']['isianteks']!='' ){
		$isianteks = $filter['GR']['isianteks'];
	}

	if ( isset($filter['GR']['batasanjumlah']) && $filter['GR']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['GR']['batasanjumlah'];
	}
}
//Categories
if (isset($_GET['page']) && $_GET['page'] == 'CA'){
	$titlepage = 'Filter Page Categories';

	$isiantekslabel = 'Name / Descriptions';
	if ( isset($filter['CA']['isianteks']) && $filter['CA']['isianteks']!='' ){
		$isianteks = $filter['CA']['isianteks'];
	}

	$pilihangandalabel = 'Parent Name';
	$getdt = @mysqli_query( $sqlcon,
		"select ca_id,ca_name,ca_desc from category order by ca_name,ca_desc"
	);
	if (isset($filter['CA']['pilihanganda'])
	&& in_array('0',$filter['CA']['pilihanganda'])){
		$selected = ' selected="selected"';
	}else{
		$selected = '';
	}
	$pilihangandaopt.= '<option value="0"'.$selected.'>TOP PARENT</option>';
	if ($getdt){while ($getdt1 = mysqli_fetch_array($getdt)){
		if (isset($filter['CA']['pilihanganda'])
		&& in_array($getdt1['ca_id'],$filter['CA']['pilihanganda'])){
			$selected = ' selected="selected"';
		}else{
			$selected = '';
		}
		$pilihangandaopt.= '<option value="'.$getdt1['ca_id'].'"'.$selected.'>'.
			htmlentities($getdt1['ca_name'].'-'.$getdt1['ca_desc']).'</option>';
	}}

	if ( isset($filter['CA']['batasanjumlah']) && $filter['CA']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['CA']['batasanjumlah'];
	}
}
//Pages Overwrite
if (isset($_GET['page']) && $_GET['page'] == 'PO'){
	$titlepage = 'Filter Page Pages Overwrite';

	$isiantekslabel = 'URL';
	if ( isset($filter['PO']['isianteks']) && $filter['PO']['isianteks']!='' ){
		$isianteks = $filter['PO']['isianteks'];
	}
	
	$isianteks1label = 'Title / Keyword / Description';
	if ( isset($filter['PO']['isianteks1']) && $filter['PO']['isianteks1']!='' ){
		$isianteks1 = $filter['PO']['isianteks1'];
	}

	if ( isset($filter['PO']['batasanjumlah']) && $filter['PO']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['PO']['batasanjumlah'];
	}
}
//Banner Top Slider
if (isset($_GET['page']) && $_GET['page'] == 'BNS'){
	$titlepage = 'Filter Page Banner Top Slider';

	$isiantekslabel = 'Title';
	if ( isset($filter['BNS']['isianteks']) && $filter['BNS']['isianteks']!='' ){
		$isianteks = $filter['BNS']['isianteks'];
	}

	if ( isset($filter['BNS']['batasanjumlah']) && $filter['BNS']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['BNS']['batasanjumlah'];
	}
}
//Banner Video Promo
if (isset($_GET['page']) && $_GET['page'] == 'BVP'){
	$titlepage = 'Filter Page Banner Video Promo';

	$isiantekslabel = 'Title';
	if ( isset($filter['BVP']['isianteks']) && $filter['BVP']['isianteks']!='' ){
		$isianteks = $filter['BVP']['isianteks'];
	}

	if ( isset($filter['BVP']['batasanjumlah']) && $filter['BVP']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['BVP']['batasanjumlah'];
	}
}
//Banner Quick Menu
if (isset($_GET['page']) && $_GET['page'] == 'BNQ'){
	$titlepage = 'Filter Page Banner Quick Menu';

	$isiantekslabel = 'Title';
	if ( isset($filter['BNQ']['isianteks']) && $filter['BNQ']['isianteks']!='' ){
		$isianteks = $filter['BNQ']['isianteks'];
	}

	if ( isset($filter['BNQ']['batasanjumlah']) && $filter['BNQ']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['BNQ']['batasanjumlah'];
	}
}
//Banner Seasons
if (isset($_GET['page']) && $_GET['page'] == 'BNE'){
	$titlepage = 'Filter Page Banner Seasons';

	$isiantekslabel = 'Title';
	if ( isset($filter['BNE']['isianteks']) && $filter['BNE']['isianteks']!='' ){
		$isianteks = $filter['BNE']['isianteks'];
	}

	if ( isset($filter['BNE']['batasanjumlah']) && $filter['BNE']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['BNE']['batasanjumlah'];
	}
}
//Banner Text
if (isset($_GET['page']) && $_GET['page'] == 'BNT'){
	$titlepage = 'Filter Page Banner Text';

	$isiantekslabel = 'Title / Anchor';
	if ( isset($filter['BNT']['isianteks']) && $filter['BNT']['isianteks']!='' ){
		$isianteks = $filter['BNT']['isianteks'];
	}

	if ( isset($filter['BNT']['batasanjumlah']) && $filter['BNT']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['BNT']['batasanjumlah'];
	}
}
//Ekspedisi
if (isset($_GET['page']) && $_GET['page'] == 'EX'){
	$titlepage = 'Filter Page Expedition';

	$isiantekslabel = 'Code / Name';
	if ( isset($filter['EX']['isianteks']) && $filter['EX']['isianteks']!='' ){
		$isianteks = $filter['EX']['isianteks'];
	}

	if ( isset($filter['EX']['batasanjumlah']) && $filter['EX']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['EX']['batasanjumlah'];
	}
}
//Ekspedisi Type
if (isset($_GET['page']) && $_GET['page'] == 'ET'){
	$titlepage = 'Filter Page Expedition Type';

	$isiantekslabel = 'Expedition';
	if ( isset($filter['ET']['isianteks']) && $filter['ET']['isianteks']!='' ){
		$isianteks = $filter['ET']['isianteks'];
	}

	if ( isset($filter['ET']['batasanjumlah']) && $filter['ET']['batasanjumlah']!='' ){
		$batasanjumlah = $filter['ET']['batasanjumlah'];
	}
}

if ($titlepage==''){
	$titlepage='Un-Authorized Request!';
}
?>