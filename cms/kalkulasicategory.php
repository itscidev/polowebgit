<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/category.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$buttondraft = draftStatus();

include_once 'kalkulasicategoryfilter.php';

$resview = numrowdb($sqlcon,"select ca_id from category cat".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$qrysel = "select cat.*,CONCAT(par.ca_name,'-',par.ca_desc) parent from category cat ".
			"left outer join category par on par.ca_id=cat.ca_ca_id".$sinfil.$sinlimit;
	//echo $qrysel;
	$getdt = mysqli_query( $sqlcon,$qrysel );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	if ($getdt1['ca_status']==1){
		$status = '<span class="label label-success">Active</span>';
	}else{
		$status = '<span class="label label-danger">Pasif</span>';
	}
	if ($getdt1 ['ca_draft']==1){
		$draft = '<span class="label label-warning">Draft</span>';
	}else{
		$draft = '';
	}
	if ($getdt1['ca_image']!='' && file_exists("../images/category/".$getdt1['ca_image'])){
		$image = '<img src="'.$path.'/images/category/'.$getdt1['ca_image'].'" style="height:50px;">' .
			'<br />' . formatBytes(filesize("../images/category/".$getdt1['ca_image']));
	}else{
		$image = '<img src="'.$path.'/images/default.png" style="width:50px;">';
	}
	$list .= "<tr id=\"tr" . $getdt1['ca_id'] . "\">" .
		"<td id=\"fca_ca_name" . $getdt1 ['ca_id'] . "\">" . htmlentities($getdt1['parent']) . "</td>" .
		"<td id=\"fca_name" . $getdt1['ca_id'] . "\" align=\"left\">" . htmlentities($getdt1['ca_name']) . "</td>" .
		"<td id=\"fca_sequence" . $getdt1 ['ca_id'] . "\">" . htmlentities($getdt1['ca_sequence']) . "</td>" . 
		"<td id=\"fca_image" . $getdt1 ['ca_id'] . "\">" . $image . "</td>" .
		"<td>".
		"<span id=\"fstatus" . $getdt1 ['ca_id'] . "\">" . $status . "</span>".
		"<span id=\"fdraft" . $getdt1 ['ca_id'] . "\">" . $draft . "</span>".
	"</td>" .
		"<td style=\"font-size:17px;\">" .
			"<p id=\"fca_ca_id".$getdt1['ca_id']."\" class=\"hide\">".$getdt1['ca_ca_id']."</p>".
			"<p id=\"fca_desc".$getdt1['ca_id']."\" class=\"hide\">".htmlentities($getdt1['ca_desc'])."</p>".
			"<p id=\"fca_status".$getdt1['ca_id']."\" class=\"hide\">".$getdt1['ca_status']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['ca_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data " . htmlentities($getdt1['ca_name']) . "\" " .
			"onClick=\"prepareedit('" . $getdt1['ca_id'] . "');\"><i class=\"fa fa-edit\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['ca_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data " . htmlentities($getdt1['ca_name']) . "\" " .
			"onClick=\"preparedelete('" . $getdt1['ca_id'] . "');\"><i class=\"fa fa-times-circle\"></i></a>" .
		"</td>" .
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"6\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

//list category
$optca_ca_id = '';
$data = @mysqli_query($sqlcon,"select * from category order by ca_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
	if ($data2['ca_status']==0){
		$disable = ' disabled="disabled"';
	}
	$optca_ca_id .= '<option value="' . $data2['ca_id'] . '"'.$disable.'>' .
		htmlentities( $data2['ca_name'].'-'.$data2['ca_desc'] ) . '</option>';
}}
?>