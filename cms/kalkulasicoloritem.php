<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/coloritem.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasicoloritemfilter.php';

$resview = numrowdb($sqlcon,"select ci_id from color_item".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$qrysel = "select color_item.*,cl_name,cl_code from color_item ".
			"left outer join `color` on cl_id=ci_cl_id".$sinfil.$sinlimit;
	//echo $qrysel;
	$getdt = mysqli_query( $sqlcon,$qrysel );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
		
	if ($getdt1['ci_image']!='' && file_exists("../images/productcolor/".$getdt1['ci_image'])){
		$image = '<img src="'.$path.'/images/productcolor/'.$getdt1['ci_image'].'" style="height:30px;">' .
			'<br />' . formatBytes(filesize("../images/productcolor/".$getdt1['ci_image']));
	}else{
		$image = '<img src="'.$path.'/images/default.png" style="width:50px;">';
	}
	$list .= "<tr id=\"tr" . $getdt1['ci_id'] . "\">" .
		"<td id=\"fci_cl_name" . $getdt1 ['ci_id'] . "\">".
			"<i class=\"fa fa-circle\" style=\"color:#".$getdt1['cl_code'].";\"></i>&nbsp;&nbsp;" . 
			htmlentities($getdt1['cl_name']) . 
		"</td>" .
		"<td id=\"fci_image" . $getdt1 ['ci_id'] . "\">" . $image . "</td>" .
		"<td id=\"fci_sequence" . $getdt1 ['ci_id'] . "\">" . htmlentities($getdt1['ci_sequence']) . "</td>" . 
		"<td style=\"font-size:17px;\">" .
			"<p id=\"fci_cl_id".$getdt1['ci_id']."\" class=\"hide\">".$getdt1['ci_cl_id']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['ci_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data\" " .
			"onClick=\"prepareedit('" . $getdt1['ci_id'] . "');\"><i class=\"fa fa-edit\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['ci_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data\" " .
			"onClick=\"preparedelete('" . $getdt1['ci_id'] . "');\"><i class=\"fa fa-times-circle\"></i></a>" .
		"</td>" .
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"4\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td></tr>";
}

//list color
$optci_cl_id = '';
$data = @mysqli_query($sqlcon,"select * from color order by cl_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
// 	if ($data2['ci_status']==0){
// 		$disable = ' disabled="disabled"';
// 	}
	$optci_cl_id .= '<option value="' . $data2['cl_id'] . '" color="' . $data2['cl_code'] . '"'.$disable.'>'.
		htmlentities( $data2['cl_name'] ) . 
	'</option>';
}}
?>