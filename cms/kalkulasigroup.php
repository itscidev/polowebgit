<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/group.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasigroupfilter.php';

$resview = numrowdb($sqlcon,"select gr_id from `group`".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select `group`.*,(".
		"SELECT GROUP_CONCAT(ga_ac_id ORDER BY ga_ac_id ASC SEPARATOR ',') ".
		"FROM group_access where ga_gr_id=gr_id) access ".
		"from `group`".$sinfil.$sinlimit
	);
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	$list .= "<tr id=\"tr" . $getdt1['gr_id'] . "\">" .
		"<td id=\"fgr_name" . $getdt1['gr_id'] . "\" align=\"left\">" . htmlentities($getdt1['gr_name']) . "</td>" .
		"<td id=\"fgr_desc" . $getdt1['gr_id'] . "\" align=\"left\">" . htmlentities($getdt1['gr_desc']) . "</td>" .
		"<td style=\"font-size:17px;\">" .
			"<p id=\"faccess" . $getdt1['gr_id'] . "\" class=\"hide\">".$getdt1['access']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['gr_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data " . htmlentities($getdt1['gr_name']) . "\" " .
			"onClick=\"prepareedit('" . $getdt1['gr_id'] . "');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['gr_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data " . htmlentities($getdt1['gr_name']) . "\" " .
			"onClick=\"preparedelete('" . $getdt1['gr_id'] . "');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>" .
		"</td>" .
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"3\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

$accessgranted = '';
$data = @mysqli_query( $sqlcon,"select * from access order by ac_name asc" );
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$id = $data2['ac_id'];
	$accessgranted .= '<div class="col-md-3 col-sm-4 col-xs-6">'.
		'<div class="checkbox">'.
			'<label>'.
				'<input name="ac['.$id.']" id="ac'.$id.'" type="checkbox" value="1">'.
				$data2['ac_name'].
			'</label>'.
		'</div>'.
	'</div>';
}}
?>