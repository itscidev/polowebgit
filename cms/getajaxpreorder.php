<?php
/* return result like this
{
  "results": [
    {
      "id": 1,
      "text": "Option 1"
    },
    {
      "id": 2,
      "text": "Option 2",
      "selected": true
    },
    {
      "id": 3,
      "text": "Option 3",
      "disabled": true
    }
  ]
} */
$result['results'] = array();
$data1 = 0;
include_once ("configuration/connect.php");

if (isset($_GET['search']) && $_GET['search']!=''){
	$keysearch = $_GET['search'];
}else{$keysearch = '';}
if ($keysearch != ''){
	$qrystr = "select po_id,po_nama,po_hp,po_pesanan ".
	"from preorder ".
	"where lower(po_nama) like '%".mysqli_real_escape_string($sqlcon,strtolower($keysearch))."%' or lower(po_hp) like '%".mysqli_real_escape_string($sqlcon,strtolower($keysearch))."%' ".
	"order by po_nama,po_hp"; 
	$data = @mysqli_query( $sqlcon,$qrystr );
	if ($data){$data1 = mysqli_num_rows($data);}
	
	if ($data1 > 0){while ($data2 = mysqli_fetch_assoc($data)){
		$jsonorder = json_decode($data2['po_pesanan'],true);
		$pesanan = '';
		foreach ($jsonorder as $key => $value) {
			if ($pesanan != ''){$pesanan .= ", ";}
			$pesanan .= $key.' '.$value;
		}
		if ($pesanan!=''){$pesanan = ' ('.$pesanan.')';}
		$linkedpo = $data2['po_nama'].', '.$data2['po_hp'].$pesanan;
		array_push($result['results'],array(
			'id' => $data2['po_id'],
			"text" => $linkedpo
		));
	}}
}

if ($sqlcon){
	mysqli_close( $sqlcon );
}

/* $get = $post = $server = $body = '';
if (isset ( $_GET )) {$get = ", Get : " . json_encode ( $_GET );}
if (isset ( $_POST )) {$post = ", Post : " . json_encode ( $_POST );}
if (isset ( $_SERVER )) {$server = ", Server : " . json_encode ( $_SERVER );}
$body = ", Body : ".file_get_contents('php://input');
$forlog = date ( "d-m-Y H:i:s" ) . ", result : " . json_encode ( $result ) . $body . $get . $post . 
		$server . PHP_EOL;
foreach ( file ( 'testing/log.txt' ) as $nline => $line ) {
	if ($nline < 150) {
		$forlog .= $line;
	}
}
$myfile = fopen ( "testing/log.txt", 'w' );
fwrite ( $myfile, $forlog );
fclose ( $myfile ); */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>