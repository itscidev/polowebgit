<?php
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Please re-login!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/contactsmtp.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['formid'])){
	include_once("configuration/connect.php");
	
	if (isset($_POST['ms_purpose'])){$ms_purpose=$_POST['ms_purpose'];}else{$ms_purpose='cms';}
	if (isset($_POST['ms_sendername'])){$ms_sendername=$_POST['ms_sendername'];}else{$ms_sendername='';}
	if (isset($_POST['ms_sendermail'])){$ms_sendermail=$_POST['ms_sendermail'];}else{$ms_sendermail='';}
	if (isset($_POST['ms_password'])){$ms_password=$_POST['ms_password'];}else{$ms_password='';}
	if (isset($_POST['ms_host'])){$ms_host=$_POST['ms_host'];}else{$ms_host='';}
	if (isset($_POST['ms_secure'])){$ms_secure=$_POST['ms_secure'];}else{$ms_secure='';}
	if (isset($_POST['ms_port'])){$ms_port=$_POST['ms_port'];}else{$ms_port='';}
	
	if ($ms_purpose==''){$result['Error']['ms_purpose']=1;}
	if ($ms_sendername==''){$result['Error']['ms_sendername']=1;}
	if ($ms_sendermail==''){$result['Error']['ms_sendermail']=1;}
	if ($ms_password==''){$result['Error']['ms_password']=1;}
	if ($ms_host==''){$result['Error']['ms_host']=1;}
	if ($ms_secure==''){$result['Error']['ms_secure']=1;}
	if ($ms_port==''){$result['Error']['ms_port']=1;}
	if (!isset($result['Error'])){
		if ($_POST['formid'] == 'new'){
			$addsql = '';
		}else{
			$addsql = " and ms_id!=".$_POST['formid'];
		}
		$cekex = mysqli_num_rows(mysqli_query( $sqlcon,
			"select ms_id from mail_smtp where ms_purpose = '".$ms_purpose."'".$addsql
		));
		if ($cekex > 0){
			$result['Error']['ms_purpose']=1;
		}
	}
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into mail_smtp (".
			"ms_purpose,ms_sendername,ms_sendermail,ms_password,".
			"ms_host,ms_secure,ms_port,ms_updateby,ms_update".
		") values (".
			"'".$ms_purpose."','".$ms_sendername."','".$ms_sendermail."','".$ms_password."',".
			"'".$ms_host."','".$ms_secure."',".$ms_port.",".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update mail_smtp set ".
			"ms_purpose='".$ms_purpose."',".
			"ms_sendername='".$ms_sendername."',".
			"ms_sendermail='".$ms_sendermail."',".
			"ms_password='".$ms_password."',".
			"ms_host='".$ms_host."',".
			"ms_secure='".$ms_secure."',".
			"ms_port=".$ms_port.",".
			"ms_update='".date("Y-m-d H:i:s")."',".
			"ms_updateby=".$cokidusr." ".
		"where ms_id=".intval($_POST['formid']));
		$result['Status']='Update';
	}
	if ($sqlcon){mysqli_close($sqlcon);}
}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>