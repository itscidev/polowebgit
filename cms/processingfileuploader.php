<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/fileuploader.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']!='' && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		if (file_exists($_POST['path'])){
			unlink($_POST['path']);
		}
		$result['Status']='Delete Success!';
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid'])){
	
	if ($_POST['formid']=='new' && (!isset($_FILES['fileupload']['name'])) ){
		$result['Error']['fileupload']=1;
	}elseif (count($_FILES['fileupload']['name'])==0){
		$result['Error']['fileupload']=1;
	}
	
	$strrep = array("'",'"',' ','/','\\','#');
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		$strrep = array("'",'"',' ','/','\\');
		foreach ($_FILES['fileupload']['name'] as $index => $imgname){
			
			$pi_image = str_replace($strrep,'',$imgname);
			move_uploaded_file(
				$_FILES['fileupload']['tmp_name'][$index],'../generalasset/'.$pi_image
			);
		}
		$result['Status']='New';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>