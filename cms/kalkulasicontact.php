<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/contact.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$list = '';
$getdt = mysqli_query( $sqlcon,"select * from contact where co_id=1" );
$jumdt = mysqli_num_rows( $getdt );
$co_address = $co_telephone = $co_fax = $co_latitude = $co_longitude = $co_linkfb = 
$co_linkyoutube = $co_linkinstagram = '';
$co_los_id = 0;
if ($jumdt > 0) {
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
		$co_address = $getdt1['co_address'];
		$co_telephone = $getdt1['co_telephone'];
		$co_fax = $getdt1['co_fax'];
		$co_latitude = $getdt1['co_latitude'];
		$co_longitude = $getdt1['co_longitude'];
		$co_linkfb = $getdt1['co_linkfb'];
		$co_linkyoutube = $getdt1['co_linkyoutube'];
		$co_linkinstagram = $getdt1['co_linkinstagram'];
		$co_los_id = $getdt1['co_los_id'];
	}
}
$optcolosid = '';
include_once 'model/lokasistep.php';
//select2 kecamatan
$field = "los_id,los_prop_lok_id,prop.lok_name prop,los_kab_lok_id,kab.lok_name kab,los_kec_lok_id,".
		"kec.lok_name kec";
$join = "left outer join lokasi prop on los_prop_lok_id=prop.lok_id ".
		"left outer join lokasi kab on los_kab_lok_id=kab.lok_id ".
		"left outer join lokasi kec on los_kec_lok_id=kec.lok_id";
$order = "3,5,7";
$getaddr = lokasistep_s($sqlcon,$field,$join,'','',$order);
if (is_array($getaddr) && $getaddr[0]>0){while ($getaddr1 = mysqli_fetch_assoc($getaddr[1])){
	$selected = '';
	if ($getaddr1['los_id'] == $co_los_id){
		$selected = ' selected="selected"';
	}
	$addaddr = '';
	if ($getaddr1['kec']!=''){
		if ($addaddr != ''){$addaddr .= ', ';}
		$addaddr .= $getaddr1['kec'];
	}
	if ($getaddr1['kab']!=''){
		if ($addaddr != ''){$addaddr .= ', ';}
		$addaddr .= $getaddr1['kab'];
	}
	if ($getaddr1['prop']!=''){
		if ($addaddr != ''){$addaddr .= ', ';}
		$addaddr .= $getaddr1['prop'];
	}
	$optcolosid .= '<option value="' . $getaddr1['los_id'] . '"' . $selected . '>' . 
		$addaddr . '</option>';
}}
?>