<?php
$filter = array();
if (isset( $_COOKIE['filter'] )) {
	setcookie( "filter", $_COOKIE['filter'], time() + 7200 );
	$filter = json_decode( $_COOKIE['filter'], true );
}

$sinfil = '';
if (isset( $filter['ST']['isianteks'] )) {
	$isianteks = strtolower($filter['ST']['isianteks']);
}else{
	$isianteks = '';
}
if ($isianteks != ''){
	$sinfil .= "lower(st_code) like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%' ".
		"or lower(st_name) like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%' ".
		"or lower(st_address) like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%'";
}

if ($sinfil != '') {
	$sinfil = " where " . $sinfil;
}

if (isset( $filter['ST']['batasanjumlah'] )) {
	$batasanjumlah = intval( $filter['ST']['batasanjumlah'] );
}else{
	$batasanjumlah = 200;
}
$sinlimit = ' limit '.$batasanjumlah;
?>