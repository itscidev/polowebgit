<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/productvariety.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from product_variety where pv_id=".$_POST['delid'] );
		$resultdel = 'Delete Success!';
	}
	if ($resultdel != ''){
		$result['Status'] = $resultdel;
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['pv_name'])){$pv_name=trim($_POST['pv_name']);}else{$pv_name='';}
	if (isset($_POST['pv_artikel'])){$pv_artikel=trim($_POST['pv_artikel']);}else{$pv_artikel='';}
	if (isset($_POST['pv_status'])){$pv_status=intval($_POST['pv_status']);}else{$pv_status=0;}
	if (isset($_POST['pv_draft'])){$pv_draft=intval($_POST['pv_draft']);}else{$pv_draft=0;}
	if (isset($_POST['pv_price'])){$pv_price=intval($_POST['pv_price']);}else{$pv_price=0;}
	if (isset($_POST['pv_qty'])){$pv_qty=intval($_POST['pv_qty']);}else{$pv_qty=0;}
	if (isset($_POST['pv_pr_id'])){$pv_pr_id=intval($_POST['pv_pr_id']);}else{$pv_pr_id=0;}
	if (isset($_POST['pv_st_id'])){$pv_st_id=intval($_POST['pv_st_id']);}else{$pv_st_id=0;}
	if (isset($_POST['pv_sz_id'])){$pv_sz_id=intval($_POST['pv_sz_id']);}else{$pv_sz_id=0;}
	if (isset($_POST['pv_ci_id'])){$pv_ci_id=intval($_POST['pv_ci_id']);}else{$pv_ci_id=0;}
	
	if ($pv_name==''){$result['Error']['pv_name']=1;}
	if ($pv_pr_id==0){$result['Error']['pv_pr_id']=1;}
	if ($pv_sz_id==0){$result['Error']['pv_sz_id']=1;}
	if ($pv_ci_id==0){$result['Error']['pv_ci_id']=1;}
	if (!isset($result['Error'])){
		$addexist = '';
		if ($_POST['formid']!='new'){$addexist = ' and pv_id!='.intval($_POST['formid']);}
		$exist = mysqli_num_rows(mysqli_query($sqlcon,
			"select pv_id from product_variety ".
			"where pv_pr_id=".$pv_pr_id." and pv_st_id=".$pv_st_id." and pv_sz_id=".$pv_sz_id." ".
				"and pv_ci_id=".$pv_ci_id.$addexist
		));
		if ($exist > 0){
			$result['Error']['in']=1;
			$result['Error']['pv_pr_id']=1;
			$result['Error']['pv_st_id']=1;
			$result['Error']['pv_sz_id']=1;
			$result['Error']['pv_ci_id']=1;
		}
	}if (!isset($result['Error']) && $pv_artikel!=''){
		$addexist = '';
		if ($_POST['formid']!='new'){$addexist = ' and pv_id!='.intval($_POST['formid']);}
		$exist = mysqli_num_rows(mysqli_query($sqlcon,
			"select pv_id from product_variety ".
			"where pv_artikel='".mysqli_real_escape_string($sqlcon,$pv_artikel)."'".$addexist
		));
		if ($exist > 0){
			$result['Error']['pv_artikel']=1;
		}
	}
	
	$strrep = array("'",'"',' ','/','\\','#');
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into product_variety (".
			"pv_status,pv_draft,pv_price,pv_qty,pv_pr_id,pv_st_id,".
			"pv_sz_id,pv_ci_id,".
			"pv_name,".
			"pv_artikel,".
			"pv_createby,pv_create,pv_updateby,pv_update".
		") values (".
			$pv_status.",".$pv_draft.",".$pv_price.",".$pv_qty.",".$pv_pr_id.",".$pv_st_id.",".
			$pv_sz_id.",".$pv_ci_id.",".
			"'".mysqli_real_escape_string( $sqlcon,$pv_name )."',".
			"'".mysqli_real_escape_string( $sqlcon,$pv_artikel )."',".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update product_variety set ".
			"pv_name='".mysqli_real_escape_string( $sqlcon,$pv_name )."',".
			"pv_artikel='".mysqli_real_escape_string( $sqlcon,$pv_artikel )."',".
			"pv_status=".$pv_status.",".
			"pv_draft=".$pv_draft.",".
			"pv_price=".$pv_price.",".
			"pv_qty=".$pv_qty.",".
			"pv_pr_id=".$pv_pr_id.",".
			"pv_st_id=".$pv_st_id.",".
			"pv_sz_id=".$pv_sz_id.",".
			"pv_ci_id=".$pv_ci_id.",".
			"pv_update='".date("Y-m-d H:i:s")."',".
			"pv_updateby=".$cokidusr." ".
		"where pv_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>