<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/paymenttype.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select pt_logo from paymenttype where pt_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['pt_logo']!='' && file_exists("../images/paymentlogo/".$cekimg2['pt_logo'])){
				unlink("../images/paymentlogo/".$cekimg2['pt_logo']);
			}
			mysqli_query( $sqlcon,"delete from paymenttype where pt_id=".$_POST['delid'] );
			$result['Status']='Delete Success!';		
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}else{
		$result['Error']='Data already deleted!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['pt_name'])){$pt_name=trim($_POST['pt_name']);}else{$pt_name='';}
	if (isset($_POST['pt_vendor'])){$pt_vendor=trim($_POST['pt_vendor']);}else{$pt_vendor='';}
	if (isset($_FILES['pt_logo']['name']) && !(
		strtolower($_FILES['pt_logo']['type'])=='image/jpeg' || strtolower($_FILES['pt_logo']['type'])=='image/png'
	)){
		$result['Error']['pt_logo']=1;
	}
	if (isset($_POST['pt_status'])){$pt_status=intval($_POST['pt_status']);}else{$pt_status=0;}
	
	if ($pt_vendor==''){$result['Error']['pt_vendor']=1;}
	if ($pt_name==''){$result['Error']['pt_name']=1;}
	if (!isset($result['Error'])){
		$addcek = '';
		if ($_POST['formid']!='new'){
			$addcek = ' and pt_id!='.intval($_POST['formid']);
		}
		$cekexist = mysqli_num_rows(mysqli_query($sqlcon,
			"select pt_id from paymenttype ".
			"where lower(pt_vendor)='".mysqli_real_escape_string($sqlcon,strtolower($pt_vendor))."' and lower(pt_name)='".mysqli_real_escape_string($sqlcon,strtolower($pt_name))."'".$addcek
		));
		if ($cekexist!=0){
			$result['Error']['pt_vendor']=1;
			$result['Error']['pt_name']=1;
		}
	}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,"insert into paymenttype (".
			"pt_name,pt_logo,".
			"pt_vendor,pt_status,".
			"pt_createby,pt_create,pt_updateby,pt_update".
		") values (".
			"'".mysqli_real_escape_string( $sqlcon,$pt_name )."','',".
			"'".mysqli_real_escape_string( $sqlcon,$pt_vendor )."',".$pt_status.",".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")");
		if (isset($_FILES['pt_logo']['name']) && $_FILES['pt_logo']['name']!=''){
			$insertid = mysqli_insert_id( $sqlcon );
			$strrep = array("'",'"',' ','/','\\');
			$pt_logo = str_replace($strrep,'', $insertid.'-'.$_FILES['pt_logo']['name']);
			move_uploaded_file(
				$_FILES['pt_logo']['tmp_name'],'../images/paymentlogo/'.$pt_logo
			);
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['pt_logo']['size']);
				
			$result['pt_logo']='<img src="../images/paymentlogo/'.$pt_logo.'" style="height:24px;"><br />' . $fz;
			mysqli_query( $sqlcon,
				"update paymenttype set pt_logo='".mysqli_real_escape_string( $sqlcon,$pt_logo )."' ".
				"where pt_id=".$insertid
			);
		}else{
			$pt_logo = '';
			$result['pt_logo']='<img src="../images/default.png" style="height:24px;">';
		}
		//$result['QryErr']=mysqli_error($sqlcon);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		if (isset($_FILES['pt_logo']['name']) && $_FILES['pt_logo']['name']!=''){
			$cekimg = mysqli_query( $sqlcon,
				"select pt_logo from paymenttype where pt_id=".intval($_POST['formid'])
			);
			$cekimg1 = mysqli_num_rows($cekimg);
			if ($cekimg1 > 0){
				$cekimg2 = mysqli_fetch_array($cekimg);
				if ($cekimg2['pt_logo']!='' && file_exists("../images/paymentlogo/".$cekimg2['pt_logo'])){
					unlink("../images/paymentlogo/".$cekimg2['pt_logo']);
				}
			}
			$strrep = array("'",'"',' ','/','\\');
			$pt_logo = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['pt_logo']['name']);
			move_uploaded_file(
				$_FILES['pt_logo']['tmp_name'],'../images/paymentlogo/'.$pt_logo
			);
			$addsql = ",pt_logo='".mysqli_real_escape_string( $sqlcon,$pt_logo )."'";
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['pt_logo']['size']);
		
			$result['pt_logo']='<img src="../images/paymentlogo/'.$pt_logo.'" style="height:30px;"><br />' . $fz;
		}else{
			$addsql = '';
		}
		mysqli_query( $sqlcon,
		"update paymenttype set ".
			"pt_name='".mysqli_real_escape_string( $sqlcon,$pt_name )."',".
			"pt_vendor='".mysqli_real_escape_string( $sqlcon,$pt_vendor )."',".
			"pt_status=".$pt_status.",".
			"pt_update='".date("Y-m-d H:i:s")."',".
			"pt_updateby=".$cokidusr.$addsql." ".
		"where pt_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>