<header class="main-header">
	<!-- Logo -->
	<a href="index.html" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>P</b>C</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>POLO</b>CMS</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<?php /*?>
				<!-- Messages: style can be found in dropdown.less-->
				<li class="dropdown messages-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-envelope-o"></i>
						<span class="label label-success">4</span>
					</a>
	                <ul class="dropdown-menu">
						<li class="header">You have 4 messages</li>
						<li>
							<!-- inner menu: contains the actual data -->
							<ul class="menu">
								<li><!-- start message -->
									<a href="#">
									<div class="pull-left">
										<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
									</div>
									<h4>
										Support Team
										<small><i class="fa fa-clock-o"></i> 5 mins</small>
									</h4>
									<p>Why not buy a new awesome theme?</p>
									</a>
								</li><!-- end message -->
							</ul>
						</li>
						<li class="footer"><a href="#">See All Messages</a></li>
					</ul>
				</li>*/?>
				<!-- Notifications: style can be found in dropdown.less -->
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Notification">
						<i class="fa fa-bell-o"></i><?php echo $notiftotal;?><!-- 
						<span class="label label-warning" id="notif-total">0</span> -->
					</a>
					<ul class="dropdown-menu" style="background-color: azure;">
						<li class="header" style="background-color: azure; border-bottom: none;"> </li>
						<li>
							<!-- inner menu: contains the actual data -->
							<ul class="menu" id="notif-ul"><?php echo $listnotiftotal;?>
								<!-- <li id="notif-1">
									<i class="fa fa-times text-red notifmargin"></i> 
									5 new members joined today
								</li>
								<li id="notif-2">
									<i class="fa fa-times text-red notifmargin"></i> 
									5 new members joined today
								</li>
								<li id="notif-2">
									<i class="fa fa-times text-red notifmargin"></i> 
									5 new members joined today
								</li> -->
							</ul>
						</li>
						<!-- <li class="footer">
							<div class="col-md-6 col-sm-6 col-xs-6" align="center" 
							style="vertical-align: middle; height: 30px;">
								<a href="notification.php">Hide all</a>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6" align="center" 
							style="vertical-align: middle; height: 30px;">
								<a href="notification.php">View all</a>
							</div>
						</li> -->
					</ul>
				</li>
				<?php /*?>
				<!-- Tasks: style can be found in dropdown.less -->
				<li class="dropdown tasks-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-flag-o"></i>
						<span class="label label-danger">9</span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">You have 9 tasks</li>
						<li>
							<!-- inner menu: contains the actual data -->
							<ul class="menu">
								<li><!-- Task item -->
									<a href="#">
										<h3>
											Design some buttons
											<small class="pull-right">20%</small>
										</h3>
										<div class="progress xs">
											<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
												<span class="sr-only">20% Complete</span>
											</div>
										</div>
									</a>
								</li><!-- end task item -->
							</ul>
						</li>
						<li class="footer">
							<a href="#">View all tasks</a>
						</li>
					</ul>
				</li>*/?>
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="images/user/<?php echo $upicture;?>" class="user-image upicture" alt="User Image">
						<span class="hidden-xs"><?php echo $getaks['usr_name'];?></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="images/user/<?php echo $upicture;?>" class="img-circle upicture" 
							alt="User Image">
							<p>
								<?php echo $getaks['usr_name'];?>
								<small>Member since <?php echo $ucreate;?><br>Last Login <?php echo $lastlog;?></small>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="profile.html" data-fancybox-type="iframe" class="btn btn-default btn-flat fancybox">Change Data</a>
							</div>
							<div class="pull-right">
								<a href="signout.html" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>
				<!-- Control Sidebar Toggle Button 
				<li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
				</li>-->
			</ul>
		</div>
	</nav>
</header>
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
	<!-- search form -->
	<form action="#" method="get" class="sidebar-form">
		<div class="input-group">
			<input type="text" name="q" class="form-control" placeholder="Search...">
			<span class="input-group-btn">
				<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
			</span>
		</div>
	</form>
	<!-- /.search form -->
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li><?php echo $mainnav;/*?>
			<li class="<?php echo $active1;?>">
				<a href="index.html"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
			</li><?php 
if ($getaks['usr_account'] == 1){?>			
			<li class="<?php echo $active2;?>">
				<a href="user.html"><i class="fa fa-users"></i> <span>User Account</span></a>
			</li><?php 
}if ($getaks['usr_labarugi'] == 1){?>
			<li class="treeview <?php echo $active3;?>">
				<a href="#">
					<i class="fa fa-line-chart"></i> <span>Report</span> <i class="fa fa-angle-left pull-right"></i>
				</a> 
				<ul class="treeview-menu">
					<li class="<?php echo $active3;?>"><a href="report.html"><i class="fa fa-circle-o"></i> Laba-Rugi</a></li>
					<li class="<?php echo $active4;?>"><a href="reportbank.html"><i class="fa fa-circle-o"></i> Bank</a></li>
				</ul>
			</li><?php 
}*/?>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>