var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#pc_nama').val('');
	$('#pc_email').val('');
	$('#pc_hp').val('');
	$('#pc_status').prop('checked', false);
	$('#pc_bukti').val('');
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#pc_nama').val($('#fpc_nama'+idnya).html());
	$('#pc_email').val($('#fpc_email'+idnya).html());
	$('#pc_hp').val($('#fpc_hp'+idnya).html());
	$('#pc_bukti').val('');
	if ($('#fpc_status'+idnya).html()=='1'){
		$('#pc_status').prop('checked', true);
	}else{
		$('#pc_status').prop('checked', false);
	}
	if ($('#pc_po_id').find("option[value='" + $('#fpc_po_id'+idnya).html() + "']").length) {
	    $('#pc_po_id').val($('#fpc_po_id'+idnya).html()).trigger('change');
	} else { 
	    // Create a DOM Option and pre-select by default
	    var newOption = new Option(
	    	$('#fpc_po_name'+idnya).html(), 
	    	$('#fpc_po_id'+idnya).html(), 
	    	true, 
	    	true
	    );
	    // Append it to the select
	    $('#pc_po_id').append(newOption).trigger('change');
	}
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {

    var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '15%' },
			{ 'sWidth'		: '11%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '38%' },
			{ 'sWidth'		: '10%' }
		]
	});

	//Initialize Select2 Elements
    function format(state) {
//		console.log(state);
		if (!state.id){
			return state.text; // optgroup	
		}else{
			return state.text;
		}
    }
	$("select.select2ajax").each(function () {
		var element = $(this);
		var urltarget = element.parent().find('p.urltarget').html();
		//'http://192.168.0.232/performances/getajaxbrand.html'
		element.select2({
			ajax: {
				url: function () {
					//console.log(urltarget);
					return urltarget;
			    },
			    dataType: 'json',
				method: "get",
		        data: function (params) {
		            return {
		                search: params.term
		            };
		        },
		        processResults: function (data) {
		        	console.log(data);
		            return {
		                results: data.results
		            };
		        },
		        cache: true
			},
			placeholder: 'Type min. 2 character',
			minimumInputLength: 2,
			allowClear: true,
			templateResult: format,
		    templateSelection: format,
			escapeMarkup: function (m) {
	            return m;
	        }
		});
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('pc_nama')){
						$('#pc_nama').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pc_email')){
						$('#pc_email').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pc_bukti')){
						$('#pc_bukti').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#pc_nama').val());
					$('#delete'+$('#formid').val()).prop('title', $('#pc_nama').val());
					
					$('#fpc_nama'+$('#formid').val()).html($('#pc_nama').val());
					$('#fpc_email'+$('#formid').val()).html($('#pc_email').val());
					$('#fpc_hp'+$('#formid').val()).html($('#pc_hp').val());
					if (out.hasOwnProperty('pc_bukti')){
						$('#fpc_bukti'+$('#formid').val()).html( out['pc_bukti'] );
					}
					if ($('#pc_po_id').select2('val')){
						$('#fpc_po_id'+$('#formid').val()).html( $('#pc_po_id').select2('val') );
						$('#fpc_po_name'+$('#formid').val()).html($('#pc_po_id').select2('data')[0].text);
					}else{
						$('#fpc_po_id'+$('#formid').val()).html('0');
						$('#fpc_po_name'+$('#formid').val()).html('');
					}
					if ($('#pc_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Approved</span>'
						);
						$('#fpc_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Not</span>'
						);
						$('#fpc_status'+$('#formid').val()).html('0');
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});