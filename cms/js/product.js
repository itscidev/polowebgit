var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#pr_name').val('');
	$('#pr_weight').val('');
	$('#pr_tagline').val('');
	$('#pr_status').prop('checked', false);
	$('#pr_draft').prop('checked', true);
	$('#pr_onsale').prop('checked', false);
	$('#pr_ca_id').select2("val","");
	$('#pr_desc').val('');
	$('#pr_info').val('');
	$('#pr_care').val('');
	$('#pr_price').val('');
	if ($(".wysihtml5-sandbox").length > 0){
		$(".wysihtml5-sandbox").contents().find("body").html('');
	}
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#pr_name').val($('#fpr_name'+idnya).html());
	$('#pr_weight').val($('#fpr_weight'+idnya).html());
	$('#pr_tagline').val($('#fpr_tagline'+idnya).html());
	$('#pr_price').val($('#fpr_price'+idnya).html());
	if ($('#fpr_status'+idnya).html()=='1'){
		$('#pr_status').prop('checked', true);
	}else{
		$('#pr_status').prop('checked', false);
	}
	if ($('#fpr_draft'+idnya).html()=='1'){
		$('#pr_draft').prop('checked', true);
	}else{
		$('#pr_draft').prop('checked', false);
	}
	if ($('#fpr_onsale'+idnya).html()=='1'){
		$('#pr_onsale').prop('checked', true);
	}else{
		$('#pr_onsale').prop('checked', false);
	}
	$('#pr_ca_id').select2("val",$('#fpr_ca_id'+idnya).html());
	$('#pr_desc').val(
		$('<textarea />').html( $('#fpr_desc'+idnya).html() ).text()
	);
	$('#pr_info').val(
			$('<textarea />').html( $('#fpr_info'+idnya).html() ).text()
		);
	$('#pr_care').val(
		$('<textarea />').html( $('#fpr_care'+idnya).html() ).text()
	);
}

function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {
	
	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true
	});
	
    var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '20%' },
			{ 'sWidth'		: '10%' }
		]
	});
	
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        },
		afterLoad: function() {
			//bootstrap WYSIHTML5 - text editor
			if ($(".wysihtml5-sandbox").length > 0){
				$('.wysihtml5-sandbox').remove();
				$("input[name='_wysihtml5_mode']").remove();
				$('.wysihtml5-toolbar').remove();
				$('.textarea').css('display','block');
			}
			$('.textarea').wysihtml5();
			setTimeout(function(){
				var urldef = window.location.href.split("/cms/");
				$("iframe.wysihtml5-sandbox").contents().find("head").append(
					'<link rel="stylesheet" href="'+urldef[0]+'/css/product-wysihtml.css">'
				);
			}, 500);
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('pr_ca_id')){
						$('#pr_ca_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pr_name')){
						$('#pr_name').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pr_weight')){
						$('#pr_weight').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#pr_name').val());
					$('#delete'+$('#formid').val()).prop('title', $('#pr_name').val());
					
					$('#fpr_name'+$('#formid').val()).html($('#pr_name').val());
					$('#fpr_weight'+$('#formid').val()).html($('#pr_weight').val());
					$('#fpr_tagline'+$('#formid').val()).html($('#pr_tagline').val());
					$('#fpr_price'+$('#formid').val()).html($('#pr_price').val());
					if ($('#pr_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fpr_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fpr_status'+$('#formid').val()).html('0');
					}
					if ($('#pr_draft').prop("checked")){
						$('#fdraft'+$('#formid').val()).html(
							'<span class="label label-warning">Draft</span>'
						);
						$('#fpr_draft'+$('#formid').val()).html('1');
					}else{
						$('#fdraft'+$('#formid').val()).html('');
						$('#fpr_draft'+$('#formid').val()).html('0');
					}
					if ($('#pr_onsale').prop("checked")){
						$('#fpr_onsale'+$('#formid').val()).html('1');
					}else{
						$('#fpr_onsale'+$('#formid').val()).html('0');
					}
					if ($('#pr_ca_id').select2('val')){
						$('#fpr_ca_id'+$('#formid').val()).html( $('#pr_ca_id').select2('val') );
						$('#fpr_ca_name'+$('#formid').val()).html($('#pr_ca_id').select2('data')[0].text);
					}else{
						$('#fpr_ca_id'+$('#formid').val()).html('0');
						$('#fpr_ca_name'+$('#formid').val()).html('');
					}
					$('#fpr_desc'+$('#formid').val()).html($('#pr_desc').val());
					$('#fpr_info'+$('#formid').val()).html($('#pr_info').val());
					$('#fpr_care'+$('#formid').val()).html($('#pr_care').val());
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});