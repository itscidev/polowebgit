var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#ev_title').val('');
	$('#ev_code').val('');
	$('#ev_status').prop('checked', false);
	$('#ev_draft').prop('checked', true);
	$('#ev_image').val('');
	$('#ev_desc').val('');
	if ($(".wysihtml5-sandbox").length > 0){
		$(".wysihtml5-sandbox").contents().find("body").html('');
	}
	var now = new Date(Date.now());
	$('#ev_start').data('daterangepicker').setStartDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#ev_start').data('daterangepicker').setEndDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#ev_start').val(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#ev_end').data('daterangepicker').setStartDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#ev_end').data('daterangepicker').setEndDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#ev_end').val(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#ev_title').val($('#fev_title'+idnya).html());
	$('#ev_code').val($('#fev_code'+idnya).html());
	$('#ev_image').val('');
	if ($('#fev_status'+idnya).html()=='1'){
		$('#ev_status').prop('checked', true);
	}else{
		$('#ev_status').prop('checked', false);
	}
	if ($('#fev_draft'+idnya).html()=='1'){
		$('#ev_draft').prop('checked', true);
	}else{
		$('#ev_draft').prop('checked', false);
	}
	$('#ev_desc').val(
		$('<textarea />').html( $('#fev_desc'+idnya).html() ).text()
	);
	$('#ev_start').data('daterangepicker').setStartDate(
		$('<textarea />').html( $('#fev_start'+idnya).html() ).text()
	);
	$('#ev_start').data('daterangepicker').setEndDate(
		$('<textarea />').html( $('#fev_start'+idnya).html() ).text()
	);
	$('#ev_end').data('daterangepicker').setStartDate(
		$('<textarea />').html( $('#fev_end'+idnya).html() ).text()
	);
	$('#ev_end').data('daterangepicker').setEndDate(
		$('<textarea />').html( $('#fev_end'+idnya).html() ).text()
	);
}

function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {
	
	$('#ev_start,#ev_end').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        locale: {
            format: 'MM/DD/YYYY HH:mm:ss'
        }
    });
	
	var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
		    { 'sWidth'		: '25%' },
			{ 'sWidth'		: '15%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '15%' },
			{ 'sWidth'		: '15%' },
			{ 'sWidth'		: '10%' }
		]
	});
	
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        },
		afterLoad: function() {
			//bootstrap WYSIHTML5 - text editor
			if ($(".wysihtml5-sandbox").length > 0){
				$('.wysihtml5-sandbox').remove();
				$("input[name='_wysihtml5_mode']").remove();
				$('.wysihtml5-toolbar').remove();
				$('.textarea').css('display','block');
			}
			$('.textarea').wysihtml5();
			setTimeout(function(){
				var urldef = window.location.href.split("/cms/");
				$("iframe.wysihtml5-sandbox").contents().find("head").append(
					'<link rel="stylesheet" href="'+urldef[0]+'/css/product-wysihtml.css">'
				);
			}, 500);
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
//			console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('ev_title')){
						$('#ev_title').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ev_code')){
						$('#ev_code').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ev_start')){
						$('#ev_start').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ev_end')){
						$('#ev_end').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ev_image')){
						$('#ev_image').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#ev_title').val());
					$('#delete'+$('#formid').val()).prop('title', $('#ev_title').val());
					
					$('#fev_title'+$('#formid').val()).html($('#ev_title').val());
					$('#fev_code'+$('#formid').val()).html($('#ev_code').val());
					$('#fev_start'+$('#formid').val()).html($('#ev_start').val());
					$('#fps_end'+$('#formid').val()).html($('#ev_end').val());
					if (out.hasOwnProperty('ev_image')){
						$('#fev_image'+$('#formid').val()).html( out['ev_image'] );
					}
					if ($('#ev_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fev_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fev_status'+$('#formid').val()).html('0');
					}
					if ($('#ev_draft').prop("checked")){
						$('#fdraft'+$('#formid').val()).html(
							'<span class="label label-warning">Draft</span>'
						);
						$('#fev_draft'+$('#formid').val()).html('1');
					}else{
						$('#fdraft'+$('#formid').val()).html('');
						$('#fev_draft'+$('#formid').val()).html('0');
					}
					$('#fev_desc'+$('#formid').val()).html($('#ev_desc').val());
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});