var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}
function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#usr_name').val('');
	$('#passwordspan').css('display','none');
	$('#usr_password').val('');
	$('#usr_accessip').val('');
	$('#usr_blacklist').val('0');
	$('#usr_gr_id').select2("val","");
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#usr_name').val( $('<textarea />').html( $('#fusr_name'+idnya).html() ).text() );
	$('#passwordspan').css('display','block');
	$('#usr_password').val('');
	$('#usr_accessip').val($('#fusr_accessip'+idnya).html());
	$('#usr_blacklist').val($('#fusr_blacklist'+idnya).html());
	$('#usr_gr_id').select2("val",$('#fusr_gr_id'+idnya).html());
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}
function prepareunlock(idnya){
	$('#unlock').parent().parent().parent().parent().addClass( "has-error" );
	$('#unlockid').val(idnya);
	$('#unlock').prop('checked', false);
}

$(document).ready(function() {
	
	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true
	});
    
    var table = $('#datalist').DataTable( {
    	'order'				: [[ 0, "asc" ]],
    	'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '30%' },
			{ 'sWidth'		: '30%' },
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '22%' },
			{ 'sWidth'		: '10%' }
		]
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					if (out['Error'].hasOwnProperty('userlogin')){
						$('#usr_name').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('password')){
						$('#usr_password').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('usr_gr_id')){
						$('#usr_gr_id').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', 'Edit Data ' + $('#usr_name').val());
					$('#delete'+$('#formid').val()).prop('title', 'Delete Data ' + $('#usr_name').val());
					$('#fusr_name'+$('#formid').val()).html( htmlentities($('#usr_name').val()) );
					$('#fusr_accessip'+$('#formid').val()).html($('#usr_accessip').val());
					$('#fusr_blacklist'+$('#formid').val()).html($('#usr_blacklist').val());
					if (parseInt($('#usr_blacklist').val())>3){
						$('#fstatus'+$('#formid').val()).html(
						'<a href="#winunlock" id="unlock'+$('#formid').val()+'" class="fancybox" '+
						'title="Unlock '+$('#usr_name').val()+'" '+
						'onClick="prepareunlock(\''+$('#formid').val()+'\');">'+
							'<span class="label label-danger">Black List</span>'+
						'</a>'
						);
					}else{
						$('#fstatus'+$('#formid').val()).html(
						'<span class="label label-success">Active</span>'
						);
					}
					if ($('#usr_gr_id').select2('val') == null){
						$('#fusr_gr_id'+$('#formid').val()).html('0');
						$('#fgr_name'+$('#formid').val()).html('');	
					}else{
						$('#fusr_gr_id'+$('#formid').val()).html($('#usr_gr_id').select2('val'));
						$('#fgr_name'+$('#formid').val()).html(
							$('#usr_gr_id').select2('data')[0]['text']
						);
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2,#form3').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			var out = JSON.parse(xhr.responseText);
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.html';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}else if (out['Status']=='Un-lock Success!'){
					alert("Un-lock Success!");
					$('#fstatus'+$('#unlockid').val()).html(
					'<span class="label label-success">Active</span>'
					);
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});