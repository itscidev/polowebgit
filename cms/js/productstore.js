var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#ps_pv_id').select2("val","");
	if (typeof $('#ps_st_id').attr('multiple')===typeof undefined 
	|| $('#ps_st_id').attr('multiple')===false) {
		$('#ps_st_id').attr('multiple','multiple');
		$('#ps_st_id').attr('name','ps_st_id[]');
		$("#ps_st_id").select2("destroy").select2({
	    	placeholder: 'Choose one!',
			allowClear: true
		});
	}
	$('#ps_st_id').select2("val","");
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#ps_pv_id').select2("val",$('#fps_pv_id'+idnya).html());
	if ($('#ps_st_id').attr('multiple')){
		$('#ps_st_id').attr('name','ps_st_id');
		$('#ps_st_id').removeAttr('multiple');
		$("#ps_st_id").select2("destroy").select2({
	    	placeholder: 'Choose one!',
			allowClear: true
		});
	}
	$('#ps_st_id').select2("val",$('#fps_st_id'+idnya).html());
}

function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {
	
	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true
	});
	
    var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '45%' },
			{ 'sWidth'		: '45%' },
			{ 'sWidth'		: '10%' }
		]
	});
	
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
//			console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('ps_pv_id')){
						$('#ps_pv_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ps_st_id')){
						$('#ps_st_id').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					//$('#edit'+$('#formid').val()).prop('title', $('#pl_name').val());
					//$('#delete'+$('#formid').val()).prop('title', $('#pl_name').val());
					
					if ($('#ps_pv_id').select2('val')){
						$('#fps_pv_id'+$('#formid').val()).html( $('#ps_pv_id').select2('val') );
						$('#fps_pv_name'+$('#formid').val()).html($('#ps_pv_id').select2('data')[0].text);
					}else{
						$('#fps_pv_id'+$('#formid').val()).html('0');
						$('#fps_pv_name'+$('#formid').val()).html('');
					}
					if ($('#ps_st_id').select2('val')){
						$('#fps_st_id'+$('#formid').val()).html( $('#ps_st_id').select2('val') );
						$('#fps_st_name'+$('#formid').val()).html($('#ps_st_id').select2('data')[0].text);
					}else{
						$('#fps_st_id'+$('#formid').val()).html('0');
						$('#fps_st_name'+$('#formid').val()).html('');
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});