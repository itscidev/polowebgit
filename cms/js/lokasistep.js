var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}
function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#los_zip').val('');
	$('#los_prop_lok_id').select2("val","");
	$('#los_kab_lok_id').select2("val","");
	$('#los_kec_lok_id').select2("val","");
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#los_zip').val($('#flos_zip'+idnya).html());
	$('#los_prop_lok_id').select2("val",$('#flos_prop_lok_id'+idnya).html());
	$('#los_kab_lok_id').select2("val",$('#flos_kab_lok_id'+idnya).html());
	$('#los_kec_lok_id').select2("val",$('#flos_kec_lok_id'+idnya).html());
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {

	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true
	});
    
	var table = $('#datalist').DataTable( {
    	'order'				: [[ 0, "asc" ]],
    	'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '15%' },
			{ 'sWidth'		: '10%' }
		]
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					if (out['Error'].hasOwnProperty('los_prop_lok_id')){
						$('#los_prop_lok_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('los_kab_lok_id')){
						$('#los_kab_lok_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('los_kec_lok_id')){
						$('#los_kec_lok_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('los_zip')){
						$('#los_zip').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
//					$('#edit'+$('#formid').val()).prop('title','Edit Data ' + $('#los_zip').val());
//					$('#delete'+$('#formid').val()).prop('title','Delete Data ' + $('#los_zip').val());
					$('#flos_zip'+$('#formid').val()).html($('#los_zip').val());
					if ($('#los_prop_lok_id').select2('val')){
						$('#flos_prop_lok_id'+$('#formid').val()).html( $('#los_prop_lok_id').select2('val') );
						$('#flos_prop_lok_name'+$('#formid').val()).html($('#los_prop_lok_id').select2('data')[0]);
					}else{
						$('#flos_prop_lok_id'+$('#formid').val()).html('0');
						$('#flos_prop_lok_name'+$('#formid').val()).html('');
					}
					if ($('#los_kab_lok_id').select2('val')){
						$('#flos_kab_lok_id'+$('#formid').val()).html( $('#los_kab_lok_id').select2('val') );
						$('#flos_kab_lok_name'+$('#formid').val()).html($('#los_kab_lok_id').select2('data')[0]);
					}else{
						$('#flos_kab_lok_id'+$('#formid').val()).html('0');
						$('#flos_kab_lok_name'+$('#formid').val()).html('');
					}
					if ($('#los_kec_lok_id').select2('val')){
						$('#flos_kec_lok_id'+$('#formid').val()).html( $('#los_kec_lok_id').select2('val') );
						$('#flos_kec_lok_name'+$('#formid').val()).html($('#los_kec_lok_id').select2('data')[0]);
					}else{
						$('#flos_kec_lok_id'+$('#formid').val()).html('0');
						$('#flos_kec_lok_name'+$('#formid').val()).html('');
					}
					
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			var out = JSON.parse(xhr.responseText);
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.html';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});