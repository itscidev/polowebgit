var bar = $('.bar');
var percent = $('.percent');

$(document).ready(function() {

	//bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
    setTimeout(function(){
		var urldef = window.location.href.split("/cms/");
		$("iframe.wysihtml5-sandbox").contents().find("head").append(
			'<link rel="stylesheet" href="'+urldef[0]+'/css/product-wysihtml.css">'
		);
	}, 500);

	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true
	});
        
	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					
				}
			}else{
				alert('Update Success!');
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});