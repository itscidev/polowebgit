var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}
function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#cl_name').val('');
	$('#cl_code').val('ff0000');
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#cl_name').val( $('<textarea />').html( $('#fcl_name'+idnya).html() ).text() );
	$('#cl_code').val( $('<textarea />').html( $('#fcl_code'+idnya).html() ).text() );
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {
	
	$('#cl_code').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	}).bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});
	
	var table = $('#datalist').DataTable( {
    	'order'				: [[ 0, "asc" ]],
    	'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '50%' },
			{ 'sWidth'		: '40%' },
			{ 'sWidth'		: '10%' }
		]
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					if (out['Error'].hasOwnProperty('cl_name')){
						$('#cl_name').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('cl_code')){
						$('#cl_code').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', 'Edit Data ' + $('#cl_name').val());
					$('#delete'+$('#formid').val()).prop('title', 'Delete Data ' + $('#cl_name').val());
					$('#fcl_name'+$('#formid').val()).html( $('#cl_name').val() );
					$('#fcl_code_display'+$('#formid').val()).find('i').css(
						'color','#'+$('#cl_code').val()
					);
					$('#fcl_code'+$('#formid').val()).html( $('#cl_code').val() );
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			var out = JSON.parse(xhr.responseText);
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.html';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});