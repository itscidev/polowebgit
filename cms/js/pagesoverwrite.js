var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}
function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#pg_name').val('');
	$('#pg_title').val('');
	$('#pg_keyword').val('');
	$('#pg_desc').val('');
	$('#pg_status').prop('checked', false);
	$('#pg_draft').prop('checked', true);
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#pg_name').val( $('<textarea />').html( $('#fpg_name'+idnya).html() ).text() );
	$('#pg_title').val( $('<textarea />').html( $('#fpg_title'+idnya).html() ).text() );
	$('#pg_keyword').val( $('<textarea />').html( $('#fpg_keyword'+idnya).html() ).text() );
	$('#pg_desc').val( $('<textarea />').html( $('#fpg_desc'+idnya).html() ).text() );
	if ($('#fpg_status'+idnya).html()=='1'){
		$('#pg_status').prop('checked', true);
	}else{
		$('#pg_status').prop('checked', false);
	}
	if ($('#fpg_draft'+idnya).html()=='1'){
		$('#pg_draft').prop('checked', true);
	}else{
		$('#pg_draft').prop('checked', false);
	}
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {

	var table = $('#datalist').DataTable( {
    	'order'				: [[ 0, "asc" ]],
    	'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '40%' },
			{ 'sWidth'		: '40%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' }
		]
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					if (out['Error'].hasOwnProperty('pg_name')){
						$('#pg_name').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', 'Edit Data ' + $('#pg_name').val());
					$('#delete'+$('#formid').val()).prop('title', 'Delete Data ' + $('#pg_name').val());
					$('#fpg_name'+$('#formid').val()).html( htmlentities($('#pg_name').val()) );
					$('#fpg_title'+$('#formid').val()).html( htmlentities($('#pg_title').val()) );
					$('#fpg_keyword'+$('#formid').val()).html( htmlentities($('#pg_keyword').val()) );
					$('#fpg_desc'+$('#formid').val()).html( htmlentities($('#pg_desc').val()) );
					if ($('#pg_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fpg_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fpg_status'+$('#formid').val()).html('0');
					}
					if ($('#pg_draft').prop("checked")){
						$('#fdraft'+$('#formid').val()).html(
							'<span class="label label-warning">Draft</span>'
						);
						$('#fpg_draft'+$('#formid').val()).html('1');
					}else{
						$('#fdraft'+$('#formid').val()).html('');
						$('#fpg_draft'+$('#formid').val()).html('0');
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			var out = JSON.parse(xhr.responseText);
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.html';
					}
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});