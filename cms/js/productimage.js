var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function setMain(idPar,idChl){
	$(".setMain" + idPar).html('Set');
	$.post( "processingproductimage.html", { setmainid: idChl }, function( out ) {
		if (out.hasOwnProperty('Error')){
			if (typeof out['Error'] == 'string'){
				alert(out['Error']);
				if (out['Error']=='Login session expired!'){
					parent.window.location.href='login.php';
				}
			}
		}else{
			$("#tr" + idChl + " > td > span > span").html('Main');
		}
	}, "json");
}
function prepareinsertnew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#pi_sequence').val('');
	$('#pi_sequence').parent().find("small").show();
	$('#pi_image').val('');
}
function preparenew(){
	prepareinsertnew();
	$('#formid').val('new');
	//$('#pi_image').attr( "multiple", "multiple" );
	$('#pi_status').prop('checked', false);
	$('#pi_draft').prop('checked', true);
	$('#pi_sku_id').select2("val","");
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#pi_image').removeAttr( "multiple" );
	$('#pi_image').val('');
	$('#pi_sequence').val($('#fpi_sequence'+idnya).html());
	$('#pi_sequence').parent().find("small").hide();
	if ($('#fpi_status'+idnya).html()=='1'){
		$('#pi_status').prop('checked', true);
	}else{
		$('#pi_status').prop('checked', false);
	}
	if ($('#fpi_draft'+idnya).html()=='1'){
		$('#pi_draft').prop('checked', true);
	}else{
		$('#pi_draft').prop('checked', false);
	}
	if ($('#pi_sku_id').find("option[value='" + $('#fpi_sku_id'+idnya).html() + "']").length) {
	    $('#pi_sku_id').val($('#fpi_sku_id'+idnya).html()).trigger('change');
	} else { 
	    // Create a DOM Option and pre-select by default
	    var newOption = new Option(
	    	$('#fpi_pr_name'+idnya).html() + '/' + 
	    	$('#fpi_st_name'+idnya).html() + '/' + 
	    	$('#fpi_ci_name'+idnya).html(), 
	    	$('#fpi_sku_id'+idnya).html(), 
	    	true, 
	    	true
	    );
	    // Append it to the select
	    $('#pi_sku_id').append(newOption).trigger('change');
	}
}

function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {
	
	//Initialize Select2 Elements
	function format(state) {
//		console.log(state);
		if (!state.id){
			return state.text; // optgroup	
		}else{
			return state.text;
		}
    }
	$("select.select2ajax").each(function () {
		var element = $(this);
		var urltarget = element.parent().find('p.urltarget').html();
		//'http://192.168.0.232/performances/getajaxbrand.html'
		element.select2({
			ajax: {
				url: function () {
					//console.log(urltarget);
					return urltarget;
			    },
			    dataType: 'json',
				method: "get",
		        data: function (params) {
		            return {
		                search: params.term
		            };
		        },
		        processResults: function (data) {
		        	//console.log(data);
		            return {
		                results: data.results
		            };
		        },
		        cache: true
			},
			placeholder: 'Type min. 2 character',
			minimumInputLength: 2,
			allowClear: true,
			templateResult: format,
		    templateSelection: format,
			escapeMarkup: function (m) {
	            return m;
	        }
		});
	});
	
    var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '26%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '24%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' }
		]
	});
	
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
//			console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('pi_sku_id')){
						$('#pi_sku_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pi_image')){
						$('#pi_image').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					prepareinsertnew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					//$('#edit'+$('#formid').val()).prop('title', $('#pi_name').val());
					//$('#delete'+$('#formid').val()).prop('title', $('#pi_name').val());
					
					if (out.hasOwnProperty('pi_image')){
						$('#fpi_image'+$('#formid').val()).html( out['pi_image'] );
					}
					$('#fpi_sequence'+$('#formid').val()).html($('#pi_sequence').val());
					if ($('#pi_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fpi_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fpi_status'+$('#formid').val()).html('0');
					}
					if ($('#pi_draft').prop("checked")){
						$('#fdraft'+$('#formid').val()).html(
							'<span class="label label-warning">Draft</span>'
						);
						$('#fpi_draft'+$('#formid').val()).html('1');
					}else{
						$('#fdraft'+$('#formid').val()).html('');
						$('#fpi_draft'+$('#formid').val()).html('0');
					}
					if ($('#pi_sku_id').select2('val')){
						$('#fpi_sku_id'+$('#formid').val()).html( $('#pi_sku_id').select2('val') );
						$('#fpi_pr_name'+$('#formid').val()).html(out['pr_name']);
						$('#fpi_st_name'+$('#formid').val()).html(out['st_name']);
						$('#fpi_ci_name'+$('#formid').val()).html(out['ci_name']);
					}else{
						$('#fpi_sku_id'+$('#formid').val()).html('0:0:0');
						$('#fpi_pr_name'+$('#formid').val()).html('');
						$('#fpi_st_name'+$('#formid').val()).html('');
						$('#fpi_ci_name'+$('#formid').val()).html('');
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});