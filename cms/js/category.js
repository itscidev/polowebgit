var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}
function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#ca_name').val('');
	$('#ca_desc').val('');
	$('#ca_sequence').val(0);
	$('#ca_status').prop('checked', true);
	$('#ca_draft').prop('checked', true);
	$('#ca_image').val('');
	$('#ca_ca_id').select2("val","");
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#ca_name').val( $('<textarea />').html( $('#fca_name'+idnya).html() ).text() );
	$('#ca_desc').val( $('<textarea />').html( $('#fca_desc'+idnya).html() ).text() );
	$('#ca_sequence').val($('#fca_sequence'+idnya).html());
	if ($('#fca_status'+idnya).html()=='1'){
		$('#ca_status').prop('checked', true);
	}else{
		$('#ca_status').prop('checked', false);
	}
	if ($('#fca_draft'+idnya).html()=='1'){
		$('#ca_draft').prop('checked', true);
	}else{
		$('#ca_draft').prop('checked', false);
	}
	$('#ca_image').val('');
	$('#ca_ca_id').select2("val",$('#fca_ca_id'+idnya).html());
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
	$('#deleteimage').prop('checked', false);
}

$(document).ready(function() {

	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true
	});
    
	var table = $('#datalist').DataTable( {
    	'order'				: [[ 0, "asc" ]],
    	'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '20%' },
			{ 'sWidth'		: '13%' },
			{ 'sWidth'		: '7%' }
		]
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					if (out['Error'].hasOwnProperty('ca_name')){
						$('#ca_name').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ca_ca_id')){
						$('#ca_ca_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ca_image')){
						$('#ca_image').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title','Edit Data ' + $('#ca_name').val());
					$('#delete'+$('#formid').val()).prop('title','Delete Data ' + $('#ca_name').val());
					$('#fca_name'+$('#formid').val()).html( $('#ca_name').val() );
					$('#fca_desc'+$('#formid').val()).html( $('#ca_desc').val() );
					$('#fca_sequence'+$('#formid').val()).html($('#ca_sequence').val());
					if ($('#ca_ca_id').select2('val')){
						$('#fca_ca_id'+$('#formid').val()).html( $('#ca_ca_id').select2('val') );
						$('#fca_ca_name'+$('#formid').val()).html($('#ca_ca_id').select2('data')[0].text);
					}else{
						$('#fca_ca_id'+$('#formid').val()).html('0');
						$('#fca_ca_name'+$('#formid').val()).html('');
					}
					if ($('#ca_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fca_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fca_status'+$('#formid').val()).html('0');
					}
					if ($('#ca_draft').prop("checked")){
						$('#fdraft'+$('#formid').val()).html(
							'<span class="label label-warning">Draft</span>'
						);
						$('#fca_draft'+$('#formid').val()).html('1');
					}else{
						$('#fdraft'+$('#formid').val()).html('');
						$('#fca_draft'+$('#formid').val()).html('0');
					}
					if (out.hasOwnProperty('ca_image')){
						$('#fca_image'+$('#formid').val()).html( out['ca_image'] );
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			var out = JSON.parse(xhr.responseText);
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.html';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}else if (out['Status']=='Delete Image Success!'){
					alert("Delete Image Success!");
					$('#fca_image'+$('#delid').val()).html( out['ca_image'] );
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});