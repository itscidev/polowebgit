var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#bn_title').val('');
	$('#bn_subtitle').val('');
	$('#bn_link').val('');
	$('#bn_sequence').val(0);
	$('#bn_status').prop('checked', false);
	$('#bn_draft').prop('checked', true);
	$('#bn_image').val('');
	$(".bns_ca_id").prop('checked', false);
	var now = new Date(Date.now());
	$('#bn_from').data('daterangepicker').setStartDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#bn_from').data('daterangepicker').setEndDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#bn_to').data('daterangepicker').setStartDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#bn_to').data('daterangepicker').setEndDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#bn_title').val($('#fbn_title'+idnya).html());
	$('#bn_subtitle').val($('#fbn_subtitle'+idnya).html());
	$('#bn_link').val($('#fbn_link'+idnya).html());
	$('#bn_sequence').val($('#fbn_sequence'+idnya).html());
	if ($('#fbn_status'+idnya).html()=='1'){
		$('#bn_status').prop('checked', true);
	}else{
		$('#bn_status').prop('checked', false);
	}
	if ($('#fbn_draft'+idnya).html()=='1'){
		$('#bn_draft').prop('checked', true);
	}else{
		$('#bn_draft').prop('checked', false);
	}
	$('#bn_image').val('');
	$(".bns_ca_id").prop('checked', false);
	if ($('#fbns_ca_id'+idnya).html() != ''){
		var splitconcat = $('#fbns_ca_id'+idnya).html().split(",");
		$.each( splitconcat, function( index, value ) {
			$("#bns_ca_id"+value).prop('checked', true);
		});
	}
	$('#bn_from').val( $('<textarea />').html( $('#fbn_from'+idnya).html() ).text() );
	$('#bn_from').data('daterangepicker').setStartDate(
		$('<textarea />').html( $('#fbn_from'+idnya).html() ).text()
	);
	$('#bn_to').val( $('<textarea />').html( $('#fbn_to'+idnya).html() ).text() );
	$('#bn_to').data('daterangepicker').setStartDate(
		$('<textarea />').html( $('#fbn_to'+idnya).html() ).text()
	);
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {
    
	var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '14%' },
			{ 'sWidth'		: '17%' },
			{ 'sWidth'		: '17%' },
			{ 'sWidth'		: '22%' },
			{ 'sWidth'		: '10%' }
		]
	});

	//Date range picker
	$('#bn_from,#bn_to').daterangepicker({
		singleDatePicker: true,
        timePicker: true,
        timePickerIncrement: 1,
        timePickerSeconds: true,
        timePicker24Hour: true,
        locale: {
            format: 'MM/DD/YYYY HH:mm:ss'
        }
    });
	
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('bn_title')){
						$('#bn_title').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('bn_image')){
						$('#bn_image').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('bn_from')){
						$('#bn_from').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('bn_to')){
						$('#bn_to').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#bn_title').val());
					$('#delete'+$('#formid').val()).prop('title', $('#bn_title').val());
					
					$('#fbn_title'+$('#formid').val()).html($('#bn_title').val());
					$('#fbn_subtitle'+$('#formid').val()).html($('#bn_subtitle').val());
					$('#fbn_link'+$('#formid').val()).html($('#bn_link').val());
					$('#fbn_sequence'+$('#formid').val()).html($('#bn_sequence').val());
					$('#fbn_from'+$('#formid').val()).html($('#bn_from').val());
					$('#fbn_to'+$('#formid').val()).html($('#bn_to').val());
					if ($('#bn_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fbn_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fbn_status'+$('#formid').val()).html('0');
					}
					if ($('#bn_draft').prop("checked")){
						$('#fdraft'+$('#formid').val()).html(
							'<span class="label label-warning">Draft</span>'
						);
						$('#fbn_draft'+$('#formid').val()).html('1');
					}else{
						$('#fdraft'+$('#formid').val()).html('');
						$('#fbn_draft'+$('#formid').val()).html('0');
					}
					if (out.hasOwnProperty('bn_image')){
						$('#fbn_image'+$('#formid').val()).html( out['bn_image'] );
					}
					$("#fbns_ca_id"+$('#formid').val()).html(out['bns_ca_id']);
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});