var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#pt_name').val('');
	$('#pt_vendor').select2("val","");
	$('#pt_logo').val('');
	$('#pt_status').select2("val","0");
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#pt_name').val($('#fpt_name'+idnya).html());
	$('#pt_vendor').select2("val",$('#fpt_vendor'+idnya).html());
	$('#pt_logo').val('');
	$('#pt_status').select2("val",$('#fpt_status'+idnya).html());
}

function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {
	
	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true
	});
	
    var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '15%' },
			{ 'sWidth'		: '30%' },
			{ 'sWidth'		: '30%' },
			{ 'sWidth'		: '15%' },
			{ 'sWidth'		: '10%' }
		]
	});
	
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        },
		afterLoad: function() {
			
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('pt_vendor')){
						$('#pt_vendor').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pt_name')){
						$('#pt_name').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pt_logo')){
						$('#pt_logo').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#pt_name').val());
					$('#delete'+$('#formid').val()).prop('title', $('#pt_name').val());
					
					$('#fpt_name'+$('#formid').val()).html($('#pt_name').val());
					if (out.hasOwnProperty('pt_logo')){
						$('#fpt_logo'+$('#formid').val()).html( out['pt_logo'] );
					}
					if ($('#pt_vendor').select2('val')){
						$('#fpt_vendor'+$('#formid').val()).html( $('#pt_vendor').select2('val') );
					}else{
						$('#fpt_vendor'+$('#formid').val()).html('');
					}
					if ($('#pt_status').select2('val')){
						$('#fpt_status'+$('#formid').val()).html( $('#pt_status').select2('val') );
						$('#fpt_status_name'+$('#formid').val()).html($('#pt_status').select2('data')[0]['text']);
					}else{
						$('#fpt_status'+$('#formid').val()).html('0');
						$('#fpt_status_name'+$('#formid').val()).html('Pasif');
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});