var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#bt_title').val('');
	$('#bt_anchor').val('');
	$('#bt_link').val('');
	$('#bt_sequence').val(0);
	$('#bt_status').prop('checked', false);
	$('#bt_draft').prop('checked', true);
	$(".StartTime,.FinishTime").show();
	var now = new Date(Date.now());
	$('#bt_from').data('daterangepicker').setStartDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$('#bt_to').data('daterangepicker').setStartDate(
		(now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + 
		now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()
	);
	$(".bts_ca_id").prop('checked', false);
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#bt_title').val($('#fbt_title'+idnya).html());
	$('#bt_anchor').val($('#fbt_anchor'+idnya).html());
	$('#bt_link').val($('#fbt_link'+idnya).html());
	$('#bt_from').val( $('<textarea />').html( $('#fbt_from'+idnya).html() ).text() );
	$('#bt_from').data('daterangepicker').setStartDate(
		$('<textarea />').html( $('#fbt_from'+idnya).html() ).text()
	);
	$('#bt_to').val( $('<textarea />').html( $('#fbt_to'+idnya).html() ).text() );
	$('#bt_to').data('daterangepicker').setStartDate(
		$('<textarea />').html( $('#fbt_to'+idnya).html() ).text()
	);
	$('#bt_sequence').val($('#fbt_sequence'+idnya).html());
	if ($('#fbt_status'+idnya).html()=='1'){
		$('#bt_status').prop('checked', true);
	}else{
		$('#bt_status').prop('checked', false);
	}
	if ($('#fbt_draft'+idnya).html()=='1'){
		$('#bt_draft').prop('checked', true);
	}else{
		$('#bt_draft').prop('checked', false);
	}
	$(".bts_ca_id").prop('checked', false);
	if ($('#fbts_ca_id'+idnya).html() != ''){
		var splitconcat = $('#fbts_ca_id'+idnya).html().split(",");
		$.each( splitconcat, function( index, value ) {
			$("#bts_ca_id"+value).prop('checked', true);
		});
	}
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {

    var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '28%' },
			{ 'sWidth'		: '32%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' }
		]
	});
	
	//Date range picker
	$('#bt_from,#bt_to').daterangepicker({
		singleDatePicker: true,
        timePicker: true,
        timePickerIncrement: 1,
        timePickerSeconds: true,
        timePicker24Hour: true,
        locale: {
            format: 'MM/DD/YYYY HH:mm:ss'
        }
    });

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('bt_title')){
						$('#bt_title').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('bt_anchor')){
						$('#bt_anchor').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('bt_from')){
						$('#bt_from').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('bt_to')){
						$('#bt_to').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#bt_title').val());
					$('#delete'+$('#formid').val()).prop('title', $('#bt_title').val());
					
					$('#fbt_title'+$('#formid').val()).html($('#bt_title').val());
					$('#fbt_anchor'+$('#formid').val()).html($('#bt_anchor').val());
					$('#fbt_link'+$('#formid').val()).html($('#bt_link').val());
					$('#fbt_sequence'+$('#formid').val()).html($('#bt_sequence').val());
					$('#fbt_from'+$('#formid').val()).html($('#bt_from').val());
					$('#fbt_to'+$('#formid').val()).html($('#bt_to').val());
					if ($('#bt_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fbt_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fbt_status'+$('#formid').val()).html('0');
					}
					if ($('#bt_draft').prop("checked")){
						$('#fdraft'+$('#formid').val()).html(
							'<span class="label label-warning">Draft</span>'
						);
						$('#fbt_draft'+$('#formid').val()).html('1');
					}else{
						$('#fdraft'+$('#formid').val()).html('');
						$('#fbt_draft'+$('#formid').val()).html('0');
					}
					$("#fbts_ca_id"+$('#formid').val()).html(out['bts_ca_id']);
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});