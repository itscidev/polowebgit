var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#mm_name').val('');
	$('#mm_email').val('');
	$('#mm_banned').val('');
	$('#mm_bannedreason').val('');
	$('#mm_foto').val('');
	$('#mm_emailstatus').prop('checked', false);
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#mm_name').val($('#fmm_name'+idnya).html());
	$('#mm_email').val($('#fmm_email'+idnya).html());
	$('#mm_banned').val($('#fmm_banned'+idnya).html());
	$('#mm_bannedreason').val($('#fmm_bannedreason'+idnya).html());
	$('#mm_foto').val('');
	if ($('#fmm_emailstatus'+idnya).html()=='1'){
		$('#mm_emailstatus').prop('checked', true);
	}else{
		$('#mm_emailstatus').prop('checked', false);
	}
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {

    var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' }
		]
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			// console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('mm_name')){
						$('#mm_name').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('mm_email')){
						$('#mm_email').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('mm_foto')){
						$('#mm_foto').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#mm_name').val());
					$('#delete'+$('#formid').val()).prop('title', $('#mm_name').val());
					
					$('#fmm_name'+$('#formid').val()).html($('#mm_name').val());
					$('#fmm_email'+$('#formid').val()).html($('#mm_email').val());
					$('#fmm_banned'+$('#formid').val()).html($('#mm_banned').val());
					$('#fmm_bannedreason'+$('#formid').val()).html($('#mm_bannedreason').val());
					if (parseInt($('#mm_banned').val())>7){
						$('#fbannedstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Banned</span>'
						);
					}else{
						$('#fbannedstatus'+$('#formid').val()).html('');
					}
					if ($('#mm_emailstatus').prop("checked")){
						$('#femailstatus'+$('#formid').val()).html(
							'<span class="label label-success">Email</span>'
						);
						$('#fmm_emailstatus'+$('#formid').val()).html('1');
					}else{
						$('#femailstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Email</span>'
						);
						$('#fmm_emailstatus'+$('#formid').val()).html('0');
					}
					if (out.hasOwnProperty('mm_foto')){
						$('#fmm_foto'+$('#formid').val()).html( out['mm_foto'] );
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});