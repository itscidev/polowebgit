var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function prepareinsertnew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#pv_qty').val('');
	$('#pv_artikel').val('');
}
function preparenew(){
	prepareinsertnew();
	$('#formid').val('new');
	$('#pv_name').val('');
	$('#pv_status').prop('checked', false);
	$('#pv_draft').prop('checked', true);
	$('#pv_pr_id').select2("val","");
	$('#pv_st_id').select2("val","");
	$('#pv_sz_id').select2("val","");
	$('#pv_ci_id').select2("val","");
	$('#pv_price').val('');
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#pv_name').val($('#fpv_name'+idnya).html());
	$('#pv_qty').val($('#fpv_qty'+idnya).html());
	$('#pv_price').val($('#fpv_price'+idnya).html());
	$('#pv_artikel').val($('#fpv_artikel'+idnya).html());
	if ($('#fpv_status'+idnya).html()=='1'){
		$('#pv_status').prop('checked', true);
	}else{
		$('#pv_status').prop('checked', false);
	}
	if ($('#fpv_draft'+idnya).html()=='1'){
		$('#pv_draft').prop('checked', true);
	}else{
		$('#pv_draft').prop('checked', false);
	}
	$('#pv_pr_id').select2("val",$('#fpv_pr_id'+idnya).html());
	$('#pv_st_id').select2("val",$('#fpv_st_id'+idnya).html());
	$('#pv_sz_id').select2("val",$('#fpv_sz_id'+idnya).html());
	$('#pv_ci_id').select2("val",$('#fpv_ci_id'+idnya).html());
}

function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {
	
	//auto create pv_name
	$('.select2').on('select2:close', function (e) {
		var pvname = '';
	    if ($("#formid").val()=='new'){
	    	if ($('#pv_pr_id').select2('val')!=null){
	    		if (pvname != ''){pvname += '/';}
	    		pvname += $('#pv_pr_id').select2('data')[0].text;
	    	}
	    	if ($('#pv_st_id').select2('val')!=null){
	    		if (pvname != ''){pvname += '/';}
	    		pvname += $('#pv_st_id').select2('data')[0].text;
	    	}
	    	if ($('#pv_sz_id').select2('val')!=null){
	    		if (pvname != ''){pvname += '/';}
	    		pvname += $('#pv_sz_id').select2('data')[0].text;
	    	}
	    	if ($('#pv_ci_id').select2('val')!=null){
	    		if (pvname != ''){pvname += '/';}
	    		pvname += $("#pv_ci_id option:selected").html();
	    	}
	    	$("#pv_name").val(pvname);
	    }
	});
	//auto fill price
	var jsonprice = jQuery.parseJSON($("#defprprice").html());
	$('#pv_pr_id').on('select2:select', function (e) {
		if ($("#formid").val()=='new'){
			var idproduct = $('#pv_pr_id').select2('val');
			//console.log(idproduct);
			$('#pv_price').val(jsonprice[idproduct]);
		}
	});
	
	//Initialize Select2 Elements
	function format(state) {
//		console.log(state);
		if (!state.id){
			return state.text; // optgroup	
		}else if (state.element.attributes.hasOwnProperty("imgsrc")){
			return state.text + '&nbsp;&nbsp;<img src="' + 
	        state.element.attributes.imgsrc.nodeValue + '" class="colorimg">';
		}else{
			return state.text;
		}
    }
	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true,
		templateResult: format,
	    templateSelection: format,
		escapeMarkup: function (m) {
            return m;
        }
	});
	
    var table = $('#datalist').DataTable( {
    	'order'				: [[0,'asc'],[1,'asc'],[2,'asc']],
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '16%' },
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '5%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '27%' },
			{ 'sWidth'		: '9%' },
			{ 'sWidth'		: '5%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' }
		]
	});
	
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
//			console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('pv_pr_id')){
						$('#pv_pr_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pv_name')){
						$('#pv_name').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pv_artikel')){
						$('#pv_artikel').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pv_st_id')){
						$('#pv_st_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pv_sz_id')){
						$('#pv_sz_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('pv_ci_id')){
						$('#pv_ci_id').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					prepareinsertnew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#pv_name').val());
					$('#delete'+$('#formid').val()).prop('title', $('#pv_name').val());
					
					$('#fpv_name'+$('#formid').val()).html($('#pv_name').val());
					$('#fpv_artikel'+$('#formid').val()).html($('#pv_artikel').val());
					$('#fpv_price'+$('#formid').val()).html($('#pv_price').val());
					$('#fpv_qty'+$('#formid').val()).html($('#pv_qty').val());
					if ($('#pv_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fpv_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fpv_status'+$('#formid').val()).html('0');
					}
					if ($('#pv_draft').prop("checked")){
						$('#fdraft'+$('#formid').val()).html(
							'<span class="label label-warning">Draft</span>'
						);
						$('#fpv_draft'+$('#formid').val()).html('1');
					}else{
						$('#fdraft'+$('#formid').val()).html('');
						$('#fpv_draft'+$('#formid').val()).html('0');
					}
					if ($('#pv_st_id').select2('val')){
						$('#fpv_st_id'+$('#formid').val()).html( $('#pv_st_id').select2('val') );
						$('#fpv_st_name'+$('#formid').val()).html($('#pv_st_id').select2('data')[0].text);
					}else{
						$('#fpv_st_id'+$('#formid').val()).html('0');
						$('#fpv_st_name'+$('#formid').val()).html('');
					}
					if ($('#pv_sz_id').select2('val')){
						$('#fpv_sz_id'+$('#formid').val()).html( $('#pv_sz_id').select2('val') );
						$('#fpv_sz_name'+$('#formid').val()).html($('#pv_sz_id').select2('data')[0].text);
					}else{
						$('#fpv_sz_id'+$('#formid').val()).html('0');
						$('#fpv_sz_name'+$('#formid').val()).html('');
					}
					if ($('#pv_ci_id').select2('val')){
						var comtext = $('#pv_ci_id').select2('data')[0];
						$('#fpv_ci_id'+$('#formid').val()).html( $('#pv_ci_id').select2('val') );
						$('#fpv_ci_name'+$('#formid').val()).html(
							comtext.text + '&nbsp;&nbsp;' + '<img src="' + 
							comtext.element.attributes.imgsrc.nodeValue + '" class="colorimg">'
						);
					}else{
						$('#fci_cl_id'+$('#formid').val()).html('0');
						$('#fci_cl_name'+$('#formid').val()).html('');
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});