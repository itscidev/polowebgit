var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}
function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#ci_sequence').val(0);
	$('#ci_image').val('');
	$('#ci_cl_id').select2("val","");
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#ci_sequence').val($('#fci_sequence'+idnya).html());
	$('#ci_image').val('');
	$('#ci_cl_id').select2("val",$('#fci_cl_id'+idnya).html());
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

$(document).ready(function() {

	//Initialize Select2 Elements
	function format(state) {
		if (!state.id){
			return state.text; // optgroup	
		}else if (state.element.attributes.hasOwnProperty("color")){
			return '<i class="fa fa-circle" style="color:#' + state.element.attributes.color.nodeValue + ';">' + 
        	'</i>&nbsp;&nbsp;' + state.text;
		}else{
			return state.text;
		}
    }
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true,
		templateResult: format,
	    templateSelection: format,
		escapeMarkup: function (m) {
            return m;
        }
	});
    
	var table = $('#datalist').DataTable( {
    	'order'				: [[ 0, "asc" ]],
    	'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '60%' },
			{ 'sWidth'		: '20%' },
			{ 'sWidth'		: '13%' },
			{ 'sWidth'		: '7%' }
		]
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					if (out['Error'].hasOwnProperty('ci_cl_id')){
						$('#ci_cl_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ci_image')){
						$('#ci_image').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
//					$('#edit'+$('#formid').val()).prop('title','Edit Data ' + $('#ci_name').val());
//					$('#delete'+$('#formid').val()).prop('title','Delete Data ' + $('#ci_name').val());
					$('#fci_sequence'+$('#formid').val()).html($('#ci_sequence').val());
					if ($('#ci_cl_id').select2('val')){
						var comtext = $('#ci_cl_id').select2('data')[0];
						$('#fci_cl_id'+$('#formid').val()).html( $('#ci_cl_id').select2('val') );
						$('#fci_cl_name'+$('#formid').val()).html(
							'<i class="fa fa-circle" style="color:#' + 
							comtext.element.attributes.color.nodeValue + ';"></i>&nbsp;&nbsp;' + 
							comtext.text
						);
					}else{
						$('#fci_cl_id'+$('#formid').val()).html('0');
						$('#fci_cl_name'+$('#formid').val()).html('');
					}
					if (out.hasOwnProperty('ci_image')){
						$('#fci_image'+$('#formid').val()).html( out['ci_image'] );
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			var out = JSON.parse(xhr.responseText);
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.html';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});