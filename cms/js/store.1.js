var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#st_code').val('');
	$('#st_name').val('');
	$('#st_dispname').val('');
	$('#st_zip').val('');
	$('#st_lat').val('');
	$('#st_long').val('');
	$('#st_status').prop('checked', false);
	$('#st_address').val('');
	if ($(".wysihtml5-sandbox").length > 0){
		$(".wysihtml5-sandbox").contents().find("body").html('');
	}
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#st_code').val($('#fst_code'+idnya).html());
	$('#st_name').val($('#fst_name'+idnya).html());
	$('#st_dispname').val($('#fst_dispname'+idnya).html());
	$('#st_zip').val($('#fst_zip'+idnya).html());
	$('#st_lat').val($('#fst_lat'+idnya).html());
	$('#st_long').val($('#fst_long'+idnya).html());
	if ($('#fst_status'+idnya).html()=='1'){
		$('#st_status').prop('checked', true);
	}else{
		$('#st_status').prop('checked', false);
	}
	$('#st_address').val(
		$('<textarea />').html( $('#fst_address'+idnya).html() ).text()
	);
}

function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}

function getGPS(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$("#gps").html("Processing...");
	$(".resultGPS").html('');
	$.post( "processinggpscoordinates.html", { st_address: $("#st_address").val(),tokensec:$("#tokensec").val() }, function( out ) {
		if (out.hasOwnProperty('Error')){
			if (typeof out['Error'] == 'string'){
				if (out['Error']=='Login session expired!'){
					alert(out['Error']);
					parent.window.location.href='login.php';
				}else{
					$(".resultGPS").html(out['Error']);
				}
			}else{
				if (out['Error'].hasOwnProperty('st_address')){
					$('#st_address').parent().addClass( "has-error" );
				}
			}
		}else if (out.hasOwnProperty('error')){
			$(".resultGPS").html(out['error']);
		}else{
			out = JSON.parse(out);
			//console.log(out);
			$.each(out, function( index, value ) {
				$(".resultGPS").append(
					'<p onclick="setGPS(' + value['lat'] + ',' + value['lon'] + '); return false;">' + 
					value['display_name'] + '<br>' + 'Lat : ' + value['lat'] + ', ' + 
					'Long : ' + value['lon'] + '</p>'
				);
			});
		}
		$("#gps").html('<a href="store.html" onclick="getGPS(); return false;">Get GPS Coordinates</a>');
	}, "json");
}
function setGPS(lat,lon){
	$("#st_lat").val( lat );
	$("#st_long").val( lon );
}

$(document).ready(function() {
	
    var table = $('#datalist').DataTable( {
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '22%' },
			{ 'sWidth'		: '22%' },
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' }
		]
	});
	
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        },
		afterLoad: function() {
			//bootstrap WYSIHTML5 - text editor
			if ($(".wysihtml5-sandbox").length > 0){
				$('.wysihtml5-sandbox').remove();
				$("input[name='_wysihtml5_mode']").remove();
				$('.wysihtml5-toolbar').remove();
				$('.textarea').css('display','block');
			}
			$('.textarea').wysihtml5();
			setTimeout(function(){
				var urldef = window.location.href.split("/cms/");
				$("iframe.wysihtml5-sandbox").contents().find("head").append(
					'<link rel="stylesheet" href="'+urldef[0]+'/css/product-wysihtml.css">'
				);
			}, 500);
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.php';
					}
				}else{
					if (out['Error'].hasOwnProperty('st_code')){
						$('#st_code').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('st_name')){
						$('#st_name').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('st_dispname')){
						$('#st_dispname').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('st_address')){
						$('#st_address').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					$('#edit'+$('#formid').val()).prop('title', $('#st_code').val());
					$('#delete'+$('#formid').val()).prop('title', $('#st_code').val());
					
					$('#fst_code'+$('#formid').val()).html($('#st_code').val());
					$('#fst_name'+$('#formid').val()).html($('#st_name').val());
					$('#fst_dispname'+$('#formid').val()).html($('#st_dispname').val());
					$('#fst_zip'+$('#formid').val()).html($('#st_zip').val());
					$('#fst_lat'+$('#formid').val()).html($('#st_lat').val());
					$('#fst_long'+$('#formid').val()).html($('#st_long').val());
					if ($('#st_status').prop("checked")){
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-success">Active</span>'
						);
						$('#fst_status'+$('#formid').val()).html('1');
					}else{
						$('#fstatus'+$('#formid').val()).html(
							'<span class="label label-danger">Pasif</span>'
						);
						$('#fst_status'+$('#formid').val()).html('0');
					}
					$('#fst_address'+$('#formid').val()).html($('#st_address').val());
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.php';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});