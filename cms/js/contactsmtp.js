var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}

function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#ms_purpose').select2('val','cms');
	$('#ms_sendername').val('');
	$('#ms_sendermail').val('');
	$('#ms_password').val('');
	$('#ms_host').val('');
	$('#ms_secure').select2('val','tls');
	$('#ms_port').val('25');
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#ms_purpose').select2('val',$('#fms_purpose'+idnya).html());
	$('#ms_sendermail').val($('#fms_sendermail'+idnya).html());
	$('#ms_sendername').val( $('<textarea />').html( $('#fms_sendername'+idnya).html() ).text() );
	$('#ms_password').val($('<textarea />').html( $('#fms_password'+idnya).html() ).text() );
	$('#ms_host').val($('#fms_host'+idnya).html());
	$('#ms_secure').select2('val',$('#fms_secure'+idnya).html());
	$('#ms_port').val($('#fms_port'+idnya).html());
}
	
$(document).ready(function() {
	
	var table = $('#datalist').DataTable( {
		'order'				: [[ 0, "asc" ]],
		'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '23%' },
			{ 'sWidth'		: '23%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '14%' },
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '8%' },
			{ 'sWidth'		: '6%' }
		]
	});
	
	//Initialize Select2 Elements
    $(".select2").select2();
    
	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					if (out['Error'].hasOwnProperty('ms_purpose')){
						$('#ms_purpose').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ms_sendermail')){
						$('#ms_sendermail').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ms_sendername')){
						$('#ms_sendername').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ms_password')){
						$('#ms_password').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ms_host')){
						$('#ms_host').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ms_secure')){
						$('#ms_secure').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ms_port')){
						$('#ms_port').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data! Wait for reloading page!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
					
					$('#edit'+$('#formid').val()).prop('title', 'Edit Data '+$('#ms_sendermail').val() );
					
					$('#fms_purpose'+$('#formid').val()).html( $('#ms_purpose').select2('data')[0].text );
					$('#fms_sendermail'+$('#formid').val()).html($('#ms_sendermail').val());
					$('#fms_sendername'+$('#formid').val()).html( $('#ms_sendername').val() );
					$('#fms_password'+$('#formid').val()).html( $('#ms_password').val() );
					$('#fms_host'+$('#formid').val()).html($('#ms_host').val());
					$('#fms_secure'+$('#formid').val()).html($('#ms_secure').select2('data')[0].text);
					$('#fms_port'+$('#formid').val()).html($('#ms_port').val());
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});