var bar = $('.bar');
var percent = $('.percent');
var reloadpage = 0;

function openfancy(urlsrc){
	$.fancybox.open({
		href		: urlsrc,
		type		: 'iframe',
		autoCenter	: true,
		width		: '92%',
		height		: '92%',
		autoSize 	: false,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect	: 'elastic',
		closeSpeed	: 150,
		closeClick	: true
	});
}
function preparenew(){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val('new');
	$('#ong_estimasi').val('');
	$('#ong_price').val('');
	$('#ong_et_id').select2("val","");
	$('#ong_origin_los_id').select2("val","");
	$('#ong_dest_los_id').select2("val","");
}
function prepareedit(idnya){
	if ( $( '.has-error' ).hasClass( 'has-error' ) ){
		$('.has-error').removeClass('has-error');
	}
	$('#formid').val(idnya);
	$('#ong_estimasi').val($('#fong_estimasi'+idnya).html());
	$('#ong_price').val($('#fong_price'+idnya).html());
	$('#ong_et_id').select2("val",$('#fong_et_id'+idnya).html());
	$('#ong_origin_los_id').select2("val",$('#fong_origin_los_id'+idnya).html());
	$('#ong_dest_los_id').select2("val",$('#fong_dest_los_id'+idnya).html());
}
function preparedelete(idnya){
	$('#delete').parent().parent().parent().parent().addClass( "has-error" );
	$('#delid').val(idnya);
	$('#delete').prop('checked', false);
}
function preparesync(){
	$('#syncronize').parent().parent().parent().parent().addClass( "has-error" );
	$('#syncronize').prop('checked', false);
}

$(document).ready(function() {

	//Initialize Select2 Elements
    $(".select2").select2({
    	placeholder: 'Choose one!',
		allowClear: true
	});
    
	var table = $('#datalist').DataTable( {
    	'order'				: [[ 0, "asc" ]],
    	'bAutoWidth'		: false,
		'aoColumns' 		: [
			{ 'sWidth'		: '20%' },
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '25%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' },
			{ 'sWidth'		: '10%' }
		]
	});

	$('.fancybox').fancybox({
		maxWidth	: '92%',
		maxHeight	: '92%',
		width		: '92%',
		height		: '92%',
		autoSize	: false,
		autoCenter	: true,
		padding		: 0,
		openEffect	: 'elastic',
		openSpeed	: 150,
		closeEffect : 'elastic',
		closeSpeed  : 150,
		closeClick	: false,
		afterShow: function () {
        	reloadpage = 0;
        },
        afterClose: function () {
        	if (reloadpage == 1){location.reload();}
        }
	});

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						parent.window.location.href='login.html';
					}
				}else{
					if (out['Error'].hasOwnProperty('ong_et_id')){
						$('#ong_et_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ong_origin_los_id')){
						$('#ong_origin_los_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ong_dest_los_id')){
						$('#ong_dest_los_id').parent().addClass( "has-error" );
					}
					if (out['Error'].hasOwnProperty('ong_price')){
						$('#ong_price').parent().addClass( "has-error" );
					}
				}
			}else{
				if (out['Status']=='New'){
					alert("Success add new data!");
					preparenew();
					$("#windata").parent().animate({scrollTop:0}, '500', 'swing');
					reloadpage = 1;
				}else{
					alert('Update Success!');
//					$('#edit'+$('#formid').val()).prop('title','Edit Data ' + $('#ong_estimasi').val());
//					$('#delete'+$('#formid').val()).prop('title','Delete Data ' + $('#ong_estimasi').val());
					$('#fong_estimasi'+$('#formid').val()).html($('#ong_estimasi').val());
					$('#fong_price'+$('#formid').val()).html($('#ong_price').val());
					if ($('#ong_et_id').select2('val')){
						$('#fong_et_id'+$('#formid').val()).html( $('#ong_et_id').select2('val') );
						$('#fong_et_name'+$('#formid').val()).html($('#ong_et_id').select2('data')[0]);
					}else{
						$('#fong_et_id'+$('#formid').val()).html('0');
						$('#fong_et_name'+$('#formid').val()).html('');
					}
					if ($('#ong_origin_los_id').select2('val')){
						$('#fong_origin_los_id'+$('#formid').val()).html( $('#ong_origin_los_id').select2('val') );
						$('#fong_origin_los_name'+$('#formid').val()).html($('#ong_origin_los_id').select2('data')[0]);
					}else{
						$('#fong_origin_los_id'+$('#formid').val()).html('0');
						$('#fong_origin_los_name'+$('#formid').val()).html('');
					}
					if ($('#ong_dest_los_id').select2('val')){
						$('#fong_dest_los_id'+$('#formid').val()).html( $('#ong_dest_los_id').select2('val') );
						$('#fong_dest_los_name'+$('#formid').val()).html($('#ong_dest_los_id').select2('data')[0]);
					}else{
						$('#fong_dest_los_id'+$('#formid').val()).html('0');
						$('#fong_dest_los_name'+$('#formid').val()).html('');
					}
					
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form2').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			var out = JSON.parse(xhr.responseText);
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.html';
					}
				}
			}else{
				if (out['Status']=='Delete Success!'){
					alert("Delete Success!");
					$('#tr'+$('#delid').val()).remove();
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
	$('#form3').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
//			console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
//			console.log(out);
			if (out.hasOwnProperty('Error')){
				if (typeof out['Error'] == 'string'){
					alert(out['Error']);
					if (out['Error']=='Login session expired!'){
						window.location.href='login.html';
					}
				}
			}else{
				if (out['Status']=='Sync Finish!'){
					if (out.hasOwnProperty('Error1')){
						alert("Sync Finish!\n" + out['Error1']);						
					}else{
						alert("Sync Finish!");	
					}
					$.fancybox.close( true );
				}
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});