<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/ekspedisi.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasiekspedisifilter.php';

$resview = numrowdb($sqlcon,"select eks_id from ekspedisi".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select * ".
		"from ekspedisi " .
		$sinfil . " group by eks_id" . $sinlimit
	);
	while ( $getdt1 = mysqli_fetch_assoc ( $getdt ) ) {
		
		if ($getdt1['eks_logo']!='' && file_exists("../images/ekspedisi/".$getdt1['eks_logo'])){
			$image = '<img src="'.$path.'/images/ekspedisi/'.$getdt1['eks_logo'].'" style="height:24px;">' .
				'<br />' . formatBytes(filesize("../images/ekspedisi/".$getdt1['eks_logo']));
		}else{
			$image = '<img src="'.$path.'/images/default.png" style="width:24px;">';
		}
			
		$list .= "<tr id=\"tr" . $getdt1 ['eks_id'] . "\">" .
		"<td id=\"feks_code" . $getdt1 ['eks_id'] . "\">" . htmlentities($getdt1['eks_code']) . "</td>" .
		"<td id=\"feks_name" . $getdt1 ['eks_id'] . "\">" . htmlentities($getdt1['eks_name']) . "</td>" .
		"<td id=\"feks_image" . $getdt1 ['eks_id'] . "\">" . $image . "</td>" .
		"<td style=\"font-size:17px;\">" . 
			"<a href=\"#windata\" id=\"edit".$getdt1['eks_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data ".htmlentities($getdt1['eks_code'])."\" ".
			"onClick=\"prepareedit('".$getdt1['eks_id']."');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
			"<a href=\"#windelete\" id=\"delete".$getdt1['eks_id']."\" class=\"fancybox\" ".
			"title=\"Delete Data ".htmlentities($getdt1['eks_code'])."\" ".
			"onClick=\"preparedelete('".$getdt1['eks_id']."');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
		"</td>" . 
		"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"4\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}
?>