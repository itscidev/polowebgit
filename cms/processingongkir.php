<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/ongkir.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['syncid']) && $_POST['syncid']=='new' && !isset($result['Error'])){
	if (isset($_POST['syncronize']) && $_POST['syncronize']==1){
		include_once 'controller/rajaongkir.php';
		$result = roLocation($sqlcon,$confro);
		
		if (!isset($result['Error'])){
			$result['Status']='Sync Finish!';
		}
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from ongkir where ong_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['ong_estimasi'])){$ong_estimasi=trim($_POST['ong_estimasi']);}else{$ong_estimasi='';}
	if (isset($_POST['ong_price'])){$ong_price=intval($_POST['ong_price']);}else{$ong_price=0;}
	if (isset($_POST['ong_et_id'])){$ong_et_id=intval($_POST['ong_et_id']);}else{$ong_et_id=0;}
	if (isset($_POST['ong_origin_los_id'])){$ong_origin_los_id=intval($_POST['ong_origin_los_id']);}else{$ong_origin_los_id=0;}
	if (isset($_POST['ong_dest_los_id'])){$ong_dest_los_id=intval($_POST['ong_dest_los_id']);}else{$ong_dest_los_id=0;}
	
	if ($ong_et_id==0){
		$result['Error']['ong_et_id']=1;
	}
	if ($ong_price==0){
		$result['Error']['ong_price']=1;
	}
	if ($ong_origin_los_id==0){
		$result['Error']['ong_origin_los_id']=1;
	}
	if ($ong_dest_los_id==0){
		$result['Error']['ong_dest_los_id']=1;
	}
	if (!isset($result['Error'])){
		$addcek = '';
		if ($_POST['formid']!='new'){
			$addcek = ' and ong_id!='.intval($_POST['formid']);
		}
		$cekexist = mysqli_num_rows(mysqli_query($sqlcon,
			"select ong_id from ongkir ".
			"where ong_et_id=".$ong_et_id." and ong_origin_los_id=".$ong_origin_los_id." ".
				"and ong_dest_los_id=".$ong_dest_los_id.$addcek
		));
		if ($cekexist!=0){
			$result['Error']['ong_et_id']=1;
			$result['Error']['ong_origin_los_id']=1;
			$result['Error']['ong_dest_los_id']=1;
		}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into ongkir (".
		"ong_et_id,ong_origin_los_id,ong_dest_los_id,".
		"ong_estimasi,ong_price,".
		"ong_createby,ong_create,ong_updateby,ong_update".
	") values (".
		$ong_et_id.",".$ong_origin_los_id.",".$ong_dest_los_id.",".
		"'".mysqli_real_escape_string($sqlcon,$ong_estimasi)."',".$ong_price.",".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$qryupd = "update ongkir set ".
		"ong_et_id=".$ong_et_id.",".
		"ong_origin_los_id=".$ong_origin_los_id.",".
		"ong_dest_los_id=".$ong_dest_los_id.",".
		"ong_estimasi='".mysqli_real_escape_string($sqlcon,$ong_estimasi)."',".
		"ong_price=".$ong_price.",".
		"ong_updateby=".$cokidusr.",ong_update='".date("Y-m-d H:i:s")."' ".
	"where ong_id=".$_POST['formid'];
	mysqli_query( $sqlcon,$qryupd );
	$result['Status']='Update';
//	$result['QryUpd']=$qryupd;
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>