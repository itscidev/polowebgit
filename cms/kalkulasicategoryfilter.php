<?php
$filter = array();
if (isset( $_COOKIE['filter'] )) {
	setcookie( "filter", $_COOKIE['filter'], time() + 7200 );
	$filter = json_decode( $_COOKIE['filter'], true );
}

$sinfil = '';
if (isset( $filter['CA']['isianteks'] )) {
	$isianteks = $filter['CA']['isianteks'];
}else{
	$isianteks = '';
}
if ($isianteks != ''){
	$sinfil .= "cat.ca_name like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%' ".
	"or cat.ca_desc like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%'";
}

if (isset($filter['CA']['pilihanganda']) && count($filter['CA']['pilihanganda'])>0) {
	$addsql = '';
	foreach ($filter['CA']['pilihanganda'] as $cont){
		if ($addsql != ''){$addsql .= ',';}
		$addsql .= $cont;
	}
	if ($sinfil != ''){$sinfil.=' and ';}
	$sinfil .= "cat.ca_ca_id in (".$addsql.")";
	unset($addsql,$cont);
}

if ($sinfil != '') {
	$sinfil = " where " . $sinfil;
} else {
	$sinfil = '';
}

if (isset( $filter['CA']['batasanjumlah'] )) {
	$batasanjumlah = intval( $filter['CA']['batasanjumlah'] );
}else{
	$batasanjumlah = 200;
}
$sinlimit = ' limit '.$batasanjumlah;
?>