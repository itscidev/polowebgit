<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/productrelated.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from product_related where pl_id=".$_POST['delid'] );
		$resultdel = 'Delete Success!';
	}
	if ($resultdel != ''){
		$result['Status'] = $resultdel;
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['pl_pr_id'])){$pl_pr_id=intval($_POST['pl_pr_id']);}else{$pl_pr_id=0;}
	if (isset($_POST['pl_rel_pr_id']) && $_POST['formid']=='new'){
		$pl_rel_pr_id=$_POST['pl_rel_pr_id'];
	}elseif (isset($_POST['pl_rel_pr_id']) && $_POST['formid']!='new'){
		$pl_rel_pr_id=intval($_POST['pl_rel_pr_id']);
	}else{$pl_rel_pr_id=0;}
	
	if ($pl_pr_id==0){$result['Error']['pl_pr_id']=1;}
	if ($pl_rel_pr_id==0){$result['Error']['pl_rel_pr_id']=1;}
	if ($_POST['formid']!='new' && $pl_pr_id==$pl_rel_pr_id){
		$result['Error']['pl_rel_pr_id']=1;
	}
	if (!isset($result['Error']) && $_POST['formid']!='new'){
		$cekex = mysqli_num_rows(mysqli_query($sqlcon,
			"select pl_id from product_related where pl_pr_id=".$pl_pr_id." and pl_rel_pr_id=".$pl_rel_pr_id
		));
		if ($cekex > 0){
			$result['Error']['pl_rel_pr_id']=1;
		}
	}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		foreach ($pl_rel_pr_id as $relid){
			if ($pl_pr_id != $relid){
				$cekex = mysqli_num_rows(mysqli_query($sqlcon,
					"select pl_id from product_related ".
					"where pl_pr_id=".$pl_pr_id." and pl_rel_pr_id=".$relid
				));
				if ($cekex == 0){
					mysqli_query( $sqlcon,"insert into product_related (".
						"pl_pr_id,pl_rel_pr_id,".
						"pl_createby,pl_create,pl_updateby,pl_update".
					") values (".
						$pl_pr_id.",".$relid.",".
						$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
					")");
				}	
			}
		}
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update product_related set ".
			"pl_pr_id=".$pl_pr_id.",".
			"pl_rel_pr_id=".$pl_rel_pr_id.",".
			"pl_update='".date("Y-m-d H:i:s")."',".
			"pl_updateby=".$cokidusr." ".
		"where pl_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>