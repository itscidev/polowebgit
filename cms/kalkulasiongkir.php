<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/ongkir.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasiongkirfilter.php';

$resview = numrowdb($sqlcon,"select ong_id from ongkir".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$qrysel = "select ongkir.*,eks_code,et_code,".
			"origprop.lok_name origpropname,origkab.lok_name origkabname,origkec.lok_name origkecname,".
			"destprop.lok_name destpropname,destkab.lok_name destkabname,destkec.lok_name destkecname ".
		"from ongkir ".
		"left outer join ekspedisi_type on et_id=ong_et_id ".
		"left outer join ekspedisi on eks_id=et_eks_id ".
		"left outer join lokasi_step orig on orig.los_id=ong_origin_los_id ".
		"left outer join lokasi_step dest on dest.los_id=ong_dest_los_id ".
		"left outer join lokasi origprop on origprop.lok_id=orig.los_prop_lok_id ".
		"left outer join lokasi origkab on origkab.lok_id=orig.los_kab_lok_id ".
		"left outer join lokasi origkec on origkec.lok_id=orig.los_kec_lok_id ".
		"left outer join lokasi destprop on destprop.lok_id=dest.los_prop_lok_id ".
		"left outer join lokasi destkab on destkab.lok_id=dest.los_kab_lok_id ".
		"left outer join lokasi destkec on destkec.lok_id=dest.los_kec_lok_id".$sinfil.$sinlimit;
	//echo $qrysel;
	$getdt = mysqli_query( $sqlcon,$qrysel );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	$list .= "<tr id=\"tr" . $getdt1['ong_id'] . "\">" .
		"<td id=\"fong_et_name" . $getdt1 ['ong_id'] . "\">".htmlentities(
			$getdt1['eks_code'].'-'.$getdt1['et_code']
		)."</td>" .
		"<td id=\"fong_origin_los_name" . $getdt1 ['ong_id'] . "\">".htmlentities(
			$getdt1['origpropname'].'/'.$getdt1['origkabname'].'/'.$getdt1['origkecname']
		)."</td>" .
		"<td id=\"fong_dest_los_name" . $getdt1 ['ong_id'] . "\">".htmlentities(
			$getdt1['destpropname'].'/'.$getdt1['destkabname'].'/'.$getdt1['destkecname']
		)."</td>" .
		"<td id=\"fong_estimasi" . $getdt1 ['ong_id'] . "\">" . htmlentities($getdt1['ong_estimasi']) . "</td>" .
		"<td id=\"fong_price" . $getdt1 ['ong_id'] . "\">" . $getdt1['ong_price'] . "</td>" .
		"<td style=\"font-size:17px;\">" .
			"<p id=\"fong_et_id".$getdt1['ong_id']."\" class=\"hide\">".$getdt1['ong_et_id']."</p>".
			"<p id=\"fong_origin_los_id".$getdt1['ong_id']."\" class=\"hide\">".$getdt1['ong_origin_los_id']."</p>".
			"<p id=\"fong_dest_los_id".$getdt1['ong_id']."\" class=\"hide\">".$getdt1['ong_dest_los_id']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['ong_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data\" " .
			"onClick=\"prepareedit('" . $getdt1['ong_id'] . "');\"><i class=\"fa fa-edit\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['ong_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data\" " .
			"onClick=\"preparedelete('" . $getdt1['ong_id'] . "');\"><i class=\"fa fa-times-circle\"></i></a>" .
		"</td>" .
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"6\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

//list ekspedisi type
$optong_et_id = '';
$data = @mysqli_query($sqlcon,
	"select et_id,et_code,et_status,eks_code ".
	"from ekspedisi_type ".
	"left outer join ekspedisi on eks_id=et_eks_id order by eks_code,et_code"
);
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
 	if ($data2['et_status']==0){
 		$disable = ' disabled="disabled"';
 	}
	$optong_et_id .= '<option value="' . $data2['et_id'] . '"'.$disable.'>'.
		htmlentities( $data2['eks_code'].'-'.$data2['et_code'] ) . 
	'</option>';
}}

//list lokasi step
$optong_los_id = '';
$data = @mysqli_query($sqlcon,
	"select los_id,prop.lok_name propname,kab.lok_name kabname,kec.lok_name kecname ".
	"from lokasi_step ".
	"left outer join lokasi prop on prop.lok_id=los_prop_lok_id ".
	"left outer join lokasi kab on kab.lok_id=los_kab_lok_id ".
	"left outer join lokasi kec on kec.lok_id=los_kec_lok_id ".
	"order by 2,3,4"
);
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
	// 	if ($data2['ong_status']==0){
	// 		$disable = ' disabled="disabled"';
	// 	}
	$optong_los_id .= '<option value="' . $data2['los_id'] . '"'.$disable.'>'.
		htmlentities( $data2['propname'].'/'.$data2['kabname'].'/'.$data2['kecname'] ) .
	'</option>';
}}
?>