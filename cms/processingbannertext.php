<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/bannertext.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from banner_text_showon where bts_bt_id=".$_POST['delid'] );
		mysqli_query( $sqlcon,"delete from banner_text where bt_id=".$_POST['delid'] );
		$result['Status'] = 'Delete Success!';
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['bt_title'])){$bt_title=$_POST['bt_title'];}else{$bt_title='';}
	if (isset($_POST['bt_anchor'])){$bt_anchor=$_POST['bt_anchor'];}else{$bt_anchor='';}
	if (isset($_POST['bt_link'])){$bt_link=$_POST['bt_link'];}else{$bt_link='';}
	if (isset($_POST['bt_sequence'])){$bt_sequence=intval($_POST['bt_sequence']);}else{$bt_sequence=0;}
	if (isset($_POST['bt_status'])){$bt_status=intval($_POST['bt_status']);}else{$bt_status=0;}
	if (isset($_POST['bt_draft'])){$bt_draft=intval($_POST['bt_draft']);}else{$bt_draft=0;}
	if (isset($_POST['bt_from'])){
		$exp = explode('/', substr($_POST['bt_from'], 0,10));
		$bt_from=$exp[2].'-'.$exp[0].'-'.$exp[1].' '.substr($_POST['bt_from'], -8);
	}else{$bt_from='';}
	if (isset($_POST['bt_to'])){
		$exp = explode('/', substr($_POST['bt_to'], 0,10));
		$bt_to=$exp[2].'-'.$exp[0].'-'.$exp[1].' '.substr($_POST['bt_to'], -8);
	}else{$bt_to='';}
	if (isset($_POST['bts_ca_id']) && is_array($_POST['bts_ca_id'])){
		$bts_ca_id = $_POST['bts_ca_id'];
	}else{$bts_ca_id=array();}
	$result['bts_ca_id'] = '';
	if (count($bts_ca_id) > 0){foreach ($bts_ca_id as $index => $content){
		if ($content == 1){
			if ($result['bts_ca_id'] != ''){$result['bts_ca_id'] .= ',';}
			$result['bts_ca_id'] .= $index;
		}
	}}
	
	if ($bt_title==''){$result['Error']['bt_title']=1;}
	if ($bt_anchor==''){$result['Error']['bt_anchor']=1;}
	if ($bt_from==''){$result['Error']['bt_from']=1;}
	if ($bt_to==''){$result['Error']['bt_to']=1;}
	if ($bt_from!='' && $bt_to!='' && strtotime($bt_from)>strtotime($bt_to)){
		$result['Error']['bt_from']=1;
		$result['Error']['bt_to']=1;
	}
	
	$strrep = array("'",'"',' ','/','\\','#');
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into banner_text (".
			"bt_status,bt_draft,bt_title,".
			"bt_sequence,bt_anchor,".
			"bt_link,bt_from,bt_to,".
			"bt_createby,bt_create,bt_updateby,bt_update".
		") values (".
			$bt_status.",".$bt_draft.",'".mysqli_real_escape_string( $sqlcon,$bt_title )."',".
			$bt_sequence.",'".mysqli_real_escape_string( $sqlcon,$bt_anchor )."',".
			"'".mysqli_real_escape_string( $sqlcon,$bt_link )."','".$bt_from."','".$bt_to."',".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);
		if (count($bts_ca_id) > 0){
			$insertid = mysqli_insert_id( $sqlcon );
			foreach ($bts_ca_id as $index => $content){
				if ($content == 1){
					mysqli_query( $sqlcon,
					"insert into banner_text_showon (".
						"bts_bt_id,bts_ca_id,".
						"bts_createby,bts_create,bts_updateby,bts_update".
					") values (".
						$insertid.",".$index.",".
						$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
					")"
					);
				}
			}
		}
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		$getnow = mysqli_fetch_array(mysqli_query( $sqlcon,
			"select GROUP_CONCAT(bts_ca_id) btscaid from banner_text_showon ".
			"where bts_bt_id=".intval($_POST['formid'])
		));
		$nowbtscaid = array();
		if (isset($getnow['btscaid']) && $getnow['btscaid']!='' && $getnow['btscaid']!=null){
			$nowbtscaid = explode(',',$getnow['btscaid']);
			foreach ($nowbtscaid as $content){
				if (!isset($bts_ca_id[$content]) || (isset($bts_ca_id[$content]) && $bts_ca_id[$content]==0)){
					mysqli_query( $sqlcon,
						"delete from banner_text_showon ".
						"where bts_bt_id=".intval($_POST['formid'])." and bts_ca_id=".$content
					);
				}
			}
		}
		if (count($bts_ca_id) > 0){
			foreach ($bts_ca_id as $index => $content){
				if ($content==1 && !in_array($index,$nowbtscaid)){
					mysqli_query( $sqlcon,
					"insert into banner_text_showon (".
						"bts_bt_id,bts_ca_id,".
						"bts_createby,bts_create,bts_updateby,bts_update".
					") values (".
						intval($_POST['formid']).",".$index.",".
						$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
					")"
					);
				}
			}
		}
		mysqli_query( $sqlcon,
		"update banner_text set ".
			"bt_title='".mysqli_real_escape_string( $sqlcon,$bt_title )."',".
			"bt_anchor='".mysqli_real_escape_string( $sqlcon,$bt_anchor )."',".
			"bt_link='".mysqli_real_escape_string( $sqlcon,$bt_link )."',".
			"bt_from='".$bt_from."',".
			"bt_to='".$bt_to."',".
			"bt_status=".$bt_status.",".
			"bt_draft=".$bt_draft.",".
			"bt_sequence=".$bt_sequence.",".
			"bt_update='".date("Y-m-d H:i:s")."',".
			"bt_updateby=".$cokidusr." ".
		"where bt_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>