<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/pagesgeneral.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	
	if (isset($_POST['pg_title'])){$pg_title=$_POST['pg_title'];}else{$pg_title='';}
	if (isset($_POST['pg_keyword'])){$pg_keyword=$_POST['pg_keyword'];}else{$pg_keyword='';}
	if (isset($_POST['pg_desc'])){$pg_desc=$_POST['pg_desc'];}else{$pg_desc='';}
	
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into pages (".
		"pg_name,pg_title,".
		"pg_keyword,".
		"pg_desc,".
		"pg_createby,pg_create,pg_updateby,pg_update".
	") values (".
		"'general','".mysqli_real_escape_string( $sqlcon,$pg_title )."',".
		"'".mysqli_real_escape_string( $sqlcon,$pg_keyword )."',".
		"'".mysqli_real_escape_string( $sqlcon,$pg_desc )."',".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	//$result['Qry']=$strquery;
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$qryupd = "update pages set ".
	"pg_title='".mysqli_real_escape_string( $sqlcon,$pg_title )."',".
	"pg_keyword='".mysqli_real_escape_string( $sqlcon,$pg_keyword )."',".
	"pg_desc='".mysqli_real_escape_string( $sqlcon,$pg_desc )."',".
	"pg_updateby=".$cokidusr.",pg_update='".date("Y-m-d H:i:s")."' ".
	"where pg_id=".$_POST['formid'];
	mysqli_query( $sqlcon,$qryupd );
	$result['Status']='Update';
//	$result['QryUpd']=$qryupd;
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>