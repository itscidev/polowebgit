<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/color.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (!isset($_POST['delete']) || (isset($_POST['delete']) && $_POST['delete']!=1)){
		$result['Error']='Confirmation box not checked!';
	}else{
		mysqli_query( $sqlcon,"delete from color where cl_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['cl_name'])){$cl_name=$_POST['cl_name'];}else{$cl_name='';}
	if (isset($_POST['cl_code'])){$cl_code=$_POST['cl_code'];}else{$cl_code='';}
	
	if ($cl_name == ''){
		$result['Error']['cl_name'] = 1;
	}elseif ($cl_code == ''){
		$result['Error']['cl_code'] = 1;
	}else{
		$addexist = '';
		if ($_POST['formid']!='new'){$addexist = ' and cl_id!='.intval($_POST['formid']);}
		$exist = mysqli_num_rows(mysqli_query($sqlcon,
			"select cl_id from color ".
			"where cl_name='".mysqli_real_escape_string($sqlcon,$cl_name)."'".$addexist
		));
		if ($exist > 0){
			$result['Error']['cl_name'] = 1;
		}
		$exist1 = mysqli_num_rows(mysqli_query($sqlcon,
			"select cl_id from color ".
			"where cl_code='".mysqli_real_escape_string($sqlcon,$cl_code)."'".$addexist
		));
		if ($exist1 > 0){
			$result['Error']['cl_code'] = 1;
		}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into color (".
		"cl_name,".
		"cl_code,".
		"cl_createby,cl_create,cl_updateby,cl_update".
	") values (".
		"'".mysqli_real_escape_string( $sqlcon,$cl_name )."',".
		"'".mysqli_real_escape_string( $sqlcon,$cl_code )."',".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	mysqli_query( $sqlcon,
	"update color set ".
	"cl_name='".mysqli_real_escape_string( $sqlcon,$cl_name )."',".
	"cl_code='".mysqli_real_escape_string( $sqlcon,$cl_code )."',".
	"cl_updateby=".$cokidusr.",cl_update='".date("Y-m-d H:i:s")."' ".
	"where cl_id=".$_POST['formid']
	);
	$result['Status']='Update';
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>