<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/productvariety.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$buttondraft = draftStatus();

include_once 'kalkulasiproductvarietyfilter.php';

$resview = numrowdb($sqlcon,"select pv_id from product_variety".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select pv.*,pr_name,st_name,sz_name,cl_name,ci_image ".
		"from product_variety pv " .
		"left outer join product on pr_id=pv_pr_id ".
		"left outer join style on st_id=pv_st_id ".
		"left outer join size on sz_id=pv_sz_id ".
		"left outer join color_item on ci_id=pv_ci_id ".
		"left outer join color on cl_id=ci_cl_id ".
		$sinfil . " group by pv_id" . $sinlimit
	);
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
	if ($getdt1 ['pv_status']==1){
		$status = '<span class="label label-success">Active</span>';
	}else{
		$status = '<span class="label label-danger">Pasif</span>';
	}
	if ($getdt1 ['pv_draft']==1){
		$draft = '<span class="label label-warning">Draft</span>';
	}else{
		$draft = '';
	}
	if ($getdt1['ci_image']!='' && file_exists("../images/productcolor/".$getdt1['ci_image'])){
		$colorimage = $getdt1['cl_name'].'&nbsp;&nbsp;'.
			'<img src="'.$path.'/images/productcolor/'.$getdt1['ci_image'].'" class="colorimg">';
	}else{
		$colorimage = $getdt1['cl_name'].'&nbsp;&nbsp;'.
			'<img src="'.$path.'/images/default.png" class="colorimg">';
	}
	
	$list .= "<tr id=\"tr" . $getdt1 ['pv_id'] . "\">" . 
	"<td id=\"fpv_pr_name" . $getdt1 ['pv_id'] . "\">" . htmlentities($getdt1['pr_name']) . "</td>" .
	"<td id=\"fpv_st_name" . $getdt1 ['pv_id'] . "\">" . htmlentities($getdt1['st_name']) . "</td>" .
	"<td id=\"fpv_sz_name" . $getdt1 ['pv_id'] . "\">" . htmlentities($getdt1['sz_name']) . "</td>" .
	"<td id=\"fpv_ci_name" . $getdt1 ['pv_id'] . "\">" . $colorimage . "</td>" .
	"<td id=\"fpv_name" . $getdt1 ['pv_id'] . "\">" . htmlentities($getdt1['pv_name']) . "</td>" .
	"<td id=\"fpv_price" . $getdt1 ['pv_id'] . "\">" . htmlentities($getdt1['pv_price']) . "</td>" .
	"<td id=\"fpv_qty" . $getdt1 ['pv_id'] . "\">" . htmlentities($getdt1['pv_qty']) . "</td>" .
	"<td>".
		"<span id=\"fstatus" . $getdt1 ['pv_id'] . "\">" . $status . "</span>".
		"<span id=\"fdraft" . $getdt1 ['pv_id'] . "\">" . $draft . "</span>".
	"</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fpv_status".$getdt1['pv_id']."\" class=\"hide\">".$getdt1['pv_status']."</p>".
		"<p id=\"fpv_artikel".$getdt1['pv_id']."\" class=\"hide\">".htmlentities($getdt1['pv_artikel'])."</p>".
		"<p id=\"fpv_draft".$getdt1['pv_id']."\" class=\"hide\">".$getdt1['pv_draft']."</p>".
		"<p id=\"fpv_pr_id".$getdt1['pv_id']."\" class=\"hide\">".$getdt1['pv_pr_id']."</p>".
		"<p id=\"fpv_st_id".$getdt1['pv_id']."\" class=\"hide\">".$getdt1['pv_st_id']."</p>".
		"<p id=\"fpv_sz_id".$getdt1['pv_id']."\" class=\"hide\">".$getdt1['pv_sz_id']."</p>".
		"<p id=\"fpv_ci_id".$getdt1['pv_id']."\" class=\"hide\">".$getdt1['pv_ci_id']."</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['pv_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data ".htmlentities($getdt1['pv_name'])."\" ".
		"onClick=\"prepareedit('".$getdt1['pv_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['pv_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data ".htmlentities($getdt1['pv_name'])."\" ".
		"onClick=\"preparedelete('".$getdt1['pv_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"9\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

//construct options kategori
$optpv_pr_id = '';$defprprice = array();
$data = @mysqli_query($sqlcon,
	"select pr_id,pr_name,pr_price,pr_status,pr_draft from product order by pr_name asc"
);
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
	if ($data2['pr_status']==0 && $data2['pr_draft']==0){
		$disable = ' disabled="disabled"';
	}
	$optpv_pr_id .= '<option value="' . $data2['pr_id'] . '"'.$disable.'>'.
		htmlentities( $data2['pr_name'] ) .
	'</option>';
	$defprprice[( $data2['pr_id'] )] = $data2['pr_price'];
}}
$defprprice = json_encode($defprprice);
//construct options style
$optpv_st_id = '';
$data = @mysqli_query($sqlcon,"select * from style order by st_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
// 	if ($data2['pr_status']==0 && $data2['pr_draft']==0){
// 		$disable = ' disabled="disabled"';
// 	}
	$optpv_st_id .= '<option value="' . $data2['st_id'] . '"'.$disable.'>'.
		htmlentities( $data2['st_name'] ) .
	'</option>';
}}
//construct options size
$optpv_sz_id = '';
$data = @mysqli_query($sqlcon,"select * from size order by sz_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
	// 	if ($data2['pr_status']==0 && $data2['pr_draft']==0){
	// 		$disable = ' disabled="disabled"';
	// 	}
	$optpv_sz_id .= '<option value="' . $data2['sz_id'] . '"'.$disable.'>'.
		htmlentities( $data2['sz_name'] ) .
	'</option>';
}}
//list color
$optpv_ci_id = '';
$data = @mysqli_query($sqlcon,
	"select ci.ci_id,ci.ci_image,cl.cl_name ".
	"from color_item ci inner join color cl on cl_id=ci_cl_id order by cl_name asc"
);
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
	// 	if ($data2['ci_status']==0){
	// 		$disable = ' disabled="disabled"';
	// 	}
	$optpv_ci_id .= '<option value="' . $data2['ci_id'] . '" imgsrc="' . 
		$path . '/images/productcolor/' . $data2['ci_image'] . 
	'"' . $disable . '>' . htmlentities( $data2['cl_name'] ) . '</option>';
}}
?>