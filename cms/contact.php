<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo $headtitle;?>
    <!-- Favicons -->
    <?php echo $favicon;?>
	<!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons 
    <link rel="stylesheet" href="css/ionicons.min.css">
	<!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.css">
	<!-- //Select2 -->
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" type="text/css" href="css/ajaxpost.css" />
	<link rel="stylesheet" type="text/css" href="css/mycss.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .textarea{
    	width: 100%; 
    	height: 200px; 
    	font-size: 14px; 
    	line-height: 18px; 
    	border: 1px solid #dddddd; 
    	padding: 10px;
    }
    </style>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

    <?php include_once 'mainheader.php';?>
	<!-- =============================================== -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Company Contact Info
	        <small>Set Company email, address, phone, etc..</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Company Info</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
					<form name="form1" id="form1" role="form" enctype="multipart/form-data" method="post" 
					action="processingcontact.php">
						<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Complete HQ Address</label>
							<textarea name="co_address" class="textarea" placeholder="Enter some text..">
							<?php echo $co_address;?>
							</textarea>
						</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Telephone</label>
							<input name="co_telephone" type="text" class="form-control" placeholder="Enter ..." 
							value="<?php echo $co_telephone;?>"/>
						</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Fax</label>
							<input name="co_fax" type="text" class="form-control" placeholder="Enter ..." 
							value="<?php echo $co_fax;?>"/>
						</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>GPS Latitude</label>
							<input name="co_latitude" type="text" class="form-control" placeholder="Enter ..." 
							value="<?php echo $co_latitude;?>"/>
						</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>GPS Longitude</label>
							<input name="co_longitude" type="text" class="form-control" placeholder="Enter ..." 
							value="<?php echo $co_longitude;?>"/>
						</div>
						</div>
						<!-- Select2 -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Sender Location ID</label>
								<select name="co_los_id" id="co_los_id" class="form-control select2" style="width: 100%;"><?php echo $optcolosid;?><!--  
								<option value="1" selected="selected">DKI Jakarta, Jakarta Barat, Cengkareng</option>
								<option value="2">DKI Jakarta, Jakarta Barat, Grogol</option>
								<option value="3">DKI Jakarta, Jakarta Barat, Kalideres</option>
								<option value="4">DKI Jakarta, Jakarta Barat, Kebon Jeruk</option>
								<option value="5">DKI Jakarta, Jakarta Barat, Kembangan</option>
							 
								--></select>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Facebook URL</label>
							<input name="co_linkfb" type="text" class="form-control" placeholder="Enter ..." 
							value="<?php echo $co_linkfb;?>"/>
						</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Youtube URL</label>
							<input name="co_linkyoutube" type="text" class="form-control" placeholder="Enter ..." 
							value="<?php echo $co_linkyoutube;?>"/>
						</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Instagram URL</label>
							<input name="co_linkinstagram" type="text" class="form-control" placeholder="Enter ..." 
							value="<?php echo $co_linkinstagram;?>"/>
						</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
							<div class="bar"></div>
							<div class="percent">0%</div>
						</div>
						<button type="submit" class="btn btn-warning">Submit</button>
						</div>
					</form>
					</div>
				</div>
			</div>
        </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	<?php include_once 'footer.php';?>
	
	</div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
	<script src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<script src="js/ajaxpost.js"></script>
    <script src="js/contact.js"></script>

  </body>
</html>