<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/fileuploader.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

function dirToArray($dir) {
	$result = array();
	$cdir = scandir($dir);
	foreach ($cdir as $key => $value){
		if (!in_array($value,array(".",".."))){
			if (is_dir($dir . DIRECTORY_SEPARATOR . $value)){
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
			}else{
				$result[] = $value;
			}
		}
	}
	return $result;
}
function readArray($array,$list = ''){
	if ($list != ''){$list .= '/';}
	$phppath = '../generalasset/'.$list;
	if ($_SERVER['SERVER_NAME'] == 'localhost'){
		$realurl = 'http://'.$_SERVER['SERVER_NAME'].'/poloweb/generalasset/'.$list;
	}else{
		$realurl = 'http://'.$_SERVER['SERVER_NAME'].'/generalasset/'.$list;
	}
	$filetype = array('jpg','jpeg','gif','png','tiff','bmp');
	if (count($array) > 0){foreach ($array as $index => $content){
		if (is_array($content) && count($content)>0){
			readArray($content,$list.$index);
		}elseif (!is_array($content)){
			$id = md5($realurl.$index);
			$size = formatBytes(filesize($phppath.$content));
			$expname = explode('.',$content);
			if (in_array(end($expname),$filetype)){
				$itemimage = '<img src="'.$realurl.$content.'" class="itemimg">';
			}else{
				$itemimage = '';
			}
			$link = $realurl.$content;
						
			$list .= "<tr id=\"tr" . $id . "\">" . 
			"<td id=\"fname" . $id . "\">" . htmlentities($link) . "</td>" .
			"<td id=\"fsize" . $id . "\">" . $size . "</td>" .
			"<td id=\"fthumb" . $id . "\">" . $itemimage . "</td>" .
			"<td style=\"font-size:17px;\">" . 
				"<p id=\"fpath" . $id . "\" class=\"hide\">".htmlentities($phppath.$content)."</p>".
				"<a href=\"#windelete\" id=\"delete".$id."\" class=\"fancybox\" ".
				"title=\"Delete Data\" onClick=\"preparedelete('".$id."');\">".
				"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
			"</td>" . 
			"</tr>";
		}
	}}
	return $list;
}

//echo "<pre>";print_r($_SERVER);echo "</pre>";
$list = '';
$result = dirToArray('../generalasset');
//echo "<pre>";print_r($result);echo "</pre>";
$list = readArray($result);

if ($list == '') {
	$list = "<tr><td colspan=\"4\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td></tr>";
}
?>