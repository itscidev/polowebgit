<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/product.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

function katComplete($sqlcon,$id,$result){
	$getkat = mysqli_query($sqlcon,
		"select ca_id,ca_name,ca_status,ca_draft from category where ca_ca_id=" . $id
	);
	$getkat1 = 0;
	if ($getkat){$getkat1 = mysqli_num_rows($getkat);}
	if ($getkat1 > 0){while ($getkat2 = mysqli_fetch_assoc($getkat)){
		$name = $getkat2['ca_name'];
		if (isset($result[$id])){
			$name = $result[$id][0] . " - " . $name;
		}
		$result[( $getkat2['ca_id'] )] = array($name,$getkat2['ca_status'],$getkat2['ca_draft']);
		
		//cek child
		$getchl = mysqli_query($sqlcon,
			"select ca_id from category where ca_ca_id=" . $getkat2['ca_id']
		);
		$getchl1 = 0;
		if ($getchl){$getchl1 = mysqli_num_rows($getchl);}
		if ($getchl1 > 0){
			$result = katComplete($sqlcon,$getkat2['ca_id'],$result);
		}
	}}
	return $result;
}
$mapkat = katComplete($sqlcon,0,array());

$buttondraft = draftStatus();

include_once 'kalkulasiproductfilter.php';

$resview = numrowdb($sqlcon,"select pr_id from product".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select * ".
		"from product " .
		$sinfil . " group by pr_id" . $sinlimit
	);
	while ( $getdt1 = mysqli_fetch_assoc ( $getdt ) ) {
		if ($getdt1 ['pr_status']==1){
			$status = '<span class="label label-success">Active</span>';
		}else{
			$status = '<span class="label label-danger">Pasif</span>';
		}
		if ($getdt1 ['pr_draft']==1){
			$draft = '<span class="label label-warning">Draft</span>';
		}else{
			$draft = '';
		}
		$getimg = mysqli_fetch_assoc(mysqli_query($sqlcon,
			"select pi_image from product_image ".
			"where pi_id=".$getdt1['pr_main_pi_id']." and pi_status=1 ".
			"limit 1"
		));
	 	if (isset($getimg['pi_image']) && $getimg['pi_image']!='' && 
	 	file_exists("../images/productitem/".$getimg['pi_image'])){
	 		$image = '<img src="'.$path.'/images/productitem/'.$getimg['pi_image'].'" style="height:50px;">' . 
	 			'<br />' . formatBytes(filesize("../images/productitem/".$getimg['pi_image']));
	 	}else{
			$image = '<img src="'.$path.'/images/default.png" style="width:50px;">';
	 	}
		
		if ($getdt1['pr_ca_id']===0 || $getdt1['pr_ca_id']===null){
			$kategory = 'not set';
		}elseif (isset($mapkat[( $getdt1['pr_ca_id'] )])){
			$kategory = $mapkat[( $getdt1['pr_ca_id'] )][0];
		}else{
			$kategory = 'not found';
		}
			
		$list .= "<tr id=\"tr" . $getdt1 ['pr_id'] . "\">" . 
		"<td id=\"fpr_ca_name" . $getdt1 ['pr_id'] . "\">" . htmlentities($kategory) . "</td>" .
		"<td id=\"fpr_name" . $getdt1 ['pr_id'] . "\">" . htmlentities($getdt1['pr_name']) . "</td>" .
		"<td id=\"fpr_price" . $getdt1 ['pr_id'] . "\">" . htmlentities($getdt1['pr_price']) . "</td>" .
		"<td>".
			"<span id=\"fstatus" . $getdt1 ['pr_id'] . "\">" . $status . "</span>".
			"<span id=\"fdraft" . $getdt1 ['pr_id'] . "\">" . $draft . "</span>".
		"</td>" .
		"<td id=\"fpr_image" . $getdt1 ['pr_id'] . "\">" . $image . "</td>" .
		"<td style=\"font-size:17px;\">" . 
			"<p id=\"fpr_status".$getdt1['pr_id']."\" class=\"hide\">".$getdt1['pr_status']."</p>".
			"<p id=\"fpr_weight".$getdt1['pr_id']."\" class=\"hide\">".$getdt1['pr_weight']."</p>".
			"<p id=\"fpr_draft".$getdt1['pr_id']."\" class=\"hide\">".$getdt1['pr_draft']."</p>".
			"<p id=\"fpr_ca_id".$getdt1['pr_id']."\" class=\"hide\">".$getdt1['pr_ca_id']."</p>".
			"<p id=\"fpr_onsale".$getdt1['pr_id']."\" class=\"hide\">".$getdt1['pr_onsale']."</p>".
			"<p id=\"fpr_care".$getdt1['pr_id']."\" class=\"hide\">".
				htmlentities($getdt1['pr_care']).
			"</p>".
			"<p id=\"fpr_info".$getdt1['pr_id']."\" class=\"hide\">".
				htmlentities($getdt1['pr_info']).
			"</p>".
			"<p id=\"fpr_desc".$getdt1['pr_id']."\" class=\"hide\">".
				htmlentities($getdt1['pr_desc']).
			"</p>".
			"<p id=\"fpr_tagline".$getdt1['pr_id']."\" class=\"hide\">".htmlentities($getdt1['pr_tagline'])."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['pr_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data ".htmlentities($getdt1['pr_name'])."\" ".
			"onClick=\"prepareedit('".$getdt1['pr_id']."');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
			"<a href=\"#windelete\" id=\"delete".$getdt1['pr_id']."\" class=\"fancybox\" ".
			"title=\"Delete Data ".htmlentities($getdt1['pr_name'])."\" ".
			"onClick=\"preparedelete('".$getdt1['pr_id']."');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
		"</td>" . 
		"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"6\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td></tr>";
}

//construct options kategori
$optpr_ca_id = '';
foreach ($mapkat as $idkat => $content){
	$disabled = '';
	if ($content[1]==0 && $content[2]==0){
		$disabled = ' disabled="disabled"';
	}
	$optpr_ca_id .= '<option value="' . $idkat . '"'.$disabled.'>' . $content[0] . '</option>';
}
?>