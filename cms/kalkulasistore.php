<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/store.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasistorefilter.php';

$resview = numrowdb($sqlcon,"select st_id from store".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select * ".
		"from store " .
		$sinfil . $sinlimit
	);
	while ( $getdt1 = mysqli_fetch_assoc ( $getdt ) ) {
		if ($getdt1 ['st_status']==1){
			$status = '<span class="label label-success">Active</span>';
		}else{
			$status = '<span class="label label-danger">Pasif</span>';
		}
			
		$list .= "<tr id=\"tr" . $getdt1 ['st_id'] . "\">" . 
		"<td id=\"fst_code" . $getdt1 ['st_id'] . "\">" . htmlentities($getdt1['st_code']) . "</td>" .
		"<td id=\"fst_name" . $getdt1 ['st_id'] . "\">" . htmlentities($getdt1['st_name']) . "</td>" .
		"<td id=\"fst_dispname" . $getdt1 ['st_id'] . "\">" . htmlentities($getdt1['st_dispname']) . "</td>" .
		"<td id=\"fst_zip" . $getdt1 ['st_id'] . "\">" . htmlentities($getdt1['st_zip']) . "</td>" .
		"<td id=\"fst_lat" . $getdt1 ['st_id'] . "\">" . htmlentities($getdt1['st_lat']) . "</td>" .
		"<td id=\"fst_long" . $getdt1 ['st_id'] . "\">" . htmlentities($getdt1['st_long']) . "</td>" .
		"<td>".
			"<span id=\"fstatus" . $getdt1 ['st_id'] . "\">" . $status . "</span>".
		"</td>" .
		"<td style=\"font-size:17px;\">" . 
			"<p id=\"fst_status".$getdt1['st_id']."\" class=\"hide\">".$getdt1['st_status']."</p>".
			"<p id=\"fst_address".$getdt1['st_id']."\" class=\"hide\">".
				htmlentities($getdt1['st_address']).
			"</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['st_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data ".htmlentities($getdt1['st_code'])."\" ".
			"onClick=\"prepareedit('".$getdt1['st_id']."');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
			"<a href=\"#windelete\" id=\"delete".$getdt1['st_id']."\" class=\"fancybox\" ".
			"title=\"Delete Data ".htmlentities($getdt1['st_code'])."\" ".
			"onClick=\"preparedelete('".$getdt1['st_id']."');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
		"</td>" . 
		"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"8\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

$tokensec = md5('/StoresCMS.Php:'.date("d"));
?>