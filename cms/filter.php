<?php include_once 'kalkulasi.php';
/*	US	: Master User
	AC	: Access
	GR	: Group
	CA	: Category
	PO	: Pages Overwrite title,keyword & description
	BNS	: Banner Top Slider
	BNQ	: Banner Quick Menu
	BVP	: Banner Video Promo
	BNE	: Banner Seasons
	BNT	: Banner Text
	SZ	: Master Size
	CL	: Master Color
	CI	: Master Color Item
	PR	: Product Item
	PV	: Product Variety
	PI	: Product Image
	PL	: Product Related
	PS	: Product Store Located
	ST	: Master Store
	EX	: Ekspedisi
	ET	: Ekspedisi Type
	LO	: Lokasi
	LS	: Lokasi Step
	ON	: Ongkir
	PRO : Pre-order
	PRC : Pre-order confirmation
	MM 	: Member
	MOR : Member Order	
	PT 	: Payment Type
*/?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Filter Page</title>
 	<!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
	<!-- daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    <link rel="stylesheet" type="text/css" href="css/ajaxpost.css" />
</head>
<body>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="box box-info" style="min-height: 400px;">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $titlepage;?></h3>
			</div><!-- /.box-header -->
<?php 
if ($titlepage == 'Un-Authorized Request!'){?>
			<div class="box-body">
				<div class="form-group">
					No Action Needed!
				</div>
			</div><?php
}else{?>
			<!-- form start -->
			<form name="form1" id="form1" role="form" action="processingfilter.php" method="post" 
			enctype="multipart/form-data">
			<div class="box-body"><?php
	$arrallow = array(
		'US','AC','GR','CA','PO','BNS','BNQ','BVP','BNE','BNT','SZ','CL','CI','PR','PV','ST','PL','PS','EX',
		'ET','LO','LS','ON','PRO','PRC','MM','MOR','PT'
	);
	if (in_array($_GET['page'],$arrallow)){?>
					<div class="form-group">
						<label><?php echo $isiantekslabel;?></label>
						<input name="isianteks" id="isianteks" class="form-control" 
						placeholder="Enter..." type="text" value="<?php echo $isianteks;?>">
					</div><?php 
	}
	if ($_GET['page']=='PO' || $_GET['page']=='ON'){?>
					<div class="form-group">
						<label><?php echo $isianteks1label;?></label>
						<input name="isianteks1" id="isianteks1" class="form-control" 
						placeholder="Enter..." type="text" value="<?php echo $isianteks1;?>">
					</div><?php 
	}
	if ($_GET['page']==''){?>
					<!-- Date range -->
					<div class="form-group">
						<label><?php echo $rentangtanggallabel;?></label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input name="rentangtanggal" id="rentangtanggal" type="text" 
							value="<?php echo $rentangtanggal;?>" class="form-control pull-left">
						</div><!-- /.input group -->
					</div><!-- /.form group -->
					<!-- Select2 -->
					<div class="form-group">
						<label><?php echo $pilihanumumlabel;?></label>
						<select name="pilihanumum" id="pilihanumum" class="form-control select2" 
						style="width: 100%;">
							<?php echo $pilihanumumopt;?>
						</select>
					</div><!-- /.form-group -->
					<!-- Select2 -->
					<div class="form-group">
						<label><?php echo $pilihanumum1label;?></label>
						<select name="pilihanumum1" id="pilihanumum1" class="form-control select2" 
						style="width: 100%;">
							<?php echo $pilihanumum1opt;?>
						</select>
					</div><!-- /.form-group --><?php 
	}
	if ($_GET['page']=='US' || $_GET['page']=='CA'){?>
					<!-- Select2 -->
					<div class="form-group">
						<label><?php echo $pilihangandalabel;?></label>
						<select name="pilihanganda[]" id="pilihanganda" class="form-control select2"
						multiple="multiple" data-placeholder="Select one or more..." style="width: 100%;">
							<?php echo $pilihangandaopt;?>
						</select>
					</div><!-- /.form-group --><?php 
	}
	if ($_GET['page']==''){?>
					<!-- Select2 -->
					<div class="form-group">
						<label><?php echo $pilihanganda1label;?></label>
						<select name="pilihanganda1[]" id="pilihanganda1" class="form-control select2"
						multiple="multiple" data-placeholder="Select type..." style="width: 100%;">
							<?php echo $pilihanganda1opt;?>
						</select>
					</div><!-- /.form-group -->
					<!-- Value range -->
					<div class="form-group">
						<label><?php echo $rentanglabel;?></label>
						<div>
						<div class="col-md-3">
							<select name="pilihankriteria" id="pilihankriteria" class="form-control select2">
							<?php echo $pilihankriteriaopt;?>
							</select>
						</div>
						<div class="col-md-3">
							<select name="pilihantanda" id="pilihantanda" class="form-control select2">
								<?php echo $pilihantandaopt;?>
							</select>
						</div>
						<div class="col-md-3">
							<input name="jumlah1" id="jumlah1" value="<?php echo $jumlah1;?>" 
							placeholder="Enter number..." class="form-control" type="text">
						</div>
						<div class="col-md-3">
							<input name="jumlah2" id="jumlah2" value="<?php echo $jumlah2;?>" 
							placeholder="Enter number..." class="form-control" type="text" 
							style="visibility: <?php echo $visibilityjumlah2;?>">
						</div>
						</div>
					</div>
					<!-- /.form group --><?php
	} 
	if ($_GET['page']!=''){?>
					<div class="form-group">
						<label>Limit Data</label>
						<input name="batasanjumlah" id="batasanjumlah" class="form-control" 
						placeholder="Enter..." type="text" value="<?php echo $batasanjumlah;?>">
					</div><?php
	}?>
			</div><!-- /.box-body -->

			<div class="box-footer" style="float:left;">
				<div class="progress">
					<div class="bar"></div>
					<div class="percent">0%</div>
				</div>
				<input name="asal" type="hidden" value="<?php echo $asal;?>" >
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			</form><?php 
}?>
		</div><!-- /.box -->
	</div>
</div>
<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/select2.full.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="js/ajaxpost.js"></script>
<script type="text/javascript">
var bar = $('.bar');
var percent = $('.percent');
$(function () {
	//Initialize Select2 Elements
    $(".select2").select2();
    
	//Date range picker
	$('#rentangtanggal').daterangepicker();

	//pilihantanda visibility
	if ($('#pilihantanda').length){
		$('#pilihantanda').select2().on("change", function(e) {
			if ($('#pilihantanda').select2('val')=='><' || $('#pilihantanda').select2('val')=='>=<'){
				$('#jumlah2').css('visibility','visible');
			}else{
				$('#jumlah2').css('visibility','hidden');
			}
		});
	}

	$('#form1').ajaxForm({
	    beforeSend: function() {
	        var percentVal = '0%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			$('.progress').css('display','block');
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
			//console.log(percentVal, position, total);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
		complete: function(xhr) {
			$('.progress').css('display','none');
			//console.log(xhr.responseText);
			var out = JSON.parse(xhr.responseText);
			if (out.hasOwnProperty('Error')){
				if (out['Error'].hasOwnProperty('batasanjumlah')){
					$('#batasanjumlah').parent().addClass( "has-error" );
				}if (out['Error'].hasOwnProperty('rentangtanggal')){
					$('#rentangtanggal').parent().addClass( "has-error" );
				}if (out['Error'].hasOwnProperty('jumlah1')){
					$('#jumlah1').parent().addClass( "has-error" );
				}if (out['Error'].hasOwnProperty('jumlah2')){
					$('#jumlah2').parent().addClass( "has-error" );
				}
			}else{
				parent.location.reload(); 
			}
//			console.log(out);
//			status.html(xhr.responseText);
		}
	});
});
</script>
</body>
</html>