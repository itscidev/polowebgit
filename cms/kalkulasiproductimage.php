<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/productimage.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$buttondraft = draftStatus();

include_once 'kalkulasiproductimagefilter.php';

$urltarget = "getajaxsku.html";

$resview = numrowdb($sqlcon,"select pi_id from product_image".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$strsql = "select pi.*,pr_main_pi_id,pr_name,st_name,cl_name,ci_image ".
		"from product_image pi " .
		"left outer join product on pr_id=pi_pr_id ".
		"left outer join style on st_id=pi_st_id ".
		"left outer join color_item on ci_id=pi_ci_id ".
		"left outer join color on cl_id=ci_cl_id ".
		$sinfil . " group by pi_id" . $sinlimit;
// 	echo $strsql;
	$getdt = mysqli_query( $sqlcon,$strsql );
	while ( $getdt1 = mysqli_fetch_assoc ( $getdt ) ) {
	if ($getdt1 ['pi_status']==1){
		$status = '<span class="label label-success">Active</span>';
	}else{
		$status = '<span class="label label-danger">Pasif</span>';
	}
	if ($getdt1['pi_id']==$getdt1['pr_main_pi_id']){
		$setMain = '<span class="label label-info setMain'.$getdt1['pi_pr_id'].'" '.
			'onclick="setMain('.$getdt1['pi_pr_id'].','.$getdt1['pi_id'].');">Main</span>';
	}else{
		$setMain = '<span class="label label-info setMain'.$getdt1['pi_pr_id'].'" '.
			'onclick="setMain('.$getdt1['pi_pr_id'].','.$getdt1['pi_id'].');">Set</span>';
	}
	if ($getdt1 ['pi_draft']==1){
		$draft = '<span class="label label-warning">Draft</span>';
	}else{
		$draft = '';
	}
	if ($getdt1['ci_image']!='' && file_exists("../images/productcolor/".$getdt1['ci_image'])){
		$colorimage = $getdt1['cl_name'].'&nbsp;&nbsp;'.
			'<img src="'.$path.'/images/productcolor/'.$getdt1['ci_image'].'" class="colorimg">';
	}else{
		$colorimage = $getdt1['cl_name'].'&nbsp;&nbsp;'.
			'<img src="'.$path.'/images/default.png" class="colorimg">';
	}
	if ($getdt1['pi_image']!='' && file_exists("../images/productitem/".$getdt1['pi_image'])){
		$itemimage = '<img src="'.$path.'/images/productitem/'.$getdt1['pi_image'].'" class="itemimg">' .
			'<br />' . formatBytes(filesize("../images/productitem/".$getdt1['pi_image']));
	}else{
		$itemimage = '<img src="'.$path.'/images/default.png" class="itemimg">';
	}
	
	$list .= "<tr id=\"tr" . $getdt1 ['pi_id'] . "\">" . 
	"<td id=\"fpi_pr_name" . $getdt1 ['pi_id'] . "\">" . htmlentities($getdt1['pr_name']) . "</td>" .
	"<td id=\"fpi_st_name" . $getdt1 ['pi_id'] . "\">" . htmlentities($getdt1['st_name']) . "</td>" .
	"<td id=\"fpi_ci_name" . $getdt1 ['pi_id'] . "\">" . $colorimage . "</td>" .
	"<td id=\"fpi_sequence" . $getdt1 ['pi_id'] . "\">" . htmlentities($getdt1['pi_sequence']) . "</td>" .
	"<td id=\"fpi_image" . $getdt1 ['pi_id'] . "\">" . $itemimage . "</td>" .
	"<td>".
		"<span class=\"pointer\">" . $setMain . "</span>".
		"<span id=\"fstatus" . $getdt1 ['pi_id'] . "\">" . $status . "</span>".
		"<span id=\"fdraft" . $getdt1 ['pi_id'] . "\">" . $draft . "</span>".
	"</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fpi_status".$getdt1['pi_id']."\" class=\"hide\">".$getdt1['pi_status']."</p>".
		"<p id=\"fpi_draft".$getdt1['pi_id']."\" class=\"hide\">".$getdt1['pi_draft']."</p>".
		"<p id=\"fpi_sku_id".$getdt1['pi_id']."\" class=\"hide\">".
			$getdt1['pi_pr_id'].":".$getdt1['pi_st_id'].":".$getdt1['pi_ci_id'].
		"</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['pi_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data\" onClick=\"prepareedit('".$getdt1['pi_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['pi_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data\" onClick=\"preparedelete('".$getdt1['pi_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"7\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"</tr>";
}

$optpi_sku_id = '';
?>