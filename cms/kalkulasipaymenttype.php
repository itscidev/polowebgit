<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/paymenttype.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}
$arrpt = array("Pasif","Sandbox","Production");

include_once 'kalkulasipaymenttypefilter.php';

$resview = numrowdb($sqlcon,"select pt_id from paymenttype".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,
		"select * ".
		"from paymenttype " .
		$sinfil . " group by pt_id" . $sinlimit
	);
	while ( $getdt1 = mysqli_fetch_assoc ( $getdt ) ) {
		
		if ($getdt1['pt_logo']!='' && file_exists("../images/paymentlogo/".$getdt1['pt_logo'])){
			$image = '<img src="'.$path.'/images/paymentlogo/'.$getdt1['pt_logo'].'" style="height:24px;">' .
				'<br />' . formatBytes(filesize("../images/paymentlogo/".$getdt1['pt_logo']));
		}else{
			$image = '<img src="'.$path.'/images/default.png" style="width:24px;">';
		}
			
		$list .= "<tr id=\"tr" . $getdt1 ['pt_id'] . "\">" .
		"<td id=\"fpt_vendor" . $getdt1 ['pt_id'] . "\">" . htmlentities($getdt1['pt_vendor']) . "</td>" .
		"<td id=\"fpt_name" . $getdt1 ['pt_id'] . "\">" . htmlentities($getdt1['pt_name']) . "</td>" .
		"<td id=\"fpt_image" . $getdt1 ['pt_id'] . "\">" . $image . "</td>" .
		"<td id=\"fpt_status_name" . $getdt1 ['pt_id'] . "\">".$arrpt[( $getdt1['pt_status'] )]."</td>" .
		"<td style=\"font-size:17px;\">" . 
			"<p id=\"fpt_status".$getdt1['pt_id']."\" class=\"hide\">".$getdt1['pt_status']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['pt_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data ".htmlentities($getdt1['pt_name'])."\" ".
			"onClick=\"prepareedit('".$getdt1['pt_id']."');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
			"<a href=\"#windelete\" id=\"delete".$getdt1['pt_id']."\" class=\"fancybox\" ".
			"title=\"Delete Data ".htmlentities($getdt1['pt_name'])."\" ".
			"onClick=\"preparedelete('".$getdt1['pt_id']."');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
		"</td>" . 
		"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"5\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

$optptstatus = '';
foreach ($arrpt as $key => $value) {
	$selected = '';
	if ($key == 0){$selected = ' selected="selected"';}
	$optptstatus .= '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
}
?>