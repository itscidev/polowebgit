<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/categorytree.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}
function getTrees($sqlcon,$resfunc = '',$tingkat = 0,$parent = 0){
	
	$decresfunc = json_decode($resfunc,true);
	$restree = $decresfunc[0];
	$idselect = $decresfunc[1];
	
	$getcat = @mysqli_query( $sqlcon,
		"select * from category where ca_ca_id=".$parent." order by ca_sequence,ca_name" 
	);
	$getcat1 = 0;
	if ($getcat){$getcat1 = mysqli_num_rows($getcat);}
	if ($getcat1 > 0){while ($getcat2 = mysqli_fetch_array($getcat)){
		if ($idselect != ''){$idselect .= ',';}
		$idselect .= $getcat2['ca_id'];
		
		$bgstatus = '';
		if ($getcat2['ca_status'] != 1){
			$bgstatus = ' style="background-color: #FF0000;"';
		}
		$paddingleft = ' style="padding-left:'.($tingkat*15+15).'px;"'; 
		$restree .= '<div class="row"'.$bgstatus.'>'.
		'<div class="col-md-6 col-sm-6 col-xs-6"'.$paddingleft.'>'.$getcat2['ca_name'].'</div>'.
		'<div class="col-md-6 col-sm-6 col-xs-6">'.$getcat2['ca_desc'].'</div>'.
		'</div>';
		
		$cekchild = mysqli_num_rows(mysqli_query( $sqlcon,
			"select ca_id from category where ca_ca_id=".$getcat2['ca_id'] 
		));
		if ($cekchild > 0){
			$encresfunc = json_encode(array($restree,$idselect));
			//$restree .= getTrees($sqlcon,$encresfunc,$tingkat+1,$getcat2['ca_id']);
			$decresfunc = json_decode(getTrees($sqlcon,$encresfunc,$tingkat+1,$getcat2['ca_id']),true);
			$restree = $decresfunc[0];
			$idselect = $decresfunc[1];
		}
	}}
	return json_encode(array($restree,$idselect));
}
$trees = '';

$resfunc = json_encode(array('',''));
$decresfunc = json_decode(getTrees($sqlcon,$resfunc),true);
$trees = $decresfunc[0];
$idselect = $decresfunc[1];
 
if ($trees == ''){
	$trees = '<h3>No Linked Trees Available!</h3>';
}else{
	$trees = '<div class="row">'.
		'<div class="col-md-12 col-sm-12 col-xs-12" align="center"><h3>Linked Trees</h3></div>'.
		'</div><div class="row">'.
		'<div class="col-md-6 col-sm-6 col-xs-6"><b>Name</b></div>'.
		'<div class="col-md-6 col-sm-6 col-xs-6"><b>Description</b></div>'.
		'</div>'.$trees;
}

//cek unlinked trees
$qryunlinked = '';
if ($idselect!='' && $idselect!=null && $idselect!='null'){
	$qryunlinked = ' where ca_id not in ('.$idselect.')';
}
$unlink = @mysqli_query($sqlcon,"select * from category".$qryunlinked);
$unlink1 = 0;
if ($unlink){$unlink1 = mysqli_num_rows($unlink);}
if ($unlink1 > 0){
	$trees .= '<div class="row">'.
		'<div class="col-md-12 col-sm-12 col-xs-12" align="center"><h3>Un-Linked Trees</h3></div>'.
		'</div><div class="row">'.
		'<div class="col-md-6 col-sm-6 col-xs-6"><b>Name</b></div>'.
		'<div class="col-md-6 col-sm-6 col-xs-6"><b>Description</b></div>'.
		'</div>';
	while ($unlink2 = mysqli_fetch_array($unlink)){
		$bgstatus = '';
		if ($unlink2['ca_status'] != 1){
			$bgstatus = ' style="background-color: #FF0000;"';
		}
		$trees .= '<div class="row">'.
			'<div class="col-md-6 col-sm-6 col-xs-6">'.$unlink2['ca_name'].'</div>'.
			'<div class="col-md-6 col-sm-6 col-xs-6">'.$unlink2['ca_desc'].'</div>'.
			'</div>';
	}
}else{
	$trees .= '<h3>No Un-Linked Trees Available!</h3>';
}
?>