<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/access.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasiaccessfilter.php';

$resview = numrowdb($sqlcon,"select ac_id from access".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$getdt = mysqli_query( $sqlcon,"select * from access".$sinfil.$sinlimit );	
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	$list .= "<tr id=\"tr" . $getdt1['ac_id'] . "\">" .
		"<td id=\"fac_name" . $getdt1['ac_id'] . "\" align=\"left\">" . htmlentities($getdt1['ac_name']) . "</td>" .
		"<td id=\"fac_desc" . $getdt1['ac_id'] . "\" align=\"left\">" . htmlentities($getdt1['ac_desc']) . "</td>" .
		"<td style=\"font-size:17px;\">" .
			"<a href=\"#windata\" id=\"edit".$getdt1['ac_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data " . htmlentities($getdt1['ac_name']) . "\" " .
			"onClick=\"prepareedit('" . $getdt1['ac_id'] . "');\">".
			"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['ac_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data " . htmlentities($getdt1['ac_name']) . "\" " .
			"onClick=\"preparedelete('" . $getdt1['ac_id'] . "');\">".
			"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>" .
		"</td>" .
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"3\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}
?>