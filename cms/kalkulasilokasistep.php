<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/lokasistep.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

include_once 'kalkulasilokasistepfilter.php';

$resview = numrowdb($sqlcon,"select los_id from lokasi_step".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$qrysel = "select lokasi_step.*,prop.lok_name propname,kab.lok_name kabname,kec.lok_name kecname ".
		"from lokasi_step ".
		"left outer join lokasi prop on prop.lok_id=los_prop_lok_id ".
		"left outer join lokasi kab on kab.lok_id=los_kab_lok_id ".
		"left outer join lokasi kec on kec.lok_id=los_kec_lok_id".$sinfil.$sinlimit;
	//echo $qrysel;
	$getdt = mysqli_query( $sqlcon,$qrysel );
	while ( $getdt1 = mysqli_fetch_array( $getdt ) ) {
	$list .= "<tr id=\"tr" . $getdt1['los_id'] . "\">" .
		"<td id=\"flos_prop_lok_name" . $getdt1 ['los_id'] . "\">".htmlentities($getdt1['propname']) ."</td>" .
		"<td id=\"flos_kab_lok_name" . $getdt1 ['los_id'] . "\">".htmlentities($getdt1['kabname']) ."</td>" .
		"<td id=\"flos_kec_lok_name" . $getdt1 ['los_id'] . "\">".htmlentities($getdt1['kecname']) ."</td>" .
		"<td id=\"flos_zip" . $getdt1 ['los_id'] . "\">" . htmlentities($getdt1['los_zip']) . "</td>" .
		"<td style=\"font-size:17px;\">" .
			"<p id=\"flos_prop_lok_id".$getdt1['los_id']."\" class=\"hide\">".$getdt1['los_prop_lok_id']."</p>".
			"<p id=\"flos_kab_lok_id".$getdt1['los_id']."\" class=\"hide\">".$getdt1['los_kab_lok_id']."</p>".
			"<p id=\"flos_kec_lok_id".$getdt1['los_id']."\" class=\"hide\">".$getdt1['los_kec_lok_id']."</p>".
			"<a href=\"#windata\" id=\"edit".$getdt1['los_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data\" " .
			"onClick=\"prepareedit('" . $getdt1['los_id'] . "');\"><i class=\"fa fa-edit\"></i></a>" .
			"<a href=\"#windelete\" id=\"delete" . $getdt1['los_id'] . "\" class=\"fancybox\" " .
			"title=\"Delete Data\" " .
			"onClick=\"preparedelete('" . $getdt1['los_id'] . "');\"><i class=\"fa fa-times-circle\"></i></a>" .
		"</td>" .
	"</tr>";
}}
if ($list == '') {
	$list = "<tr><td colspan=\"5\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

//list lokasi
$optlos_lok_id = '';
$data = @mysqli_query($sqlcon,"select * from lokasi order by lok_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
// 	if ($data2['los_status']==0){
// 		$disable = ' disabled="disabled"';
// 	}
	$optlos_lok_id .= '<option value="' . $data2['lok_id'] . '"'.$disable.'>'.
		htmlentities( $data2['lok_name'] ) . 
	'</option>';
}}
?>