<?php
$filter = array();
if (isset( $_COOKIE['filter'] )) {
	setcookie( "filter", $_COOKIE['filter'], time() + 7200 );
	$filter = json_decode( $_COOKIE['filter'], true );
}

$sinfil = '';
if (isset( $filter['US']['isianteks'] )) {
	$isianteks = $filter['US']['isianteks'];
}else{
	$isianteks = '';
}
if ($isianteks != ''){
	$sinfil .= "usr_name like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%'";
}

if (isset($filter['US']['pilihanganda']) && count($filter['US']['pilihanganda'])>0) {
	$addsql = '';
	foreach ($filter['US']['pilihanganda'] as $cont){
		if ($addsql != ''){$addsql .= ',';}
		$addsql .= $cont;
	}
	if ($sinfil != ''){$sinfil.=' and ';}
	$sinfil .= "usr_gr_id in (".$addsql.")";
	unset($addsql,$cont);
}

if ($sinfil != '') {
	$sinfil = " where " . $sinfil;
} else {
	$sinfil = '';
}

if (isset( $filter['US']['batasanjumlah'] )) {
	$batasanjumlah = intval( $filter['US']['batasanjumlah'] );
}else{
	$batasanjumlah = 200;
}
$sinlimit = ' limit '.$batasanjumlah;
?>