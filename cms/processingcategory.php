<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/category.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select ca_image from category where ca_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['ca_image']!='' && file_exists("../images/category/".$cekimg2['ca_image'])){
				unlink("../images/category/".$cekimg2['ca_image']);
			}
			mysqli_query( $sqlcon,"delete from category where ca_id=".$_POST['delid'] );
			$result['Status']='Delete Success!';		
		}elseif (isset($_POST['deleteimage']) && $_POST['deleteimage']==1){
			if ($cekimg2['ca_image']!='' && file_exists("../images/category/".$cekimg2['ca_image'])){
				unlink("../images/category/".$cekimg2['ca_image']);
			}
			mysqli_query( $sqlcon,"update category set ca_image='' where ca_id=".$_POST['delid'] );
			$result['ca_image']='<img src="../images/default.png" style="height:50px;">';
			$result['Status']='Delete Image Success!';
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}else{
		$result['Error']='Data already deleted!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['ca_name'])){
		$arrstrrep = array('/');
		$ca_name = str_replace($arrstrrep,'',$_POST['ca_name']);
	}else{$ca_name='';}
	if (isset($_POST['ca_desc'])){$ca_desc=$_POST['ca_desc'];}else{$ca_desc='';}
	if (isset($_POST['ca_sequence'])){$ca_sequence=intval($_POST['ca_sequence']);}else{$ca_sequence=0;}
	if (isset($_POST['ca_status'])){$ca_status=intval($_POST['ca_status']);}else{$ca_status=0;}
	if (isset($_POST['ca_ca_id'])){$ca_ca_id=intval($_POST['ca_ca_id']);}else{$ca_ca_id=0;}
	
	if ($ca_name == ''){$result['Error']['ca_name'] = 1;}
	if (isset($_FILES['ca_image']['name']) && !(
		strtolower($_FILES['ca_image']['type'])=='image/jpeg' || strtolower($_FILES['ca_image']['type'])=='image/png'
	)){
		$result['Error']['ca_image']=1;
	}
	if ($ca_ca_id==$_POST['formid'] && $_POST['formid']!='new' && $_POST['formid']>0){
		$result['Error']['ca_ca_id']=1;
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into category (".
		"ca_ca_id,ca_name,".
		"ca_desc,".
		"ca_status,ca_image,ca_sequence,".
		"ca_createby,ca_create,ca_updateby,ca_update".
	") values (".
		$ca_ca_id.",'".mysqli_real_escape_string( $sqlcon,$ca_name )."',".
		"'".mysqli_real_escape_string( $sqlcon,$ca_desc )."',".
		$ca_status.",'',".$ca_sequence.",".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	if (isset($_FILES['ca_image']['name']) && $_FILES['ca_image']['name']!=''){
		$insertid = mysqli_insert_id( $sqlcon );
		$strrep = array("'",'"',' ','/','\\');
		$ca_image = str_replace($strrep,'', $insertid.'-'.$_FILES['ca_image']['name']);
		move_uploaded_file(
			$_FILES['ca_image']['tmp_name'],'../images/category/'.$ca_image
		);
			
		include_once 'configuration/function.php';
		$fz = formatBytes($_FILES['ca_image']['size']);
			
		$result['ca_image']='<img src="../images/category/'.$ca_image.'" style="height:50px;"><br />' . $fz;
		mysqli_query( $sqlcon,
			"update category set ca_image='".mysqli_real_escape_string( $sqlcon,$ca_image )."' ".
			"where ca_id=".$insertid
		);
	}else{
		$ca_image = '';
		$result['ca_image']='<img src="../images/default.png" style="height:50px;">';
	}
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	if (isset($_FILES['ca_image']['name']) && $_FILES['ca_image']['name']!=''){
		$cekimg = mysqli_query( $sqlcon,
			"select ca_image from category where ca_id=".intval($_POST['formid'])
		);
		$cekimg1 = mysqli_num_rows($cekimg);
		if ($cekimg1 > 0){
			$cekimg2 = mysqli_fetch_array($cekimg);
			if ($cekimg2['ca_image']!='' && file_exists("../images/category/".$cekimg2['ca_image'])){
				unlink("../images/category/".$cekimg2['ca_image']);
			}
		}
		$strrep = array("'",'"',' ','/','\\');
		$ca_image = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['ca_image']['name']);
		move_uploaded_file(
			$_FILES['ca_image']['tmp_name'],'../images/category/'.$ca_image
		);
		$addsql = ",ca_image='".mysqli_real_escape_string( $sqlcon,$ca_image )."'";
			
		include_once 'configuration/function.php';
		$fz = formatBytes($_FILES['ca_image']['size']);
	
		$result['ca_image']='<img src="../images/category/'.$ca_image.'" style="height:50px;"><br />' . $fz;
	}else{
		$addsql = '';
	}
	$qryupd = "update category set ".
	"ca_name='".mysqli_real_escape_string( $sqlcon,$ca_name )."',".
	"ca_desc='".mysqli_real_escape_string( $sqlcon,$ca_desc )."',".
	"ca_ca_id=".$ca_ca_id.",ca_status=".$ca_status.",ca_sequence=".$ca_sequence.",".
	"ca_updateby=".$cokidusr.",ca_update='".date("Y-m-d H:i:s")."'".$addsql." ".
	"where ca_id=".$_POST['formid'];
	mysqli_query( $sqlcon,$qryupd );
	$result['Status']='Update';
//	$result['QryUpd']=$qryupd;
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>