<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/memberorder.html') == 0){
	$result['Error']='Ilegal access detected!';
}
/*if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from cart where cr_id=".$_POST['delid'] );
		$result['Status'] = 'Delete Success!';
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}*/
if (isset($_POST['formid']) && !isset($result['Error'])){
	if (isset($_POST['cr_awb'])){$cr_awb=$_POST['cr_awb'];}else{$cr_awb='';}
	if (isset($_POST['cr_status'])){$cr_status=intval($_POST['cr_status']);}else{$cr_status=0;}
	
	// $arrstatus = array('Waiting Payment','Canceled','Paid','Payment Failed','On Packaging','On Delivery','Delivered','Delivery Failed');
	$arrneedawb = array(5,6,7);
	if ($cr_awb=='' && in_array($cr_status,$arrneedawb)){$result['Error']['cr_awb']=1;}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into cart (".
			"cr_status,cr_awb,".
			"cr_create,cr_updateby,cr_update".
		") values (".
			$cr_status.",'".mysqli_real_escape_string( $sqlcon,$cr_awb )."',".
			"'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update cart set ".
			"cr_awb='".mysqli_real_escape_string( $sqlcon,$cr_awb )."',".
			"cr_status=".$cr_status.",".
			"cr_update='".date("Y-m-d H:i:s")."',".
			"cr_updateby=".$cokidusr." ".
		"where cr_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>