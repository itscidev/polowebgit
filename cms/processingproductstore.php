<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/productstore.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from product_store where ps_id=".$_POST['delid'] );
		$resultdel = 'Delete Success!';
	}
	if ($resultdel != ''){
		$result['Status'] = $resultdel;
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['ps_pv_id'])){$ps_pv_id=intval($_POST['ps_pv_id']);}else{$ps_pv_id=0;}
	if (isset($_POST['ps_st_id']) && $_POST['formid']=='new'){
		$ps_st_id=$_POST['ps_st_id'];
	}elseif (isset($_POST['ps_st_id']) && $_POST['formid']!='new'){
		$ps_st_id=intval($_POST['ps_st_id']);
	}else{$ps_st_id=0;}
	
	if ($ps_pv_id==0){$result['Error']['ps_pv_id']=1;}
	if ($ps_st_id==0){$result['Error']['ps_st_id']=1;}
	if ($_POST['formid']!='new' && $ps_pv_id==$ps_st_id){
		$result['Error']['ps_st_id']=1;
	}
	if (!isset($result['Error']) && $_POST['formid']!='new'){
		$cekex = mysqli_num_rows(mysqli_query($sqlcon,
			"select ps_id from product_store where ps_pv_id=".$ps_pv_id." and ps_st_id=".$ps_st_id
		));
		if ($cekex > 0){
			$result['Error']['ps_st_id']=1;
		}
	}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		foreach ($ps_st_id as $stid){
			$cekex = mysqli_num_rows(mysqli_query($sqlcon,
				"select ps_id from product_store ".
				"where ps_pv_id=".$ps_pv_id." and ps_st_id=".$stid
			));
			if ($cekex == 0){
				mysqli_query( $sqlcon,"insert into product_store (".
					"ps_pv_id,ps_st_id,".
					"ps_createby,ps_create,ps_updateby,ps_update".
				") values (".
					$ps_pv_id.",".$stid.",".
					$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
				")");
			}	
		}
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update product_store set ".
			"ps_pv_id=".$ps_pv_id.",".
			"ps_st_id=".$ps_st_id.",".
			"ps_update='".date("Y-m-d H:i:s")."',".
			"ps_updateby=".$cokidusr." ".
		"where ps_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>