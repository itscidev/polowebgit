<?php
$filter = array();
if (isset( $_COOKIE['filter'] )) {
	setcookie( "filter", $_COOKIE['filter'], time() + 7200 );
	$filter = json_decode( $_COOKIE['filter'], true );
}

$sinfil = '';
if (isset( $filter['PO']['isianteks'] )) {
	$isianteks = $filter['PO']['isianteks'];
}else{
	$isianteks = '';
}
if ($isianteks != ''){
	$sinfil .= "pg_name like '%" . mysqli_real_escape_string( $sqlcon,$isianteks ) . "%'";
}

if (isset( $filter['PO']['isianteks1'] )) {
	$isianteks1 = $filter['PO']['isianteks1'];
}else{
	$isianteks1 = '';
}
if ($isianteks1 != ''){
	if ($sinfil != ''){
		$sinfil .= " and ";
	}
	$sinfil .= "(pg_title like '%" . mysqli_real_escape_string( $sqlcon,$isianteks1 ) . "%' ".
		"or pg_keyword like '%" . mysqli_real_escape_string( $sqlcon,$isianteks1 ) . "%' ".
		"or pg_desc like '%" . mysqli_real_escape_string( $sqlcon,$isianteks1 ) . "%')";
}

if ($sinfil != '') {
	$sinfil = " where " . $sinfil . " and pg_name!='general'";
} else {
	$sinfil = " where pg_name!='general'";
}

if (isset( $filter['PO']['batasanjumlah'] )) {
	$batasanjumlah = intval( $filter['PO']['batasanjumlah'] );
}else{
	$batasanjumlah = 200;
}
$sinlimit = ' limit '.$batasanjumlah;
?>