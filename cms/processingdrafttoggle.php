<?php
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);

if (!isset($_COOKIE['login']) || (isset($_COOKIE['login']) && $_COOKIE['login']=='')){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER'])){
	$result['Server'] = $_SERVER;
	$result['Error']='Ilegal access detected!';
}else{
	$arrallow = array(
		'/bannerhomeslider.html','/bannerhomequickmenu.html','/bannerseasons.html','/bannertext.html',
		'/category.html','/pagesoverwrite.html','/product.html','/productvariety.html','/productimage.html',
		'/event.html'
	);
	$allow = 0;
	foreach ($arrallow as $content){
		if (substr_count($_SERVER['HTTP_REFERER'], $content) > 0){
			$allow = 1;
		}
	}
	if ($allow == 0){
		$result['Error']='Ilegal access detected!';
	}
}
if (isset($_POST['toggledraft']) && $_POST['toggledraft']=='do' && !isset($result['Error'])){
	$path = '/';
	if (isset($_SERVER['SCRIPT_NAME']) && substr_count($_SERVER['SCRIPT_NAME'],'/poloweb/')>0){
		$path .= 'poloweb/';
	}
	if (isset($_COOKIE['draftstatus']) && $_COOKIE['draftstatus']=='1'){
		$draftstatus = 0;
		unset($_COOKIE["draftstatus"]);
		setcookie("draftstatus",null,time()-1,$path);
	}else{
		$draftstatus = 1;
		setcookie ( 'draftstatus', 1, time () + (60 * 60 * 24 * 30),$path );
	}
	$result['Status'] = $draftstatus;
}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>