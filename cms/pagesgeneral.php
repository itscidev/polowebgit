<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo $headtitle;?>
    <!-- Favicons -->
    <?php echo $favicon;?>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Ionicons 
    <link rel="stylesheet" href="css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="dist/css/AdminLTE.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	         folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	
	<!-- Fancybox simple pop up -->
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/ajaxpost.css" />
	<link rel="stylesheet" type="text/css" href="css/mycss.css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

	<?php include_once 'mainheader.php';?>
	<!-- =============================================== -->
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				General Pages Setting
				<small>Set general title, keyword & description</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li class="active">General Pages</li>
			</ol>
		</section>

		<!-- Main content -->
        <section class="content">
        	<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="box box-primary">
		                <div class="box-body">
		                
                <form name="form1" id="form1" role="form" action="processingpagesgeneral.php" method="post" 
				enctype="multipart/form-data">
					<!-- text input -->
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Title</label> <input name="pg_title" id="ca_name" class="form-control"
								type="text" placeholder="Enter..." value="<?php echo $pg_title;?>">
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Keyword</label> <input name="pg_keyword" id="pg_keyword" type="text" 
								class="form-control" placeholder="Enter..." value="<?php echo $pg_keyword;?>">
							<p class="help-block">Separate with , (comma)</p>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Description</label> <input name="pg_desc" id="pg_desc" type="text" 
								class="form-control" placeholder="Enter..." value="<?php echo $pg_desc;?>">
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 box-footer">
						<div class="progress">
							<div class="bar"></div>
							<div class="percent">0%</div>
						</div>
						<input name="formid" id="formid" type="hidden" value="<?php echo $formid;?>">
						<button type="submit" class="btn btn-warning">Submit</button>
					</div>
				</form>
				
		                </div><!-- /.box-body -->
					</div><!-- /.box -->
	            </div>
        	</div>
        </section><!-- /.content -->
		
		</div>
		<!-- /.content-wrapper -->

		<?php include_once 'footer.php';?>

	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="dist/js/demo.js"></script>
	<script src="js/ajaxpost.js"></script>
	<script src="js/pagesgeneral.js"></script>
	
</body>
</html>