<?php
//special case for favicon
$HTTP_HOST = array(
	'cms.poloindonesia.com'
);

if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://poloindonesia.com';
}else{
	$path = '..';
}
$favicon = '<link rel="shortcut icon" type="image/png" href="'.$path.'/images/favicon.png">';
unset($HTTP_HOST);

//special case for title head
$headtitle = '<title>CMS POLO Indonesia</title>';

if (isset($_COOKIE['login']) && $_COOKIE['login']!=''){
	header("location:index.html");
	exit();
}else{
	$pesan='';
	if (isset($_POST['uname'])){
		include_once 'configuration/connect.php';
		
		if ($_POST['uname'] == ''){ $pesan='No User detected!'; }
		if (!filter_var($_POST['uname'], FILTER_VALIDATE_EMAIL)){ $pesan='User Name is not email!'; }
		if (!isset($_POST['security']) || !isset($_POST['hsecurity']) || $_POST['security']==''
		|| $_POST['hsecurity']=='' || md5($_POST['security'])!=$_POST['hsecurity']){
			$pesan='Security Code not right!';
		}
		if ($pesan==''){
			$getusr = mysqli_query( $sqlcon,
				"select usr_id from user ".
				"where usr_name='".mysqli_real_escape_string( $sqlcon,$_POST['uname'] )."'"
			);
			$getusr1 = mysqli_num_rows($getusr);
			if ($getusr1 == 0){$pesan = 'User not found!';}
		}
		
		if ($pesan==''){
			$getusr2 = mysqli_fetch_array($getusr);
			$torand = array(
				'0','1','2','3','4','5','6','7','8','9',
				'a','b','c','d','e','f','g','h','i','j',
				'k','l','m','n','o','p','q','r','s','t',
				'u','v','w','x','y','z'
			);
			$maxchr = 10;$newpss='';
			for ($i=0;$i<11;$i++){
				$newpss .= $torand[ rand(0,35) ];
			}
			
			if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) ){
				$clientip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}elseif( isset($_SERVER['REMOTE_ADDR']) ){
				$clientip = $_SERVER['REMOTE_ADDR'];
			}else{
				$clientip = 'Un-known';
			}
			
			//execute sendmail
			$Subject	= 'CMS POLO Indonesia Reset Password Facility';
			$Message	= 'Dear Admin<br /><br />You or someone else has request for new password.<br />'.
				'This is your new password : <b>'.$newpss.'</b><br />'.
				'Request Date : '.date("d-m-Y H:i:s").'<br />'.
				'From IP : '.$clientip.'<br /><br />'.
				'<b>Don\'t forget to change your password directly after login</b><br /><br />Thank You';
			$AltMessage	= 'Dear Admin\n\nYou or someone else has request for new password.\n'.
				'This is your new password : '.$newpss.'\n'.
				'Request Date : '.date("d-m-Y H:i:s").'\n'.
				'From IP : '.$clientip.'\n\n'.
				'Don\'t forget to change your password directly after login\n\nThank You';
			$ToEmail	= $_POST['uname'];
			include_once 'configuration/function.php';
			$semail		= sendEmail($Subject,$Message,$AltMessage,$ToEmail,'cms','','',$sqlcon);

			if ($semail=='1'){
				mysqli_query($sqlcon,"update user set usr_password='".md5($newpss)."' where usr_id=".$getusr2['usr_id']);
				$pesan = 'Password already reset. Please cek your email.';
			}else{
				$pesan = $semail;
			}
		}
		if ($sqlcon){mysqli_close($sqlcon);}
	}
	
	$num1 = rand(0,90);
	$num2 = rand(0,9);
	$security = $num1.' + '.$num2.' = ?';
	$hsecurity = md5($num1+$num2);
}
?>