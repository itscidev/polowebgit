<?php
$confro = array(
	"fullservice"=>0,//0/1
	"baseUrl"=>"https://api.rajaongkir.com/starter",//https://pro.rajaongkir.com/api
	"key"=>"216bedeb35a1485bd7afffdc0299cccc",
	"courier"=>array('jne','pos','tiki')
// 	,"courier"=>array('jne','pos','tiki','rpx','esl','pcp','pandu','wahana','sicepat','jnt','pahala',
// 		'cahaya','sap','jet','dse','slis','first','ncs','star','ninja','lion','idl','rex')
);

include_once __DIR__ . '/../model/ekspedisi.php';
include_once __DIR__ . '/../model/lokasi.php';
include_once __DIR__ . '/../model/lokasistep.php';
include_once __DIR__ . '/../model/ongkir.php';
include_once __DIR__ . '/../model/cart.php';
include_once __DIR__ . '/../model/cartawb.php';

function roLocationKecamatan($sqlcon,$confro,$propid=0,$kabid=0,$zip=''){
	$propid = intval($propid);
	$kabid = intval($kabid);
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $confro['baseUrl']."/subdistrict?city=".$kabid,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"key: ".$confro['key']
		)
	));
	$output = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
		$result['Error'] = $err;
	} else {
		$result = json_decode($output,true);
		if (!isset($result['rajaongkir']['status']['code'])){
			$result['Error'] = 'No respond found!';
		}elseif ($result['rajaongkir']['status']['code']!=200){
			$result['Error'] = $result['rajaongkir']['status']['code'].' : '.
				$result['rajaongkir']['status']['description'];
		}else{
			$result = $result['rajaongkir']['results'];
			$maplok = array();
			if (count($result)>0){foreach ($result as $content){
				//master lokasi
				$kecid = 0;
				$strlok = $content['subdistrict_name'];
				if (isset($maplok[$strlok])){
					$kecid = $maplok[$strlok];
				}else{
					$qrylok = lokasi_autoiu($sqlcon,$strlok,$content['subdistrict_id']);
					if (is_array($qrylok)){
						$kecid = $qrylok[0];
						$maplok[$strlok] = $kecid;
					}
				}
				
				if ($kecid!=0){
					lokasistep_autoiu($sqlcon,$propid,$kabid,$kecid,$zip);
				}else{
					if (!isset($result['Error1'])){$result['Error1'] = "";}else{$result['Error1'] .= "\n";}
					$result['Error1'] .= json_encode($content);
				}
			}}
		}
	}
	return $result;
}
function roLocation($sqlcon,$confro){
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $confro['baseUrl']."/city",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"key: ".$confro['key']
		)
	));
	$output = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
		$result['Error'] = $err;
	} else {
		$result = json_decode($output,true);
		if (!isset($result['rajaongkir']['status']['code'])){
			$result['Error'] = 'No respond found!';
		}elseif ($result['rajaongkir']['status']['code']!=200){
			$result['Error'] = $result['rajaongkir']['status']['code'].' : '.
				$result['rajaongkir']['status']['description'];
		}else{
			$result = $result['rajaongkir']['results'];
			$maplok = array();
			if (count($result)>0){foreach ($result as $content){
				//master lokasi
				$propid = 0;
				$strlok = $content['province'];
				if (isset($maplok[$strlok])){
					$propid = $maplok[$strlok];
				}else{
					$qrylok = lokasi_autoiu($sqlcon,$strlok,$content['province_id']);
					if (is_array($qrylok)){
						$propid = $qrylok[0];
						$maplok[$strlok] = $propid;
					}
				}
				
				$kabid = 0;
				$strlok = $content['city_name'];
				if (isset($maplok[$strlok])){
					$kabid = $maplok[$strlok];
				}else{
					$qrylok = lokasi_autoiu($sqlcon,$strlok,$content['city_id']);
					if (is_array($qrylok)){
						$kabid = $qrylok[0];
						$maplok[$strlok] = $kabid;
					}
				}
				
				$kecid = 0;
				$zip = $content['postal_code'];
				if ($propid!=0 && $kabid!=0){
					if ($confro['fullservice']==0){
						lokasistep_autoiu($sqlcon,$propid,$kabid,$kecid,$zip);
					}else{
						$reskec = roLocationKecamatan($sqlcon,$confro,$propid,$kabid,$zip);
						if (isset($reskec['Error1'])){
							if (!isset($result['Error1'])){
								$result['Error1'] = "";
							}else{$result['Error1'] .= "\n";}
							$result['Error1'] .= $reskec['Error1'];
						}
					}
				}else{
					if (!isset($result['Error1'])){$result['Error1'] = "";}else{$result['Error1'] .= "\n";}
					$result['Error1'] .= json_encode($content)."(p:".$propid.",k:".$kabid.")";
				}
				
				if (count($maplok)>500){
					array_shift($maplok);
				}
			}}
		}
	}
	return $result;
}
function roPrice($sqlcon,$confro,$originid,$origintype='city',$destid,$desttype='city',$courier,$losoriginid,$losdestid){
	$originid = intval($originid);
	$destid = intval($destid);
	if ($confro['fullservice']==1){
		$postpar = "origin=".$originid."&originType=".$origintype."&destination=".$destid."&".
			"destinationType=".$desttype."&weight=1000&courier=".$courier;
	}else{
		$postpar = "origin=".$originid."&destination=".$destid."&weight=1000&courier=".$courier;
	}
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $confro['baseUrl']."/cost",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $postpar,
		CURLOPT_HTTPHEADER => array(
			"content-type: application/x-www-form-urlencoded",
			"key: ".$confro['key']
		)
	));
	$output = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
		$result['Error'] = $err;
	} else {
		$result = json_decode($output,true);
		if (!isset($result['rajaongkir']['status']['code'])){
			$result['Error'] = 'No respond found!';
		}elseif ($result['rajaongkir']['status']['code']!=200){
			$result['Error'] = $result['rajaongkir']['status']['code'].' : '.
					$result['rajaongkir']['status']['description'];
		}else{
			$result = $result['rajaongkir']['results'];
			if (count($result)>0){foreach ($result as $content){
				//master ekspedisi
				$eksid = ekspedisi_autoiu($sqlcon,$content['code'],$content['name']);

				if (count($content['costs'])){foreach ($content['costs'] as $type){
					$typeid = ekspedisitype_autoiu($sqlcon,$eksid[0],$type['service'],$type['description'],1,1000);
					$ongkirid = ongkir_autoiu(
						$sqlcon,$typeid[0],$losoriginid,$losdestid,$type['cost'][0]['etd'],$type['cost'][0]['value']
					);
				}}
			}}
		}
	}
	return $result;
}
function roAWB($sqlcon,$confro,$ekscode='',$awb=''){
	
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $confro['baseUrl']."/waybill",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "waybill=".$awb."&courier=jne".$ekscode,
		CURLOPT_HTTPHEADER => array(
			"content-type: application/x-www-form-urlencoded",
			"key: your-api-key"
		)
	));
	$output = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
		$result['Error'] = $err;
	} else {
		$result = json_decode($output,true);
		if (!isset($result['rajaongkir']['status']['code'])){
			$result['Error'] = 'No respond found!';
		}elseif ($result['rajaongkir']['status']['code']!=200){
			$result['Error'] = $result['rajaongkir']['status']['code'].' : '.
				$result['rajaongkir']['status']['description'];
		}else{
			$result = $result['rajaongkir']['result'];
		}
	}
	return $result;
}
?>