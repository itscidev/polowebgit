<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/productrelated.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}


include_once 'kalkulasiproductrelatedfilter.php';

$resview = numrowdb($sqlcon,"select pl_id from product_related".$sinfil,$batasanjumlah);
$notifview = $resview[1];

$list = '';
if ($resview[0] > 0) {
	$strsql = "select pl.*,par.pr_name parname,rel.pr_name relname ".
		"from product_related pl " .
		"left outer join product par on par.pr_id=pl_pr_id ".
		"left outer join product rel on rel.pr_id=pl_rel_pr_id ".
		$sinfil . " group by pl_id" . $sinlimit;
// 	echo $strsql;
	$getdt = mysqli_query( $sqlcon,$strsql );
	while ( $getdt1 = mysqli_fetch_assoc ( $getdt ) ) {
	
	$list .= "<tr id=\"tr" . $getdt1 ['pl_id'] . "\">" . 
	"<td id=\"fpl_pr_name" . $getdt1 ['pl_id'] . "\">" . htmlentities($getdt1['parname']) . "</td>" .
	"<td id=\"fpl_rel_pr_name" . $getdt1 ['pl_id'] . "\">" . htmlentities($getdt1['relname']) . "</td>" .
	"<td style=\"font-size:17px;\">" . 
		"<p id=\"fpl_pr_id".$getdt1['pl_id']."\" class=\"hide\">".$getdt1['pl_pr_id']."</p>".
		"<p id=\"fpl_rel_pr_id".$getdt1['pl_id']."\" class=\"hide\">".$getdt1['pl_rel_pr_id']."</p>".
		"<a href=\"#windata\" id=\"edit".$getdt1['pl_id']."\" class=\"fancybox\" ".
		"title=\"Edit Data\" onClick=\"prepareedit('".$getdt1['pl_id']."');\">".
		"<i class=\"fa fa-edit btn btn-warning btn-sm\"></i></a>".
		"<a href=\"#windelete\" id=\"delete".$getdt1['pl_id']."\" class=\"fancybox\" ".
		"title=\"Delete Data\" onClick=\"preparedelete('".$getdt1['pl_id']."');\">".
		"<i class=\"fa fa-times-circle btn btn-warning btn-sm\"></i></a>".
	"</td>" . 
	"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"3\">No Data!</td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}

//construct options parent & related
$optpl_pr_id = '';
$data = @mysqli_query($sqlcon,"select * from product order by pr_name asc");
$data1 = 0;
if ($data){$data1 = mysqli_num_rows($data);}
if ($data1 > 0){while ($data2 = mysqli_fetch_array($data)){
	$disable = '';
 	if ($data2['pr_status']==0 && $data2['pr_draft']==0){
 		$disable = ' disabled="disabled"';
 	}
	$optpl_pr_id .= '<option value="' . $data2['pr_id'] . '"'.$disable.'>'.
		htmlentities( $data2['pr_name'] ) .
	'</option>';
}}
$optpl_rel_pr_id = $optpl_pr_id;
?>