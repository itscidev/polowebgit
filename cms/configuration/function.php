<?php
function thisGroupAcc($arrAllow='',$getaks=''){
	$thisgroupacc = 0;
	if (is_array($arrAllow) && is_array($getaks) && count($arrAllow)>0 && count($getaks)>0){
		foreach ($arrAllow as $indexaks){
			if (isset($getaks[( $indexaks )]) && $getaks[( $indexaks )]==1){
				$thisgroupacc = 1;
			}
		}
	}
	return $thisgroupacc;
}
function draftStatus(){
	$draftstatus = "Off";
	if (isset($_COOKIE['draftstatus']) && $_COOKIE['draftstatus']=='1'){
		$draftstatus = "On";
	}
	return '<button type="button" id="buttonDraft" class="btn btn-warning" title="Toggle Show Draft" '.
			'onclick="toggledraft();">Draft '.$draftstatus.'</button>';
}
function numrowdb($sqlcon,$query,$limit){
	$getdt = @mysqli_query( $sqlcon,$query );
	$getdt1 = 0;
	if ($getdt){$getdt1 = mysqli_num_rows($getdt);}
	$notif = '';
	if ($getdt1 > $limit){
		$notif = '<small class="text-red">Not all data shown. Only '.
			number_format($limit,0,'.',',').' of '.number_format($getdt1,0,'.',',').'</small>';
	}
	return array($getdt1,$notif);
}
function formatBytes($bytes){
	$s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
	$e = floor(log($bytes)/log(1024));
	
	if ($e >= 1){
		$return = sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
	}else{
		$return = $bytes.' '.$s[0];
	}
	
	if ($bytes < 60000){
		$return = '<span style="color:#5858ff;">' . $return . '</span>';
	}elseif ($bytes >= 60000 && $bytes < 220000){
		$return = '<span style="color:#009300;">' . $return . '</span>';
	}elseif ($bytes >= 220000 && $bytes < 440000){
		$return = '<span style="color:#b5b500;">' . $return . '</span>';
	}else{
		$return = '<span style="color:#ff0000;">' . $return . '</span>';
	}
	return $return;
}
// menu class decide @kalkulasi.php
function classMenu($key = array()) {
	$classDecide = array ();
	$parent = 0;
	if (count ( $key ) > 0) {
		$posnow = $_SERVER ['SCRIPT_NAME'];
		$exppos = explode ( '/', $posnow );
		$exppos = $exppos [ (count($exppos)-1) ];
		foreach ( $key as $key1 ) {
			if ($key1 == $exppos) {
				$classDecide [$key1] = ' class="active"';
				$parent = 1;
			} else {
				$classDecide [$key1] = '';
			}
		}
	}
	if ($parent == 1) {
		$classDecide ['parent'] = ' active';
	} else {
		$classDecide ['parent'] = '';
	}
	return json_encode ( $classDecide );
}
function sendEmail(
	$Subject = '', $Message = '', $AltMessage = '', $ToEmail = '', $Purpose = 'cms', $attachment = '',
	$embedimage = '', $sqlcon = '', $sender = ''
) {
	//include_once 'configuration/connect.php';
	if ($sender != ''){
		//$mailset2 = [ms_host,ms_sendermail,ms_sendername,ms_password,ms_secure,ms_port]
		$mailset2 = json_decode($sender,true);
		$mailset1 = count($mailset2);
	}else{
		$mailset = mysqli_query( $sqlcon,"select * from mail_smtp where ms_purpose = '".$Purpose."'" );
		$mailset1 = mysqli_num_rows($mailset);
		if ($mailset1 == 0){
			$pesan = 'Message could not be sent. Mail setting not found!';
		}else{
			$mailset2 = mysqli_fetch_array($mailset);
		}
	}
	
	if ($mailset1 > 0){
		
		//execute sendmail
		//echo $_SERVER['SCRIPT_NAME'];
		$scriptname = explode('/',$_SERVER['SCRIPT_NAME']);
		if ((isset($scriptname[2]) && $scriptname[2]=='cms') 
		|| (isset($scriptname[1]) && $scriptname[1]=='cms')){
			include_once "PHPMailer/PHPMailerAutoload.php";
		}elseif ((isset($scriptname[2]) && $scriptname[2]=='faspay') 
		|| (isset($scriptname[1]) && $scriptname[1]=='faspay')){
			include_once "../cms/PHPMailer/PHPMailerAutoload.php";
		}elseif (substr_count($_SERVER['SCRIPT_NAME'], 'processinghomemail.php') > 0 
		|| substr_count($_SERVER['SCRIPT_NAME'], 'processingmembership.php') > 0){
			include_once "cms/PHPMailer/PHPMailerAutoload.php";
		}
		
		$mail = new PHPMailer;
		//$mail->SMTPDebug = 2;									// Enable verbose debug output
		$mail->isSMTP();										// Set mailer to use SMTP
		//$mail->Host = 'smtp.mail.yahoo.com';					// Specify main SMTP servers
		$mail->Host = $mailset2['ms_host'];
		$mail->SMTPAuth = true;									// Enable SMTP authentication
		//$mail->Username = 'richardjuniar@yahoo.com';			// SMTP username
		$mail->Username = $mailset2['ms_sendermail'];
		//$mail->Password = 'Dogied0g';							// SMTP password
		$mail->Password = $mailset2['ms_password'];
		//$mail->SMTPSecure = 'tls';								// tls/ssl
		$mail->SMTPSecure = $mailset2['ms_secure'];
		//$mail->Port = 25;										// 465/587/25
		$mail->Port = $mailset2['ms_port'];
		//$mail->setFrom('richardjuniar@yahoo.com', 'Richard Juniar');
		$mail->setFrom($mailset2['ms_sendermail'], $mailset2['ms_sendername']);
		//$mail->addAddress('joe@example.net', 'Joe User');		// Add a recipient
		$mail->addAddress($ToEmail);							// Name is optional
		//$mail->addReplyTo('richardjuniar@yahoo.com', 'Richard Juniar');
		$mail->addReplyTo($mailset2['ms_sendermail'], $mailset2['ms_sendername']);
		//$mail->addCC('cc@example.com');
		//$mail->addBCC('bcc@example.com');
		//$mail->addAttachment('/var/tmp/file.tar.gz');			// Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');	// Optional name
		if (is_array($attachment)){
			foreach ($attachment as $file){
				$mail->addAttachment($file);
			}
		}
		if (is_array($embedimage)){
			foreach ($embedimage as $image){
				$mail->AddEmbeddedImage($image[0], $image[1]);				
			}
		}
		$mail->isHTML(true);									// Set email format to HTML
		$mail->Subject = $Subject;
		$mail->Body    = $Message;
		$mail->AltBody = $AltMessage;
		
		if(!$mail->send()) {
			$pesan = 'Message could not be sent. Try again later. '.$mail->ErrorInfo;
		} else {
			$pesan = '1';
		}
	}
	return $pesan;
}
?>