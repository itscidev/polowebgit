<?php
function bannertextcarousel($sqlcon,$posisi=0){
	$headerslide = '';
	if (substr_count( $_SERVER['SCRIPT_NAME'], '/eventdetail.php' ) == 0){
		$sld = @mysqli_query($sqlcon,
			"select * from banner_text ".
			"inner join banner_text_showon on bts_bt_id=bt_id ".
			"where bt_status=1 and bts_ca_id=".$posisi." ".
			"order by bt_sequence asc,bt_update desc"
		);
		$sld1 = 0;
		if ($sld){$sld1 = mysqli_num_rows($sld);}
		if ($sld1 > 0){while ($sld2 = mysqli_fetch_array($sld)){
			$headerslide .= '<div class="swiper-slide">'.$sld2['bt_anchor'].'</div>';
		}}	
	}
	if ($headerslide != ''){
		$headerslide = '<div class="row headerrowrunningtext">'.
			'<div id="headerrunningtext" class="col-xs-12">'.
				'<div id="runningtextcontainer" class="swiper-container">'.
					'<div class="swiper-wrapper">'.$headerslide.'</div>'.
				'</div>'.
			'</div>'.
		'</div>';
	}
	return $headerslide;
}
function colToUse($jumdata = 0,$pos = 0){
	$awal = $akhir = '';
	$col = 'col-sm-3';
	if ($pos % 4 == 1){$awal = 'ok';}
	if ($pos % 4 == 0 || $pos==$jumdata){$akhir = 'ok';}
	
	if ($jumdata == 3){
		$col = 'col-sm-4';
		if ($pos % 3 == 1){$awal = 'ok';}
		if ($pos % 3==0 || $pos==$jumdata){$akhir = 'ok';}
	}elseif ($jumdata == 2){
		$col = 'col-sm-6';
		if ($pos % 2 == 1){$awal = 'ok';}
		if ($pos % 2==0 || $pos==$jumdata){$akhir = 'ok';}
	}elseif ($jumdata == 1){
		$col = 'col-sm-12';
		$awal = 'ok';
		$akhir = 'ok';
	}
	return array($col,$awal,$akhir);
}
function getTrees($sqlcon,$path = '',$tingkat = 0,$parent = 0,$breadcrumb = ''){
	
	$resfunc = '';
	
	$getcat = @mysqli_query( $sqlcon,
		"select * from category where ca_ca_id=".$parent." and ca_status=1 order by ca_sequence,ca_name" 
	);
	$no = 1;
	$getcat1 = 0;
	if ($getcat){$getcat1 = mysqli_num_rows($getcat);}
	if ($getcat1 > 0){while ($getcat2 = mysqli_fetch_array($getcat)){
		if ($tingkat < 3){
			$cekchild = mysqli_num_rows(mysqli_query( $sqlcon,
				"select ca_id from category where ca_ca_id=".$getcat2['ca_id']
			));
			if ($tingkat == 0){
				$parentcrumb = $path.'mainparent/'.rawurlencode(urlencode($getcat2['ca_name']));
				$nbreadcrumb = $path.'categories/'.rawurlencode(urlencode($getcat2['ca_name']));
				if ($cekchild == 0){
$resfunc .= '<li class="nodropdown">'.
	'<a href="'.$parentcrumb.'" onclick="smartmenu(\''.$parentcrumb.'\');return false;">'.
		htmlentities(strtoupper($getcat2['ca_name'])).
	'</a>'.
'</li>';
				}else{
$resfunc .= '<li class="dropdown">'.
	'<a href="'.$parentcrumb.'" class="dropdown-toggle" data-toggle="dropdown" '.
	'onclick="smartmenu(\''.$parentcrumb.'\');return false;">'.
		//'<span> '.htmlentities(strtoupper($getcat2['ca_name'])).' </span>'.
		'<i class="showonmobile fa fa-chevron-left"></i>'.htmlentities(strtoupper($getcat2['ca_name'])).
	'</a>'.
	'<ul class="dropdown-menu multi col'.$cekchild.'width">'.
	'<div class="row">'.
		'<div class="col-sm-12">';
				}
			}elseif ($tingkat == 1){
				$nbreadcrumb = $breadcrumb.'/'.rawurlencode(urlencode($getcat2['ca_name']));
				$usedcol = colToUse($getcat1,$no);
				if ($usedcol[1] == 'ok'){
$resfunc .= 
		'<div class="row">';
				}
$resfunc .=	//'<div class="'.$usedcol[0].' w3layouts-nav-agile w3layouts-mens-nav-agileits-1">'.
			'<div class="'.$usedcol[0].'">'.
				'<ul class="multi-column-dropdown">'.
					'<li class="heading">'.
						'<a href="'.$nbreadcrumb.'" onclick="smartmenu(\''.$nbreadcrumb.'\');return false;">'.
							'<i class="showonmobile fa fa-chevron-left"></i>'.
							htmlentities(ucwords($getcat2['ca_name'])).
							'<i class="hideonmobile fa fa-angle-right" aria-hidden="true"></i>'.
						'</a>'.
					'</li>';
			}elseif ($tingkat == 2){
				$nbreadcrumb = $breadcrumb.'/'.rawurlencode(urlencode($getcat2['ca_name']));
$resfunc .=			'<li class="headingchild"><a href="'.$nbreadcrumb.'">'.
						/* '<i class="hideonmobile fa fa-angle-right" aria-hidden="true"></i>'. */
						htmlentities(ucwords($getcat2['ca_name'])).
					'</a></li>';
			}
			
			if ($cekchild > 0){
				$resfunc .= getTrees($sqlcon,$path,$tingkat+1,$getcat2['ca_id'],$nbreadcrumb);
			}
			
			if ($tingkat == 1){
$resfunc .=		'</ul>'.
			'</div>';
				if ($usedcol[2] == 'ok'){
$resfunc .=	
		'</div>';
				}
			}
			if ($tingkat==0 && $cekchild>0){
$resfunc .= 
		'</div>'.
		/* '<div class="col-sm-3 hideonmobile">'.
			//'<div class="col-sm-12 w3layouts-nav-agile w3layouts-mens-nav-agileits '.
			//'w3layouts-mens-nav-agileits-4">';
			'<div class="col-sm-12">';
//$resfunc .=				$path.'images/'.$getcat2['ca_image'];
				if (isset($getcat2['ca_image']) && $getcat2['ca_image']!='' 
				&& file_exists('images/category/'.$getcat2['ca_image'])){
$resfunc .= 	'<a href="'.$parentcrumb.'">'.
					'<img src="'.$path.'images/category/'.$getcat2['ca_image'].'" '.
					'alt="'.htmlentities($getcat2['ca_name']).'">'.
				'</a>';
				}
$resfunc .= '</div>'.
		'</div>'. */
		'<div class="clearfix"></div>'.
		/* '<p class="promo">Use Promo Code <span>#CFFGTY56</span> and take 30% off '.
		'the products. <a href="#">Details</a></p>'. */
	'</div>'.
	'</ul>'.
'</li>';
			}
		}
		$no++;
	}}
	return $resfunc;
}
?>