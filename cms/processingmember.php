<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/member.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select mm_foto from member where mm_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		$resultdel = '';
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['mm_foto']!='' && file_exists("../images/foto/".$cekimg2['mm_foto'])){
				unlink("../images/foto/".$cekimg2['mm_foto']);
			}
			mysqli_query( $sqlcon,"delete from member where mm_id=".$_POST['delid'] );
			$resultdel = 'Delete Success!';
		}
		if ($resultdel != ''){
			$result['Status'] = $resultdel;
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}else{
		$result['Error']='Data not found!';
	}
}
if (isset($_POST['formid']) && !isset($result['Error'])){
	if (isset($_POST['mm_name'])){$mm_name=$_POST['mm_name'];}else{$mm_name='';}
	if (isset($_POST['mm_email'])){$mm_email=$_POST['mm_email'];}else{$mm_email='';}
	if (isset($_POST['mm_bannedreason'])){$mm_bannedreason=$_POST['mm_bannedreason'];}else{$mm_bannedreason='';}
	if (isset($_POST['mm_banned'])){$mm_banned=intval($_POST['mm_bannedreason']);}else{$mm_banned=0;}
	if (isset($_POST['mm_emailstatus'])){$mm_emailstatus=intval($_POST['mm_emailstatus']);}else{$mm_emailstatus=0;}
	
	if ($mm_name==''){$result['Error']['mm_name']=1;}
	if ($mm_email==''){
		$result['Error']['mm_email']=1;
	}elseif (!filter_var($mm_email, FILTER_VALIDATE_EMAIL)){
		$result['Error']['mm_email']=1;
	}
	if (isset($_FILES['mm_foto']['name']) && !(
		strtolower($_FILES['mm_foto']['type'])=='image/jpeg' || strtolower($_FILES['mm_foto']['type'])=='image/png'
	)){
		$result['Error']['mm_foto']=1;
	}

	$strrep = array("'",'"',' ','/','\\','#');
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into member (".
			"mm_emailstatus,mm_name,mm_foto,".
			"mm_email,mm_bannedreason,mm_banned,".
			"mm_create,mm_updateby,mm_update".
		") values (".
			$mm_emailstatus.",'".mysqli_real_escape_string( $sqlcon,$mm_name )."','',".
			"'".mysqli_real_escape_string( $sqlcon,$mm_email )."','".mysqli_real_escape_string( $sqlcon,$mm_bannedreason )."',".$mm_banned.",".
			"'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);

		$insertid = mysqli_insert_id( $sqlcon );
		if (isset($_FILES['mm_foto']['name']) && $_FILES['mm_foto']['name']!=''){
			$mm_foto = str_replace($strrep,'', $insertid.'-'.$_FILES['mm_foto']['name']);
			move_uploaded_file(
				$_FILES['mm_foto']['tmp_name'],'../images/foto/'.$mm_foto
			);
			
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['mm_foto']['size']);
			
			$result['mm_foto']='<img src="'.$path.'/images/foto/'.$mm_foto.'" style="height:50px;"><br />' . $fz;
			mysqli_query( $sqlcon,
				"update member set mm_foto='".mysqli_real_escape_string( $sqlcon,$mm_foto )."' ".
				"where mm_id=".$insertid
			);
		}else{
			$mm_foto = '';
			$result['mm_foto']='<img src="'.$path.'/images/default.png" style="height:50px;">';
		}
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';

		$addsql = '';
		if (isset($_FILES['mm_foto']['name']) && $_FILES['mm_foto']['name']!=''){
			$cekimg = mysqli_query( $sqlcon,
				"select mm_foto from member where mm_id=".intval($_POST['formid'])
			);
			$cekimg1 = mysqli_num_rows($cekimg);
			if ($cekimg1 > 0){
				$cekimg2 = mysqli_fetch_array($cekimg);
				if ($cekimg2['mm_foto']!='' && file_exists("../images/foto/".$cekimg2['mm_foto'])){
					unlink("../images/foto/".$cekimg2['mm_foto']);
				}
			}
			$mm_foto = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['mm_foto']['name']);
			move_uploaded_file(
				$_FILES['mm_foto']['tmp_name'],'../images/foto/'.$mm_foto
			);
			$addsql .= ",mm_foto='".mysqli_real_escape_string( $sqlcon,$mm_foto )."'";
			
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['mm_foto']['size']);
				
			$result['mm_foto']='<img src="'.$path.'/images/foto/'.$mm_foto.'" style="height:50px;"><br />' . $fz;
		}
		
		mysqli_query( $sqlcon,
		"update member set ".
			"mm_name='".mysqli_real_escape_string( $sqlcon,$mm_name )."',".
			"mm_email='".mysqli_real_escape_string( $sqlcon,$mm_email )."',".
			"mm_bannedreason='".mysqli_real_escape_string( $sqlcon,$mm_bannedreason )."',".
			"mm_banned=".$mm_banned.",".
			"mm_emailstatus=".$mm_emailstatus.",".
			"mm_update='".date("Y-m-d H:i:s")."',".
			"mm_updateby=".$cokidusr.$addsql." ".
		"where mm_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>