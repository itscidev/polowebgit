<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/access.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (!isset($_POST['delete']) || (isset($_POST['delete']) && $_POST['delete']!=1)){
		$result['Error']='Confirmation box not checked!';
	}else{
		mysqli_query( $sqlcon,"delete from group_access where ga_ac_id=".$_POST['delid'] );
		mysqli_query( $sqlcon,"delete from access where ac_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['ac_name'])){$ac_name=$_POST['ac_name'];}else{$ac_name='';}
	if (isset($_POST['ac_desc'])){$ac_desc=$_POST['ac_desc'];}else{$ac_desc='';}
	
	if ($ac_name == ''){
		$result['Error']['ac_name'] = 1;
	}else{
		$addexist = '';
		if ($_POST['formid']!='new'){$addexist = ' and ac_id!='.intval($_POST['formid']);}
		$exist = mysqli_num_rows(mysqli_query($sqlcon,
			"select ac_id from access ".
			"where ac_name='".mysqli_real_escape_string($sqlcon,$ac_name)."'".$addexist
		));
		if ($exist > 0){$result['Error']['ac_name'] = 1;}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into access (".
		"ac_name,".
		"ac_desc,".
		"ac_createby,ac_create,ac_updateby,ac_update".
	") values (".
		"'".mysqli_real_escape_string( $sqlcon,$ac_name )."',".
		"'".mysqli_real_escape_string( $sqlcon,$ac_desc )."',".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	mysqli_query( $sqlcon,$strquery);
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	mysqli_query( $sqlcon,
	"update access set ".
	"ac_name='".mysqli_real_escape_string( $sqlcon,$ac_name )."',".
	"ac_desc='".mysqli_real_escape_string( $sqlcon,$ac_desc )."',".
	"ac_updateby=".$cokidusr.",ac_update='".date("Y-m-d H:i:s")."' ".
	"where ac_id=".$_POST['formid']
	);
	$result['Status']='Update';
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>