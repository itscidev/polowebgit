<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/product.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from product where pr_id=".$_POST['delid'] );
		$resultdel = 'Delete Success!';
	}
	if ($resultdel != ''){
		$result['Status'] = $resultdel;
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['pr_name'])){$pr_name=$_POST['pr_name'];}else{$pr_name='';}
	if (isset($_POST['pr_tagline'])){$pr_tagline=$_POST['pr_tagline'];}else{$pr_tagline='';}
	if (isset($_POST['pr_weight'])){$pr_weight=intval($_POST['pr_weight']);}else{$pr_weight=0;}
	if (isset($_POST['pr_status'])){$pr_status=intval($_POST['pr_status']);}else{$pr_status=0;}
	if (isset($_POST['pr_draft'])){$pr_draft=intval($_POST['pr_draft']);}else{$pr_draft=0;}
	if (isset($_POST['pr_onsale'])){$pr_onsale=intval($_POST['pr_onsale']);}else{$pr_onsale=0;}
	if (isset($_POST['pr_price'])){$pr_price=intval($_POST['pr_price']);}else{$pr_price=0;}
	if (isset($_POST['pr_ca_id'])){$pr_ca_id=intval($_POST['pr_ca_id']);}else{$pr_ca_id=0;}
	if (isset($_POST['pr_desc'])){$pr_desc=$_POST['pr_desc'];}else{$pr_desc='';}
	if (isset($_POST['pr_info'])){$pr_info=$_POST['pr_info'];}else{$pr_info='';}
	if (isset($_POST['pr_care'])){$pr_care=$_POST['pr_care'];}else{$pr_care='';}
	
	if ($pr_name==''){$result['Error']['pr_name']=1;}
	if ($pr_ca_id==0){$result['Error']['pr_ca_id']=1;}
	if ($pr_weight==0){$result['Error']['pr_weight']=1;}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,"insert into product (".
			"pr_status,pr_draft,pr_onsale,pr_price,pr_ca_id,".
			"pr_name,pr_weight,".
			"pr_tagline,".
			"pr_desc,".
			"pr_info,".
			"pr_care,".
			"pr_createby,pr_create,pr_updateby,pr_update".
		") values (".
			$pr_status.",".$pr_draft.",".$pr_onsale.",".$pr_price.",".$pr_ca_id.",".
			"'".mysqli_real_escape_string( $sqlcon,$pr_name )."',".$pr_weight.",".
			"'".mysqli_real_escape_string( $sqlcon,$pr_tagline )."',".
			"'".mysqli_real_escape_string( $sqlcon,$pr_desc )."',".
			"'".mysqli_real_escape_string( $sqlcon,$pr_info )."',".
			"'".mysqli_real_escape_string( $sqlcon,$pr_care )."',".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")");
		//$result['QryErr']=mysqli_error($sqlcon);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update product set ".
			"pr_name='".mysqli_real_escape_string( $sqlcon,$pr_name )."',".
			"pr_tagline='".mysqli_real_escape_string( $sqlcon,$pr_tagline )."',".
			"pr_weight=".$pr_weight.",".
			"pr_status=".$pr_status.",".
			"pr_draft=".$pr_draft.",".
			"pr_onsale=".$pr_onsale.",".
			"pr_price=".$pr_price.",".
			"pr_ca_id=".$pr_ca_id.",".
			"pr_desc='".mysqli_real_escape_string( $sqlcon,$pr_desc )."',".
			"pr_info='".mysqli_real_escape_string( $sqlcon,$pr_info )."',".
			"pr_care='".mysqli_real_escape_string( $sqlcon,$pr_care )."',".
			"pr_update='".date("Y-m-d H:i:s")."',".
			"pr_updateby=".$cokidusr." ".
		"where pr_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>