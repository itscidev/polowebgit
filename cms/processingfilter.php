<?php
$result = array();
$filter = array();
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/filter.php') == 0){
	$result['Error'] = 'Ilegal access detected!';
}
if (!isset($_POST['asal']) || $_POST['asal'] == ''){
	$result['Error'] = 'User modul not detected!';
}

if (!isset($result['Error'])){
	if (isset($_POST['batasanjumlah']) && intval($_POST['batasanjumlah']) == 0){
		$result['Error']['batasanjumlah'] = 1;
	}
}

if (!isset($result['Error'])){
	if (isset($_COOKIE['filter'])){
		$filter = json_decode($_COOKIE['filter'],true);
	}
	$arrfilcode = array(
		'US','AC','GR','CA','PO','BNS','BNQ','BVP','BNE','BNT','SZ','CL','CI','PR','PV','ST','PL','PS','EX',
		'ET','LO','LS','ON','PRO','PRC','MM','PT','MOR'
	);
	foreach ($arrfilcode as $arrfilcode1){
		if ($_POST['asal'] == $arrfilcode1) {
			$filter[ $arrfilcode1 ] = $_POST;
		}
	}
	
	setcookie("filter", json_encode($filter), time()+7200);
	$result['Status'] = 'Success';
}

/*
if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}
if (isset($_SERVER)){$result['SERVER']=$_SERVER;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>