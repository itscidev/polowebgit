<?php include_once 'kalkulasi.php';?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Profile Page</title>
 	<!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
	<!-- daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">    
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
</head>
<body>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="box box-info" style="min-height: 400px;">
			<div class="box-header with-border">
				<h3 class="box-title">Profile Information</h3>
			</div><!-- /.box-header -->
			
			<div class="box-body">
				<!-- form start -->
				<form name="form1" id="form1" role="form" action="profile.php" method="post" 
				enctype="multipart/form-data">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Password</label> <input name="cpassword" type="password"
							class="form-control"
							placeholder="Do not Enter, if there is no changes!" value="">
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Re-type Password</label> <input name="rpassword"
							type="password" class="form-control"
							placeholder="Do not Enter, if there is no changes!" value="">
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="photo">Foto</label>
						<input name="foto" id="foto" type="file">
						<p class="help-block">Best Resolution : 160px X 160px, .jpg or .png</p>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<?php echo $pesan;?>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				</form>
			</div>
			<!-- /.box-body -->
		</div><!-- /.box -->
	</div>
</div>
<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/select2.full.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- date-picker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
</body>
</html>
