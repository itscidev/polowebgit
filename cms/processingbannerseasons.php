<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/bannerseasons.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select bn_image,bn_image1 from banner where bn_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		$resultdel = '';
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['bn_image']!='' && file_exists("../images/banner/".$cekimg2['bn_image'])){
				unlink("../images/banner/".$cekimg2['bn_image']);
			}
			if ($cekimg2['bn_image1']!='' && file_exists("../images/banner/".$cekimg2['bn_image1'])){
				unlink("../images/banner/".$cekimg2['bn_image1']);
			}
			mysqli_query( $sqlcon,"delete from banner_showon where bns_bn_id=".$_POST['delid'] );
			mysqli_query( $sqlcon,"delete from banner where bn_id=".$_POST['delid'] );
			$resultdel = 'Delete Success!';
		}
		if ($resultdel != ''){
			$result['Status'] = $resultdel;
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}else{
		$result['Error']='Data not found!';
	}
}
if (isset($_POST['formid'])){
	$bn_purpose = 'Seasons';
	if (isset($_POST['bn_title'])){$bn_title=$_POST['bn_title'];}else{$bn_title='';}
	if (isset($_POST['bn_subtitle'])){$bn_subtitle=$_POST['bn_subtitle'];}else{$bn_subtitle='';}
	if (isset($_POST['bn_link'])){$bn_link=$_POST['bn_link'];}else{$bn_link='';}
	if (isset($_POST['bn_sequence'])){$bn_sequence=intval($_POST['bn_sequence']);}else{$bn_sequence=0;}
	if (isset($_POST['bn_status'])){$bn_status=intval($_POST['bn_status']);}else{$bn_status=0;}
	if (isset($_POST['bn_draft'])){$bn_draft=intval($_POST['bn_draft']);}else{$bn_draft=0;}
	if (isset($_POST['bn_from'])){
		$exp = explode('/', substr($_POST['bn_from'], 0,10));
		$bn_from=$exp[2].'-'.$exp[0].'-'.$exp[1].' '.substr($_POST['bn_from'], -8);
	}else{$bn_from='';}
	if (isset($_POST['bn_to'])){
		$exp = explode('/', substr($_POST['bn_to'], 0,10));
		$bn_to=$exp[2].'-'.$exp[0].'-'.$exp[1].' '.substr($_POST['bn_to'], -8);
	}else{$bn_to='';}
	if (isset($_POST['bns_ca_id']) && is_array($_POST['bns_ca_id'])){
		$bns_ca_id = $_POST['bns_ca_id'];
	}else{$bns_ca_id=array();}
	$result['bns_ca_id'] = '';
	if (count($bns_ca_id) > 0){foreach ($bns_ca_id as $index => $content){
		if ($content == 1){
			if ($result['bns_ca_id'] != ''){$result['bns_ca_id'] .= ',';}
			$result['bns_ca_id'] .= $index;
		}
	}}
	
	if ($bn_title==''){$result['Error']['bn_title']=1;}
	if ($_POST['formid']=='new' && (
		!isset($_FILES['bn_image']['name']) || (
			isset($_FILES['bn_image']['name']) && $_FILES['bn_image']['name']==''
		)
	)){
		$result['Error']['bn_image']=1;
	}
	if (isset($_FILES['bn_image']['name']) && !(
		strtolower($_FILES['bn_image']['type'])=='image/jpeg' || strtolower($_FILES['bn_image']['type'])=='image/png'
	)){
		$result['Error']['bn_image']=1;
	}
	/* if ($_POST['formid']=='new' && (
		!isset($_FILES['bn_image1']['name']) || (
			isset($_FILES['bn_image1']['name']) && $_FILES['bn_image1']['name']==''
		)
	)){
		$result['Error']['bn_image1']=1;
	} */
	if (isset($_FILES['bn_image1']['name']) && !(
		strtolower($_FILES['bn_image1']['type'])=='image/jpeg' || strtolower($_FILES['bn_image1']['type'])=='image/png'
	)){
		$result['Error']['bn_image1']=1;
	}
	if ($bn_from==''){$result['Error']['bn_from']=1;}
	if ($bn_to==''){$result['Error']['bn_to']=1;}
	if ($bn_from!='' && $bn_to!='' && strtotime($bn_from)>strtotime($bn_to)){
		$result['Error']['bn_from']=1;
		$result['Error']['bn_to']=1;
	}
	
	$strrep = array("'",'"',' ','/','\\','#');
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into banner (".
			"bn_purpose,bn_status,bn_draft,".
			"bn_title,".
			"bn_image,bn_image1,bn_sequence,bn_subtitle,".
			"bn_link,bn_from,".
			"bn_to,".
			"bn_createby,bn_create,bn_updateby,bn_update".
		") values (".
			"'".mysqli_real_escape_string( $sqlcon,$bn_purpose )."',".$bn_status.",".$bn_draft.",".
			"'".mysqli_real_escape_string( $sqlcon,$bn_title )."',".
			"'','',".$bn_sequence.",'".mysqli_real_escape_string( $sqlcon,$bn_subtitle )."',".
			"'".mysqli_real_escape_string( $sqlcon,$bn_link )."','".$bn_from."',".
			"'".$bn_to."',".
			$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);
		$insertid = mysqli_insert_id( $sqlcon );
		if (count($bns_ca_id) > 0){
			foreach ($bns_ca_id as $index => $content){
				if ($content == 1){
					mysqli_query( $sqlcon,
					"insert into banner_showon (".
						"bns_bn_id,bns_ca_id,".
						"bns_createby,bns_create,bns_updateby,bns_update".
					") values (".
						$insertid.",".$index.",".
						$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
					")"
					);
				}
			}
		}
		if (isset($_FILES['bn_image']['name']) && $_FILES['bn_image']['name']!=''){
			$bn_image = str_replace($strrep,'', $insertid.'-'.$_FILES['bn_image']['name']);
			move_uploaded_file(
				$_FILES['bn_image']['tmp_name'],'../images/banner/'.$bn_image
			);
			
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['bn_image']['size']);
			
			$result['bn_image']='<img src="'.$path.'/images/banner/'.$bn_image.'" style="height:50px;"><br />' . $fz;
			mysqli_query( $sqlcon,
				"update banner set bn_image='".mysqli_real_escape_string( $sqlcon,$bn_image )."' ".
				"where bn_id=".$insertid
			);
		}else{
			$bn_image = '';
			$result['bn_image']='<img src="'.$path.'/images/default.png" style="height:50px;">';
		}
		if (isset($_FILES['bn_image1']['name']) && $_FILES['bn_image1']['name']!=''){
			$bn_image1 = str_replace($strrep,'', $insertid.'m-'.$_FILES['bn_image1']['name']);
			move_uploaded_file(
				$_FILES['bn_image1']['tmp_name'],'../images/banner/'.$bn_image1
			);
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['bn_image1']['size']);
				
			$result['bn_image1']='<img src="'.$path.'/images/banner/'.$bn_image1.'" style="height:50px;"><br />' . $fz;
			mysqli_query( $sqlcon,
				"update banner set bn_image1='".mysqli_real_escape_string( $sqlcon,$bn_image1 )."' ".
				"where bn_id=".$insertid
			);
		}else{
			$bn_image1 = '';
			$result['bn_image1']='<img src="'.$path.'/images/default.png" style="height:50px;">';
		}
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		$getnow = mysqli_fetch_array(mysqli_query( $sqlcon,
			"select GROUP_CONCAT(bns_ca_id) bnscaid from banner_showon ".
			"where bns_bn_id=".intval($_POST['formid'])
		));
		$nowbnscaid = array();
		if (isset($getnow['bnscaid']) && $getnow['bnscaid']!='' && $getnow['bnscaid']!=null){
			$nowbnscaid = explode(',',$getnow['bnscaid']);
			foreach ($nowbnscaid as $content){
				if (!isset($bns_ca_id[$content]) || (isset($bns_ca_id[$content]) && $bns_ca_id[$content]==0)){
					mysqli_query( $sqlcon,
					"delete from banner_showon ".
					"where bns_bn_id=".intval($_POST['formid'])." and bns_ca_id=".$content
					);
				}
			}
		}
		if (count($bns_ca_id) > 0){
			foreach ($bns_ca_id as $index => $content){
				if ($content==1 && !in_array($index,$nowbnscaid)){
					mysqli_query( $sqlcon,
					"insert into banner_showon (".
						"bns_bn_id,bns_ca_id,".
						"bns_createby,bns_create,bns_updateby,bns_update".
					") values (".
						intval($_POST['formid']).",".$index.",".
						$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
					")"
					);
				}
			}
		}
		$addsql = '';
		if (isset($_FILES['bn_image']['name']) && $_FILES['bn_image']['name']!=''){
			$cekimg = mysqli_query( $sqlcon,
				"select bn_image from banner where bn_id=".intval($_POST['formid'])
			);
			$cekimg1 = mysqli_num_rows($cekimg);
			if ($cekimg1 > 0){
				$cekimg2 = mysqli_fetch_array($cekimg);
				if ($cekimg2['bn_image']!='' && file_exists("../images/banner/".$cekimg2['bn_image'])){
					unlink("../images/banner/".$cekimg2['bn_image']);
				}
			}
			$bn_image = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['bn_image']['name']);
			move_uploaded_file(
				$_FILES['bn_image']['tmp_name'],'../images/banner/'.$bn_image
			);
			$addsql .= ",bn_image='".mysqli_real_escape_string( $sqlcon,$bn_image )."'";
			
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['bn_image']['size']);
				
			$result['bn_image']='<img src="'.$path.'/images/banner/'.$bn_image.'" style="height:50px;"><br />' . $fz;
		}
		if (isset($_FILES['bn_image1']['name']) && $_FILES['bn_image1']['name']!=''){
			$cekimg = mysqli_query( $sqlcon,
				"select bn_image1 from banner where bn_id=".intval($_POST['formid'])
			);
			$cekimg1 = mysqli_num_rows($cekimg);
			if ($cekimg1 > 0){
				$cekimg2 = mysqli_fetch_array($cekimg);
				if ($cekimg2['bn_image1']!='' && file_exists("../images/banner/".$cekimg2['bn_image1'])){
					unlink("../images/banner/".$cekimg2['bn_image1']);
				}
			}
			$bn_image1 = str_replace($strrep, '', $_POST['formid'].'m-'.$_FILES['bn_image1']['name']);
			move_uploaded_file(
				$_FILES['bn_image1']['tmp_name'],'../images/banner/'.$bn_image1
			);
			$addsql .= ",bn_image1='".mysqli_real_escape_string( $sqlcon,$bn_image1 )."'";
				
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['bn_image1']['size']);
		
			$result['bn_image1']='<img src="'.$path.'/images/banner/'.$bn_image1.'" style="height:50px;"><br />' . $fz;
		}
				
		mysqli_query( $sqlcon,
		"update banner set ".
			"bn_title='".mysqli_real_escape_string( $sqlcon,$bn_title )."',".
			"bn_subtitle='".mysqli_real_escape_string( $sqlcon,$bn_subtitle )."',".
			"bn_link='".mysqli_real_escape_string( $sqlcon,$bn_link )."',".
			"bn_from='".$bn_from."',".
			"bn_to='".$bn_to."',".
			"bn_status=".$bn_status.",".
			"bn_draft=".$bn_draft.",".
			"bn_sequence=".$bn_sequence.",".
			"bn_update='".date("Y-m-d H:i:s")."',".
			"bn_updateby=".$cokidusr.$addsql." ".
		"where bn_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>