<?php
$filter = array ();
$sinfil = "";
if (isset ( $_COOKIE ['filter'] )) {
	setcookie ( "filter", $_COOKIE ['filter'], time () + 7200 );
	$filter = json_decode ( $_COOKIE ['filter'], true );
}

if (isset ( $filter ['BNT'] ['isianteks'] )) {
	$isianteks = $filter['BNT']['isianteks'];
}else{
	$isianteks = '';
}
if ($isianteks != '') {
	if ($sinfil != ''){$sinfil .= " and ";}
	$sinfil .= "lower(bt_title) like '%" . mysqli_real_escape_string( $sqlcon,strtolower($isianteks) ) . "%' ".
		"or lower(bt_anchor) like '%" . mysqli_real_escape_string( $sqlcon,strtolower($isianteks) ) . "%' ";
}
if ($sinfil != ''){
	$sinfil = ' where ' . $sinfil;
}

if (isset ( $filter ['BNT'] ['batasanjumlah'] )) {
	$batasanjumlah = intval( $filter['BNT']['batasanjumlah'] );
}else{
	$batasanjumlah = 200;
}
$sinlimit = ' limit '.$batasanjumlah;

if (! isset ( $filter ['BNT'] )) {
	$filter ['BNT'] = array (
			'isianteks'		=> $isianteks,
			'batasanjumlah'	=> $batasanjumlah
	);
	setcookie ( "filter", json_encode ( $filter ), time () + 7200 );
}
unset($isianteks,$filter);
?>