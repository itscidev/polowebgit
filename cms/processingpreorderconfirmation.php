<?php
function resetPreOrder($sqlcon,$pc_po_id,$pc_status){
	$postat = mysqli_query($sqlcon,"select po_status from preorder where po_id=".$pc_po_id." and po_status<4");
	$postat1 = 0;
	if ($postat){$postat1 = mysqli_num_rows($postat);}
	if ($postat1 > 0){
		$postat2 = mysqli_fetch_assoc($postat);
		$set = '';
		if ($pc_status==1 && $postat2['po_status']<3){
			$set = 'po_status=3';
		}elseif ($pc_status==0 && $postat2['po_status']!=2){//not In Line
			$set = 'po_status=2';
		}
		if ($set != ''){
			mysqli_query($sqlcon,'update preorder set '.$set.' where po_id='.$pc_po_id);
		}
	}
}
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/preorderconfirmation.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	$cekimg = mysqli_query( $sqlcon,"select pc_bukti from preorder_confirmation where pc_id=".$_POST['delid'] );
	$cekimg1 = mysqli_num_rows($cekimg);
	if ($cekimg1 > 0){
		$cekimg2 = mysqli_fetch_array($cekimg);
		if (isset($_POST['delete']) && $_POST['delete']==1){
			if ($cekimg2['pc_bukti']!='' && file_exists("../preorder/bukti/".$cekimg2['pc_bukti'])){
				unlink("../preorder/bukti/".$cekimg2['pc_bukti']);
			}
			mysqli_query( $sqlcon,"delete from preorder_confirmation where pc_id=".$_POST['delid'] );
			$result['Status']='Delete Success!';
		}else{
			$result['Error']='Confirmation box not checked!';
		}
	}
}
if (isset($_POST['formid'])){
	if (isset($_POST['pc_nama'])){$pc_nama=$_POST['pc_nama'];}else{$pc_nama='';}
	if (isset($_POST['pc_email'])){$pc_email=$_POST['pc_email'];}else{$pc_email='';}
	if (isset($_POST['pc_hp'])){$pc_hp=$_POST['pc_hp'];}else{$pc_hp='';}
	if (isset($_POST['pc_status'])){$pc_status=intval($_POST['pc_status']);}else{$pc_status=0;}
	if (isset($_POST['pc_po_id'])){$pc_po_id=intval($_POST['pc_po_id']);}else{$pc_po_id=0;}
	
	if ($pc_nama==''){$result['Error']['pc_nama']=1;}
	if ($pc_email==''){
		$result['Error']['pc_email']=1;
	}elseif (!filter_var($pc_email, FILTER_VALIDATE_EMAIL)){
		$result['Error']['pc_email']=1;
	}
	if (isset($_FILES['pc_bukti']['type']) && strtolower($type)!='image/jpeg' && strtolower($type)!='image/png'){
		$result['Error']['pc_bukti']=1;
	}
	
	$strrep = array("'",'"',' ','/','\\','#');
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into preorder_confirmation (".
			"pc_status,pc_nama,".
			"pc_email,".
			"pc_hp,pc_bukti,pc_po_id,".
			"pc_create,pc_updateby,pc_update".
		") values (".
			$pc_status.",'".mysqli_real_escape_string( $sqlcon,$pc_nama )."',".
			"'".mysqli_real_escape_string( $sqlcon,$pc_email )."',".
			"'".mysqli_real_escape_string( $sqlcon,$pc_hp )."','',".$pc_po_id.",".
			"'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);
		$insertid = mysqli_insert_id( $sqlcon );
		if (isset($_FILES['pc_bukti']['name']) && $_FILES['pc_bukti']['name']!=''){
			$pc_bukti = str_replace($strrep,'', $insertid.'-'.$_FILES['pc_bukti']['name']);
			move_uploaded_file(
				$_FILES['pc_bukti']['tmp_name'],'../preorder/bukti/'.$pc_bukti
			);
			
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['pc_bukti']['size']);
			
			$result['pc_bukti']='<img src="'.$path.'/preorder/bukti/'.$pc_bukti.'" style="height:50px;"><br />' . $fz;
			mysqli_query( $sqlcon,
				"update preorder_confirmation set pc_bukti='".mysqli_real_escape_string( $sqlcon,$pc_bukti )."' ".
				"where pc_id=".$insertid
			);
		}else{
			$pc_bukti = '';
			$result['pc_bukti']='<img src="'.$path.'/images/default.png" style="height:50px;">';
		}

		//cek & change preorder status
		if ($pc_po_id != 0){
			resetPreOrder($sqlcon,$pc_po_id,$pc_status);
		}

		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		$addsql = '';
		if (isset($_FILES['pc_bukti']['name']) && $_FILES['pc_bukti']['name']!=''){
			$cekimg = mysqli_query( $sqlcon,
				"select pc_bukti from preorder_confirmation where pc_id=".intval($_POST['formid'])
			);
			$cekimg1 = mysqli_num_rows($cekimg);
			if ($cekimg1 > 0){
				$cekimg2 = mysqli_fetch_array($cekimg);
				if ($cekimg2['pc_bukti']!='' && file_exists("../preorder/bukti/".$cekimg2['pc_bukti'])){
					unlink("../preorder/bukti/".$cekimg2['pc_bukti']);
				}
			}
			$pc_bukti = str_replace($strrep, '', $_POST['formid'].'-'.$_FILES['pc_bukti']['name']);
			move_uploaded_file(
				$_FILES['pc_bukti']['tmp_name'],'../preorder/bukti/'.$pc_bukti
			);
			$addsql .= ",pc_bukti='".mysqli_real_escape_string( $sqlcon,$pc_bukti )."'";
			
			include_once 'configuration/function.php';
			$fz = formatBytes($_FILES['pc_bukti']['size']);
				
			$result['pc_bukti']='<img src="'.$path.'/preorder/bukti/'.$pc_bukti.'" style="height:50px;"><br />' . $fz;
		}
		mysqli_query( $sqlcon,
		"update preorder_confirmation set ".
			"pc_nama='".mysqli_real_escape_string( $sqlcon,$pc_nama )."',".
			"pc_email='".mysqli_real_escape_string( $sqlcon,$pc_email )."',".
			"pc_hp='".mysqli_real_escape_string( $sqlcon,$pc_hp )."',".
			"pc_po_id=".$pc_po_id .",".
			"pc_status=".$pc_status.",".
			"pc_update='".date("Y-m-d H:i:s")."',".
			"pc_updateby=".$cokidusr.$addsql." ".
		"where pc_id=".intval($_POST['formid'])
		);

		//cek & change preorder status
		if ($pc_po_id != 0){
			resetPreOrder($sqlcon,$pc_po_id,$pc_status);
		}
		
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>