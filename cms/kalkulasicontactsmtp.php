<?php
if (substr_count ( $_SERVER ['SCRIPT_NAME'], '/contactsmtp.php' ) == 0) {
	header ( 'location:index.html' );
	exit ();
}

$list = $notifview = '';
$jumdt =  mysqli_num_rows ( mysqli_query ( $sqlcon,"select * from mail_smtp" ) ) ;
if ($jumdt > 0) {
	$getdt = mysqli_query ( $sqlcon,
		"select * from mail_smtp ".
		"order by ms_purpose asc,ms_sendername asc,ms_sendermail asc"
	);
	while ( $getdt1 = mysqli_fetch_array ( $getdt ) ) {
		$list .= "<tr id=\"tr" . $getdt1 ['ms_id'] . "\">" .  
		"<td id=\"fms_purpose" . $getdt1 ['ms_id'] . "\">" . $getdt1 ['ms_purpose'] . "</td>" . 
		"<td id=\"fms_sendername" . $getdt1 ['ms_id'] . "\">" . $getdt1 ['ms_sendername'] . "</td>" .
		"<td id=\"fms_sendermail" . $getdt1 ['ms_id'] . "\">" . $getdt1['ms_sendermail'] . "</td>" .
		"<td id=\"fms_password" . $getdt1 ['ms_id'] . "\">" . $getdt1['ms_password'] . "</td>" .
		"<td id=\"fms_host" . $getdt1 ['ms_id'] . "\">" . $getdt1['ms_host'] . "</td>" .
		"<td id=\"fms_secure" . $getdt1 ['ms_id'] . "\">" . $getdt1['ms_secure'] . "</td>" .
		"<td id=\"fms_port" . $getdt1 ['ms_id'] . "\">" . $getdt1['ms_port'] . "</td>" .
		"<td style=\"font-size:17px;\">" . 
			"<a href=\"#windata\" id=\"edit".$getdt1['ms_id']."\" class=\"fancybox\" ".
			"title=\"Edit Data ".htmlentities($getdt1['ms_sendermail'])."\" ".
			"onClick=\"prepareedit('".$getdt1['ms_id']."');\"><i class=\"fa fa-edit\"></i></a>".
		"</td>" . 
		"</tr>";
	}
}
if ($list == '') {
	$list = "<tr><td colspan=\"8\">No Data!</td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td>".
	"<td class=\"hide\"></td><td class=\"hide\"></td><td class=\"hide\"></td></tr>";
}
?>