<?php
include_once("configuration/connect.php");
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/pagesoverwrite.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from pages where pg_id=".$_POST['delid'] );
		$result['Status']='Delete Success!';		
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid']) && $_POST['formid']!=''){
	if (isset($_POST['pg_name'])){$pg_name=$_POST['pg_name'];}else{$pg_name='';}
	if (isset($_POST['pg_title'])){$pg_title=$_POST['pg_title'];}else{$pg_title='';}
	if (isset($_POST['pg_keyword'])){$pg_keyword=$_POST['pg_keyword'];}else{$pg_keyword='';}
	if (isset($_POST['pg_desc'])){$pg_desc=$_POST['pg_desc'];}else{$pg_desc='';}
	if (isset($_POST['pg_status'])){$pg_status=intval($_POST['pg_status']);}else{$pg_status=0;}
	if (isset($_POST['pg_draft'])){$pg_draft=intval($_POST['pg_draft']);}else{$pg_draft=0;}
	
	if ($pg_name == ''){
		$result['Error']['pg_name'] = 1;
	}else{
		if (isset($_POST['formid']) && $_POST['formid']=='new'){
			$addex = '';
		}else{
			$addex = ' and pg_id!='.$_POST['formid'];
		}
		$cekex = mysqli_num_rows(mysqli_query($sqlcon,
			"select pg_id from pages where pg_name='".mysqli_real_escape_string($sqlcon,$pg_name)."'".$addex
		));
		if ($cekex > 0){
			$result['Error']['pg_name'] = 1;
		}
	}
}
if (isset($_POST['formid']) && $_POST['formid']=='new' && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$strquery = "insert into pages (".
		"pg_name,".
		"pg_title,".
		"pg_keyword,".
		"pg_desc,".
		"pg_status,pg_draft,".
		"pg_createby,pg_create,pg_updateby,pg_update".
	") values (".
		"'".mysqli_real_escape_string( $sqlcon,$pg_name )."',".
		"'".mysqli_real_escape_string( $sqlcon,$pg_title )."',".
		"'".mysqli_real_escape_string( $sqlcon,$pg_keyword )."',".
		"'".mysqli_real_escape_string( $sqlcon,$pg_desc )."',".
		$pg_status.",".$pg_draft.",".
		$cokidusr.",'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
	")";
	//$result['Qry']=$strquery;
	mysqli_query( $sqlcon,$strquery);
	$result['Status']='New';
}if (isset($_POST['formid']) && $_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
	include 'configuration/cookie.php';
	$qryupd = "update pages set ".
	"pg_name='".mysqli_real_escape_string( $sqlcon,$pg_name )."',".
	"pg_title='".mysqli_real_escape_string( $sqlcon,$pg_title )."',".
	"pg_keyword='".mysqli_real_escape_string( $sqlcon,$pg_keyword )."',".
	"pg_desc='".mysqli_real_escape_string( $sqlcon,$pg_desc )."',".
	"pg_status=".$pg_status.",pg_draft=".$pg_draft.",".
	"pg_updateby=".$cokidusr.",pg_update='".date("Y-m-d H:i:s")."' ".
	"where pg_id=".$_POST['formid'];
	mysqli_query( $sqlcon,$qryupd );
	$result['Status']='Update';
//	$result['QryUpd']=$qryupd;
}
if ($sqlcon){mysqli_close($sqlcon);}
/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>