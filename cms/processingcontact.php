<?php
$result=array();
if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Please re-login!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/contact.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (!isset($result['Error'])){
	
	include_once("configuration/connect.php");
	include 'configuration/cookie.php';
	
	mysqli_query( $sqlcon,
	"update contact set ".
		"co_address='".mysqli_real_escape_string( $sqlcon,$_POST['co_address'] )."',".
		"co_telephone='".mysqli_real_escape_string( $sqlcon,$_POST['co_telephone'] )."',".
		"co_fax='".mysqli_real_escape_string( $sqlcon,$_POST['co_fax'] )."',".
		"co_latitude='".mysqli_real_escape_string( $sqlcon,$_POST['co_latitude'] )."',".
		"co_longitude='".mysqli_real_escape_string( $sqlcon,$_POST['co_longitude'] )."',".
		"co_los_id=".intval( $_POST['co_los_id'] ).",".
		"co_linkfb='".mysqli_real_escape_string( $sqlcon,$_POST['co_linkfb'] )."',".
		"co_linkyoutube='".mysqli_real_escape_string( $sqlcon,$_POST['co_linkyoutube'] )."',".
		"co_linkinstagram='".mysqli_real_escape_string( $sqlcon,$_POST['co_linkinstagram'] )."',".
		"co_update='".date("Y-m-d H:i:s")."',".
		"co_updateby=".$cokidusr." ".
	"where co_id=1");
	
	if ($sqlcon){mysqli_close($sqlcon);}
	$result['Status']='Update';
}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>