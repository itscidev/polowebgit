<?php
include_once("configuration/connect.php");
$result=array();
//special case for path
$HTTP_HOST = array(
	'dev.poloindonesia.com','www.dev.poloindonesia.com',
	'poloindonesia.com','www.poloindonesia.com'
);
$path = '..';
if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],$HTTP_HOST)){
	$path = 'http://'.$_SERVER['HTTP_HOST'];
}

if (!isset($_COOKIE['login']) || $_COOKIE['login']==''){
	$result['Error']='Login session expired!';
}
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/preorder.html') == 0){
	$result['Error']='Ilegal access detected!';
}
if (isset($_POST['delid']) && $_POST['delid']>0 && !isset($result['Error'])){
	if (isset($_POST['delete']) && $_POST['delete']==1){
		mysqli_query( $sqlcon,"delete from preorder where po_id=".$_POST['delid'] );
		$result['Status'] = 'Delete Success!';
	}else{
		$result['Error']='Confirmation box not checked!';
	}
}
if (isset($_POST['formid']) && !isset($result['Error'])){
	if (isset($_POST['po_nama'])){$po_nama=$_POST['po_nama'];}else{$po_nama='';}
	if (isset($_POST['po_email'])){$po_email=$_POST['po_email'];}else{$po_email='';}
	if (isset($_POST['po_hp'])){$po_hp=$_POST['po_hp'];}else{$po_hp='';}
	if (isset($_POST['po_alamat'])){$po_alamat=$_POST['po_alamat'];}else{$po_alamat='';}
	if (isset($_POST['po_keterangan'])){$po_keterangan=$_POST['po_keterangan'];}else{$po_keterangan='';}
	if (isset($_POST['po_status'])){$po_status=intval($_POST['po_status']);}else{$po_status=0;}
	
	if ($po_nama==''){$result['Error']['po_nama']=1;}
	if ($po_email==''){
		$result['Error']['po_email']=1;
	}elseif (!filter_var($po_email, FILTER_VALIDATE_EMAIL)){
		$result['Error']['po_email']=1;
	}
	
	if ($_POST['formid']=='new' && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"insert into preorder (".
			"po_status,po_nama,".
			"po_email,po_alamat,".
			"po_hp,po_keterangan,".
			"po_create,po_updateby,po_update".
		") values (".
			$po_status.",'".mysqli_real_escape_string( $sqlcon,$po_nama )."',".
			"'".mysqli_real_escape_string( $sqlcon,$po_email )."','".mysqli_real_escape_string( $sqlcon,$po_alamat )."',".
			"'".mysqli_real_escape_string( $sqlcon,$po_hp )."','".mysqli_real_escape_string( $sqlcon,$po_keterangan )."',".
			"'".date("Y-m-d H:i:s")."',".$cokidusr.",'".date("Y-m-d H:i:s")."'".
		")"
		);
		$result['Status']='New';
	}elseif ($_POST['formid']!='' && $_POST['formid']>0 && !isset($result['Error'])){
		include 'configuration/cookie.php';
		mysqli_query( $sqlcon,
		"update preorder set ".
			"po_nama='".mysqli_real_escape_string( $sqlcon,$po_nama )."',".
			"po_email='".mysqli_real_escape_string( $sqlcon,$po_email )."',".
			"po_hp='".mysqli_real_escape_string( $sqlcon,$po_hp )."',".
			"po_alamat='".mysqli_real_escape_string( $sqlcon,$po_alamat )."',".
			"po_keterangan='".mysqli_real_escape_string( $sqlcon,$po_keterangan )."',".
			"po_status=".$po_status.",".
			"po_update='".date("Y-m-d H:i:s")."',".
			"po_updateby=".$cokidusr." ".
		"where po_id=".intval($_POST['formid'])
		);
		$result['Status']='Update';
	}
}
if ($sqlcon){mysqli_close($sqlcon);}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>