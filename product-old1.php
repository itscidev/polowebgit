<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $heads;?>
	
	<!-- Meta-Tags -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- //Meta-Tags -->
	
	<!-- Custom-StyleSheet-Links -->
	<link rel="shortcut icon" type="image/png" href="<?php echo $path;?>images/favicon.png" >
	<!-- Bootstrap-CSS -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/bootstrap.css">
	<!-- Index-Page-CSS -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/mystyle.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style1.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/product.css">
	
	<!-- Fonts -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fonts.css">
	<!-- //Fonts -->
	
	<!-- Font-Awesome-File-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/font-awesome.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>fonts/fontawesome-webfont.ttf">
	<!-- //Font-Awesome-File-Links -->
	
	<!-- Swiper -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/swiper.min.css">
	<!-- //Swiper -->
	
	<!-- Select2 -->
    <link rel="stylesheet" href="<?php echo $path;?>cms/plugins/select2/select2.min.css">
	
</head>
<!-- //Head -->
<!-- Body -->
<body>

<!-- Header -->
<?php include_once 'header.php';?>
<!-- //Header -->
	
<!-- content -->
<div class="container">
<div class="row rowheader">
	<div class="col-md-3 col-sm-12 hidefilter">
		<i class="fa fa-filter" aria-hidden="true"></i><span class="hidestring">Hide</span><span> Filters - 4449 items</span>
	</div>
	<div class="col-md-2 col-sm-4">
		<select name="sort" class="select2">
			<option></option>
			<option value="PLH">Price Low to High</option>
			<option value="PHL">Price High to Low</option>
			<option value="MP">Most Popular</option>
			<option value="BS">Best Seller</option>
			<option value="N">Newest</option>
		</select>
	</div>
	<div class="col-md-4 col-sm-4 titlecategory">
		<h4>Enthecwear</h4>
	</div>
	<div class="col-md-3 col-sm-4 pagingsection" align="right">
		<i class="fa fa-chevron-left"></i><div>1 / 3</div><i class="fa fa-chevron-right"></i>
	</div>
</div>
<div class="women_main">
	<!-- start sidebar -->
	<div class="col-sm-3 col-xs-12 s-d">
	  <div class="w_sidebar">
		<section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Categories<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<div class="col col-4">
					<label class="checkbox"><input type="checkbox" name="checkbox" checked="checked"><i></i>Dresses</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Polo Shirt</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Sweaters</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Hats</label>
					<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>Syall</label>
				</div>
			</div>
		</section>
		<section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Size<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<div class="col col-4">
					<label class="checkbox"><input type="checkbox" name="checkbox" checked="checked"><i></i>All Size</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>S</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>M</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>L</label>
					<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>XL</label>
				</div>
			</div>
		</section>
		<section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Fit<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<div class="col col-4">
					<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Regular Fit</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Custom Fit</label>
				</div>
			</div>
		</section>
		<section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Colour<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<ul class="w_nav2">
					<li><a class="color1" href="#"></a></li>
					<li><a class="color2" href="#"></a></li>
					<li><a class="color3" href="#"></a></li>
					<li><a class="color4" href="#"></a></li>
					<li><a class="color5" href="#"></a></li>
					<li><a class="color6" href="#"></a></li>
					<li><a class="color7" href="#"></a></li>
					<li><a class="color8" href="#"></a></li>
					<li><a class="color9" href="#"></a></li>
					<li><a class="color10" href="#"></a></li>
					<li><a class="color12" href="#"></a></li>
					<li><a class="color13" href="#"></a></li>
					<li><a class="color14" href="#"></a></li>
					<li><a class="color15" href="#"></a></li>
					<li><a class="color5" href="#"></a></li>
					<li><a class="color6" href="#"></a></li>
					<li><a class="color7" href="#"></a></li>
					<li><a class="color8" href="#"></a></li>
					<li><a class="color9" href="#"></a></li>
					<li><a class="color10" href="#"></a></li>
				</ul>
			</div>
		</section>
		<!-- <section class="sky-form">
			<h4>discount</h4>
			<div class="row1 scroll-pane">
				<div class="col col-4">
					<label class="radio"><input type="radio" name="radio" checked=""><i></i>60 % and above</label>
					<label class="radio"><input type="radio" name="radio"><i></i>50 % and above</label>
					<label class="radio"><input type="radio" name="radio"><i></i>40 % and above</label>
				</div>
				<div class="col col-4">
					<label class="radio"><input type="radio" name="radio"><i></i>30 % and above</label>
					<label class="radio"><input type="radio" name="radio"><i></i>20 % and above</label>
					<label class="radio"><input type="radio" name="radio"><i></i>10 % and above</label>
				</div>
			</div>						
		</section> -->
	</div>
   </div><!-- 
--><div class="col-sm-9 col-xs-12 w_content">
		<!-- grids_of -->
		<div class="col-sm-4 col-xs-6">
		<!-- <div class="grids_of">
			<div class="grid1_of"> -->
				<div class="content_box">
					<a href="<?php echo $urlcat.rawurlencode('Flag Edition Brasil');?>">
						<img src="<?php echo $path;?>images/productitem/FlagEdition-Brasil.jpg" class="img-responsive" alt="Flag Edition Brasil"/>
					</a>
					<h4>
						<a href="<?php echo $urlcat.rawurlencode('Flag Edition Brasil');?>"> Flag Edition Brasil</a>
						<br /><small>Custom Fit</small>
					</h4>
					<div class="grid_1 simpleCart_shelfItem">
						<div class="item_add"><span class="item_price"><h6>IDR 899.000,-</h6></span></div>
					</div>
				</div>
			</div><!-- 
			 <div class="grid1_of">
			 --><div class="col-sm-4 col-xs-6">
				<div class="content_box">
					<a href="<?php echo $urlcat.rawurlencode('Flag Edition England');?>">
						<img src="<?php echo $path;?>images/productitem/FlagEdition-England.jpg" class="img-responsive" alt="Flag Edition England"/>
					</a>
					<h4>
						<a href="<?php echo $urlcat.rawurlencode('Flag Edition England')?>"> Flag Edition England</a>
						<br /><small>Regular Fit</small>
					</h4>
					<div class="grid_1 simpleCart_shelfItem">
						<div class="item_add"><span class="item_price"><h6>IDR 899.000,-</h6></span></div>
					</div>
				</div>
			</div><!-- 
			 <div class="grid1_of">
			 --><div class="col-sm-4 col-xs-6">
				<div class="content_box">
					<a href="<?php echo $urlcat.rawurlencode('Flag Edition France')?>">
						<img src="<?php echo $path;?>images/productitem/FlagEdition-France.jpg" class="img-responsive" alt="Flag Edition France"/>
					</a>
					<h4>
						<a href="<?php echo $urlcat.rawurlencode('Flag Edition France')?>"> Flag Edition France</a>
						<br /><small>Slim Fit</small>
					</h4>
					<div class="grid_1 simpleCart_shelfItem">
						<div class="item_add"><span class="item_price"><h6>IDR 899.000,-</h6></span></div>
					</div>
				</div>
			</div><!-- 
			<div class="clearfix"></div>
		</div>
		<div class="grids_of">
			 <div class="grid1_of">
			 --><div class="col-sm-4 col-xs-6">
				<div class="content_box">
					<a href="<?php echo $path.'details/'.$_GET['id'].'/Wangky'?>">
						<img src="<?php echo $path;?>images/productitem/WANGKY-yellow.jpg" class="img-responsive" alt="Wangky"/>
						<div class="labeldiscount">
							<img alt="special promo discount" src="<?php echo $path;?>images/icon/label-discount.png">
							<div class="discnumber">29</div>
						</div>
					</a>
					<h4 class="withlabeldisc">
						<a href="<?php echo $path.'details/'.$_GET['id'].'/Wangky'?>"> Wangky</a>
						<br /><small>Regular Fit</small>
					</h4>
					<div class="grid_1 simpleCart_shelfItem withlabeldisc">
						<div class="item_add"><span class="item_price"><h6>IDR 899.000,-</h6></span></div>
					</div>
					<div class="grid_1 withlabeldisc">
						<img class="colortype" img-src="<?php echo $path;?>images/productitem/WANGKY-yellow.jpg" alt="WANGKY yellow" 
						src="<?php echo $path;?>images/productcolor/c-yellow-blue.jpg">
						<img class="colortype" img-src="<?php echo $path;?>images/productitem/WANGKY-white.jpg" alt="WANGKY white" 
						src="<?php echo $path;?>images/productcolor/c-grey-purple.jpg">
						<img class="colortype" img-src="<?php echo $path;?>images/productitem/WANGKY-toscamuda.jpg" alt="WANGKY toscamuda" 
						src="<?php echo $path;?>images/productcolor/c-blue-grey.jpg">
						<img class="colortype" img-src="<?php echo $path;?>images/productitem/WANGKY-orange.jpg" alt="WANGKY orange" 
						src="<?php echo $path;?>images/productcolor/c-orange-blue.jpg">
						<img class="colortype" img-src="<?php echo $path;?>images/productitem/WANGKY-merahstg.jpg" alt="WANGKY merah" 
						src="<?php echo $path;?>images/productcolor/c-pink-black.jpg">
						<img class="colortype" img-src="<?php echo $path;?>images/productitem/WANGKY-birumuda.jpg" alt="WANGKY birunavy" 
						src="<?php echo $path;?>images/productcolor/c-blue-purple.jpg">
						<img class="colortype" img-src="<?php echo $path;?>images/productitem/WANGKY-birunavy.jpg" alt="WANGKY birumuda" 
						src="<?php echo $path;?>images/productcolor/c-blue-red.jpg">
						<img class="colortype" img-src="<?php echo $path;?>images/productitem/WANGKY-biru.jpg" alt="WANGKY biru" 
						src="<?php echo $path;?>images/productcolor/c-blue-yellow.jpg">
					</div>
				</div>
			</div>
			
			<div class="clearfix"></div>
		</div>
		<!-- end grids_of -->
		
		
	</div>
	<div class="clearfix"></div>
	
	<!-- end content -->
</div>
</div>

	<?php include_once 'footer.php';?>

<!-- Default-JavaScript -->
<script src="<?php echo $path;?>js/jquery-2.2.3.js"></script>
<!-- Supportive-Modernizr-JavaScript -->
<script src="<?php echo $path;?>js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script src="<?php echo $path;?>js/modernizr.custom.js"></script>
<!-- cart-js -->
<script src="<?php echo $path;?>js/minicart.js"></script>
<!-- Pricing-Popup-Box-JavaScript -->
<script src="<?php echo $path;?>js/jquery.magnific-popup.js" type="text/javascript"></script>
<!-- Model-Slider-JavaScript-Files -->
<!-- <script src="js/jquery.film_roll.js"></script> -->
<!-- Bootstrap-JavaScript -->
<script src="<?php echo $path;?>js/bootstrap.js"></script>
<script src="<?php echo $path;?>js/myscript.js"></script>
<script src="<?php echo $path;?>js/swiper.min.js"></script>
<!-- Select2 -->
<script src="<?php echo $path;?>cms/plugins/select2/select2.full.min.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script type="application/x-javascript"> 
	addEventListener("load", function() { 
		setTimeout(hideURLbar, 0); 
	}, false); 
	function hideURLbar(){ window.scrollTo(0,1); }

	//Initialize Select2 Elements
	$(".select2").select2({
		placeholder: "Sort by :",
		allowClear: true
	}); 

	<!-- //cart-js --> 
	<!-- Shopping-Cart-JavaScript -->
	/* w3l.render();
    w3l.cart.on('w3agile_checkout', function (evt) {
    	var items, len, i;

    	if (this.subtotal() > 0) {
    		items = this.items();

    		for (i = 0, len = items.length; i < len; i++) { 
    		}
    	}
    }); */
	$(document).ready(function(){
		
		$(".colortype").mouseover(function() {
			$(this).closest("div.content_box").find("img.img-responsive").attr("src", $(this).attr("img-src") );
		});

		$("h4.showhidefiltersection").click(function() {
			if ($(this).find("i").hasClass('up')){
				$(this).find("i").removeClass('up');
				$(this).parent().find("div.row1.scroll-pane").removeClass('up');
			}else{
				$(this).find("i").addClass('up');
				$(this).parent().find("div.row1.scroll-pane").addClass('up');
			}
		});

		$(".hidefilter").click(function() {
			if ($(".hidestring").html() == 'Hide'){
				$(".hidestring").html('Show');
				$("div.col-sm-3.s-d").hide('slow');
				$("div.col-sm-9.w_content").addClass('col-sm-12').removeClass('col-sm-9');
			}else{
				$(".hidestring").html('Hide');
				$("div.col-sm-3.s-d").show('slow');
				$("div.col-sm-12.w_content").addClass('col-sm-9').removeClass('col-sm-12');
			}
		});

		/*hide filter on load, khusus mobile*/
		setTimeout(function(){
			if ($(".hideonmobile").css('display')=='none' && $(".hidestring").html()=='Hide'){
				$(".hidefilter").trigger( "click" );
			}
		}, 2000);
		
		<!-- Pricing-Popup-Box-JavaScript -->
		/* $('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		}); */
	});
</script>
</body>
</html>