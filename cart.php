<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Select2 -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>plugin/select2/select2.min.css">
	<!-- //Select2 -->

    <!-- Fancybox simple pop up --> 
	<link type="text/css" media="screen" rel="stylesheet" href="<?php echo $path;?>plugin/fancybox/jquery.fancybox.css?v=2.1.5" />
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/o-cart.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->
	<div class="cartsection">
	<div class="col-sm-7 col-xs-12 detail">
		<h3>
			SHOPPING CART (<span class="cartcount"><?php echo $cartcountp;?></span>)
			<span class="cartsubtotal floatright"><?php echo $cartsubtotalp;?></span>
			<p class="hidden" id="cartSubTotalNum"><?php echo $cartsubtotal;?></p>
			<span class="floatright">&nbsp;|&nbsp;</span>
			<span class="cartestweight floatright"><?php echo $totweightp;?></span>
		</h3>
		<?php echo $citembig;?>
		<!-- <div class="cartitem">
			<div class="cartitemimg">
				<img alt="item1" src="<?php //echo $path;?>images/productitem/WANGKY-toscamuda.jpg">
			</div>
			<div class="cartitemdesc cartpage">
				<p>Wangky - Regular Fit</p>
				<p>COLOR : <img alt="item1" src="<?php //echo $path;?>images/productcolor/c-blue-grey.jpg"></p>
				<p>SIZE : M</p>
				<p class="forprice"><span class="strightout">Rp 400.000</span>Rp 300.000</p>
			</div>
			<div class="cartitemqty cartpage">
				<p>QTY</p>
				<select name="qty1">
					<option value="1" selected="selected">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
				</select>
			</div>
			<div class="cartitemdel cartpage">
				<i class="fa fa-times pointer" onclick="cartdelitem(30);return false;"></i>
			</div>
		</div>
		<div class="cartitem">
			<div class="cartitemimg">
				<img alt="item1" src="<?php //echo $path;?>images/productitem/WANGKY-merahstg.jpg">
			</div>
			<div class="cartitemdesc">
				<p>Wangky - Regular Fit</p>
				<p>COLOR : <img alt="item1" src="<?php //echo $path;?>images/productcolor/c-pink-black.jpg"></p>
				<p>SIZE : M</p>
				<p>QTY : 1</p>
				<p><span class="strightout">Rp 400.000</span>Rp 300.000</p>
				<p><select name="qty1">
					<option value="1" selected="selected">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					</select>
					<i class="fa fa-pencil"></i>
					<i class="fa fa-times"></i>
				</p>
			</div>
		</div>
		<div class="cartitem">
			<div class="cartitemimg">
				<img alt="item1" src="<?php //echo $path;?>images/productitem/WANGKY-yellow.jpg">
			</div>
			<div class="cartitemdesc">
				<p>Wangky - Regular Fit</p>
				<p>COLOR : <img alt="item1" src="<?php //echo $path;?>images/productcolor/c-yellow-blue.jpg"></p>
				<p>SIZE : M</p>
				<p>QTY : 1</p>
				<p><span class="strightout">Rp 400.000</span>Rp 300.000</p>
				<p><select name="qty1">
					<option value="1" selected="selected">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					</select>
					<i class="fa fa-pencil"></i>
					<i class="fa fa-times"></i>
				</p>
			</div>
		</div> -->
	</div>
	<div class="col-sm-5 col-xs-12 checkout">
		<div class="co-promo">
			<div class="co-head">APPLY PROMO CODE<i class="fa fa-angle-down"></i></div>
			<div class="co-promo-result">Code Use : <span><?php echo $codeuse;?></span></div>
			<p class="hidden" id="vchid"><?php echo $vchid;?></p>
		</div>
		<div class="codeform">
			<p>Promo Code</p>
			<input name="promocode" placeholder="ENTER PROMO CODE" value="">
			<div class="promobutton">APPLY</div>
		</div><?php 
if ($memid!=0){?>
		<div class="co-address">
			<div class="co-head">SHIPMENT ADDRESS</div>
			<div class="shipmentinfo">Address Use : <span><?php echo $addressuse;?></span>
				<div><?php echo $addressdetail;?></div> 
			</div>
			<div class="changeaddressbutton"><span>Change Address</span></div><!-- 
			 --><div class="addaddressbutton" onclick="preparenewaddr();"><span>Add New Address</span></div>
			<p class="hidden" id="addresschooseid"><?php echo $addresschooseid;?></p>
		</div>
		<div class="courier">
			<div class="co-head">SHIPPING COURIER</div>
			<div class="courierinfo">Courier Use : <span><?php echo $courier;?><!-- 
			JNE Reg (2-3 Days) Rp 13.000 --></span></div>
			<div class="changecourierbutton"><span>Change Courier</span></div>
			<p class="hidden" id="cartOngkirNum"><?php echo $ongkir;?></p>
			<p class="hidden" id="ongkirchooseid"><?php echo $ongkirchooseid;?></p>
		</div>
		<div class="payment">
			<div class="payment-head">PAYMENT METHOD<i class="fa fa-angle-down"></i></div>
			<div class="payment-result">
				Payment Use : 
				<img src="<?php echo $paymentlogo;?>">
				<span><?php echo $paymentname;?></span>
			</div>
			<p class="hidden" id="paymentchooseid"><?php echo $paymentchooseid;?></p>
		</div>
		<div class="paymentform"><?php echo $paymentlist;?><!-- 
			<p title="Credit Card">
				<img alt="Credit Card" src="<?php echo $path;?>images/paymentlogo/creditcard.jpg">
				Credit Card
			</p>
			<p title="BCA Klikpay">
				<img alt="BCA Klikpay" src="<?php echo $path;?>images/paymentlogo/bcaklikpay.jpg">
				BCA Klikpay
			</p>
			<p title="BCA Internet Banking">
				<img alt="BCA Internet Banking" src="<?php echo $path;?>images/paymentlogo/klikbca.jpg">
				BCA Internet Banking
			</p>
			<p title="OVO">
				<img alt="OVO" src="<?php echo $path;?>images/paymentlogo/ovo.jpg">
				OVO
			</p>
			<p title="Home Credit">
				<img alt="Home Credit" src="<?php echo $path;?>images/paymentlogo/HomeCredit.jpg">
				Home Credit
			</p>
		 --></div><?php 
}?>
		<div class="headbold">TOTAL<span id="cartTotal"><?php echo $totalp;?></span></div>
		<div class="headredbold">DISCOUNT<span id="cartDiscTotal"><?php echo $discountp;?></span></div>
		<p class="hidden" id="cartDiscTotalNum"><?php echo $discount;?></p>
		<div class="headbold">GRAND TOTAL<span id="cartGrandTotal"><?php echo $grandtotalp;?></span></div>
		<div class="co-comment">
			<div class="co-head">ADDITIONAL MESSAGES</div>
			<textarea name="comment" id="comment"></textarea>
		</div>
		<div class="checkoutmsg"><?php echo $checkoutmsg;?></div><?php 
if ($memid!=0){?>
		<div class="paybutton" onclick="lastCheck();">CHECK OUT</div><?php 
}?>
	</div>
	<div class="popupwin popupaddress">
		<i class="fa fa-times popupclose" title="Close"></i>
		<div class="popupcontent">
			<div class="newaddress">
				<h3>Add New Address</h3>
				<form name="addressform" id="addressform" method="post" enctype="multipart/form-data">
				<div class="givespace">
					<label for="addrsaveas">Save as this Name</label>
					<input name="addrsaveas" id="addrsaveas" type="text" placeholder="Enter Save Name">
				</div>
				<div class="col-sm-6 col-xs-12">
					<label for="addrnama">Receiver Name</label>
					<input name="addrnama" id="addrnama" type="text" placeholder="Enter Receiver Name">
				</div>
				<div class="col-sm-6 col-xs-12">
					<label for="addrhp">Phone Number</label>
					<input name="addrhp" id="addrhp" type="text" placeholder="Enter Phone Number">
				</div>
				<div class="col-sm-6 col-xs-12">
					<label for="addrlosid">Town / Sub-District</label>
					<select name="addrlosid" id="addrlosid"><?php echo $optaddrlosid;?><!--  
						<option value="1" selected="selected">DKI Jakarta, Jakarta Barat, Cengkareng</option>
						<option value="2">DKI Jakarta, Jakarta Barat, Grogol</option>
						<option value="3">DKI Jakarta, Jakarta Barat, Kalideres</option>
						<option value="4">DKI Jakarta, Jakarta Barat, Kebon Jeruk</option>
						<option value="5">DKI Jakarta, Jakarta Barat, Kembangan</option>
					 --></select>
				</div>
				<div class="col-sm-6 col-xs-12">
					<label for="addrzip">Zip Code</label>
					<input name="addrzip" id="addrzip" type="text" placeholder="Enter Zip Code">
				</div>
				<div class="col-xs-12">
					<label for="addraddress">Complete Address</label>
					<textarea name="addraddress" id="addraddress" rows="3" placeholder="Enter Complete Address"></textarea>
				</div>
				<div class="col-xs-12 addrmsg"></div>
				<div class="col-sm-6 col-xs-12">
					<div class="addrcancel">CLOSE</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<input name="cartmode" id="cartmode" type="hidden" value="new">
					<input name="addrid" id="addrid" type="hidden" value="">
					<div class="addrsave" onclick="saveAddress(); return false;">SAVE</div>
				</div>
				</form>
			</div>
			<div class="existingaddress">
				<h3>Use this Existing Address</h3>
				<table>
					<tbody><?php echo $existingaddr;?>
					<!-- <tr class="addrrow">
						<td class="addrusethis">Yuna (081704786984)<br />
						PT. Primajaya Pantes Garment, Jl. Semanan Raya No. 27A. 
						Kalideres, Kota Administrasi Jakarta Barat, 11850.</td>
						<td class="addrdelthis"><i class="fa fa-times" title="Delete this address"></i></td>
					</tr>
					<tr class="addrrow">
						<td class="addrusethis">Astrid (081704786984)<br />
						PT. Primajaya Pantes Garment, Jl. Semanan Raya No. 27A. 
						Kalideres, Kota Administrasi Jakarta Barat, 11850.</td>
						<td class="addrdelthis"><i class="fa fa-times" title="Delete this address"></i></td>
					</tr>
					<tr class="addrrow">
						<td class="addrusethis">Rizki (081704786984)<br />
						PT. Primajaya Pantes Garment, Jl. Semanan Raya No. 27A. 
						Kalideres, Kota Administrasi Jakarta Barat, 11850.</td>
						<td class="addrdelthis"><i class="fa fa-times" title="Delete this address"></i></td>
					</tr> -->
					</tbody>
				</table>
				<div class="col-xs-12">
					<div class="addrcancel">CLOSE</div>
				</div>
			</div>
		</div>
	</div>
	<div class="popupwin popupcourier">
		<i class="fa fa-times popupclose" title="Close"></i>
		<div class="popupcontent">
			<h3>Choice of Courier</h3>
			<table>
				<tbody><?php echo $existingcourier;?><!-- 
				<tr class="crrrow">
					<td class="crrusethis">JNE OKE (3-5 Days) Rp 9.000 / 1000 Gr</td>
					<td class="crrestweight">1600 Gr</td>
					<td class="crrcost">Rp 18.000</td>
				</tr>
				<tr class="crrrow">
					<td class="crrusethis">JNE Reg (2-3 Days) Rp 13.000 / 1000 Gr</td>
					<td class="crrestweight">1600 Gr</td>
					<td class="crrcost">Rp 26.000</td>
				</tr>
				<tr class="crrrow">
					<td class="crrusethis">JNE YES (1-2 Days) Rp 23.000 / 1000 Gr</td>
					<td class="crrestweight">1600 Gr</td>
					<td class="crrcost">Rp 46.000</td>
				</tr>
				<tr class="crrrow">
					<td class="crrusethis">J&T Reg (3-5 Days) Rp 10.000 / 1000 Gr</td>
					<td class="crrestweight">1600 Gr</td>
					<td class="crrcost">Rp 20.000</td>
				</tr>
				--></tbody>
			</table>
			<div class="col-xs-12">
				<div class="crrcancel">CLOSE</div>
			</div>
		</div>
	</div>
<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
	<script src="<?php echo $path;?>plugin/select2/select2.min.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
	<script src="<?php echo $path;?>plugin/fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<script src="<?php echo $path;?>js/o-cart.js"></script>
</body><!-- //Body -->
</html>