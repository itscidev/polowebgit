<!DOCTYPE html>
<!--[if lt IE 8 ]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 8)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- Basic Page Needs
   ================================================== -->
   <meta charset="utf-8">
	<title>POLO Indonesia</title>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta http-equiv="cache-control" content="no-cache" />

   <!-- Mobile Specific Metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
    ================================================== -->
   <link type="text/css" media="all" rel="stylesheet" href="css/font-awesome/css/font-awesome.css">
   <link type="text/css" media="all" rel="stylesheet" href="css/mystyle-fix.css">

   <!-- Script
   ================================================== -->
	

   <!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.png" >
	
	<style type="text/css">
	/* div.cf em{
		color: #000000 !important;
	}
	.blink_me {
	  animation: blinker 1s linear infinite;
	}
	
	@keyframes blinker {
	  50% {
	    opacity: 0;
	  }
	} */
	div#counter > span{
		margin-right: 6.3vw;
	}
	</style>
</head>

<body>

	<div id="preloader">      
      <div id="status">
         <img src="images/preloader.gif" height="64" width="64" alt="">
      </div>
   </div>

   <!-- Intro Section
   ================================================== -->
	<div class="content">
		<img src="images/background-cs-fix.jpg" alt="Polo Indonesia Official Site" >
	</div>
	<div id="counter" class="cf" style="color: #fff; margin-top: -27.45vw; margin-left: 12.3vw; font-size: 4.6vw;">
		<span>34</span>
		<span>12</span>
		<span>50</span>
		<span>33</span>
	</div>
	<p id="timeendset" style="display: none;"><?php echo date("Y-m-01",mktime(0,0,0,date("m")+1,date("d"),date("Y")));?></p>
   <!-- footer
   ================================================== -->
   <!-- <footer>

      <div class="row">

         <div class="twelve columns">            

            <ul class="copyright">
               <li>&copy; Copyright 2017 POLO Indonesia</li>
               <li>Design by <a title="POLO Indonesia Team" href="http://www.poloindonesia.com/">POLO Indonesia Team</a></li>          
            </ul>

         </div>          

      </div>

      <div id="go-top"><a class="smoothscroll" title="Back to Top" href="#intro"><i class="icon-up-open"></i></a></div>

   </footer>  --><!-- Footer End-->
	
   <!-- Java Script
   ================================================== -->
   <!-- <script src="falldowneffect/init.js"></script> -->
   <script src="js/jquery-1.10.2.min.js"></script>
   <script src="js/jquery.countdown.js"></script>
   <script src="js/init1.js"></script>
   <script src="js/myscript.js"></script>
</body>

</html>