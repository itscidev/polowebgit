<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/o-about.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<div class="col-xs-12 aboutdiv">
		<img src="<?php echo $path;?>images/auxiliary/about.jpg" title="About Polo Indonesia" 
		alt="Polo Indonesia" />
		<div class="aboutcontent">
			<h3>About Us</h3>
			<p>Polo Indonesia is a local leader in the design, marketing, and distribution of 
			premium lifestyle products, including apparel, accessories, home furnishings, and other licensed 
			product categories. For five decades, our long-standing reputation and distinctive image have 
			been consistently developed across an expanding number of products, brands, sales channels, 
			and international markets. We believe that our market reach, breadth of product offerings, and 
			multichannel distribution are unique among luxury and apparel companies.</p>
			<h3>Our Products & Brands</h3>
			<p>Since 1967, our distinctive brand image has been consistently developed across an expanding 
			number of products, price tiers, and markets. Our products, which include apparel, accessories, 
			and fragrance collections for men and women, as well as childrenswear and home furnishings, 
			compose one of the world’s most widely recognized families of consumer brands. Reflecting a 
			distinctive American perspective, we have been an innovator in aspirational lifestyle branding 
			and believe that, under the direction of internationally renowned designer Polo Indonesia, we 
			have had a considerable influence on the way people dress and the way that fashion is 
			advertised and celebrated throughout the world. We combine consumer insights with our design, 
			marketing, and imaging skills to offer, along with our licensing alliances, broad lifestyle 
			product collections with a unified vision.</p>
		</div>
	</div>
	
	<?php //echo $swiperbybrand;?>

<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
	<script src="<?php echo $path;?>js/about.js"></script>
</body><!-- //Body -->
</html>