<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $heads;?>
	
	<!-- Meta-Tags -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- //Meta-Tags -->
	
	<!-- Custom-StyleSheet-Links -->
	<link rel="shortcut icon" type="image/png" href="<?php echo $path;?>images/favicon.png" >
	<!-- Bootstrap-CSS -->	   
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/bootstrap.css">
	<!-- Index-Page-CSS -->	   
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style.css">
	<!-- Header-Slider-CSS --> 
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fluid_dg.css" id="fluid_dg-css">
		
	<!-- Fonts -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fonts.css">
	<!-- //Fonts -->
	
	<!-- Font-Awesome-File-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/font-awesome.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>fonts/fontawesome-webfont.ttf">
	<!-- //Font-Awesome-File-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<!-- Banner -->
	<div class="agileheader-banner">
		<img src="<?php echo $path;?>images/banner.jpg" alt="Groovy Apparel">
	</div>
	<!-- //Banner -->
		
	<!-- About-Us -->
	<div class="wthreeabout">
		<div class="about-us-grids">
			<div class="col-md-6 about-us-grid about-us-images">
				<div class="col-md-6 about-us-image about-us-image1">
					<img src="<?php echo $path;?>images/about-image-1.jpg" alt="Groovy Apparel">
				</div>
				<div class="col-md-6 about-us-image about-us-image2">
					<img src="<?php echo $path;?>images/about-image-2.jpg" alt="Groovy Apparel">
				</div>
				<div class="col-md-6 about-us-image about-us-image3">
					<img src="<?php echo $path;?>images/about-image-3.jpg" alt="Groovy Apparel">
				</div>
				<div class="col-md-6 about-us-image about-us-image4">
					<img src="<?php echo $path;?>images/about-image-4.jpg" alt="Groovy Apparel">
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="col-md-6 about-us-grid about-us-text">
				<h1>ABOUT US</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras commodo varius vehicula. Mauris porta metus vitae nulla dignissim egestas. Aliquam sed molestie eros, in pharetra purus. Praesent consequat porta viverra. Praesent gravida dapibus condimentum. Vestibulum vel lacus aliquet, laoreet mi eu, tempor est. Quisque volutpat finibus tristique. Duis at nibh eget nulla pretium pretium. Quisque arcu sem, dignissim eu egestas quis, vulputate non mauris. Nam varius, odio vel interdum interdum, lectus ante imperdiet odio, id consectetur mauris quam et lectus. Ut auctor nec metus egestas auctor. Nullam congue ornare dictum. Sed et volutpat lectus. Integer vehicula pharetra ligula, in vehicula magna vehicula eu. Mauris at tortor vel eros faucibus feugiat. In hac habitasse platea dictumst. Phasellus molestie pretium mi id finibus. Nam et purus enim. Morbi nec fermentum enim. Suspendisse potenti. Sed tincidunt augue in pretium lobortis. Donec laoreet nunc a nisi vulputate pulvinar. Morbi aliquam, mi ultricies efficitur feugiat, risus magna id consectetur mauris quam et lectus.</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //About-Us -->



	<!-- Be-Yourself -->
	<div class="aitsyourself">
		<div class="aitsyourself-grids">

			<div class="col-md-5 aitsyourself-grid aitsyourself-grid1">
				
			</div>

			<div class="col-md-3 aitsyourself-grid aitsyourself-grid2">
				<h3>“The appearance of things changes according to the emotions; and thus we see magic and beauty in them, while the magic and beauty are really in ourselves.”</h3>
				<h4>― Kahlil Gibran</h4>
			</div>

			<div class="col-md-4 aitsyourself-grid aitsyourself-grid3">
				
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
	<!-- //Be-Yourself -->



	<!-- Commercial -->
	<div class="aitscommercial">
			
		<div class="col-md-6 aitscommercial-grid aitscommercial-grid1">
			<h2>DERNIÈRE MODE</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras commodo varius vehicula. Mauris porta metus vitae nulla dignissim egestas. Aliquam sed molestie eros, in pharetra purus. Praesent consequat porta viverra. Praesent gravida dapibus condimentum. Vestibulum vel lacus aliquet, laoreet mi eu, tempor est. Quisque volutpat finibus tristique. Duis at nibh eget nulla pretium pretium. Quisque arcu sem, dignissim eu egestas quis, vulputate non mauris.</p>
			<p class="w3teaseraits">Check out our latest commercial, teaser and behind the scenes film.</p>
			<a class="popup-with-zoom-anim" href="#small-dialog4">WATCH NOW</a>
		</div>

		<div class="col-md-6 aitscommercial-grid aitscommercial-grid2">
			<img src="<?php echo $path;?>images/commercial.jpg" alt="Groovy Apparel">
		</div>
		<div class="clearfix"></div>

		<div id="small-dialog4" class="mfp-hide agileinfo">
			<div class="pop_up">
				<iframe src="https://player.vimeo.com/video/184401441"></iframe>
			</div>
		</div>

	</div>
	<!-- //Commercial -->



	<!-- Accessories -->
	<div class="w3laccessoriesaits">
		<div class="col-md-6 w3laccessoriesaits-grids w3laccessoriesaits-grids1">
			<div class="w3laccessoriesaits-grid w3laccessoriesaits-grid1">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo $path;?>images/accessory-1.jpg" alt="Groovy Apparel">
						<figcaption></figcaption>
					</figure>
				</div>
				<h4>EAU FRAICHE</h4>
				<p>Lorem Ipsum Dolor Site Amet</p>
				<h6><a href="women.html">Shop Now</a></h6>
			</div>
		</div>
		<div class="col-md-6 w3laccessoriesaits-grids w3laccessoriesaits-grids2">
			<div class="w3laccessoriesaits-grid w3laccessoriesaits-grid2">
				<div class="grid">
					<figure class="effect-apollo">
						<img src="<?php echo $path;?>images/accessory-2.jpg" alt="Groovy Apparel">
						<figcaption></figcaption>
					</figure>
				</div>
				<h4>PIGMENT POWDER</h4>
				<p>Lorem Ipsum Dolor Site Amet</p>
				<h6><a href="women.html">Shop Now</a></h6>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- //Accessories -->



	<!-- Shoes -->
	<div class="w3lsshoesaits">
		<div class="w3lsshoesaits-info">
			<h3>SHOES TO DIE FOR</h3>
			<p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.</p>
			<div class="wthreeshop-a">
				<a href="women.html">SHOP WOMEN'S SHOES</a>
			</div>
		</div>
	</div>
	<!-- //Shoes -->



	<!-- New-Fashions -->
	<div class="agilenwew3lsfashions">
		<div class="container">

			<h3>AVANT-GARDE FASHION</h3>
			<h4>Star Studded Launching of the Festival Collection in NYFW in</h4>

			<div class="agilenwew3lsfashions-aitstimer">
				<div class="clock">
					<div class="column days">
						<div class="timer" id="days"></div>
						<div class="text">DAYS</div>
					</div>
					<div class="timer days">:</div>
					<div class="column">
						<div class="timer" id="hours"></div>
						<div class="text">HOURS</div>
					</div>
					<div class="timer">:</div>
					<div class="column">
						<div class="timer" id="minutes"></div>
						<div class="text">MINUTES</div>
					</div>
					<div class="timer">:</div>
					<div class="column">
						<div class="timer" id="seconds"></div>
						<div class="text">SECONDS</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- //New-Fashions -->



	<!-- Newsletter -->
	<div class="w3lsnewsletter" id="w3lsnewsletter">
		<div class="container">
			<div class="w3lsnewsletter-grids">
				<div class="col-md-5 w3lsnewsletter-grid w3lsnewsletter-grid-1 subscribe">
					<p>Subscribe to our Newsletter</p>
				</div>
				<div class="col-md-7 w3lsnewsletter-grid w3lsnewsletter-grid-2 email-form">
					<form action="#" method="post">
						<input class="email" type="email" name="Email" placeholder="Email Address" required="">
						<input type="submit" class="submit" value="SUBSCRIBE">
					</form>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //Newsletter -->

	<?php include_once 'footer.php';?>
	

<!-- Default-JavaScript -->
<script src="<?php echo $path;?>js/jquery-2.2.3.js"></script>
<!-- Supportive-Modernizr-JavaScript -->
<script src="<?php echo $path;?>js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script src="<?php echo $path;?>js/modernizr.custom.js"></script>
<!-- cart-js -->
<script src="<?php echo $path;?>js/minicart.js"></script>
	<script>
        w3l.render();

        w3l.cart.on('w3agile_checkout', function (evt) {
        	var items, len, i;

        	if (this.subtotal() > 0) {
        		items = this.items();

        		for (i = 0, len = items.length; i < len; i++) { 
        		}
        	}
        });
    </script>  
	<!-- //cart-js --> 


		<!-- Dropdown-Menu-JavaScript -->
			<script>
				$(document).ready(function(){
					$(".dropdown").hover(
						function() {
							$('.dropdown-menu', this).stop( true, true ).slideDown("fast");
							$(this).toggleClass('open');
						},
						function() {
							$('.dropdown-menu', this).stop( true, true ).slideUp("fast");
							$(this).toggleClass('open');
						}
					);
				});
			</script>
		<!-- //Dropdown-Menu-JavaScript -->
<!-- Pricing-Popup-Box-JavaScript -->
<script src="<?php echo $path;?>js/jquery.magnific-popup.js" type="text/javascript"></script>
			<script>
				$(document).ready(function() {
					$('.popup-with-zoom-anim').magnificPopup({
						type: 'inline',
						fixedContentPos: false,
						fixedBgPos: true,
						overflowY: 'auto',
						closeBtnInside: true,
						preloader: false,
						midClick: true,
						removalDelay: 300,
						mainClass: 'my-mfp-zoom-in'
					});
				});
			</script>
		<!-- //Popup-Box-JavaScript -->

		<!-- Countdown-Timer-JavaScript-File-Links -->
			<script type="text/javascript" src="<?php echo $path;?>js/moment.js"></script>
			<script type="text/javascript" src="<?php echo $path;?>js/moment-timezone-with-data.js"></script>
			<script type="text/javascript" src="<?php echo $path;?>js/timer.js"></script>
		<!-- //Countdown-Timer-JavaScript-File-Links -->

<!-- Model-Slider-JavaScript-Files -->
<!-- <script src="js/jquery.film_roll.js"></script> -->
<!-- Bootstrap-JavaScript -->
<script src="<?php echo $path;?>js/bootstrap.js"></script>
			
</body>
<!-- //Body -->
</html>