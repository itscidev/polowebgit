<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
//path defined
if (isset($_SERVER['HTTP_HOST']) && (
	$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
	|| $_SERVER['HTTP_HOST']=='172.16.1.19'
)){
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/';
	$pathcook = '/poloweb/';
}else{
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/';
	$pathcook = '/';
}
$cartmode = array('cartdel','cartadd','cartupd','addrdel','addradd','addrupd','lastcheck','courierupd');
$result = array();
include_once 'cms/configuration/connect.php';
include_once 'cms/model/memberaddress.php';
include_once 'cms/model/lokasistep.php';
include_once 'cms/model/productvariety.php';
include_once 'cms/model/productimage.php';
include_once 'cms/model/ongkir.php';
include_once 'cms/model/ekspedisi.php';
include_once 'cms/controller/rajaongkir.php';
include_once 'cms/model/ekspedisitype.php';
include_once 'cms/model/cart.php';
include_once 'cms/model/paymenttype.php';

/*if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/cart')==0){
	$result['Error']='Ilegal access detected!';
}else*/if (!isset($_POST['cartmode']) || (isset($_POST['cartmode']) && !in_array($_POST['cartmode'],$cartmode))){
	$result['Error']='No action needed!';
}elseif (!isset($_COOKIE['loginmem']) && $_POST['cartmode']!='cartdel' && $_POST['cartmode']!='cartadd' 
&& $_POST['cartmode']!='cartupd'){
	$result['Error']='Login session expired!';
}elseif ((
		$_POST['cartmode']=='cartdel' || $_POST['cartmode']=='cartadd' || $_POST['cartmode']=='cartupd'
	) && (
		!isset($_POST['idvariety']) || (isset($_POST['idvariety']) && intval($_POST['idvariety'])==0)
)) {
	$result['Error'] = 'No value detected!';
}elseif ($_POST['cartmode']=='courierupd' && !isset($_POST['courier'])) {
	$result['Error'] = 'Courier not detected!';
}elseif ($_POST['cartmode']=='cartupd' && (!isset($_POST['qty']) || (isset($_POST['qty']) && intval($_POST['qty'])==0))) {
	$result['Error'] = 'Invalid Qty!';
}else{
	// ------------------------- cart section ----------------------------
	if ($_POST['cartmode']=='cartadd' || $_POST['cartmode']=='cartdel' || $_POST['cartmode']=='cartupd'){
		$idvariety = intval($_POST['idvariety']);
	}
	if ($_POST['cartmode']=='courierupd'){
		$courier = intval($_POST['courier']);
	}
	if ($_POST['cartmode']=='cartadd'){
		$getdata = 1;
	}
	if ($_POST['cartmode']=='cartupd'){
		$qtyupd = intval($_POST['qty']);
	}
	$cartsubtotal = 0;
	$cartqty = 0;
	$cartweight = 0;
	$continuetosave = 1;

	if (isset($_COOKIE["cartvst"])){
		$cartvst = json_decode($_COOKIE["cartvst"],true);
		if (isset($idvariety) && isset($cartvst[$idvariety]) && $_POST['cartmode']=='cartdel'){
			unset($cartvst[$idvariety]);
		}
		if (isset($idvariety) && isset($cartvst[$idvariety]) && $_POST['cartmode']=='cartadd'){
			$result['Error'] = 'Product was in cart!';
			$getdata = 0;
		}
		if (count($cartvst)>0){foreach ($cartvst as $idpv => $qty){
			$field = "pv_price,pr_weight,pv_qty";
			$join = "inner join product on pr_id=pv_pr_id";
			$where = "pv_id=".$idpv;
			$cekpv = productvariety_s($sqlcon,$field,$join,$where);
			if (is_array($cekpv) && $cekpv[0]>0){
				$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
				
				if (isset($idvariety) && $_POST['cartmode']=='cartupd' && $idvariety==$idpv){
					if ($qtyupd <= $cekpv1['pv_qty']){
						$qty = $qtyupd;
						$cartvst[$idpv] = $qty;
					}else{
						$result['Error'] = "Max. qty available now is ".$cekpv1['pv_qty']."!";
					}
				}else{
					if ($cekpv1['pv_qty']==0){
						unset($cartvst[$idpv]);
						$continuetosave = 0;
					}elseif ($qty > $cekpv1['pv_qty']){
						$qty = $cekpv1['pv_qty'];
						$cartvst[$idpv] = $qty;
						$continuetosave = 0;
					}
				}
				$cartsubtotal = $cartsubtotal + ($qty*$cekpv1['pv_price']);
				$cartweight = $cartweight + ($qty*$cekpv1['pr_weight']);
				$cartqty = $cartqty + $qty;
			}elseif (is_array($cekpv) && $cekpv[0]==0){
				unset($cartvst[$idpv]);
			}
		}}
	}
		
	if (isset($getdata) && $getdata==1){
		
		$field = "pv_qty,pv_price,pv_status,pv_pr_id,pr_name,pr_weight,pr_status,pv_st_id,st_name,pv_sz_id,sz_name,pv_ci_id,ci_image,cl_name";
		$join = "inner join product on pr_id=pv_pr_id ".
				"left outer join style on st_id=pv_st_id ".
				"left outer join size on sz_id=pv_sz_id ".
				"left outer join color_item on ci_id=pv_ci_id ".
				"left outer join color on cl_id=ci_cl_id";
		$where = "pv_id=".$idvariety;
// 		$result['qry'] = $where;
		$cekpv = productvariety_s($sqlcon,$field,$join,$where);
		if (!is_array($cekpv)){
			$result['Error'] = $cekpv;
		}elseif (is_array($cekpv) && $cekpv[0]==0){
			$result['Error'] = "Variant not exist anymore!";
		}elseif (is_array($cekpv) && $cekpv[0]>0){
			$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
			if ($cekpv1['pr_status'] != 1){
				$result['Error'] = 'Product no longer available!';
			}elseif ($cekpv1['pv_status'] != 1){
				$result['Error'] = 'Variant no longer available!';
			}elseif ($cekpv1['pv_qty'] < 1){
				$result['Error'] = 'Out of stock!';
			}else{
				$result['price'] = number_format($cekpv1['pv_price'],0,',','.');
				$field = "pi_image";
				$where = "pi_pr_id=".$cekpv1['pv_pr_id']." and pi_st_id=".$cekpv1['pv_st_id']." ".
						"and pi_ci_id=".$cekpv1['pv_ci_id']." and pi_status=1";
				$order = "pi_sequence asc";
				$getimg = productimage_s($sqlcon,$field,'',$where,'',$order,1);
				if (is_array($getimg) && $getimg[0]>0){
					$getimg1 = mysqli_fetch_assoc($getimg[1]);
					if ($getimg1['pi_image']!='' && file_exists("images/productitem/".$getimg1['pi_image'])){
						$result['image'] = $getimg1['pi_image'];
					}
				}
				if ($cekpv1['pv_st_id']!=0){
					$stname = ' - ';
					if ($cekpv1['st_name']==''){
						$stname .= 'Undefined!';
					}else{
						$stname .= $cekpv1['st_name'];
					}
				}
				$result['name'] = $cekpv1['pr_name'].$stname;
				if ($cekpv1['pv_ci_id'] != 0){
					$coloritm = 'Undefined!';
					if ($cekpv1['ci_image']!='' && file_exists("images/productcolor/".$cekpv1['ci_image'])){
						$coloritm = $cekpv1['ci_image'];
					}
					$result['color'] = $coloritm;
					$result['colorname'] = $cekpv1['cl_name'];
				}
				if ($cekpv1['pv_sz_id'] != 0){
					if ($cekpv1['sz_name']==''){
						$result['size'] = 'Undefined!';
					}else{
						$result['size'] = $cekpv1['sz_name'];
					}
				}
				
				$cartsubtotal = $cartsubtotal + $cekpv1['pv_price'];
				$cartvst[$idvariety] = 1;
				$cartqty = $cartqty + 1;
				$cartweight = $cartweight + $cekpv1['pr_weight'];
				$result['Status'] = "Add to cart success!";
			}
		}else{
			$result['Error'] = "Not define error!";
		}
	}
	
	$result['cartsubtotal'] = 'Rp '.number_format($cartsubtotal,0,',','.');
	$result['cartsubtotalnom'] = $cartsubtotal;
	$result['cartqty'] = number_format($cartqty,0,',','.');
	$result['cartweight'] = number_format($cartweight,0,',','.').' Gram';

	if (count($cartvst)>0){
		setcookie( 'cartvst',json_encode($cartvst),time()+60*60*24*30,$pathcook );
	}elseif (isset($_COOKIE["cartvst"])){
		unset($_COOKIE["cartvst"]);
		setcookie( 'cartvst',null,-1,$pathcook );
	}

	if ($_POST['cartmode']=='cartdel' && !isset($result['Error'])){
		$result['Status'] = "Delete cart item success!";
	}elseif ($_POST['cartmode']=='cartupd' && !isset($result['Error'])){
		$result['Status'] = "Update cart item success!";
	}
}

// --------------------------- address section ----------------------------
if (!isset($result['Error']) && isset($_SERVER['HTTP_REFERER']) && substr_count($_SERVER['HTTP_REFERER'],'/cart')>0){
	$explvst = explode(':',$_COOKIE['loginmem']);
	$memid = $explvst[0];
if ($_POST['cartmode']=='addradd' || $_POST['cartmode']=='addrupd' || $_POST['cartmode']=='addrdel'){
	if (isset($_POST['addrid'])){$addrid=intval($_POST['addrid']);}else{$addrid=0;}
	if (isset($_POST['addrsaveas'])){$addrsaveas=trim($_POST['addrsaveas']);}else{$addrsaveas='';}
	if (isset($_POST['addrnama'])){$addrnama=trim($_POST['addrnama']);}else{$addrnama='';}
	if (isset($_POST['addrhp'])){$addrhp=trim($_POST['addrhp']);}else{$addrhp='';}
	if (isset($_POST['addrlosid'])){$addrlosid=intval($_POST['addrlosid']);}else{$addrlosid=0;}
	if (isset($_POST['addrzip'])){$addrzip=intval($_POST['addrzip']);}else{$addrzip=0;}
	if (isset($_POST['addraddress'])){$addraddress=trim($_POST['addraddress']);}else{$addraddress='';}
	
	if ($_POST['cartmode']=='addradd'){
		if ($addrsaveas==''){
			if (!isset($result['Error'])){$result['Error']='';}
			if ($result['Error']!=''){$result['Error'] .= '<br>';}
			$result['Error'] .= 'Please fill save as field';
		}
		if ($addrnama==''){
			if (!isset($result['Error'])){$result['Error']='';}
			if ($result['Error']!=''){$result['Error'] .= '<br>';}
			$result['Error'] .= 'Please fill name field';
		}
		if ($addrhp==''){
			if (!isset($result['Error'])){$result['Error']='';}
			if ($result['Error']!=''){$result['Error'] .= '<br>';}
			$result['Error'] .= 'Please fill phone field';
		}
		if ($addrlosid==''){
			if (!isset($result['Error'])){$result['Error']='';}
			if ($result['Error']!=''){$result['Error'] .= '<br>';}
			$result['Error'] .= 'Please select town/sub district field';
		}
		if ($addraddress==''){
			if (!isset($result['Error'])){$result['Error']='';}
			if ($result['Error']!=''){$result['Error'] .= '<br>';}
			$result['Error'] .= 'Please fill address field';
		}
	}elseif (($_POST['cartmode']=='addrupd' || $_POST['cartmode']=='addrdel') && $addrid==0){
		$result['Error']='Un-known record!';
	}elseif ($_POST['cartmode']=='addrupd' || $_POST['cartmode']=='addrdel'){
		$field = "mma_id,mma_los_id,cr_mma_id";
		$join = "left outer join cart on cr_mma_id=mma_id";
		$where = "mma_mm_id=".$memid." and mma_id=".$addrid;
		$limit = 1;
		$exist = memberaddress_s($sqlcon,$field,$join,$where,'','',$limit);

		if (gettype( $exist )=='string'){
			$result['Error']=$exist;
		}elseif(!(is_array($exist) && $exist[0]>0)){
			$result['Error']='Record not exist!';
		}
	}
	
	if (!isset($result['Error'])){
		$roorigintype = $rodesttype = '';
		$rooriginid = $rodestid = 0;
		if ($_POST['cartmode']=='addradd'){
			$newid = memberaddress_i($sqlcon,$memid,$addrsaveas,$addrnama,$addrhp,$addrlosid,$addrzip,$addraddress);

			$field = "mma_id,mma_saveas,mma_los_id,prop.lok_name prop,kab.lok_name kab,kab.lok_rajaongkir_id kabrajaongkirid,kec.lok_name kec,kec.lok_rajaongkir_id kecrajaongkirid,mma_zip,mma_name,mma_phone,mma_address";
			$join = "inner join lokasi_step on los_id=mma_los_id ".
				"left outer join lokasi prop on los_prop_lok_id=prop.lok_id ".
				"left outer join lokasi kab on los_kab_lok_id=kab.lok_id ".
				"left outer join lokasi kec on los_kec_lok_id=kec.lok_id";
			$where = "mma_id=".$newid[0];
			$getaddr = memberaddress_s($sqlcon,$field,$join,$where);
			$getaddr1 = mysqli_fetch_assoc($getaddr[1]);
			$addaddr = '';
			if ($getaddr1['kec']!=''){
				if ($addaddr != ''){$addaddr .= ', ';}
				$addaddr .= $getaddr1['kec'];
			}
			if ($getaddr1['kab']!=''){
				if ($addaddr != ''){$addaddr .= ', ';}
				$addaddr .= $getaddr1['kab'];
			}
			if ($getaddr1['prop']!=''){
				if ($addaddr != ''){$addaddr .= ', ';}
				$addaddr .= $getaddr1['prop'];
			}
			if ($getaddr1['mma_zip']!=0){
				if ($addaddr != ''){$addaddr .= ', ';}
				$addaddr .= $getaddr1['mma_zip'];
			}
			if (isset($getaddr1['kecrajaongkirid']) && $getaddr1['kecrajaongkirid']!=0){
				$rodestid = $getaddr1['kecrajaongkirid'];
				$rodesttype = 'subdistrict';
			}elseif (isset($getaddr1['kabrajaongkirid']) && $getaddr1['kabrajaongkirid']!=0){
				$rodestid = $getaddr1['kabrajaongkirid'];
				$rodesttype = 'city';
			}
			$result['newaddr'] = '<tr class="addrrow row'.$getaddr1['mma_id'].'">'.
				'<td class="addrusethis" onclick="addrupd('.$getaddr1['mma_id'].');">'.
					'<strong>Saved as : '.$getaddr1['mma_saveas'].'</strong><br>'.$getaddr1['mma_name'].' ('.$getaddr1['mma_phone'].')<br>'.$getaddr1['mma_address'].' '.$addaddr.'.'.
					'<p class="hidden">'.json_encode($getaddr1).'</p>'.
				'</td>'.
				'<td class="addrdelthis"><i class="fa fa-times" title="Delete this address" '.
				'onclick="addrdel('.$getaddr1['mma_id'].');"></i></td>'.
			'</tr>';
			$result['Status']='addradd';
			$result['addrslctid']=$newid[0];
		}elseif ($_POST['cartmode']=='addrupd' || $_POST['cartmode']=='addrdel'){
			$exist1 = mysqli_fetch_assoc($exist[1]);
			if ($_POST['cartmode']=='addrupd'){
				$addrlosid = $exist1['mma_los_id'];
				$result['Status']='addrupd';
				$result['addrslctid'] = $exist1['mma_id'];
			}else{
				if (isset($exist1['cr_mma_id']) && $exist1['cr_mma_id']>0){
					$set = "mma_deleted=1";
					memberaddress_u($sqlcon,$set,$addrid);
				}else{
					memberaddress_d($sqlcon,$addrid);
				}
				$result['Status']='addrdel!';

				//get next address
				$field = "mma_id,mma_los_id";
				$join = "";
				$where = "mma_mm_id=".$memid." and mma_deleted=0";
				$limit = 1;
				$exist = memberaddress_s($sqlcon,$field,$join,$where,'','',$limit);
				if (gettype( $exist )=='string'){
					$addrlosid = 0;
					$result['info']='Temporary failure!';
					$result['addrslctid'] = 0;
				}elseif(is_array($exist) && $exist[0]>0){
					$exist1 = mysqli_fetch_assoc($exist[1]);
					$addrlosid = $exist1['mma_los_id'];
					$result['addrslctid'] = $exist1['mma_id'];
				}else{
					$addrlosid = 0;
					$result['info']='No more address data!';
					$result['addrslctid'] = 0;
				}
			}

			$field = "kab.lok_rajaongkir_id kabrajaongkirid,kec.lok_rajaongkir_id kecrajaongkirid";
			$join = "left outer join lokasi kab on los_kab_lok_id=kab.lok_id ".
				"left outer join lokasi kec on los_kec_lok_id=kec.lok_id";
			$where = "los_id=".$addrlosid;
			$limit = 1;
			$getaddr = lokasistep_s($sqlcon,$field,$join,$where,'','',$limit);
			$getaddr1 = mysqli_fetch_assoc($getaddr[1]);
			if (isset($getaddr1['kecrajaongkirid']) && $getaddr1['kecrajaongkirid']!=0){
				$rodestid = $getaddr1['kecrajaongkirid'];
				$rodesttype = 'subdistrict';
			}elseif (isset($getaddr1['kabrajaongkirid']) && $getaddr1['kabrajaongkirid']!=0){
				$rodestid = $getaddr1['kabrajaongkirid'];
				$rodesttype = 'city';
			}
		}
		if (isset($result['addrslctid']) && $result['addrslctid']!=0){
			setcookie( 'cartaddrid',$result['addrslctid'],time()+60*60*24*30,$pathcook );
		}else{
			unset($_COOKIE['cartaddrid']);
			setcookie( 'cartaddrid',null,-1,$pathcook );
		}

		if (isset($addrlosid) && $addrlosid>0){
			//get default raja ongkir sender location id
			$sendlocid = mysqli_fetch_assoc(mysqli_query($sqlcon,
				"select co_los_id,kab.lok_rajaongkir_id sendkab,kec.lok_rajaongkir_id sendkec ".
				"from contact ".
				"inner join lokasi_step on los_id=co_los_id ".
				"inner join lokasi kab on kab.lok_id=los_kab_lok_id ".
				"left outer join lokasi kec on kec.lok_id=los_kec_lok_id"
			));
			if (isset($sendlocid['sendkec']) && $sendlocid['sendkec']!=0){
				$rooriginid = $sendlocid['sendkec'];
				$roorigintype = 'subdistrict';
			}elseif (isset($sendlocid['sendkab']) && $sendlocid['sendkab']!=0){
				$rooriginid = $sendlocid['sendkab'];
				$roorigintype = 'city';
			}
			//cek ongkir updating, cek last update
			if (isset($sendlocid['co_los_id']) && $sendlocid['co_los_id']!=0 && $rooriginid!=0){
				$field = "ong_id";
				$where = "ong_origin_los_id=".$sendlocid['co_los_id']." and ong_dest_los_id=".$addrlosid." and ong_update>='".date("Y-m-d")." 00:00:00'";
				$order = "ong_update desc";
				$limit = 1;
				$onglupdt = ongkir_s($sqlcon,$field,'',$where,'',$order,$limit);
				if (is_array($onglupdt) && $onglupdt[0]==0){
					//cek eks used setting
					$eksstat = array();
					$field = "eks_code,et_status";
					$join = "left outer join ekspedisi_type on et_eks_id=eks_id";
					$order = "1,2";
					$qeksstat = ekspedisi_s($sqlcon,$field,$join,'','',$order);
					if (is_array($qeksstat) && $qeksstat[0]>0){while ($qeksstat1 = mysqli_fetch_assoc($qeksstat[1])){
						if (isset($eksstat[($qeksstat1['eks_code'])]) && $eksstat[($qeksstat1['eks_code'])]==0){
							$eksstat[($qeksstat1['eks_code'])] = $qeksstat1['et_status'];	
						}else{
							$eksstat[($qeksstat1['eks_code'])] = $qeksstat1['et_status'];
						}
					}}
					foreach ($confro['courier'] as $ekscode) {
						if (!isset($eksstat[$ekscode]) || (isset($eksstat[$ekscode]) && $eksstat[$ekscode]==1)){
							$ropr = roPrice($sqlcon,$confro,$rooriginid,$roorigintype,$rodestid,$rodesttype,$ekscode,$sendlocid['co_los_id'],$addrlosid);
							// $result['ropr'][$ekscode] = $ropr;
						}
					}
				}else{
					// $result['ekserr'] = $onglupdt;
					// $result['whrqry'] = $where;
				}
			}

			// get result ongkir
			$field = "ong_id,eks_code,et_code,et_kelipatanberat,ong_estimasi,ong_price";
			$join = "inner join ekspedisi_type on et_id=ong_et_id inner join ekspedisi on eks_id=et_eks_id";
			$where = "ong_origin_los_id=".$sendlocid['co_los_id']." and ong_dest_los_id=".$addrlosid." and et_status=1";
			$order = "2,5";
			$getong = ongkir_s($sqlcon,$field,$join,$where,'',$order);
			$priceb = 0;
			$existingcourier = '';
			if (is_array($getong) && $getong[0]>0){while ($getong1 = mysqli_fetch_assoc($getong[1])){
				$toteksprice = $getong1['ong_price'] * ceil($cartweight/$getong1['et_kelipatanberat']);
				if ($priceb==0 || $priceb>$toteksprice){
					$courier = $getong1['ong_id'];
					$priceb = $toteksprice;
				}
				$existingcourier .= '<tr class="crrrow row'.$getong1['ong_id'].'" onclick="crrusethis('.$getong1['ong_id'].',1);">'.
				'<td class="crrusethis">'.
					$getong1['eks_code'].' '.$getong1['et_code'].' ('.$getong1['ong_estimasi'].') Rp '.number_format($getong1['ong_price'],0,',','.').' / '.$getong1['et_kelipatanberat'].'Gr'.
					'<p class="hidden">'.json_encode(array(
						'toteksprice'=>$toteksprice,
						'show'=>$getong1['eks_code'].' '.$getong1['et_code'].' ('.$getong1['ong_estimasi'].') Rp '.number_format($toteksprice,0,',','.')
					)).'</p>'.
				'</td>'.
				'<td class="crrestweight">'.number_format($cartweight,0,',','.').' Gr</td>'.
				'<td class="crrcost">Rp '.number_format($toteksprice,0,',','.').'</td>'.
			'</tr>';
			}}else{
				$courier = 0;
				$existingcourier = '<tr class="crrrow nodata row0" onclick="crrusethis(0,1);">'.
					'<td class="crrusethis">No expedition available!'.
						'<p class="hidden">'.json_encode(array(
							'toteksprice'=>0,
							'show'=>'No expedition available!'
						)).'</p>'.
					'</td>'.
				'</tr>';
				// $existingcourier = '<tr class="crrrow nodata"><td class="crrusethis">No expedition available!</td></tr>';
			}
		}else{
			$courier = 0;
			$existingcourier = '<tr class="crrrow nodata row0" onclick="crrusethis(0,1);">'.
				'<td class="crrusethis">No expedition available!'.
					'<p class="hidden">'.json_encode(array(
						'toteksprice'=>0,
						'show'=>'No expedition available!'
					)).'</p>'.
				'</td>'.
			'</tr>';
			// $existingcourier = '<tr class="crrrow nodata"><td class="crrusethis">No expedition available!</td></tr>';
		}
		if ($courier){
			setcookie( 'cartcourier',$courier,time()+60*60*24*30,$pathcook );
		}
		
		$result['courier'] = $courier;
		$result['existcourier'] = $existingcourier;
		//$result['QryErr']=mysqli_error($sqlcon);
	}
}elseif ($_POST['cartmode']=='courierupd'){
	if ($courier == 0){
		unset($_COOKIE["cartcourier"]);
		setcookie( 'cartcourier',null,-1,$pathcook );
	}else{
		setcookie( 'cartcourier',$courier,time()+60*60*24*30,$pathcook );
	}
	$result['Status']='courierupd';
}elseif ($_POST['cartmode']=='lastcheck'){
	if (isset($_POST['addresschooseid'])){$addrid=intval($_POST['addresschooseid']);}else{$addrid=0;}
	if (isset($_POST['ongkirchooseid'])){$ongid=intval($_POST['ongkirchooseid']);}else{$ongid=0;}
	if (isset($_POST['vchid'])){$vchid=intval($_POST['vchid']);}else{$vchid=0;}
	if (isset($_POST['paytypeid'])){$paytypeid=intval($_POST['paytypeid']);}else{$paytypeid=0;}

	if ($continuetosave==0){
		$result['Error'] = 'Changeing qty availability! Please wait for page reloading.';
	}elseif (count($cartvst) == 0){
		$result['Error'] = 'No item on cart!';
	}elseif ($addrid == 0){
		$result['Error'] = 'No address set!';
	}elseif ($ongid == 0){
		$result['Error'] = 'No expedition set!';
	}elseif ($paytypeid == 0){
		$result['Error'] = 'Payment type not selected!';
	}else{
		$field = "pt_id";
		$where = "pt_id=".$paytypeid." and pt_status in (1,2)";
		$paytype = paymenttype_s($sqlcon,$field,'',$where);
		if (is_array($paytype) && $paytype[0]>0){
			$result['Status'] = 'ok';
		}else{
			$result['Error'] = 'Payment Type not available!';
		}
		// //ongkir price
		// $field = "et_kelipatanberat,ong_price";
		// $join = "inner join ekspedisi_type on et_id=ong_et_id inner join ekspedisi on eks_id=et_eks_id";
		// $where = "ong_id=".$ongid;
		// $getong = ongkir_s($sqlcon,$field,$join,$where);
		// if (is_array($getong) && $getong[0]>0){
		// 	$getong1 = mysqli_fetch_assoc($getong[1]);
		// 	$ongprice = $getong1['ong_price'] * ceil($cartweight/$getong1['et_kelipatanberat']);
		// }
		// $discount=0;
		//$cartid = cart_i($sqlcon,$addrid,$ongid,$ongprice,$discount,$vchid,$comment,0);
		//$result['cartid'] = $cartid[0];
		// unset all cart cookie
		// setcookie( 'cartvst',null,-1,$pathcook );
		// setcookie( 'cartaddrid',null,-1,$pathcook );
		// setcookie( 'cartcourier',null,-1,$pathcook );

		// $field = "count(cr_id) jpending";
		// $join  = "inner join member_address on mma_id=cr_mma_id";
		// $where = "mma_mm_id=".$memid;
		// $totpend = cart_s($sqlcon,$field,$join,$where);
		// if (is_array($totpend)){
		// 	$totpend1 = pg_fetch_assoc($totpend);
		// 	$result['totalpending'] = $totpend1['jpending'];
		// }
	}
}
}

if ($sqlcon){mysqli_close($sqlcon);}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>