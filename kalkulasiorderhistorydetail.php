<?php
//path defined
if (isset($_SERVER['HTTP_HOST']) && (
	$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
	|| $_SERVER['HTTP_HOST']=='172.16.1.19'
)){
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/';
	$pathcook = '/poloweb/';
}else{
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/';
	$pathcook = '/';
}

if (substr_count( $_SERVER['SCRIPT_NAME'], '/orderhistorydetail.php' ) == 0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}elseif (!isset($_SERVER['HTTP_REFERER'])){
	header ( 'location:'.$path.'index.html' );
	exit ();
}elseif (substr_count( $_SERVER['HTTP_REFERER'], '/orderhistory.html' )==0 && substr_count( $_SERVER['HTTP_REFERER'], '/memberorder.html' )==0){
	header ( 'location:'.$path.'index.html' );
	exit ();
}elseif (!isset($_GET['cartid']) || (isset($_GET['cartid']) && intval($_GET['cartid'])==0)){
	header ( 'location:'.$path.'index.html' );
	exit ();
}else{
	if (substr_count( $_SERVER['HTTP_REFERER'], '/orderhistory.html' )>0 && !isset($_COOKIE['loginmem'])){
		header ( 'location:'.$path.'index.html' );
		exit ();
	}

	include_once 'cms/configuration/connect.php';
	include_once 'cms/model/cart.php';
	include_once 'cms/model/cartdetail.php';

	$addwhere = '';
	if (isset($_COOKIE["loginmem"])){
		$explvst = explode(':',$_COOKIE["loginmem"]);
		$memid = $explvst[0];
		$addwhere = " and mma_mm_id=".$memid;
	}

	$arrstatus = array('Waiting Payment','Canceled','Paid','Payment Failed','On Packaging','On Delivery','Delivered','Delivery Failed');

	$field = "cart.*,et.et_code,eks_code,mma.*,pt.pt_name,cw_result";
	$join = "inner join member_address mma on mma_id=cr_mma_id ".
		"inner join ongkir on ong_id=cr_ong_id ".
		"inner join ekspedisi_type et on et_id=ong_et_id ".
		"inner join ekspedisi eks on eks_id=et_eks_id ".
		"inner join paymenttype pt on pt_id=cr_pt_id ".
		"left outer join cart_awb on cw_cr_id=cr_id";
	$where = "cr_id=".intval($_GET['cartid']).$addwhere;
	$cart = cart_s($sqlcon,$field,$join,$where);
	$listbuy = '';
	if (is_array($cart) && $cart[0]>0){
		$cart1 = mysqli_fetch_assoc($cart[1]);
		$invno = $cart1['cr_id'];
		$invstatus = $arrstatus[( $cart1['cr_status'] )];
		$invdate = date("d F Y",strtotime($cart1['cr_create']));
		$invcomment = '-';
		if ($cart1['cr_comment']!=''){
			$invcomment = str_replace(array(chr(13),chr(10)), array("<br>",""), $cart1['cr_comment']);
		}
		$ekstype = '-';
		$deliprog = 0;
		if (isset($cart1['eks_code']) && $cart1['eks_code']!=''){
			$ekstype = $cart1['eks_code'];
			if (isset($cart1['et_code']) && $cart1['et_code']!=''){
				$ekstype .= " - ".$cart1['et_code'];
			}
			if (isset($cart1['cr_awb']) && $cart1['cr_awb']!=''){
				$ekstype .= "<br>AWB No : ".$cart1['cr_awb'];
				$deliprog = 1;
			}
		}
		$address = '-';
		if (isset($cart1['mma_name']) && $cart1['mma_name']!=''){
			$address = "To : ".$cart1['mma_name']."<br>".str_replace(array(chr(13),chr(10)), array("<br>",""), $cart1['mma_address']);
			if ($cart1['mma_zip']!=0){
				$address .= " ".$cart1['mma_zip'];
			}
			$address .= "<br>Phone : ".$cart1['mma_phone'];
		}
		$paytype = $cart1['pt_name'];
		$tong = $cart1['cr_ong_price'];
		$tongp = number_format($tong,0,',','.');
		$tdisc = $cart1['cr_discount'];
		$tdiscp = number_format($tdisc,0,',','.');
		$trlist = '';
		$field = "*";
		$join = "inner join product_variety on pv_id=crd_pv_id ".
			"inner join product on pr_id=pv_pr_id ".
			"left outer join product_image on pi_id=pr_main_pi_id ".
			"left outer join style on st_id=pv_st_id ".
			"left outer join size on sz_id=pv_sz_id ".
			"left outer join color_item on ci_id=pv_ci_id";
		$where = "crd_cr_id=".$cart1['cr_id'];
		$det = cartdetail_s($sqlcon,$field,$join,$where);
		$tprod = 0;
		if (is_array($det) && $det[0]>0){while ($det1 = mysqli_fetch_assoc($det[1])){
			$tprod = $tprod + ($det1['crd_qty']*$det1['crd_price']);
			$prodname = $det1['pr_name'];
			if (isset($det1['st_name']) && $det1['st_name']!=''){
				$prodname .= ' - '.$det1['st_name'];
			}
			if (isset($det1['sz_name']) && $det1['sz_name']!=''){
				$prodname .= ', Size '.$det1['sz_name'];
			}
			if (isset($det1['ci_image']) && $det1['ci_image']!='' && file_exists("images/productcolor/".$det1['ci_image'])){
				$prodname .= ', Color <img src="'.$path.'images/productcolor/'.$det1['ci_image'].'" alt="'.$det1['pr_name'].'" class="imgcolor">';
			}
			$prodimg = '';
			if (isset($det1['pi_image']) && $det1['pi_image']!='' && file_exists("images/productitem/".$det1['pi_image'])){
				$prodimg .= '<img src="'.$path.'images/productitem/'.$det1['pi_image'].'" alt="'.$det1['pr_name'].'">';
			}
			$trlist .= '<tr>'.
				'<td width="15%" align="center">'.$prodimg.'</td>'.
				'<td width="*">'.$prodname.'</td>'.
				'<td width="15%">'.number_format($det1['crd_qty'],0,',','.').' pcs<br>Rp '.number_format($det1['crd_price'],0,',','.').'</td>'.
				'<td width="20%"><label>Total</label>Rp '.number_format(($det1['crd_qty']*$det1['crd_price']),0,',','.').'</td>'.
			'</tr>';
		}}else{
			$trlist = '<tr><td>Data possibly deleted!</td></tr>';
		}
		$tprodp = number_format($tprod,0,',','.');
		$total = $tong + $tprod - $tdisc;
		$totalp = number_format($total,0,',','.');

		$deliprogp = '';
		if ($deliprog == 1){
			if (isset($cart1['cw_result']) && $cart1['cw_result']!=''){
				$delires = $cart1['cw_result'];
			}else{
				$resro = roAWB($sqlcon,$confro,$cart1['eks_code'],$cart1['cr_awb']);
				if (isset($resro['Error'])){
					$deliprogp .= '<tr>'.
						'<td>Internal error : '.$resro['Error'].'.</td>'.
					'</tr>';
				}else{
					if (isset($resro['delivered'])==true){
						cartawb_autoiu($sqlcon,$cart1['cr_id'],json_encode($resro['result']));
						//status = array('Waiting Payment','Canceled','Paid','Payment Failed','On Packaging','On Delivery','Delivered','Delivery Failed');
						//delivered
						$set = "cr_status=6";
					}else{
						//on progress
						$set = "cr_status=5";
					}
					cart_u($sqlcon,$set,$cart1['cr_id']);
					$delires = $resro['result'];
				}
			}
			if ($delires != ''){
				$delires = json_decode($delires,true);
			}
			if (count($delires)>0){
				if (isset($delires['details']['waybill_date'])){
					$deliprogp .= '<tr>'.
						'<td width="30%">'.date("l, j F Y",strtotime($delires['details']['waybill_date'])).' '.$delires['details']['waybill_time'].' WIB</td>'.
						'<td width="70%">Received by expedition counter.</td>'.
					'</tr>';
				}
				if (isset($delires['manifest']) && count($delires['manifest'])>0){foreach ($delires['manifest'] as $key => $value) {
					$deliprogp .= '<tr>'.
						'<td width="30%">'.date("l, j F Y",strtotime($value['manifest_date'])).' '.$value['manifest_time'].' WIB</td>'.
						'<td width="70%">'.$value['manifest_description'].' at '.$value['city_name'].'.</td>'.
					'</tr>';
				}}
				if (isset($delires['delivery_status']['status']) && $delires['delivery_status']['status']=='DELIVERED'){
					if (isset($delires['delivery_status']['pod_receiver']) && $delires['delivery_status']['pod_receiver']!=''){
						$receiver = 'Package received by '.$delires['delivery_status']['pod_receiver'];
					}else{
						$receiver = 'Package delivered successfully.';
					}
					$deliprogp .= '<tr>'.
						'<td width="30%">'.date("l, j F Y",strtotime($delires['delivery_status']['pod_date'])).' '.$delires['details']['pod_time'].' WIB</td>'.
						'<td width="70%">'.$receiver.'.</td>'.
					'</tr>';
				}
			}
		}else{
			$deliprogp .= '<tr>'.
				'<td>No progress yet.</td>'.
			'</tr>';
		}
	}else{
		$listbuy = '<h3>Order not Found!</h3>';
	}
}
?>