<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/paymentstatus.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<div class="container">
		<div class="row">
			<div class="col-md-offset-3 col-md-7 col-sm-offset-1 col-sm-10 col-xs-12"><?php 
if ($errmsg==''){?>
				<br><h3>Payment Status</h3><br>
				<p>Thank you for shoping at our webstore. Your invoice status is</p><br><?php 
	if ($paystat==1){?>
				<h4>Waiting for Payment</h4><br>
				<p>Please do transfer money to below bank account :<br><br>Bank : BCA<br>Account : xxxxxx<br>Name : xxxxxxx</p><br><?php
	}else{?>
				<h4><?php echo $invoicestatus;?></h4><br><?php
	}
	if ($cart1['cr_status']==0){?>
				<p>This order will auto terminated after 15 hour, until <b><?php echo $autoterminate;?></b></p><?php 
	}?>
				<p>See <a href="<?php echo $path;?>orderhistory.html" title="My shopping list">my shopping list</a>.</p><?php
}else{?>
				<h3><?php echo $errmsg;?></h3><?php
}?>
				
			</div>
		</div>
	</div><br><br><?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
</body><!-- //Body -->
</html>