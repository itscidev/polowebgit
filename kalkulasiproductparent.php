<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/productparent.php' ) == 0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}

$addid = 'and bns_ca_id=0 ';
if (isset($_GET['id'])){
	$getid = @mysqli_query($sqlcon,
		"select ca_id from category ".
		"where lower(ca_name)='".mysqli_real_escape_string($sqlcon,strtolower(trim($_GET['id'])))."' ".
			"and ca_status=1 ".
		"order by ca_sequence asc,ca_update desc"
	);
	$getid1 = 0;
	if ($getid){$getid1 = mysqli_num_rows($getid);}
	if ($getid1 > 0){
		$getid2 = mysqli_fetch_array($getid);
		$addid = 'and bns_ca_id='.$getid2['ca_id'].' ';
		//untuk banner text caraosel
		$posisi = $getid2['ca_id'];
	}
}

//get header slide
$headerslide = '';
if ($draftmode == 1){
	$sqldraft = 'and bn_draft=1 ';
}else{
	$sqldraft = 'and bn_status=1 ';
}
$sld = @mysqli_query($sqlcon,
	"select * from banner ".
	"inner join banner_showon on bns_bn_id=bn_id ".
	"where bn_purpose='TopSlide' and bn_from<='".date("Y-m-d H:i:s")."' ".
		"and bn_to>='".date("Y-m-d H:i:s")."' ".$sqldraft.$addid.
	"order by bn_sequence asc,bn_update desc"
);
$sld1 = 0;
if ($sld){$sld1 = mysqli_num_rows($sld);}
if ($sld1 > 0){while ($sld2 = mysqli_fetch_array($sld)){
	if (isset($sld2['bn_image']) && $sld2['bn_image']!='' 
	&& file_exists("images/banner/".$sld2['bn_image'])){
		$datathumb = '';
		if (isset($sld2['bn_image1']) && $sld2['bn_image1']!='' 
		&& file_exists("images/banner/".$sld2['bn_image1'])){
			$datathumb = ' data-thumb="'.$path.'images/banner/'.$sld2['bn_image1'].'"';
		}
		$imgsld = '<img data-src="'.$path.'images/banner/'.$sld2['bn_image'].'" class="swiper-lazy" '.
			'alt="'.htmlentities($sld2['bn_title']).'">';
		if (trim($sld2['bn_link'])!=''){
			$imgsld = '<a href="'.$sld2['bn_link'].'">'.$imgsld.'</a>';
		}
		//$headerslide .= '<div'.$datathumb.' data-src="'.$path.'images/banner/'.$sld2['bn_image'].'"></div>';
		$headerslide .= '<div class="swiper-slide homepurp">'.
			$imgsld.
			'<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>'.
		'</div>';
	}
}}
if ($headerslide != ''){
	/* $headerslide = '<div class="w3slideraits">'.
	'<div class="fluid_dg_wrap fluid_dg_emboss pattern_1 fluid_dg_white_skin" id="fluid_dg_wrap_4">'.
		$headerslide.
	'</div>'.
	'</div>'; */
	$headerslide = '<div class="row">'.
		'<div id="slidepromo" class="swiper-container">'.
			'<div class="swiper-wrapper">'.$headerslide.'</div>'.
			// Add Pagination
			'<div class="swiper-pagination swiper-pagination-white"></div>'.
			// Add Arrows
			'<div class="swiper-button-next"></div>'.
			'<div class="swiper-button-prev"></div>'.
		'</div>'.
	'</div>';
}
/* $arrban = array('deletethis-MENS.jpg');
if (isset($_GET['id']) && $_GET['id']=='Men'){
	$arrban = array('deletethis-MEN-banner-1.jpg','deletethis-MEN-banner-2.jpg');
}elseif (isset($_GET['id']) && $_GET['id']=='Women'){
	$arrban = array('deletethis-ladies-banner1.jpg','deletethis-ladies-banner2.png');
}if (isset($_GET['id']) && $_GET['id']=='Bag'){
	$arrban = array('deletethis-bags-banner1.jpg','deletethis-bags-banner2.jpg','deletethis-bags-banner3.jpg');
} 
$headerslide = '<div class="row">'.
	'<div id="slidepromo" class="swiper-container">'.
		'<div class="swiper-wrapper">'; */
			/* '<div class="swiper-slide">'.
        		'<img data-src="'.$path.'images/banner/deletethis-MENS.jpg" class="swiper-lazy">'.
				'<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>'.
			'</div>'. */
/* foreach ($arrban as $content){
	$headerslide .= '<div class="swiper-slide">'.
			'<img data-src="'.$path.'images/banner/'.$content.'" class="swiper-lazy">'.
			'<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>'.
		'</div>';
}
$headerslide .= '</div>'.
		// Add Pagination
		'<div class="swiper-pagination swiper-pagination-white"></div>'.
		// Add Arrows
		'<div class="swiper-button-next"></div>'.
		'<div class="swiper-button-prev"></div>'.
	'</div>'.
'</div>'; */


//get Video Section
$videosection = $newinsection = '';
$sld = @mysqli_query($sqlcon,
	"select * from banner ".
	"inner join banner_showon on bns_bn_id=bn_id ".
	"where bn_purpose='VideoPromo' and bn_sequence=1 ".
		"and bn_from<='".date("Y-m-d H:i:s")."' and bn_to>='".date("Y-m-d H:i:s")."' ".$sqldraft.$addid.
	"order by bn_update desc limit 1"
);
$sld1 = 0;
if ($sld){$sld1 = mysqli_num_rows($sld);}
if ($sld1 > 0){
	$sld2 = mysqli_fetch_assoc($sld);
	if (isset($sld2['bn_image']) && $sld2['bn_image']!=''
			&& file_exists("images/banner/".$sld2['bn_image'])){
		$videosection = '<div class="col-sm-offset-1 col-sm-7 col-xs-12">'.
				'<video width="90%" height="auto" controls>'.
				'<source src="'.$path.'images/banner/'.$sld2['bn_image'].'" type="video/mp4">'.
				'Your browser does not support the video tag.'.
				'</video>'.
				'</div>';
	}
}
$sld = @mysqli_query($sqlcon,
	"select * from banner ".
	"inner join banner_showon on bns_bn_id=bn_id ".
	"where bn_purpose='VideoPromo' and bn_sequence!=1 ".
		"and bn_from<='".date("Y-m-d H:i:s")."' and bn_to>='".date("Y-m-d H:i:s")."' ".$sqldraft.$addid.
	"order by bn_update desc limit 1"
);
$sld1 = 0;
if ($sld){$sld1 = mysqli_num_rows($sld);}
if ($sld1 > 0){
	$sld2 = mysqli_fetch_assoc($sld);
	if (isset($sld2['bn_image']) && $sld2['bn_image']!=''
			&& file_exists("images/banner/".$sld2['bn_image'])){
		$newinsection = '<img alt="'.$sld2['bn_title'].'" src="'.$path.'images/banner/'.$sld2['bn_image'].'">'.
				'<p><i class="fa fa-angle-double-right" aria-hidden="true"></i>NEW IN</p>';
		if (isset($sld2['bn_link']) && $sld2['bn_link']!=null && $sld2['bn_image']!=''){
			$newinsection = '<a href="'.$sld2['bn_link'].'" title="'.$sld2['bn_title'].'">'.
					$newinsection.
					'</a>';
		}
		$newinsection = '<div class="col-sm-3 col-xs-12">'.$newinsection.'</div>';
	}
}
if ($videosection!='' || $newinsection!=''){
	$videosection = '<div class="row videosection">'.$videosection.$newinsection.'</div>';
}

//get quick menu
$quickmenu = '';
/* $sld = @mysqli_query($sqlcon,
	"select * from banner where bn_purpose='HomeQuickMenu' and bn_status=1 ".
	"order by bn_sequence asc,bn_update desc"
);
$sld1 = 0;
if ($sld){$sld1 = mysqli_num_rows($sld);}
if ($sld1 > 0){while ($sld2 = mysqli_fetch_array($sld)){
	if (isset($sld2['bn_image']) && $sld2['bn_image']!=''
	&& file_exists("images/banner/".$sld2['bn_image'])){
		$quickmenu .= '<div class="col-md-4 agileshopping">
			<div class="agileshoppinggrid agileshoppinggrid1" '.
			'onclick="location.href=\''.$sld2['bn_link'].'\'; return false;">'.
				'<div class="grid">'.
					'<figure class="effect-apollo">'.
						'<img src="'.$path.'images/banner/'.$sld2['bn_image'].'" '.
						'alt="'.htmlentities($sld2['bn_title']).'">'.
						'<figcaption></figcaption>'.
					'</figure>'.
				'</div>'. 
			'</div>'.
			'<div id="lineheightzero">&nbsp;</div>'.
			'<div class="grid1">'.
				'<a href="'.$sld2['bn_link'].'">'.htmlentities($sld2['bn_title']).'</a>'.
			'</div>'.
		'</div>';
	}
}} */
/* $arrqm = array('deletethis-QUICK-MENU.jpg','deletethis-QUICK-MENU-1.jpg','deletethis-QUICK-MENU-2.jpg');
$arrurl = array('categories/Men/ACCESSORIES','categories/Men/JACKET','categories/Men/FLAG EDITION');
$arranc = array('Accessories','Jacket','Flag Edition');
if (isset($_GET['id']) && $_GET['id']=='Women'){
	$arrqm = array('deletethis-ladies-page1.jpg','deletethis-ladies-page2.jpg','deletethis-ladies-page3.jpg');
	$arrurl = array('categories/Women/Accessories','categories/Women/Clothing/Coat','categories/Women/Clothing');
	$arranc = array('Accessories','Coat','Clothing');
}if (isset($_GET['id']) && $_GET['id']=='Bag'){
	$arrqm = array('deletethis-bags-page1.jpg','deletethis-bags-page2.jpg','deletethis-bags-page3.jpg');
	$arrurl = array('categories/Bag/Leather/Hand%20Bag','categories/Bag/Leather/Sling%20Bag','categories/Bag/Leather/Trolly');
	$arranc = array('Hand Bag','Sling Bag','Trolly');
}
foreach ($arrqm as $index => $content){
	$quickmenu .= '<div class="col-md-4 agileshopping">
		<div class="agileshoppinggrid agileshoppinggrid1" '.
		'onclick="location.href=\''.$arrurl[$index].'\'; return false;">'.
			'<div class="grid">'.
				'<figure class="effect-apollo">'.
					'<img src="'.$path.'images/banner/'.$content.'" alt="'.$arranc[$index].'">'.
					'<figcaption></figcaption>'.
				'</figure>'.
			'</div>'.
		'</div>'.
		'<div id="lineheightzero">&nbsp;</div>'.
		'<div class="grid1">'.
			'<a href="'.$arrurl[$index].'">'.$arranc[$index].'</a>'.
		'</div>'.
	'</div>';
} */
$sld = @mysqli_query($sqlcon,
	"select * from banner ".
	"inner join banner_showon on bns_bn_id=bn_id ".
	"where bn_purpose='QuickMenu' and bn_from<='".date("Y-m-d H:i:s")."' ".
		"and bn_to>='".date("Y-m-d H:i:s")."' ".$sqldraft.$addid.
	"order by bn_sequence asc,bn_update desc"
);
$sld1 = 0;
if ($sld){$sld1 = mysqli_num_rows($sld);}
if ($sld1 > 0){while ($sld2 = mysqli_fetch_array($sld)){
	if (isset($sld2['bn_image']) && $sld2['bn_image']!=''
	&& file_exists("images/banner/".$sld2['bn_image'])){
		$quickmenu .= '<div class="col-md-4 col-xs-12 agileshopping">
			<div class="agileshoppinggrid agileshoppinggrid1" '.
			'onclick="location.href=\''.$sld2['bn_link'].'\'; return false;">'.
				'<div class="grid">'.
					'<figure class="effect-apollo">'.
						'<img src="'.$path.'images/banner/'.$sld2['bn_image'].'" '.
						'alt="'.htmlentities($sld2['bn_title']).'">'.
						'<figcaption></figcaption>'.
					'</figure>'.
				'</div>'. 
			'</div>'.
			'<div id="lineheightzero">&nbsp;</div>'.
			/* '<div>&nbsp;</div>'. */
			'<div class="grid1">'.
				'<a href="'.$sld2['bn_link'].'">'.htmlentities($sld2['bn_title']).'</a>'.
			'</div>'.
		'</div>';
		/* $quickmenu .= '<div class="col-md-4 agileshoppinggrid agileshoppinggrid1" '.
		'onclick="location.href=\''.$sld2['bn_link'].'\'; return false;">'.
			'<div class="grid">'.
				'<figure class="effect-apollo">'.
					'<img src="'.$path.'images/banner/'.$sld2['bn_image'].'" '.
					'alt="'.htmlentities($sld2['bn_title']).'">'.
					'<figcaption></figcaption>'.
				'</figure>'.
			'</div>'.
		'</div>';
		$addquickmenu .= '<div class="col-md-4 agileshoppinggrids-bottom-grid agileshoppinggrids-bottom-grid3">'.
			'<div class="wthreeshop-a">'.
				'<a href="'.$sld2['bn_link'].'">'.htmlentities($sld2['bn_title']).'</a>'.
			'</div>'.
		'</div>'; */
	}
}}
if ($quickmenu != ''){
	$quickmenu = '<div class="row quickmenurow">'.
		$quickmenu.
	'</div>';
/* 
	$quickmenu = '<div class="agileshopping">'.
		'<div class="container">'.
			'<div class="agileshoppinggrids">'.
				'<div class="col-md-12 agileshoppinggrid agileshoppinggrid1">'.
					'<h3>CHOOSE THE BEST FOR YOU</h3>'.
					'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots '. 
					'in a piece of classical Latin literature from 45 BC, making it over 2000 years old. '.
					'Richard McClintock.</p>'.
				'</div>'.
				$quickmenu.
			'</div>'.
			'<div class="agileshoppinggrids-bottom">'.
				'<div class="agileshoppinggrids-bottom-grids">'.
					$addquickmenu.
					'<div class="clearfix"></div>'.
				'</div>'.
			'</div>'.
		'</div>'.
	'</div>'; */
}
//get seasons style
$seasonsstyle = '';
$seasons = @mysqli_query($sqlcon,
	"select * from banner ".
	"inner join banner_showon on bns_bn_id=bn_id ".
	"where bn_purpose='Seasons' and bn_from<='".date("Y-m-d H:i:s")."' ".
		"and bn_to>='".date("Y-m-d H:i:s")."' ".$sqldraft.$addid.
	"order by bn_sequence asc,bn_update desc limit 1"
);
$seasons1 = 0;
if ($seasons){$seasons1 = mysqli_num_rows($seasons);}
if ($seasons1 > 0){
	$seasons2 = mysqli_fetch_array($seasons);
	$bn_title = $bn_titlelong = $bn_content = '';
	if (isset($seasons2['bn_title']) && $seasons2['bn_title']!=''){
		$bn_title = $seasons2['bn_title'];
		$bn_titlelong = '<h2>'.
			'<i>'.$seasons2['bn_title'].'</i>'.
		'</h2>';
	}
	if (isset($seasons2['bn_image']) && $seasons2['bn_image']!=''
			&& file_exists("images/banner/".$seasons2['bn_image'])){
		$bn_content = '<div class="col-md-12 agileshopping">'.
			'<div class="agileshoppinggrid agileshoppinggrid1">'.
				'<div class="grid">'.
					'<figure class="effect-apollo" style="border:none;">'.
						'<img src="'.$path.'images/banner/'.$seasons2['bn_image'].'" alt="'.$bn_title.'"/>'.
					'</figure>'.
				'</div>'.
			'</div>'.
		'</div>'.
		'<div id="explorenow">'.
			'<b><i class="fa fa-angle-double-right"></i> EXPLORE NOW</b>'.
		'</div>';
	}
	$seasonsstyle = '<div class="col-xs-12 seasonstrend">'.
		$bn_titlelong . $bn_content .
	'</div>';
}
?>