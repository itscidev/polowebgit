<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Select2 -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>plugin/select2/select2.min.css">
	<!-- //Select2 -->
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/o-regulations.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<div class="col-xs-12 regulationhead">
		<img src="<?php echo $path;?>images/auxiliary/regulations.jpg" title="Polo Site Regulations" 
		alt="Polo Site Regulations" />
	</div>
	<div class="col-xs-12">
		<ol class="regulationbreadcrumb">
			<li>Regulations<i class="fa fa-angle-right"></i></li>
			<li class="active">Privacy Policy</li>
			<li class="showonmobile"><i class="fa fa-angle-down"></i></li>
		</ol>
	</div>
	<div class="col-xs-12 regulationbody">
		<div class="regulationcategory hideonmobile">
			<div class="catParent">Shipping</div>
			<div class="catChild" roleid="1">Order Status & Tracking</div>
			<div class="catChild" roleid="2">Shipping & Handling</div>
			<div class="catParent">Returns</div>
			<div class="catChild" roleid="3">Return Policy</div>
			<div class="catChild" roleid="4">Making a Return</div>
			<div class="catChild" roleid="5">Free Online Returns</div>
			<div class="catParent">Shopping & Payment</div>
			<div class="catChild" roleid="6">Promotion Codes</div>
			<div class="catChild" roleid="7">Payment Method</div>
			<div class="catChild" roleid="8">Canceling Or Changing An Order</div>
			<div class="catParent">Gift Services</div>
			<div class="catChild" roleid="9">Gift Services | Online & In-Store</div>
			<div class="catParent">Account Information</div>
			<div class="catChild" roleid="10">Address Book</div>
			<div class="catChild" roleid="11">Benefits of Registering</div>
			<div class="catChild" roleid="12">Forgotten Password?</div>
			<div class="catChild" roleid="13">Storing Credit Card Information</div>
			<div class="catChild" roleid="14">Order History</div>
			<div class="catChild" roleid="15">Pricing Guarantee</div>
			<div class="catChild" roleid="16">Sales Tax</div>
			<div class="catParent">About Our Products</div>
			<div class="catChild" roleid="17">Alterations</div>
			<div class="catChild" roleid="18">Product Care & Policy</div>
			<div class="catChild" roleid="19">Sizing Guidelines</div>
			<div class="catParent">Privacy Notice</div>
			<div class="catChild" roleid="20">Privacy</div>
			<div class="catParent">FAQ's</div>
			<div class="catChild" roleid="21">Frequently Asked Questions</div>
			<div class="catParent">Company Information</div>
			<div class="catChild" roleid="22">Contact Us</div>
			<div class="catChild" roleid="23">Terms of Use & Copyright</div>
			<div class="catChild" roleid="24">Operating Guidelines</div>
		</div><!-- 
	--><div class="regulationcontent">
			<div class="childregcont c1">
				<h3>Order Status & Tracking</h3>
				<p>If you have a registered account with RalphLauren.com, it is easy to check the status 
				of your orders. Simply go to Order History under My Account. From there, click on any 
				order number for its complete order details, including its tracking number. Clicking on 
				the tracking number takes you directly to the shipping carrier’s website, where you can 
				follow the progress of your shipment(s). If you do not have a RalphLauren.com account yet, 
				set one up under My Account. You also can track an order by entering your order number and 
				billing ZIP code under Track Your Order. 
				</p>
			</div>
			<div class="childregcont c2">
				<h3>Shipping & Handling</h3>
				<p><b>Order Restrictions</b></p>
				<p>Fast, 2-Business-Day, Next-Day, and Saturday delivery options are not available for 
				orders containing fragrances, lighting, rugs, and orders greater than 30 units. Lamps 
				will ship with the base and the shade as 2 separate packages. Gift cards are shipped 
				separately free of charge.</p>
				<p><b>Delivery Timing</b></p>
				<p>Pending order verification and credit approval, orders should arrive as specified above. 
				Please note that the estimated delivery date is an estimate only and is not guaranteed. 
				Your estimated delivery date will be shown during checkout and included in your order 
				confirmation email. Your items may arrive sooner than the estimated delivery date. Ralph 
				Lauren reserves the right to send packages via other shipping methods based on the 
				shipping destination as long as orders will still arrive within the arrival times 
				outlined on RalphLauren.com. When you choose the Economy delivery option, packages are 
				delivered Monday through Saturday 8 AM to 4:30 PM. Orders sent by Fast, 2-Business-Day 
				or Next-Day shipping methods are delivered Monday through Friday from 8 AM to 4:30 PM. 
				We recommend choosing Fast, 2-Business-Day or Next-Day delivery if you are shipping your 
				package to a business address. You will be asked to provide a telephone number on the 
				shipping page of checkout, which is important in the event the courier needs to arrange 
				a delivery time with you. You will also be asked to provide an email address, to which we 
				will send confirmation that your order was placed.</p>
				<p><b>Saturday Delivery</b></p>
				<p>To ensure prompt delivery, orders placed with the Saturday delivery option will be 
				shipped via the Next-Day option if placed outside of the hours specified above. We do 
				not recommend choosing Saturday delivery if you are placing your order outside of these 
				hours. Saturday delivery is not guaranteed to remote rural areas in the United States.</p>
				<p><b>U.S. Territories, P.O. Boxes, and International</b></p>
				<p>Shipments to Puerto Rico, the US Virgin Islands, Guam, the Marshall Islands, the 
				Northern Mariana Islands, Samoa, and any P.O. boxes are shipped via U.S. Postal Service 
				Priority Mail. Fast, 2- Business-Day, Next-Day, and Saturday delivery options are not 
				available for these destinations. Please allow 5 to 10 business days for delivery to 
				these destinations. We also ship to more than 20 countries outside the US. For a complete 
				list, click on the country selector on the top left of any RalphLauren.com page. 
				Additionally, there are Ralph Lauren stores all over the world. Use our store locator to 
				find the one nearest you.</p>
			</div>
			<div class="childregcont c3">
				<h3>Return Policy</h3>
				<p><b>MAKING A RETURN</b></p>
				<p>We want you to be satisfied with your RalphLauren.com purchases for free of charge. If 
				that’s not the case, we’ll gladly accept merchandise for return at no cost to you, 
				provided it:
				<ul>
					<li>Is in new condition</li>
					<li>Is accompanied by the original invoice</li>
					<li>Is returned according to the schedule below</li>
				</ul>
				</p>
				<p><b>TIMING FOR A RETURN</b></p>
				<p>Within 30 Days : 
				<ul>
				<li>Full-Price Items</li>
				<li>Sale Items</li>
				<li>Watches & Fine Jewelry</li>
				<li>Gifts You Received</li>
				</ul>
				Cannot Return : 
				<ul>
				<li>Personalized Items</li>
				<li>Gift Boxes</li>
				<li>Home Lighting</li>
				</ul>
				Watches : <br />Watches must be in perfect condition, with all protective materials, tags, 
				and stickers attached. To return watches, please call 877-639-7934.
				</p>
				<p>Purchases made between November 13, 2018 and December 24, 2018 can be returned until 
				February 1, 2019.</p>
				<p><b>ALLOW UP TO TWO WEEKS TO PROCESS YOUR RETURN</b></p>
				<p>It could take up to two weeks for us to receive and process your return. We will 
				credit your original form of payment unless otherwise specified.</p>
				<p><b>Purchases With Credit Cards</b></p>
				<p>Any credit will be refunded to the original tender.</p>
 				<p><b>Purchases With Gift Cards</b></p>
				<p>If any portion of your purchase was paid for by gift card, you will receive the full 
				value of your return on a RalphLauren.com Virtual Gift Card.</p>
				<p><b>Purchases With PayPal or PayPal Credit</b></p>
				<p>PayPal purchases will be reimbursed directly to your PayPal or PayPal Credit account.</p>
				<p><b>MAKING A RETURN TO ANY OF OUR US RALPH LAUREN STORES</b></p>
				<p>For expedited service, we encourage you to make your return by mail using the 
				postage-paid return label provided with your original package. All of our stores, 
				excluding outlet stores, accept returns; however, they must be shipped to our warehouse 
				before they can be credited.</p>
				<p>All returns must conform to the guidelines of our return policy and must be 
				accompanied by the original invoice.</p>
			</div>
			<div class="childregcont c4">
				<h3>Making a Return</h3>
				<ul>
					<li>STEP 1 : Complete the form.</li>
					<li>STEP 2 : Pack the box.</li>
					<li>STEP 3 : Drop it off.</li>
				</ul>
				<p><b>MAKING A RETURN BY MAIL</b></p>
				<p>Option 1: Use the postage-paid return label
				<ul>
					<li>Complete the return form and include it with your return</li>
					<li>Affix the return label to your box</li>
					<li>Leave your package where you normally leave outgoing mail or take it to your local 
					post office</li>
				</ul>
				Please allow up to two weeks for us to receive and process your return. Return tracking 
				information will be available up to three days after mailing. You will receive an email 
				once your return is processed. If you have lost or misplaced your return label, print a 
				new one using your order number and billing zip code. If you need further assistance, 
				please call us at 888-475-7674 or email us at CustomerAssistance@RalphLauren.com.</p>
				<p>Option 2: Pay the return postage yourself<br />The package should be taken to your 
				postal carrier and mailed to the following address:<br />
				RL E-COMMERCE / RETURNS DEPARTMENT<br />
				201 NORTH PENDLETON STREET<br />
				HIGH POINT, NC 27260<br />
				This address can only be used to return items purchased at RalphLauren.com. If you would 
				like to return items purchased elsewhere, please return the items to the original place 
				of purchase or call us at 888-475-7674 and we will be happy to assist you.</p>
				<p><b>MAKING A RETURN TO ANY OF OUR DOMESTIC U.S. RALPH LAUREN STORES</b></p>
				<p>For expedited service, we encourage you to make your return by mail using the 
				postage-paid return label provided with your original package. All of our stores, 
				excluding outlet stores, accept returns; however, they must be shipped to our warehouse 
				before they can be credited.</p>
				<p>All returns must conform to the guidelines of our Return policy and must be 
				accompanied by the original invoice.</p>
			</div>
			<div class="childregcont c5">
				<h3>Free Online Returns</h3>
				<p>Click here to start a return online.</p>
			</div>
			<div class="childregcont c6">
				<h3>Promotion Codes</h3>
				<p>Steps for redeeming a Promotional Code:
				<ul>
				<li>Enter your Promotional Code in Shopping Bag or Checkout.</li>
				<li>Click “Apply.”</li>
				</ul>
				If your Promotional Code qualifies, your deduction will be displayed in the payment 
				summary. To enter more than one promotional code, enter each code individually in the 
				promotional code box and click “Apply.”</p>
				<p>Please note that we reserve the right to cancel orders in the event that an 
				unauthorized promotion code is used.</p>
			</div>
			<div class="childregcont c7">
				<h3>Payment Method</h3>
				<p><b>Credit cards accepted:</b>
				<ul>
				<li>American Express</li>
				<li>Discover</li>
				<li>MasterCard</li>
				<li>Visa</li>
				</ul></p>
   				<p><b>Other forms of payment:</b>
				<ul>
				<li>Online gift certificates</li>
				<li>Ralph Lauren gift cards</li>
				<li>PayPal</li>
				<li>PayPal Credit</li>
				<li>Credit Card gift cards*</li>
				</ul></p>
				<p><b>We do not accept:</b>
				<ul>
				<li>CODs</li>
				<li>Layaway plan</li>
				<li>Personal checks</li>
				<li>Money orders</li>
				</ul></p>
				<p><b>Important information about payment:</b></p>
				<p>For your convenience, your credit card information can be stored in your 
				RalphLauren.com account. This is secure information and cannot be accessed by anyone 
				other than the account holder. For more information, see our Privacy Notice. For added 
				security, your billing address at RalphLauren.com must exactly match the address on file 
				at your credit card company. The processing of your order may be delayed if these 
				addresses are different.</p>
				<p>An authorization hold is placed on the card at the time of order placement, but your 
				credit card will not be charged until your order ships. For orders that ship the same day 
				as they are placed, the authorization hold may appear as a second charge. This hold is 
				released from the account within 3–5 business days depending on your bank’s specific 
				policies. If you are using a debit or check card for payment, the funds will immediately 
				be deducted from your account.</p>
				<p>* Credit Card gift cards can only be redeemed for orders less than the full amount 
				available on the gift card. Orders cannot be split between two forms of payment when 
				using a Credit Card gift card.</p>
			</div>
			<div class="childregcont c8">
				<h3>Canceling Or Changing An Order</h3>
				<p><b>CANCELING AN ORDER</b></p>
				<p>If you'd like to cancel an order, please call Customer Assistance at 888-475-7674.</p>
				<p><b>CHANGING AN ORDER</b></p>
				<p>Once your RalphLauren.com order has been processed, you cannot make changes. For 
				assistance, please call our Customer Assistance representatives at 888-475-7674.</p>
			</div>
			<div class="childregcont c9">
				<h3>Gift Services | Online & In-Store</h3>
				<p><b>Online Gift Services</b></p>
				<p>We are pleased to offer gift box packaging, personal gift messaging, physical and 
				virtual e-gift cards, gift card balance check, free returns and order status tracking.</p>
				<p><b>GIFT PACKAGING</b></p>
				<p>Gift boxes are offered with most purchases at RalphLauren.com for $5 per item. When 
				prompted at checkout, just let us know that you’d like to make your purchase a gift.</p>
				<p>Each gift will arrive elegantly presented in our signature navy gift box packaging, with tissue 
				paper, a ribbon, and a gift invoice excluding pricing information.</p>
				<p>On the bottom of each gift wrapped box is a small sticker that contains a number 
				corresponding to each item on your packaging invoice.</p>
				<p>Items must be gift packaged individually, with the exception of our pre-packaged gift 
				sets. Returning gifts by recipients is easy, since all gifts come with both a gift receipt 
				and a postage-paid return label. Gift recipients who return an item will be issued a 
				Virtual Gift Card from RalphLauren.com. Personalized items cannot be returned.</p>
 				<p><b>PERSONAL GIFT MESSAGING</b></p>
				<p>If you’ve entered a complimentary gift message at checkout, we will print your 
				personal message on a card that is packed with your gift, as well as a receipt 
				(excluding pricing information), that allows for easy returns.</p>
				<p><b>PHYSICAL & VIRTUAL GIFT CARDS</b></p>
				<p>Send one of our beautifully packaged gift cards, redeemable in stores and online at 
				RalphLauren.com, or email virtual gift cards, which will arrive within 24 hours.<br />
				Shop Gift Cards & Check Balance</p>
				<p><b>EFFORTLESS RETURNS</b></p>
				<p>If you are not entirely satisfied with your recent RalphLauren.com order, we will 
				gladly accept the return of items that are in new condition and have not been worn, 
				washed, or altered (excluding personalized items, gift boxes and home lighting.<br />
				For your convenience a postage-paid return label is provided with all orders.<br />
				Returns must be made according to the guidelines of our Return Policy<br />
				To make a return by mail Click Here<br />
				To expedite your return process, start your return online here Free Online Returns<br />
				For additional return information, please visit our Frequently Asked Questions (FAQ)
				</p>
				<p><b>TRACK YOUR ORDER</b></p>
				<p>If you are wondering where your order is, you can put in your order number, order 
				email and billing zip code to track its location. Track My Order</p>
				<p><b>In-Store Gift Services</b></p>
				<p>When visiting a Ralph Lauren store location, we are pleased to offer personal shopping, 
				signature gift packaging, complimentary alterations, returns and unique personalization 
				services.</p>
				<p><b>PERSONAL SHOPPERS</b></p>
				<p>We are happy to assist you with gift selections, and will provide you with the perfect 
				options for everyone on your list.</p>
				<p><b>SIGNATURE GIFT PACKAGING</b></p>
				<p>With our compliments, each gift will be packaged in our signature navy gift box, with 
				tissue paper and a ribbon.</p>
				<p><b>COMPLIMENTARY ALTERATIONS</b></p>
				<p>We offer complimentary alterations on full-price men’s suits and Collection apparel 
				in select Ralph Lauren stores. Click here for a list of stores that offer these services. 
				Customers must present a copy of their invoice to the store for alterations to be made. 
				Alterations may take up to 10 days.</p>
				<p>Complimentary alterations are made only on items purchased at full price (not on sale 
				items). If you have purchased a men’s suit or women’s Collection item on sale and wish 
				to have alterations made, you may visit any of the select Ralph Lauren stores that offer 
				these services and pay an alterations fee.</p>
				<p>If you have additional questions about alterations in our Ralph Lauren stores, contact 
				a Customer Assistance representative at 888-475-7674 or email us at 
				CustomerAssistance@RalphLauren.com.</p> 
				<p><b>STORE RETURNS</b></p>
				<p>Returns are accepted at all of our domestic U.S. Ralph Lauren stores for an immediate 
				credit (excluding outlet stores).</p>
				<p>However, returns must be shipped back to our warehouse before a credit can be issued 
				if the original payment method was PayPal.</p>
				<p>All returns must conform to the guidelines of our Return Policy and must be accompanied 
				by the original invoice</p>
				<p>For additional return information, please visit our Frequently Asked Questions (FAQ)</p>
				<p><b>UNIQUE PERSONALIZATION SERVICES</b></p>
				<p>Presenting unparalleled options to make each gift personal, from monogramming, 
				engraving, and embossing to the Made to Order program and the iconic Personalization 
				collection.</p>
				<p>To view our endless personalization options, Visit our Custom Shop</p>
 				<p><b>SPECIAL HOLIDAY HOURS</b></p>
				<p>Ralph Lauren will open its doors early and close late to accommodate your busy 
				schedule.</p>
				<p>Find a store near you for location and hours.</p>
				<p>For your online purchase, review our available shipping methods to determine which 
				method to select at checkout to ensure that your gifts will arrive by the holidays.</p>
			</div>
			<div class="childregcont c10">
				<h3>Address Book</h3>
				<p>When you register with RalphLauren.com, you will have the option to create your own 
				address book. This feature allows you to store not only your own billing and shipping 
				information, but also the names and addresses of people you frequently send gifts to. 
				It’s easy to add, change, or delete information from your address list at any time. 
				(Please note: The information that you store in your address list is for your use only. 
				RalphLauren.com will not contact anyone listed unless we have a specific communication 
				about a delivery.)</p>
				<p>Although we allow you to add international addresses to your address book, we do not 
				ship outside the United States. The international billing feature is designed for our 
				Canadian customers who frequently send gifts to the US or who maintain US residences. We 
				do, however, have Ralph Lauren stores all over the world. Please use Our Stores to find 
				the store nearest you.</p>
			</div>
			<div class="childregcont c11">
				<h3>Benefits of Registering</h3>
				<p>By creating an account, you can store credit card information for easier and faster 
				checkout, maintain an address book, create and share wish lists, review your order 
				history, and easily update billing and shipping information. Not only will shopping be 
				easier, but your entire experience with us will become customized over time.</p>
				<p>Create an Account</p>
			</div>
			<div class="childregcont c12">
				<h3>Forgotten Password?</h3>
				<p>If you’ve forgotten your password, you can reset it here or contact a Customer 
				Assistance representative for help at 888-475-7674.</p>
			</div>
			<div class="childregcont c13">
				<h3>Storing Credit Card Information</h3>
				<p><b>SECURELY ADD A CREDIT CARD TO YOUR ACCOUNT</b></p>
				<p>RalphLauren.com allows registered users to store a credit card number securely under 
				their account for easy shopping and checkout. All credit cards stored on RalphLauren.com 
				are encrypted for user protection so that no one—including Customer Assistance and 
				information technology—can access your credit card number.</p>
				<p>Once you make a purchase from our site, the credit card you use at checkout can be 
				stored for you in your User Profile. You will only be able to delete your credit card 
				information, not make updates to it.</p>
				<p>If you forget your password, contact a Customer Assistance representative at 
				888-475-7674, or click Forgot Password.</p>
				<p>If you have any other concerns about this function, please read the Forgotten Password 
				section of the Account Information page or our Privacy Notice.</p>
			</div>
			<div class="childregcont c14">
				<h3>Order History</h3>
				<p><b>Order Status</b></p>
				<p>We will send an email confirming the details of your order and the shipping method.</p>
				<p>You may check your order status through the Order History area of My Account or the 
				Track Your Orders option. You can also contact a Customer Assistance representative at 
				888-475-7674. Your Order History lists the current status of pending orders.</p>
				<p>Our Customer Assistance hours of operation are:<br />
				Monday–Friday, 9 AM–midnight<br />
				Saturday–Sunday, 11 AM–11 PM<br />
				Memorial Day, 11 AM–11 PM<br />
				Independence Day: 11 AM–6 PM<br />
				Labor Day, 9 AM–6 PM<br />
				Thanksgiving Day: Closed<br />
				Christmas Eve, 9 AM–7 PM<br />
				Christmas Day: Closed<br />
				New Year's Eve, 11 AM–9 PM<br />
				New Year's Day: Closed<br />
				All hours are Eastern Time</p>
				<p><b>Shipping Status</b></p>
				<p>If you have a registered account with RalphLauren.com, it is easy to check the status 
				of your orders. Simply go to Order History under Order Information. From there, click on 
				any order number for its complete order details, including its tracking number. Clicking 
				on the tracking number takes you directly to the shipping carrier's website, where you 
				can follow the progress of your shipment(s).</p>
				<p>If you do not have a RalphLauren.com account yet, set one up under My Account. You 
				also can track an order by entering your order number and billing ZIP code under Check 
				Order.</p>
				<p><b>Changes to Orders</b></p>
				<p>Once you place an order at RalphLauren.com, there is a limited period of time before 
				your order is fully processed that you will be able to cancel your order. If the cancel 
				option is available, you will find a button next to your order on the order tracking page 
				at the following link: RalphLauren.com/Orders</p>
				<p>For assistance, please call a Customer Assistance representative at 888-475-7674.</p>
			</div>
			<div class="childregcont c15">
				<h3>Pricing Guarantee</h3>
				<p>We work hard to ensure the accuracy of pricing at RalphLauren.com, but despite our 
				best efforts, pricing errors may occur. In the event that a product is listed at an 
				incorrect price due to a typographical error, RalphLauren.com shall have the right to 
				refuse or cancel orders placed for the product listed at the incorrect price, regardless 
				of whether the order has been confirmed and your credit card charged.</p>
			</div>
			<div class="childregcont c16">
				<h3>Sales Tax</h3>
				<p>Applicable sales tax will be included on all orders based on shipping address. Sales 
				tax amount is estimated until order has shipped and shipping confirmation has been sent. 
				We do not charge sales tax on the purchase of Gift Certificates; however, items paid for 
				with Gift Certificates will be taxed.</p>
				<p>The state and local governments require that we charge tax for items shipped to any 
				state where Ralph Lauren has a physical presence (such as a store, warehouse, or contact 
				center).</p>
			</div>
			<div class="childregcont c17">
				<h3>Alterations</h3>
				<p><b>ALTERATIONS IN RALPH LAUREN STORES</b></p>
				<p>We offer complimentary alterations on full-price men’s suits and Collection apparel in 
				select Ralph Lauren stores. Click here for a list of stores that offer these services. 
				Customers must present a copy of their invoice to the store for alterations to be made. 
				Alterations may take up to 10 days.</p>
				<p>Complimentary alterations are made only on items purchased at full price (not on sale 
				items). If you have purchased a men’s suit or women’s Collection item on sale and wish to 
				have alterations made, you may visit any of the select Ralph Lauren stores that offer 
				these services and pay an alterations fee.</p>
				<p>If you have additional questions about alterations in our Ralph Lauren stores, contact 
				a Customer Assistance representative at 888-475-7674 or email us at 
				CustomerAssistance@RalphLauren.com.</p>
			</div>
			<div class="childregcont c18">
				<h3>Product Care & Policy</h3>
				<p><b>PRODUCT INFORMATION AND CARE</b></p>
				<p>If you have a question on the proper care or fabric treatment of any of our items, 
				please reference the item on our website at RalphLauren.com, contact a Customer Assistance 
				representative at 888-475-7674, or email us at CustomerAssistance@RalphLauren.com.</p>
				<p><b>CASHMERE CARE</b></p>
				<p>Ralph Lauren cashmere is among the finest in the world and with proper care, your 
				sweaters can be worn for a lifetime.</p>
				<p>The qualities that make cashmere so precious also make it very delicate, so gentle 
				handling is required to keep it looking new.<br />
				Hand-wash in cold water<br />
			    Use a mild detergent<br />
			    Let soak for a minimum of 30 minutes<br />
			    Rinse well with room temperature water<br />
			    Lay flat to dry (this can take several days)<br />
			    When storing, fold and lay flat—never hang cashmere</p>
				<p><b>THE RALPH LAUREN POLICY ON FUR</b></p>
				<p><b>Ralph Lauren has a long-standing commitment not to use fur products in our apparel 
				and accessories. All furlike pieces featured in the Collection are constructed from 
				shearling.</b></p>
			</div>
			<div class="childregcont c19">
				<h3>Sizing Guidelines</h3>
				<p><b>FINDING THE RIGHT SIZE</b></p>
				<p>Please use the following guidelines along with a fabric tape measure to be certain 
				you’re ordering the right size. When taking your measurements, wear little or no 
				clothing.</p>
				<p>Waist : Measure around your natural waistline, keeping the tape comfortably loose.</p>
				<p>Hips : Stand with your heels together and measure around the fullest part of your 
				hips.</p>
				<p>Chest : Wrap the tape around the fullest part of your chest or bust, including your 
				shoulder blades, then drop your arms to your sides to measure.</p>
				<p>Neck : Measure around the middle of neck (at the Adam’s apple). Allow room for your 
				index finger to fit between the tape and your neck for a comfortable fit.</p>
				<p>Inseam : With the appropriate shoes on, measure from your crotch to your desired pant 
				length.</p>
				<p><b>SIZE CHARTS</b></p>
				<p>Because sizes across our brands vary, we offer size charts for all clothing to help 
				you find the best fit. Click on the “Size Chart” link on any item’s product page for the 
				most accurate/specific size information.</p>
			</div>
			<div class="childregcont c20">
				<h3>Privacy - Ralph Lauren Corporation</h3>
				<p><b>WEBSITE, STORE AND RESTAURANT PRIVACY NOTICE</b></p>
				<p>Last modified: August 23, 2018</p>
				<p>Ralph Lauren Corporation and its affiliates (collectively, “Ralph Lauren”) respect 
				your concerns about privacy. This Privacy Notice applies to personal information we 
				collect (i) on this website, (ii) at Ralph Lauren Stores, Ralph Lauren Factory Outlet 
				Stores (collectively, the “Stores”), and (iii) through the other channels described 
				below. Ralph Lauren is responsible for this website.</p>
				<p>The Privacy Notice describes the types of personal information we collect on this 
				site, in our Stores, and in other ways discussed below, how we may use that information 
				and with whom we may share it. The notice also describes the measures we take to protect 
				the security of the personal information. We also tell you how you can reach us to ask 
				us to (i) access, change, or not use the personal information you have provided to us; 
				(ii) withdraw any consent you previously provided to us; (iii) tell us to refrain from 
				communicating with you about our products or services; and (iv) answer any questions you 
				may have about our privacy practices.</p>
				<p>Information We Collect and How We Use It</p>
				<p>Individuals may choose to provide personal information (such as name, contact 
				details, and payment information) through this website, in our Stores, and through the 
				other channels described below. Here are the ways you may provide the information and 
				the types of information you may submit. We also tell you how we may use the 
				information.</p>
				<p><b>Website Shopping</b></p>
				<p>You may be able to purchase Ralph Lauren products and gift cards on this website. To 
				process your purchase, we ask for information such as your name, billing and shipping 
				address, telephone number, email address, and payment card information. We use this 
				information to process your purchase, confirm your order, and deliver the items to you. 
				If you give us permission, we also may use your contact information to communicate with 
				you about our products, services, and promotions.<br />If you purchase an item or gift 
				card on this site or ask us to deliver your purchase to someone else, we ask for the 
				recipient’s name, postal address, and email address. We use this information to send the 
				gift card or item to the recipient. If you ask us to do so, we also may use this 
				information to notify the recipient of your gift before it arrives.</p>
				<p><b>My Account</b></p>
				<p>You may be able to register to create an account on this website. To do so, we ask for 
				information such as your name and email address. You also may provide your age and 
				gender.<br />Through the “My Account” feature, you can Store billing and shipping 
				information so you won’t have to enter it again when you make another purchase on this 
				site. You also can Store payment card information and details of your past purchases. In 
				addition, you can save your credit card information on your account for faster checkout. 
				To use this feature, we ask you to provide your credit card information and the 
				corresponding billing address.<br />You can also use the “My Account” feature to create 
				a wish list of products you want and share your list by email with friends and family. If 
				you choose to share your list, we ask for your name and email address and the recipient’s 
				email address. This information is used only to email your list to your desired 
				recipients.<br />You can create and save an address book on your account. You can delete 
				entries in the address book at any time. We will not use or disclose the information you 
				delete from your address book.</p>
				<p><b>Surveys, Sweepstakes, and Promotions</b></p>
				<p>On this website, you may be able to participate in surveys, sweepstakes, and other 
				promotions. If you choose to participate in these promotions, we may ask you for 
				information such as your name, email address, postal address, and telephone number. We 
				use the information you provide to administer our contests, sweepstakes and other 
				promotions. We also may use the information to communicate with you about our products, 
				services, and promotions, but only if you have given us your consent to do so.</p>
				<p><b>Electronic Newsletters</b></p>
				<p>You may be able to sign up on this site to receive electronic newsletters and 
				promotional emails from Ralph Lauren, including new ideas, special offers and event 
				information. To sign up, we may ask you for information such as your name, email address, 
				postal address, telephone number, age, gender, shopping preferences and email preferences. 
				We use this information to send you communications about our products and services, 
				invitations to specials sales and events at Stores located near you, and notices of new 
				arrivals. We also use this information to tailor our newsletters to reflect your 
				preferences. You can choose at any time not to receive marketing emails from us by 
				clicking on the unsubscribe link within our marketing emails. You also may use the email 
				preferences section of the “My Account” feature to opt out of receiving marketing emails 
				or to specify which, if any, communications you would like to receive from us. There is 
				no charge to you to unsubscribe.</p>
				<p><b>Mobile Marketing</b></p>
				<p>You may be able to sign up to receive communications about our products and services 
				through your mobile device by submitting your mobile telephone number online or by texting 
				us. If you sign up for mobile communications, we may send you autodialed marketing text 
				messages about our products, services and promotions. It is your responsibility to notify 
				Ralph Lauren in the event that you disconnect your mobile number or change carriers. In 
				the U.S., you may opt out of receiving text message communications on your mobile device 
				by sending a text message with the word “STOP” to short code 89448. You will receive one 
				additional text message, confirming that your request has been received. Outside the U.S., 
				you may opt out of receiving these communications by contacting us as specified in the 
				“How to Contact Us” section below. Ralph Lauren does not charge you to unsubscribe. 
				Depending on the terms of your mobile phone contract, you may incur charges for receiving 
				and sending text messages on your mobile device. Ralph Lauren is not responsible for these 
				charges. Message frequency varies per month. Consent is not a condition of purchase. 
				Autodialed marketing messages will be sent to the mobile number you provide at opt-in. 
				Message and data rates may apply. Text HELP to 89448 for help. The wireless carriers are 
				not liable for delayed or undelivered messages.</p>
				<p><b>Blogs</b></p>
				<p>You may be able to submit comments to blogs we host on this website. You may include 
				personal information in your comment, and you also may submit your name with your comment. 
				We use your name to identify you as a commenter.</p>
				<p><b>Email to a Friend</b></p>
				<p>You may be able to share certain content from this website with your friends. To send 
				information to your friends, we ask for your friends’ and your email addresses. We use 
				this information only to fulfill your request. We do not retain the information you 
				provide in connection with this feature, except to send the requested content. We will not 
				include any other advertisements within the messages sent to your friends.</p>
				<p><b>Contact Us</b></p>
				<p>If you communicate with us through the “Contact Us” link on this site, we may ask you 
				for information such as your name, email address, ZIP code, and telephone number. We use 
				this information to respond to and communicate with you about your questions and comments.</p>
				<p><b>Other Site Features</b></p>
				<p>We offer visitors to this website various features for their information and enjoyment, 
				which we may change from time to time. We may ask you to submit certain personal 
				information so we can provide you with these features and send you communications about 
				our products, services, and promotions.</p>
				<p><b>Information Collected by Automated Means</b></p>
				<p>We obtain certain information by automated means when you visit our website, such as 
				through cookies, web beacons, and web server logs. The information we collect in this 
				manner includes the IP address of the device you use to connect to the internet, unique 
				device identifier, browser characteristics, device characteristics, operating system, 
				language preferences, referring URLs, information on actions taken on our site, dates and 
				times of website visits, and the pages accessed. By collecting this information, we learn 
				how to best tailor our website to our visitors and understand what products and services 
				you might prefer. Both we and others (such as our service providers) may collect personal 
				information about our visitors’ online activities over time and across third-party 
				websites.</p>
				<p>Like many companies, we use “cookies” on this website. Cookies are bits of text that 
				are placed on your computer’s hard drive when you visit certain websites. Cookies may 
				enhance your online experience by saving your preferences while you are visiting a 
				particular site. We use cookies, for example, to preserve the contents of your virtual 
				shopping bag between visits. We also use cookies to measure activity on the site and 
				determine which areas and features of the site are the most popular.</p>
				<p>Most browsers will tell you how to stop accepting new cookies, how to be notified when 
				you receive a new cookie, and how to disable existing cookies. Without cookies, however, 
				you may not be able to take full advantage of all our site features. For example, if you 
				turn off cookies or refuse them, you will not be able to make purchases on the site.</p>
				<p>Certain pages on this website contain “web beacons” (also known as internet tags, 
				pixel tags, and clear GIFs). Web beacons are used for internal purposes, such as to 
				obtain the IP address of the computer that downloaded the page on which the beacon 
				appears, the URL of the page on which the beacon appears, the time the page containing 
				the beacon was viewed, and the type of browser used to view the page. We also retain 
				service providers who use web beacons to assess the effectiveness of our email 
				communications and learn what pages our visitors viewed.</p>
				<p>We use information collected through these automated means for purposes such as (i) 
				customizing our users’ visits to our website, (ii) delivering content tailored to our 
				users’ interests and the manner in which our users browse our sites, and (iii) managing 
				our sites and other aspects of our business. To the extent required by applicable law, we 
				will obtain your consent before collecting information using cookies or similar automated 
				means.</p>
				<p>We also may use third-party web analytics services, such as those of Adobe Analytics 
				and Google Analytics, to measure the effectiveness of our advertising and determine how 
				visitors use this website. These service providers’ tools, including cookies and web 
				beacons, allow us to collect information such as the state and ZIP code from which our 
				visitors come and the IP addresses of our visitors’ computers. To opt out of aggregation 
				and analysis of data collected by Adobe Analytics and Google Analytics, please click here 
				and here.</p>
				<p><b>Interest-Based Advertising</b></p>
				<p>We may collect information about your online activities on our website to provide you 
				with advertising about products and services tailored to your individual interests. We 
				also may obtain information for this purpose from third-party websites on which our ads 
				are served. This section of our Privacy Notice provides details and explains how to 
				exercise your choices.</p>
				<p>You may see certain ads on other websites because we work with advertising partners 
				(including advertising networks) to engage in remarketing and retargeting activities. Our 
				advertising partners allow us to target our messaging to users through demographic, 
				interest-based, and contextual means. These partners track your online activities over 
				time and across websites by collecting information through automated means, including 
				through the use of third-party cookies, web server logs, and web beacons. They use this 
				information to show you advertisements that may be tailored to your individual interests. 
				The information our advertising partners may collect includes data about your visits to 
				websites that participate in the relevant advertising networks, such as the pages or 
				advertisements you view and the actions you take on the websites. This data collection 
				takes place both on our website and on third-party websites that participate in the ad 
				networks. This process also helps us track the effectiveness of our marketing efforts. 
				For example, we utilize Netmining’s targeted advertising services to show you our ads on 
				other websites based on your prior visits to our site and other online activity. You can 
				opt out of this feature by clicking here.</p>
				<p>You also may opt out of ad network interest-based advertising generally through the 
				Network Advertising Initiative website or by visiting 
				http://www.aboutads.info/choices/.</p>
				<p><b>Our Stores</b></p>
				<p>Here are some of the ways we collect personal information in our Stores and a 
				description of how we use the information:</p>
				<p><b>Shopping</b></p>
				<p>When you buy products or obtain services in our Stores, we ask you for information we 
				need to process your purchase or provide you with the services you requested. Depending on 
				the payment method you use, and as permitted by applicable law, we may ask for information 
				such as your name, mailing address, telephone number, driver’s license or ID number, 
				passport number, and checking account information or payment card information. We also 
				collect information about your purchase, such as the items you bought, the price of the 
				items, the payment method you used, and if the product was on sale. We use this 
				information to process your transaction.</p>
				<p><b>Customer Registry</b></p>
				<p>You may join our Customer Registry at our Stores. To join, we ask for your name, postal 
				address, email address, and mobile and home telephone numbers. You also may choose to 
				provide us with your birth date and gender. We use this information to communicate with 
				you about our products, services and promotions, and for other marketing and Customer 
				Assistance purposes.</p>
				<p><b>Customer Satisfaction Surveys</b></p>
				<p>When you buy products or obtain services in our Stores, we may ask you to complete a 
				customer satisfaction survey. If you choose to do so, you may provide information such as 
				your name, email address, telephone number, age, gender, and other demographic 
				information, in addition to your comments. Providing any of this information is optional. 
				We use the information you provide to improve our products and services, and enhance your 
				shopping experience.</p>
				<p><b>Ralph Lauren Credit Card</b></p>
				<p>At some of our Stores, you may obtain and complete an application for a Ralph Lauren 
				credit card. Ralph Lauren credit cards are offered by a third-party credit card issuer. If 
				you choose to complete an application at one of our Stores, at your request, we will 
				forward your application to the issuer or you may submit the application directly to the 
				issuer by postal mail. Ralph Lauren does not maintain any information associated with your 
				credit card application. The information you provide in connection with your credit card 
				application is managed solely pursuant to the issuer’s terms of use and privacy notice. We 
				strongly recommend that you review these documents if you apply for a Ralph Lauren credit 
				card. This Privacy Notice does not govern the collection or use of the information you 
				provide in connection with your credit card application.</p>
				<p><b>Other Ways We Collect Personal Information</b></p>
				<p>In-Store Wi-Fi Data Collection<br />In certain Stores, we use a service that analyzes 
				Wi-Fi signals from mobile devices to measure Store occupancy and to identify customer 
				traffic patterns in the Store. The service determines the location of a smartphone or 
				wireless device by observing Wi-Fi or Bluetooth signals that are broadcast from that 
				device. Individual devices are identified by a media access control (“MAC”) address that 
				was assigned to the device when it was manufactured. For the service to collect 
				information from your device, the device’s Wi-Fi must be turned on while you are in the 
				Store. If your device’s Wi-Fi function is enabled, you will receive a pop-up message that 
				asks whether you agree to allow your device’s Wi-Fi data to be collected while you are 
				connected to the in-Store Wi-Fi network. The service provider that collects information 
				from mobile devices in our Stores hashes MAC addresses to anonymize the addresses before 
				using them for analysis purposes. The service provider’s reports to us contain only 
				aggregated statistics about visitors to the Store. If you change your mind after agreeing 
				to allow your device’s Wi-Fi data to be collected, you can opt out by turning off your 
				device’s Wi-Fi function or by clicking here.</p>
				<p>QR Codes<br />We use QR codes in connection with certain promotions, contests, and 
				special events. If you scan our QR code, you will get an invitation on your mobile device 
				to join our mailing list. To sign up, we ask you for information such as your name, 
				mailing address, email address, telephone numbers, and age. We use the information to 
				communicate with you about our products, services, and promotions.</p>
				<p>Customer Assistance<br />You may call our Customer Assistance line for information 
				about our products or personal shopping advice. If you do so, we may ask you for 
				information such as your name, email address, postal address, telephone number, and, if 
				you choose to make a purchase, your payment card information. We use this information to 
				respond to your request and process your transaction.</p>
				<p><b>Department Stores</b></p>
				<p>Our products are available for sale in many department Stores. Personal information 
				collected in these department Stores is not provided to Ralph Lauren. This Privacy Notice 
				does not address the information practices of department Stores in which our products are 
				sold. Department Stores may have their own privacy notices or policies, which we strongly 
				suggest you review when you provide them with your personal information. We are not 
				responsible for the department stores’ personal information practices.</p>
				<p><b>Information We Share</b></p>
				<p>We do not sell or otherwise disclose personal information about our customers, except 
				as described here. We may share the personal information you provide to us with our 
				affiliates worldwide, licensees, and joint venture partners for the purposes described in 
				this Privacy Notice. We also may share the information with our service providers who 
				perform services on our behalf based on our instructions. These service providers are not 
				authorized by us to use or disclose the information except as necessary to perform 
				services on our behalf or comply with legal requirements.</p>
				<p>As described above, if you apply for a Ralph Lauren credit card at one of our Stores, 
				we may provide the information on your application to the credit card issuer to fulfill 
				your request.</p>
				<p>We also may disclose information about you (i) if we are required to do so by law or 
				legal process (such as a court order), (ii) in response to a request by law enforcement 
				authorities, or (iii) when we believe disclosure is necessary or appropriate to prevent 
				physical harm or financial loss or in connection with an investigation of suspected or 
				actual illegal activity. We also reserve the right to transfer personal information we 
				have about you in the event we sell or transfer all or a portion of our business or 
				assets. Should such a sale or transfer occur, we will use reasonable efforts to direct the 
				transferee to use personal information you have provided to us in a manner that is 
				consistent with our Privacy Notice. Following such a sale or transfer, you may contact the 
				entity to which we transferred your personal information with any inquiries concerning the 
				processing of that information.</p>
				<p><b>Links to Other Sites</b></p>
				<p>This website may contain links to other sites for your convenience and information. 
				These sites may be operated by companies not affiliated with Ralph Lauren. Linked sites 
				may have their own privacy policies, which we strongly suggest you review if you visit any 
				linked websites. We are not responsible for the content of any websites that are not 
				affiliated with Ralph Lauren, any use of those sites, or the privacy practices of those 
				sites.</p>
				<p><b>How We Protect Personal Information</b></p>
				<p>We maintain appropriate administrative, technical, and physical safeguards to protect 
				the personal information you provide on this website or in our Stores against accidental, 
				unlawful, or unauthorized destruction, loss, alteration, access, disclosure or use and 
				other unlawful forms of processing. We use Secure Sockets Layer (“SSL”) technology to 
				protect the transmission over the internet of your payment card data. You can check the 
				security of your connection by looking at the URL line of your browser. When accessing a 
				secure site, the first characters of the site address change from “http” to “https.” If 
				you are using a web browser or firewall that does not permit communications through secure 
				sites, you will not be able to make purchases on our site.</p>
				<p>We strive to protect the confidentiality and security of Social Security numbers 
				(“SSNs”) in our possession, custody, or control by employing reasonable safeguards seeking 
				to (i) restrict employee access to and use of SSNs, (ii) prevent unauthorized third-party 
				access to SSNs, and (iii) prohibit unlawful disclosure of SSNs.</p>
				<p><b>Transfer of Personal Information to Other Countries</b></p>
				<p>We may transfer the personal information we collect about you to recipients in 
				countries other than the country in which you originally provided it. These countries may 
				not have the same data protection laws as the country in which you provided the 
				information. When we transfer your information to recipients in other countries, we will 
				protect that information as described in this Privacy Notice and will comply with 
				applicable legal requirements providing adequate protection for the transfer of personal 
				information to recipients in countries other than the one in which you provided the 
				information.</p>
				<p>If you are located in the European Economic Area (“EEA”) or Switzerland, with respect 
				to transfers of personal information to the U.S., Ralph Lauren and its affiliate Club 
				Monaco US, LLC are certified under the EU-U.S. and the Swiss-U.S. Privacy Shield 
				frameworks developed by the U.S. Department of Commerce, and the European Commission and 
				Swiss Federal Data Protection and Information Commissioner, respectively, regarding the 
				transfer of personal information from the EEA or Switzerland to the U.S. Click here to 
				view our EU/Swiss-U.S. Privacy Shield Privacy Policy.</p>
				<p><b>Access and Correction</b></p>
				<p>Subject to applicable law, we provide you with access to personal information we have 
				retained about you and the ability to review, correct, update, block, or delete the 
				information where it is inaccurate. Subject to applicable law, we may limit or deny access 
				to personal information where providing such access is unreasonably burdensome or 
				expensive under the circumstances. Please submit your request to the address specified in 
				the “How to Contact Us” section below. Please include your name, email address, and postal 
				address in your request.</p>
				<p><b>Your Choices</b></p>
				<p>We offer you certain choices about how we communicate with you and what information we 
				collect from you. You may withdraw any consent you previously provided to us or object at 
				any time on legitimate grounds and free of charge to the processing of your personal 
				information, and we will apply your preferences going forward.</p>
				<p>You can choose not to receive marketing communications from us by clicking on the 
				unsubscribe link in our marketing emails, clicking here, or contacting us as specified in 
				the “How to Contact Us” section below. Please include your name, email address and postal 
				address if you send us your request by email or postal mail. Our website is not designed 
				to respond to “do not track” signals received from browsers.</p>
				<p>If you are a California resident, you may ask us to refrain from sharing your 
				information with certain of our affiliates and other third parties for their marketing 
				purposes.</p>
				<p>To opt out of aggregation and analysis of data collected by Adobe Analytics and Google 
				Analytics, please click here and here. To opt out of Netmining’s targeted advertising 
				services, click here. To learn how to opt out of ad network interest-based advertising 
				generally, click here or visit http://www.aboutads.info/choices/.</p>
				<p>To opt out of Wi-Fi data collection in our Stores after you have opted in, either turn 
				off your device’s Wi-Fi function or click here.</p>
				<p><b>Children’s Privacy</b></p>
				<p>This website is not directed to children under the age of 13 and we do not knowingly 
				collect personal information from children under the age of 13 on the site. If we become 
				aware that we have inadvertently received personal information from a visitor under the 
				age of 13 on this site, we will delete the information from our records.</p>
				<p><b>Updates to Our Privacy Notice</b></p>
				<p>This Privacy Notice may be updated periodically and without prior notice to you to 
				reflect changes in our personal information practices or relevant laws. We will post a 
				prominent notice on the site to notify you of any significant changes to our Privacy 
				Notice and indicate at the top of the notice when it was updated.</p>
				<p><b>How to Contact Us</b></p>
				<p>If you have any questions or comments about this Privacy Notice, or if you would like 
				us to update information we have about you or your preferences, please contact us as 
				indicated below:</p>
				<p>Ralph Lauren Corporation<br />
				Attention: Chief Privacy Officer<br />
				c/o Legal Department<br />
				625 Madison Avenue<br />
				12th Floor<br />
				New York, New York 10022</p>
				<p>Email: eu.privacy@ralphlauren.com</p>
				<p><b>Effective date: August 23, 2018</b></p>
				<p>Ralph Lauren Corporation, along with its affiliate Club Monaco US, LLC (collectively, 
				“Ralph Lauren”), respect your concerns about privacy. Ralph Lauren participates in the 
				EU-U.S. and the Swiss-U.S. Privacy Shield frameworks (“Privacy Shield”) issued by the U.S. 
				Department of Commerce. Ralph Lauren commits to comply with the Privacy Shield Principles 
				with respect to Consumer Personal Data the company receives from the EU and Switzerland 
				in reliance on the Privacy Shield. This Policy describes how Ralph Lauren implements the 
				Privacy Shield Principles for Consumer Personal Data.</p>
				<p>For purposes of this Policy:</p>
				<p>“Controller” means a person or organization which, alone or jointly with others, 
				determines the purposes and means of the processing of Personal Data.</p>
				<p>“Consumer” means any natural person who is located in the EU or Switzerland, but 
				excludes any individual acting in his or her capacity as a Worker.</p>
				<p>“EU” means the European Union and Iceland, Liechtenstein and Norway.
				</p>
				<p>“Partner” means any licensing or franchising partner or other third-party operating a 
				Store under any of the Ralph Lauren brands.</p>
				<p>“Personal Data” means any information, including Sensitive Data, that is (i) about an 
				identified or identifiable individual, (ii) received by Ralph Lauren in the U.S. from the 
				EU or Switzerland, and (iii) recorded in any form.</p>
				<p>“Privacy Shield Principles” means the Principles and Supplemental Principles of the 
				Privacy Shield.</p>
				<p>“Processor” means any natural or legal person, public authority, agency or other body 
				that processes Personal Data on behalf of a Controller.</p>
				<p>“Sensitive Data” means Personal Data specifying medical or health conditions, racial or 
				ethnic origin, political opinions, religious or philosophical beliefs, trade union 
				membership, sex life, the commission or alleged commission of any offense, any proceedings 
				for any offense committed or alleged to have been committed by the individual or the 
				disposal of such proceedings, or the sentence of any court in such proceedings.</p>
				<p>“Sites” means www.ralphlauren.com, www.clubmonaco.com, www.ralphlauren.de, 
				www.ralphlauren.fr and www.ralphlauren.co.uk here.</p>
				<p>“Store” means a Store operated under any of the Ralph Lauren brands by Ralph Lauren or 
				any of its affiliates or Partners, which Store is located in the EU or Switzerland.</p>
				<p>“Worker” means any current, former or prospective employee, temporary worker, intern, 
				contractor or other non-permanent employee of Ralph Lauren or any of its affiliates, who 
				is located in the EU or Switzerland.</p>
				<p>Ralph Lauren’s Privacy Shield certification, along with additional information about 
				the Privacy Shield, can be found at https://www.privacyshield.gov/. For more information 
				about Consumer Personal Data processing with respect to information obtained through Ralph 
				Lauren’s Sites and Stores, please visit the company’ s privacy notices on the Sites.</p>
				<p><b>Types of Personal Data Ralph Lauren Collects</b></p>
				<p>Ralph Lauren collects Personal Data directly from Consumers through the Sites and in 
				Stores. Ralph Lauren obtains, uses, discloses and otherwise processes Personal Data about 
				its Consumers to (i) process transactions they request, including e-commerce and mobile 
				transactions, (ii) improve its Stores, Sites, quality of service, and Consumers’ shopping 
				experience, (iii) send communications about its products, services, campaigns and 
				promotions, (iv) prevent and detect fraud and abuse, (v) process information or claims in 
				connection with incidents at Stores, (vi) enable service providers to perform certain 
				activities on the company’ s behalf, and (vii) comply with legal obligations, policies 
				and procedures and for internal administrative and analytics purposes.</p>
				<p>Name;<br />
			    age;<br />
			    date of birth;<br />
			    gender;<br />
			    demographic information;<br />
			    billing and shipping address;<br />
			    driver’ s license or ID number;<br />
			    passport number;<br />
			    telephone number;<br />
			    email address;<br />
			    payment card information;<br />
			    checking account information;<br />
			    shopping preferences and purchase history;<br />
			    information obtained by automated means when Consumers visit the Sites, such as through 
			    cookies, web beacons, and web server logs (including the IP address of the device the 
			    Consumer uses to connect to the internet, domain name, unique device identifier, browser 
			    characteristics, device characteristics, operating system, language preferences, 
			    referring URLs, information on actions taken on the Sites, dates and times of Site visits 
			    and the pages accessed); and any other information Consumers provide or generate on the 
			    Sites and in Stores.</p>
				<p>Ralph Lauren also may obtain and use Consumer Personal Data in other ways for which 
				the company provides specific notice at the time of collection.</p>
				<p>In addition, Ralph Lauren obtains Personal Data, such as contact information and 
				financial account information, of representatives of vendors and other business partners.
				Ralph Lauren uses this information to manage its relationships with its vendors and 
				business partners, process payments, expenses and reimbursements, and carry out the 
				company’ s obligations under its contracts with the relevant organizations.</p>
				<p>Ralph Lauren’s privacy practices regarding the processing of Consumer Personal Data 
				comply with the Privacy Shield Principles of Notice; Choice; Accountability for Onward 
				Transfer; Security; Data Integrity and Purpose Limitation; Access; and Recourse, 
				Enforcement and Liability.</p>
				<p><b>Notice</b></p>
				<p>Ralph Lauren provides information in this Policy and the privacy notices on the Sites 
				about its Consumer Personal Data practices, including the types of Personal Data Ralph 
				Lauren collects, the types of third parties to which Ralph Lauren discloses the Personal 
				Data and the purposes for doing so, the rights and choices Consumers have for limiting 
				the use and disclosure of their Personal Data, and how to contact Ralph Lauren about its 
				practices concerning Personal Data.</p>
				<p>Relevant information also may be found in notices pertaining to specific data 
				processing activities.</p>
				<p><b>Choice</b></p>
				<p>Ralph Lauren generally offers Consumers the opportunity to choose whether their 
				Personal Data may be (i) disclosed to third-party Controllers or (ii) used for a purpose 
				that is materially different from the purposes for which the information was originally 
				collected or subsequently authorized by the relevant Consumer. To the extent required by 
				the Privacy Shield Principles, Ralph Lauren obtains opt-in consent for certain uses and 
				disclosures of Sensitive Data. Consumers may contact Ralph Lauren as indicated below 
				regarding the company’ s use or disclosure of their Personal Data. Unless Ralph Lauren 
				offers Consumers an appropriate choice, the company uses Personal Data only for purposes 
				that are materially the same as those indicated in this Policy.</p>
				<p>Ralph Lauren shares Consumer Personal Data with its affiliates and subsidiaries. Ralph 
				Lauren may disclose Consumer Personal Data without offering an opportunity to opt out, 
				and may be required to disclose the Personal Data, (i) to third-party Processors the 
				company has retained to perform services on its behalf and pursuant to its instructions, 
				(ii) if it is required to do so by law or legal process, or (iii) in response to lawful 
				requests from public authorities, including to meet national security, public interest or 
				law enforcement requirements. Ralph Lauren also reserves the right to transfer Personal 
				Data in the event of an audit or if the company sells or transfers all or a portion of 
				its business or assets (including in the event of a merger, acquisition, joint venture, 
				reorganization, dissolution or liquidation).</p>
				<p><b>Accountability for Onward Transfer of Personal Data</b></p>
				<p>This Policy and the privacy notices on the Sites describe Ralph Lauren’s sharing of 
				Consumer Personal Data.</p>
				<p>Except as permitted or required by applicable law, Ralph Lauren provides Consumers 
				with an opportunity to opt out of sharing their Personal Data with third-party 
				Controllers. Ralph Lauren requires third-party Controllers to whom it discloses Consumer 
				Personal Data to contractually agree to (i) only process the Personal Data for limited 
				and specified purposes consistent with the consent provided by the relevant Consumer, 
				(ii) provide the same level of protection for Personal Data as is required by the Privacy 
				Shield Principles, and (iii) notify Ralph Lauren and cease processing Personal Data (or 
				take other reasonable and appropriate remedial steps) if the third-party Controller 
				determines that it cannot meet its obligation to provide the same level of protection for 
				Personal Data as is required by the Privacy Shield Principles.</p>
				<p>With respect to transfers of Consumer Personal Data to third-party Processors, Ralph 
				Lauren (i) enters into a contract with each relevant Processor, (ii) transfers Personal 
				Data to each such Processor only for limited and specified purposes, (iii) ascertains 
				that the Processor is obligated to provide the Personal Data with at least the same level 
				of privacy protection as is required by the Privacy Shield Principles, (iv) takes 
				reasonable and appropriate steps to ensure that the Processor effectively processes the 
				Personal Data in a manner consistent with Ralph Lauren’s obligations under the Privacy 
				Shield Principles, (v) requires the Processor to notify Ralph Lauren if the Processor 
				determines that it can no longer meet its obligation to provide the same level of 
				protection as is required by the Privacy Shield Principles, (vi) upon notice, including 
				under (v) above, takes reasonable and appropriate steps to stop and remediate 
				unauthorized processing of the Personal Data by the Processor, and (vii) provides a 
				summary or representative copy of the relevant privacy provisions of the Processor 
				contract to the Department of Commerce, upon request. Ralph Lauren remains liable under 
				the Privacy Shield Principles if the company’ s third-party Processor onward transfer 
				recipients process relevant Personal Data in a manner inconsistent with the Privacy 
				Shield Principles, unless Ralph Lauren proves that it is not responsible for the event 
				giving rise to the damage.</p>
				<p><b>Security</b></p>
				<p>Ralph Lauren takes reasonable and appropriate measures to protect Consumer Personal 
				Data from loss, misuse and unauthorized access, disclosure, alteration and destruction, 
				taking into account the risks involved in the processing and the nature of the Personal 
				Data.</p>
				<p><b>Data Integrity and Purpose Limitation</b></p>
				<p>Ralph Lauren limits the Consumer Personal Data it processes to that which is relevant 
				for the purposes of the particular processing. Ralph Lauren does not process Consumer 
				Personal Data in ways that are incompatible with the purposes for which the information 
				was collected or subsequently authorized by the relevant Consumer. In addition, to the 
				extent necessary for these purposes, Ralph Lauren takes reasonable steps to ensure that 
				the Personal Data the company processes is (i) reliable for its intended use, and (ii) 
				accurate, complete and current. In this regard, Ralph Lauren relies on its Consumers to 
				update and correct the relevant Personal Data to the extent necessary for the purposes 
				for which the information was collected or subsequently authorized. Consumers may contact 
				Ralph Lauren as indicated below to request that Ralph Lauren update or correct relevant 
				Personal Data.</p>
				<p>Subject to applicable law, Ralph Lauren retains Consumer Personal Data in a form that 
				identifies or renders identifiable the relevant Consumer only for as long as it serves a 
				purpose that is compatible with the purposes for which the Personal Data was collected or 
				subsequently authorized by the Consumer.</p>
				<p><b>Access</b></p>
				<p>Consumers generally have the right to access their Personal Data. Accordingly, where 
				appropriate, Ralph Lauren provides Consumers with reasonable access to the Personal Data 
				Ralph Lauren maintains about them. Ralph Lauren also provides a reasonable opportunity 
				for those Consumers to correct, amend or delete the information where it is inaccurate 
				or has been processed in violation of the Privacy Shield Principles, as appropriate. 
				Ralph Lauren may limit or deny access to Personal Data where the burden or expense of 
				providing access would be disproportionate to the risks to the Consumer’ s privacy in 
				the case in question, or where the rights of persons other than the Consumer would be 
				violated. Consumers may request access to their Personal Data by contacting Ralph Lauren 
				as indicated below.</p>
				<p><b>Recourse, Enforcement and Liability
				</b></p>
				<p>Ralph Lauren has mechanisms in place designed to help assure compliance with the 
				Privacy Shield Principles. Ralph Lauren conducts an annual self-assessment of its 
				Consumer Personal Data practices to verify that the attestations and assertions Ralph 
				Lauren makes about its Privacy Shield privacy practices are true and that Ralph Lauren’s 
				privacy practices have been implemented as represented and in accordance with the Privacy 
				Shield Principles.</p>
				<p>Consumers may file a complaint concerning Ralph Lauren’s processing of their Personal 
				Data. Ralph Lauren will take steps to remedy issues arising out of its alleged failure 
				to comply with the Privacy Shield Principles. Consumers may contact Ralph Lauren as 
				specified below about complaints regarding Ralph Lauren’s Consumer Personal Data 
				practices.</p>
				<p>If a Consumer’ s complaint cannot be resolved through Ralph Lauren’s internal 
				processes, Ralph Lauren will cooperate with JAMS pursuant to the JAMS Privacy Shield 
				Program, which is described on the JAMS website at 
				https://www.jamsadr.com/eu-us-privacy-shield. JAMS mediation may be commenced as provided 
				for in the JAMS rules. Following the dispute resolution process, the mediator or the 
				Consumer may refer the matter to the U.S. Federal Trade Commission, which has Privacy 
				Shield investigatory and enforcement powers over Ralph Lauren. Under certain 
				circumstances, Consumers also may be able to invoke binding arbitration to address 
				complaints about Ralph Lauren’s compliance with the Privacy Shield Principles.</p>
			</div>
			<div class="childregcont c21">
				<h3>Frequently Asked Questions</h3>
				<h4>Shipping</h4>
				<p><b>When will my order arrive?</b></p>
				<p>You can expect your items to arrive by the estimated delivery date, which is shown 
				for each item during checkout. Once your order ships you will receive an email with 
				tracking information. Your items may arrive sooner than the estimated delivery date. 
				Please also note that the estimated delivery date is an estimate only and is not 
				guaranteed.<br />Shipments to Alaska, Hawaii, Puerto Rico, US Virgin Islands, Guam, 
				Marshall Islands, Northern Mariana Islands, Samoa, and any P.O. boxes are shipped via 
				U.S. Postal Service Priority Mail. Economy, 2-Business Day, Next Day, and Saturday 
				delivery options are not available. Please allow 5 to 10 business days for delivery.<br />
				RalphLauren.com is proud to offer shipping to APO addresses. The delivery time for APO 
				shipping may vary.<br />Delivery schedule dates are estimated. Actual delivery dates may 
				vary depending on shipping locations.</p>
				<p><b>How can I track my package?</b></p>
				<p>When your order ships, you will receive a confirmation email containing tracking 
				information. You can also track your order by logging into your account; select “recent 
				orders” followed by “order details.” From there, you will be able to see if the order is 
				awaiting fulfillment or has shipped.<br />If you do not have an account, you can check 
				your order status Here by entering your order number, email address and billing zip 
				code.</p>
				<p><b>When will my order ship?</b></p>
				<p>Pending order verification and credit approval, orders placed with Economy ship within 
				1 to 3 days and orders placed with Fast Shipping will ship within 1 to 2 days. Orders 
				placed with an expedited shipping method will ship within 1 day.<br />If you do not have 
				an account, you can check your order status here by entering your order number, email 
				address, and billing zip code.</p>
				<p><b>Do you ship outside the United States?</b></p>
				<p>We ship to more than 20 countries outside the US. For a complete list, click on the 
				country selector next to the store locator at the top of any RalphLauren.com page. 
				Additionally, there are Ralph Lauren stores all over the world. Use our store locator to 
				find the one nearest you.</p>
				<p><b>By which carrier will my order be shipped?</b></p>
				<p>We use different carriers to get customers their orders as quickly and efficiently as 
				possible. Once your order has shipped, you will receive an email with the name of the 
				carrier and any tracking information.<br />Orders sent by 2-Business Day or Next Day 
				shipping methods are delivered Monday through Friday from 8 AM through 4:30 PM.<br />
				Orders placed with Saturday delivery will arrive Saturday if placed between 3:01 PM ET 
				Thursday and 3 PM ET Friday. If an order is placed after these hours, it will be shipped 
				via Next Day shipping, and will arrive on the next business day.<br />Please note: Orders 
				greater than 30 units are not eligible for expedited shipping (2-Business Day, Next Day, 
				or Saturday delivery).</p>
				<p><b>How can I ship to multiple addresses?</b></p>
				<p>Select “ship to multiple locations” during checkout. Then, choose the appropriate 
				address from the drop down list, if applicable, or enter an address by clicking “add 
				new.” You’ll then select a shipping method for each address. Please note that shipping 
				fees are applied for each address based on the method chosen.</p>
				<p><b>When shipping to multiple addresses, why is shipping being charged even though I 
				met the threshold for free shipping?</b></p>
				<p>Shipping fees are applied for each address based on the method chosen. Meeting the 
				threshold for an individual shipment will remove the fee from that portion of the order 
				only. To avoid all shipping fees, the threshold must be met for each shipment</p>
				<p><b>How long is the order processing time?</b></p>
				<p>Order processing times vary based on the shipping method. For information, click 
				Shipping & Handling</p>
				<p><b>How long does it take for a personalized item to be delivered?</b></p>
				<p>Personalized items are processed within 3-4 business days. Once shipped, the delivery 
				date is dependent upon the shipping method chosen.</p>
				<p><b>Does the invoice include the prices if I ship the item as a gift?</b></p>
				<p>No. Selecting gift packaging or gift messaging removes the prices from the invoice.</p>
				<p><b>How will the family I am shipping gifts to know which gift is for each 
				person?</b></p>
				<p>On the bottom of each gift wrapped box is a small sticker that contains a number 
				corresponding to each item on your packing invoice.</p>
				<h4>Returns & Exchanges</h4>
				<p><b>If I’m not happy with my purchase, can I return it?</b></p>
				<p>If you are not entirely satisfied with your recent RalphLauren.com order, we will 
				gladly accept the return of items that are in new condition and have not been worn, 
				washed, or altered.<br />Returns must be made according to the guidelines of our Return 
				Policy.<br />Personalized items cannot be returned.</p>
				<p><b>Is there a charge for making a return?</b></p>
				<p>No. We will provide a postage-paid return label free of charge. We also offer Free 
				Online Returns.</p>
				<p><b>How long will it take to process my return?</b></p>
				<p>Please allow up to 12 days for us to receive and process your return. You will receive 
				an email notification upon the completion of return processing. We are not able to issue 
				credit until your return package has been received and processed.</p>
			</div>
			<div class="childregcont c22">
				<h3>Contact Us</h3>
				<p><b>WE WANT TO HEAR FROM YOU...</b></p>
				<p>Chat Now</p>
				<p>It is our priority to provide you with the exceptional service you have come to expect 
				from Ralph Lauren. Your feedback is important to us.</p>
				<p>Please note that RalphLauren.com does not use your email for promotional purposes or 
				disclose it to a third party. Privacy Notice</p>
				<p>Please be sure to include in any email or postal mail request your full name, email 
				address, postal address, and any message.</p>
				<p>For your protection, please do not include your credit card number when emailing Ralph 
				Lauren.Com</p>
				<form id="contactus" name="contactus" method="post" enctype="multipart/form-data" 
				action="<?php echo $path;?>processingregulations.html">
				<div class="col-xs-12 contact">
					<label for="contactnama">Full Name *</label>
					<input name="contactnama" id="contactnama" type="text" placeholder="Enter Full Name">
				</div>
				<div class="col-xs-12 contact">
					<label for="contactemail">E-Mail *</label>
					<input name="contactemail" id="contactemail" type="text" placeholder="Enter Email">
				</div>
				<div class="col-xs-12 contact">
					<label for="contactphone">Phone Number</label>
					<input name="contactphone" id="contactphone" type="text" placeholder="Enter Phone Number">
				</div>
				<div class="col-xs-12 contact">
					<label for="contactsubject">Select a Subject *</label>
					<select name="contactsubject" id="contactsubject">
						<option value="1" selected="selected">Order Status</option>
						<option value="2">Order Changes</option>
						<option value="3">Returns</option>
						<option value="4">My Account Information</option>
						<option value="5">Shipping</option>
						<option value="6">Placing an Order</option>
						<option value="7">Payment and Billing</option>
						<option value="8">Promotions</option>
						<option value="9">Gift Certificates</option>
						<option value="10">Product Questions</option>
						<option value="11">Privacy Notice Questions</option>
						<option value="12">Product Warranties</option>
						<option value="13">International Inquiries</option>
						<option value="14">Other</option>
					</select>
				</div>
				<div class="col-xs-12 contact">
					<label for="contactinvoice">Order Number</label>
					<input name="contactinvoice" id="contactinvoice" type="text" placeholder="Enter Invoice Order Number">
				</div>
				<div class="col-xs-12 contact">
					<label for="contactmessage">Your Message *</label>
					<textarea name="contactmessage" id="contactmessage" rows="5" placeholder="Your Message"></textarea>
				</div>
				<div class="col-xs-12 contact">
					<label for="contactsec">Robot Protection *</label>
					<input name="contactsec" class="secprint" type="text" placeholder="<?php echo $secprint;?>">
					<input name="contacttoken" class="sectoken" type="hidden" value="<?php echo $sectoken;?>">
				</div>
				<div class="col-xs-12 contact divcontactsend">
					<div class="contactsend" id="contactsend" onclick="contactSend(); return false;">SEND</div>
				</div>
				</form>
				<p>* (Mandatory Field)</p>
			</div>
			<div class="childregcont c23">
				<h3>Terms of Use & Copyright</h3>
				<p>Your Use of This Site Is Governed By An Agreement To Arbitrate Disputes. You Agree To 
				Submit All Disputes Concerning This Agreement, Your Use of This Web site, and Any 
				Products or Services You Purchase Through the Web site To Confidential, Binding 
				Individual Arbitration, And You Agree To Give Up Your Right To Represent Other Persons In 
				A Class Action or Similar Proceeding.</p>
				<p><b>Terms of Use</b></p>
				<p>Please carefully read the following Terms of Use before using the RalphLauren.com 
				website (the “Site”). By accessing this Site, you agree to be bound by these Terms of 
				Use. These Terms of Use may be updated from time to time. Accordingly, you should check 
				the date of the Terms of Use (which appear at the end of this document) and review any 
				changes since the last version. If at any time you do not agree to these Terms of Use, 
				please do not use this Site.<br />This Site is operated by Ralph Lauren Media LLC (“Ralph 
				Lauren”). Throughout the Site, the terms “we,” “us,” and “our” refer to Ralph Lauren. 
				Ralph Lauren offers this Site, including all information, tools and services available 
				from this Site, to you, the user, conditioned upon your acceptance of all the terms, 
				conditions, policies, and notices stated here. Your continued use of this Site 
				constitutes your agreement to these Terms of Use.</p>
				<p><b>Accuracy, Completeness, and Timeliness of Information on This Site</b></p>
				<p>We are not responsible if information made available on this Site is not accurate, 
				complete, or current. The material on this Site is provided for general information only 
				and should not be relied upon or used as the sole basis for making decisions without 
				consulting primary, more accurate, more complete, or more timely sources of information. 
				Any reliance on the material on this Site is at your own risk. This Site may contain 
				certain historical information. Historical information is not necessarily current and is 
				provided for your reference only. We reserve the right to modify the contents of this 
				Site at any time, but we have no obligation to update any information on the Site. You 
				agree that it is your responsibility to monitor changes to the Site.</p>
				<p><b>Orders, Prohibition on Reselling, and Price</b></p>
				<p>The information on this Site does not constitute a binding offer to sell products 
				described on the Site or to make such products available in your area. We reserve the 
				right at any time after receipt of your order to accept or decline your order, or any 
				portion thereof, in our sole discretion, even after your receipt of an order confirmation 
				or after your credit card has been charged. You may not purchase any item from this Site 
				for resale by you or any other person, and you may not resell any item purchased from 
				this Site. The prices displayed on the Site are quoted in US dollars and are intended to 
				be valid and effective only in the United States. In the event a product is listed at an 
				incorrect price, we have the right to refuse or cancel orders placed for the product 
				listed at the incorrect price, regardless of whether the order has been confirmed or your 
				credit card charged. If your credit card has already been charged for the purchase and 
				your order is canceled, we will issue a credit to your credit card account.</p>
				<p><b>Product Information</b></p>
				<p>Select product can be found in our full price stores in the United States while 
				supplies last. In some cases, merchandise displayed for sale on the Site may not be 
				available in stores. The particular technical specifications and settings of your 
				computer and its display could affect the accuracy of its display of the colors of 
				products offered on the Site.</p>
				<p><b>Use of Material on the Site</b></p>
				<p>All content on this Site (including, without limitation, text, design, graphics, 
				logos, icons, images, audio clips, downloads, interfaces, code, and software, as well as 
				the selection and arrangement thereof) is the exclusive property of and owned by Ralph 
				Lauren, its licensors, or its content providers and is protected by copyright, trademark, 
				and other applicable laws. You may access, copy, download, and print the material 
				contained on the Site for your personal and non-commercial use, provided you do not 
				modify or delete any copyright, trademark, or other proprietary notice that appears on 
				the material you access, copy, download, or print. Any other use of content on the Site, 
				including but not limited to the modification, distribution, transmission, performance, 
				broadcast, publication, uploading, licensing, reverse engineering, transfer or sale of, 
				or the creation of derivative works from, any material, information, software, products 
				or services obtained from the Site, or use of the Site for purposes competitive to Ralph 
				Lauren, is expressly prohibited. Ralph Lauren reserves the right to refuse or cancel any 
				person’s registration for this Site, remove any person from this Site, or prohibit any 
				person from using this Site for any reason whatsoever. Ralph Lauren, or its licensors or 
				content providers, retain full and complete title to the material provided on the Site, 
				including all associated intellectual property rights, and provide this material to you 
				under a license that is revocable at any time in Ralph Lauren’s sole discretion. Ralph 
				Lauren neither warrants nor represents that your use of materials on this Site will not 
				infringe rights of third parties not affiliated with Ralph Lauren.<br />You may not use 
				contact information provided on the Site for unauthorized purposes, including marketing. 
				You may not use any hardware or software intended to damage or interfere with the proper 
				working of the Site or to surreptitiously intercept any system, data, or personal 
				information from the Site. You agree not to interrupt or attempt to interrupt the 
				operation of the Site in any way. Ralph Lauren reserves the right, in its sole 
				discretion, to limit or terminate your access to or use of the Site at any time without 
				notice. You are personally liable for any orders that you place or charges or other 
				liabilities that you incur prior to termination. Termination of your access or use will 
				not waive or affect any other right or relief to which Ralph Lauren may be entitled, at 
				law or in equity.</p>
				<p><b>Material You Submit</b></p>
				<p>You acknowledge that you are responsible for any material you may submit via the Site, 
				including the legality, reliability, appropriateness, originality, and copyright of any 
				such material. You may not upload to, distribute, or otherwise publish through this Site 
				any content that (i) is false, fraudulent, libelous, defamatory, obscene, threatening, 
				invasive of privacy or publicity rights, infringing on intellectual property rights, 
				abusive, illegal, or otherwise objectionable; (ii) may constitute or encourage a criminal 
				offense, violate the rights of any party, or otherwise give rise to liability or violate 
				any law; or (iii) may contain software viruses, political campaigning, chain letters, 
				mass mailings, or any form of spam. You may not use a false email address or other 
				identifying information, impersonate any person or entity, or otherwise mislead as to the 
				origin of any content. You may not upload commercial content onto the Site.<br />If you 
				do submit material, and unless we indicate otherwise, you grant Ralph Lauren and its 
				affiliates a nonexclusive, royalty-free, perpetual, irrevocable, and fully sublicensable 
				right to use, reproduce, modify, adapt, publish, translate, create derivative works from, 
				distribute, and display such material throughout the world in any media. You grant Ralph 
				Lauren and its affiliates the right to use the name you submit in connection with such 
				material, if they so choose. All personal information provided via this Site will be 
				handled in accordance with the Site’s online Privacy Notice. You represent and warrant 
				that you own or otherwise control all the rights to the content you post; that the 
				content is accurate; that use of the content you supply does not violate any provision 
				herein and will not cause injury to any person or entity; and that you will indemnify 
				Ralph Lauren for all claims resulting from content you supply.</p>
				<p><b>Conduct on the Site</b></p>
				<p>Some features that may be available on this Site require registration. By registering 
				at and in consideration of your use of the Site, you agree to provide true, accurate, 
				current, and complete information about yourself.<br />Some features on this Site require 
				use of a password. You are responsible for protecting your password. You agree that you 
				will be responsible for any and all statements made, and acts or omissions that occur, 
				through the use of your password. If you have any reason to believe or become aware of 
				any loss, theft, or unauthorized use of your password, notify Ralph Lauren immediately. 
				Ralph Lauren may assume that any communications Ralph Lauren receives under your password 
				have been made by you unless Ralph Lauren receives notice otherwise.<br />You or third 
				parties acting on your behalf are not allowed to frame this Site or use our proprietary 
				marks as meta tags, without our written consent. These marks include, but are not limited 
				to, “Polo,” “Ralph Lauren,” “Polo Ralph Lauren,” and “Lauren.” You may not use frames or 
				utilize framing techniques or technology to enclose any content included on the Site 
				without Ralph Lauren’s express written consent. Further, you may not utilize any Site 
				content in any meta tags or any other “hidden text” techniques or technologies without 
				Ralph Lauren’s express written consent.</p>
				<p><b>Links</b></p>
				<p>This Site may contain links to other websites, some of which are operated by Ralph 
				Lauren or its affiliates and others of which are operated by third parties. These links 
				are provided as a convenience to you and as an additional avenue of access to the 
				information contained therein. We have not necessarily reviewed all the information on 
				those other sites and are not responsible for the content of those or any other sites or 
				any products or services that may be offered through those or any other sites. Inclusion 
				of links to other sites should not be viewed as an endorsement of the content of linked 
				sites. Different terms and conditions may apply to your use of any linked Sites. Ralph 
				Lauren is not responsible for any losses, damages, or other liabilities incurred as a 
				result of your use of any linked sites.</p>
				<p><b>Trademarks and Copyrights</b></p>
				<p>Trademarks, logos, and service marks displayed on this Site are registered and 
				unregistered trademarks of Ralph Lauren Media LLC and Ralph Lauren Corporation, their 
				licensors or content providers, or other third parties. All of these trademarks, logos, 
				and service marks are the property of their respective owners. Nothing on this Site shall 
				be construed as granting, by implication, estoppel, or otherwise, any license or right 
				to use any trademark, logo, or service mark displayed on the Site without the owner’s 
				prior written permission, except as otherwise described herein. Ralph Lauren reserves 
				all rights not expressly granted in and to the Site and its content. This Site and all 
				of its content, including but not limited to text, design, graphics, interfaces, and 
				code, and the selection and arrangement thereof, is protected as a compilation under the 
				copyright laws of the United States and other countries.</p>
				<p><b>Infringement Notice</b></p>
				<p>We respect the intellectual property rights of others and request that you do the 
				same. If you think your work has been copied in a manner that constitutes copyright 
				infringement, you may notify our Legal Department, who can be reached at:<br />
				625 Madison Avenue<br />
				New York, New York<br />
				Or by phone at: 212-705-8200</p>
				<p>In order for us to more effectively assist you, the notification must include ALL of 
				the following:
				<ul>
				<li>A physical or electronic signature of the copyright owner or the person authorized 
				to act on the owner’s behalf;</li>
				<li>A description of the copyrighted work you claim has been infringed;</li>
				<li>Information reasonably sufficient to locate the material in question on the Site;</li>
				<li>Your name, address, telephone number, email address, and all other information 
				reasonably sufficient to permit Ralph Lauren to contact you;</li>
				<li>A statement by you that you have a good faith belief that the disputed use is not 
				authorized by the copyright owner, its agent, or the law; and</li>
				<li>A statement by you, made under penalty of perjury, that the above information in 
				your notice is accurate and that you are the copyright owner or are authorized to act 
				on behalf of the copyright owner.</li>
				</ul>Ralph Lauren is under no obligation to post, forward, transmit, distribute, or 
				otherwise provide any material available on this Site, including material you provide to 
				us, and so we have an absolute right to remove any material from the Site in our sole 
				discretion at any time.</p>
				<p><b>DISCLAIMERS</b></p>
				<p>YOUR USE OF THIS SITE IS AT YOUR SOLE RISK. THE SITE IS PROVIDED ON AN “AS IS” AND “AS 
				AVAILABLE” BASIS. WE RESERVE THE RIGHT TO RESTRICT OR TERMINATE YOUR ACCESS TO THE SITE 
				OR ANY FEATURE OR PART THEREOF AT ANY TIME. RALPH LAUREN EXPRESSLY DISCLAIMS ALL 
				WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
				IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND ANY 
				WARRANTIES THAT MATERIALS ON THE SITE ARE NONINFRINGING, AS WELL AS WARRANTIES IMPLIED 
				FROM A COURSE OF PERFORMANCE OR COURSE OF DEALING; THAT ACCESS TO THE SITE WILL BE 
				UNINTERRUPTED OR ERROR-FREE; THAT THE SITE WILL BE SECURE; THAT THE SITE OR THE SERVER 
				THAT MAKES THE SITE AVAILABLE WILL BE VIRUS-FREE; OR THAT INFORMATION ON THE SITE WILL 
				BE COMPLETE, ACCURATE, OR TIMELY. IF YOU DOWNLOAD ANY MATERIALS FROM THIS SITE, YOU DO 
				SO AT YOUR OWN DISCRETION AND RISK. YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO 
				YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH 
				MATERIALS. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM RALPH 
				LAUREN OR THROUGH OR FROM THE SITE SHALL CREATE ANY WARRANTY OF ANY KIND. RALPH LAUREN 
				DOES NOT MAKE ANY WARRANTIES OR REPRESENTATIONS REGARDING THE USE OF THE MATERIALS ON 
				THIS SITE IN TERMS OF THEIR COMPLETENESS, CORRECTNESS, ACCURACY, ADEQUACY, USEFULNESS, 
				TIMELINESS, RELIABILITY, OR OTHERWISE.<br />IN CERTAIN JURISDICTIONS, THE LAW MAY NOT 
				PERMIT THE DISCLAIMER OF WARRANTIES, SO THE ABOVE DISCLAIMER MAY NOT APPLY TO YOU.</p>
				<p><b>LIMITATION OF LIABILITY</b></p>
				<p>YOU ACKNOWLEDGE AND AGREE THAT YOU ASSUME FULL RESPONSIBILITY FOR YOUR USE OF THE 
				SITE. YOU ACKNOWLEDGE AND AGREE THAT ANY INFORMATION YOU SEND OR RECEIVE DURING YOUR USE 
				OF THE SITE MAY NOT BE SECURE AND MAY BE INTERCEPTED BY UNAUTHORIZED PARTIES. YOU 
				ACKNOWLEDGE AND AGREE THAT YOUR USE OF THE SITE IS AT YOUR OWN RISK AND THAT THE SITE IS 
				MADE AVAILABLE TO YOU AT NO CHARGE. RECOGNIZING SUCH, YOU ACKNOWLEDGE AND AGREE THAT, TO 
				THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, NEITHER RALPH LAUREN NOR ITS AFFILIATES, 
				SUPPLIERS, OR THIRD-PARTY CONTENT PROVIDERS WILL BE LIABLE FOR ANY DIRECT, INDIRECT, 
				PUNITIVE, EXEMPLARY, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR OTHER DAMAGES ARISING OUT OF 
				OR IN ANY WAY RELATED TO THE SITE, OR ANY OTHER SITE YOU ACCESS THROUGH A LINK FROM THIS 
				SITE OR FROM ANY ACTIONS WE TAKE OR FAIL TO TAKE AS A RESULT OF COMMUNICATIONS YOU SEND 
				TO US, OR THE DELAY OR INABILITY TO USE THE SITE, OR FOR ANY INFORMATION, PRODUCTS, OR 
				SERVICES ADVERTISED IN OR OBTAINED THROUGH THE SITE, RALPH LAUREN’S REMOVAL OR DELETION 
				OF ANY MATERIALS SUBMITTED OR POSTED ON ITS SITE, OR OTHERWISE ARISING OUT OF THE USE OF 
				THE SITE, WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY, OR OTHERWISE, EVEN IF RALPH 
				LAUREN, ITS AFFILIATES, OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF 
				DAMAGES. THIS DISCLAIMER APPLIES, WITHOUT LIMITATION, TO ANY DAMAGES OR INJURY ARISING 
				FROM ANY FAILURE OF PERFORMANCE, ERROR, OMISSION, INTERRUPTION, DELETION, DEFECTS, DELAY 
				IN OPERATION OR TRANSMISSION, COMPUTER VIRUSES, FILE CORRUPTION, COMMUNICATION-LINE 
				FAILURE, NETWORK OR SYSTEM OUTAGE, YOUR LOSS OF PROFITS, OR THEFT, DESTRUCTION, 
				UNAUTHORIZED ACCESS TO, ALTERATION OF, LOSS OR USE OF ANY RECORD OR DATA, AND ANY OTHER 
				TANGIBLE OR INTANGIBLE LOSS. YOU SPECIFICALLY ACKNOWLEDGE AND AGREE THAT NEITHER RALPH 
				LAUREN NOR ITS SUPPLIERS SHALL BE LIABLE FOR ANY DEFAMATORY, OFFENSIVE, OR ILLEGAL 
				CONDUCT OF ANY USER OF THE SITE. YOUR SOLE AND EXCLUSIVE REMEDY FOR ANY OF THE ABOVE 
				CLAIMS OR ANY DISPUTE WITH RALPH LAUREN IS TO DISCONTINUE YOUR USE OF THE SITE. YOU AND 
				RALPH LAUREN AGREE THAT ANY CAUSE OF ACTION ARISING OUT OF OR RELATED TO THE SITE MUST 
				COMMENCE WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES OR THE CAUSE OF ACTION IS 
				PERMANENTLY BARRED. BECAUSE SOME JURISDICTIONS DO NOT ALLOW LIMITATIONS ON HOW LONG AN 
				IMPLIED WARRANTY LASTS, OR THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR 
				INCIDENTAL DAMAGES, ALL OR A PORTION OF THE ABOVE LIMITATION MAY NOT APPLY TO YOU.</p>
				<p><b>Indemnification</b></p>
				<p>You agree to indemnify, defend, and hold harmless Ralph Lauren and its affiliates and 
				their officers, directors, employees, contractors, agents, licensors, service providers, 
				subcontractors, and suppliers from and against any and all losses, liabilities, expenses,
				 damages, and costs, including reasonable attorneys’ fees and court costs, arising or 
				 resulting from your use of the Site and any violation of these Terms of Use. If you 
				 cause a technical disruption of the Site or the systems transmitting the Site to you or 
				 others, you agree to be responsible for any and all losses, liabilities, expenses, 
				 damages, and costs, including reasonable attorneys’ fees and court costs, arising or 
				 resulting from that disruption. Ralph Lauren reserves the right, at its own expense, to 
				 assume exclusive defense and control of any matter otherwise subject to indemnification 
				 by you and, in such case, you agree to cooperate with Ralph Lauren in the defense of 
				 such matter.</p>
				<p><b>Applicable Law</b></p>
				<p>The laws of the State of New York govern these Terms of Use and your use of the Site. 
				We recognize that it is possible for you to obtain access to this Site from any 
				jurisdiction in the world, but we have no practical ability to prevent such access. This 
				Site has been designed to comply with the laws of the State of New York and of the United 
				States. If any material on this Site, or your use of the Site, is contrary to the laws of 
				the place where you are when you access it, the Site is not intended for you, and we ask 
				you not to use the Site. You are responsible for informing yourself of the laws of your 
				jurisdiction and complying with them.</p>
				<p><b>Disputes</b></p>
				<p>Ralph Lauren, including its subsidiaries, affiliates, agents, employees, predecessors 
				in interest, successors and assigns, and you agree that any and all disputes or claims 
				relating in any way to these Terms of Use, your use of the Site, or to any products or 
				services sold or distributed by us or through the Site, including disputes concerning 
				the scope or applicability of this agreement to arbitration (“Disputes”), as well as 
				questions as to the arbitrability of any and all Disputes, and even if the events giving 
				rise to Disputes occurred before this Terms of Use became effective, will be resolved in 
				a confidential, individual and fair arbitration process, and not in court. Each of us 
				agrees to give up our right to sue in court (except small claims court), our right to 
				have our claims heard by a jury, and our right to represent, in a class action or 
				otherwise, anyone but ourselves.<br />The only exceptions to this arbitration agreement 
				are that (i) each of you and we retain the right to sue in small claims court and (ii) 
				each of you and we may bring suit in court against the other to enjoin infringement or 
				other misuse of intellectual property rights.<br />This agreement evidences a transaction 
				in interstate commerce, and thus the Federal Arbitration Act (“FAA”), 9 U.S.C. §§ 1-16, 
				and federal arbitration law apply and govern the interpretation and enforcement of this 
				Terms of Use.<br />There is no judge or jury in arbitration, and court review of an 
				arbitration award is limited. However, an arbitrator can award on an individual basis 
				the same damages and relief as a court (including injunctive and declaratory relief or 
				statutory damages), and must follow the terms of these Terms of Use as a court would.</p>
				<p><b>Commencing DIsputes</b></p>
				<p>To begin an arbitration proceeding, you must send a letter requesting arbitration and 
				describing your claim to our registered agent Corporation Service Company, 2711 
				Centerville Road, Wilmington, DE 19808. During the first 45 days after we receive your 
				notice (the “Pre-Arbitration Period”), we may try to reach a settlement of the dispute. 
				If we do not resolve the dispute ourselves within the Pre-Arbitration Period, you may 
				commence arbitration. The arbitration will be conducted by Judicial Arbitration and 
				Mediation Services, Inc. (“JAMS”), pursuant to the JAMS Streamlined Arbitration Rules & 
				Procedures effective July 1, 2014 (the “JAMS Rules”), as modified by this Agreement. The 
				JAMS Rules are available at at http://www.jamsadr.com/rules-streamlined-arbitration/ 
				www.adr.org or by calling 1-800-352-5267.<br />Arbitration firms ordinarily charge fees 
				to both sides to conduct arbitrations. The claimant in arbitration heard by JAMS has to 
				pay $250 to start a case, whether the claimant wins or loses. In the event you commence 
				arbitration for an amount less than $10,000, after we receive notice that you have done 
				so, we will reimburse you for your payment of this filing fee and we will pay JAMS any 
				case management fees associated with the arbitration and the professional fees for the 
				arbitrator’s services, pursuant to the JAMS Rules. We will not seek to recover the 
				filing fee we reimbursed to you unless the arbitrator determines that you brought your 
				claim frivolously or for an improper purpose.<br />Each of us may incur attorneys’ fees 
				during the arbitration. Each of us will bear our own attorneys’ fees, except in the 
				following circumstances. If applicable law permits a prevailing party to recover 
				attorneys’ fees, the prevailing party may seek fees as applicable law permits. If 
				applicable law does not permit a prevailing party to recover its attorneys’ fees, but 
				you prevail in the arbitration and win an award at least 25% greater than our highest 
				pre-arbitration settlement offer, then we will pay your reasonable attorneys’ fees for 
				time reasonably expended at rates that prevail for attorneys in your home county, in an 
				amount not to exceed the greater of $10,000 or 20% of the arbitrator’s damages award to 
				you. If we prevail in the arbitration, we will seek to recover our reasonable attorneys’ 
				fees and reimbursement of arbitration costs only if applicable law permits a prevailing 
				party to see fees or if the arbitrator finds that you brought a claim frivolously or for 
				an improper purpose and applicable law does not preclude us from seeking our fees and 
				costs.</p>
				<p><b>The Arbitration</b></p>
				<p>The arbitration will be conducted by one neutral arbitrator selected with the 
				participation and involvement of both parties pursuant to the JAMS Rules.<br />If your 
				claim is for $10,000 or less, we agree, pursuant to the JAMS Rules, that the dispute 
				should be resolved without an oral hearing, unless the arbitrator requests otherwise. Any 
				in-person hearing for a claim of less than $10,000 must be conducted in New York, New 
				York. If your claim is for more than $10,000, the manner and place of the hearing will 
				be determined in accordance with the JAMS Rules.<br />Regardless of how the arbitration 
				proceeds, the arbitrator shall issue a reasoned written decision sufficient to explain 
				his or her findings and conclusions. The arbitrator’s decision and award are final and 
				binding, subject only to the limited court review permitted under the FAA, and judgment 
				on the award may be entered in any court of competent jurisdiction.</p>
				<p><b>Class Action Waiver: No Consolidation of Arbitral Claims</b></p>
				<p>We agree that the arbitrator may award declaratory or injunctive relief only in favor 
				of the individual party seeking relief and only to the extent necessary to provide 
				relief warranted by that party's individual claim. The arbitrator may not order us to 
				pay any monies to or take any actions with respect to persons other than you, unless we 
				explicitly consent in advance, after an arbitrator is selected, to permit the arbitrator 
				to enter such an order. YOU AND WE AGREE THAT EACH MAY BRING CLAIMS AGAINST THE OTHER 
				ONLY IN YOUR OR OUR INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY 
				CLASS ACTION, REPRESENTATIVE OR MULTI-CLAIMANT PROCEEDING. Further, unless we both agree 
				in writing, the arbitrator may not consolidate other persons’ claims with yours or ours, 
				and may not otherwise preside over any form of a representative, multi-claimant or class 
				arbitration proceeding. If this specific provision is found to be unenforceable, then 
				the entirety of this arbitration provision shall be null and void.<br />If for any reason 
				a claim proceeds in court rather than in arbitration, we each waive any right to a jury 
				trial and we each irrevocably consent to the exclusive jurisdiction of the courts 
				located in New York County, New York.</p>
				<p><b>Changes to These Terms of Use</b></p>
				<p>We reserve the right, in our sole discretion, to change these Terms of Use at any 
				time by posting revised terms on the Site. It is your responsibility to check 
				periodically for any changes we may make to these Terms of Use. Your continued use of 
				this Site following the posting of changes to these Terms of Use or other policies means 
				you accept the changes.<br />In the event we make material changes to the Terms of Use, 
				notice of these changes will be posted on the homepage of this website and the revised 
				Terms of Use will take effect 30 days after their publication on this Site.</p>
				<p><b>Entire Agreement and Admissibility</b></p>
				<p>This agreement and any policies or operating rules posted on this Site constitute the 
				entire agreement and understanding between you and Ralph Lauren with respect to the 
				subject matter thereof and supersede all prior or contemporaneous communications and 
				proposals, whether oral or written, between the parties with respect to such subject 
				matter. A printed version of these Terms of Use shall be admissible in judicial or 
				administrative proceedings based on or relating to use of the Site to the same extent and 
				subject to the same conditions as other business documents and records originally 
				generated and maintained in printed form.</p>
				<p><b>Severability</b></p>
				<p>If any provision of this agreement is unlawful, void, or unenforceable, the remaining 
				provisions of the agreement will remain in place.</p>
				<p><b>How to Contact Us</b></p>
				<p>If you have questions or comments about these Terms of Use or this Site, please 
				contact a Customer Assistance representative at 888-475-7674 or email us at 
				CustomerAssistance@RalphLauren.com.</p>
			</div>
			<div class="childregcont c24">
				<h3>Operating Guidelines</h3>
				<p>At Ralph Lauren Corporation, we are committed to producing high quality products 
				responsibly. We require our suppliers acrossall regions of the world to meet our 
				corporate, social and regulatory standards. We focus on strategic collaboration and 
				partnerships with our suppliers to increase information sharing and to achieve meaningful 
				progress on sourcing efficiencies, human rights and a sustainable supply chain.</p>
				<p>RALPH LAUREN and its affiliates and subsidiaries (including CHAPS and CLUB MONACO) 
				are dedicated to conducting our operations throughout the world on principles of 
				ethicalbusiness practices and the recognition of the dignity of their employees. We 
				expect our business partners and their suppliers, productsuppliers, material and trim 
				suppliers, serviceproviders, and subcontractors to respect and adhere to our guidelines 
				and to all applicablelaws and regulations in the operation of their business. Our 
				business partners and their affiliates must be transparent in all recordkeeping and 
				embrace a fundamental effort toward operational efficiencies, environmental 
				sustainability and continuous improvement.</p>
				<p><b>LEGAL AND ETHICAL STANDARDS</b></p>
				<p>Our business relationship is built on a mutual respect for and adherence to all 
				legalrequirements and the highest ethical standards. We expect our business partners to 
				observe all applicable international standards, andnational and local laws and 
				regulations while operating their businesses.</p>
				<p><b>WAGES, BENEFITS, WORKING HOURS, AND TRANSPARENT RECORD KEEPING</b></p>
				<p>Our business partners must comply withall lawsregulating local wages, overtime 
				compensation, and legally mandated benefits. Wage and benefit policies must be 
				consistentwith prevailing national standards. Under ordinary business circumstances, 
				employees must not be required to work excessiveworking hours per week including 
				overtime and have the option of at least one day off in seven. We expect that all 
				recordkeepingwill be accurate and transparent at all times. We have zero tolerance for 
				non-transparent record keeping.</p>
				<p><b>HEALTH AND SAFETY</b></p>
				<p>Our business partners must ensure that their employees are provided a safe and 
				healthy workenvironment, and are not subject to unsanitary or hazardous conditions.</p>
				<p><b>ENVIRONMENTAL SUSTAINABILITY</b></p>
				<p>Our business partners must embrace a fundamental concern for environmental 
				protectionand conduct their operations consistent with local and internationally 
				recognized environmental laws and best practices. Theymustalso operate and source in 
				such a manner that respects the environment and local communities, with particular 
				concern to avoiddeforestation, pollution, habitat loss, and rising greenhouse gas 
				emissions.</p>
				<p><b>CHILD LABOR</b></p>
				<p>Our business partners must not use child labor, defined as employees under the age of 
				16.</p>
				<p><b>FORCED OR BONDED LABOR</b></p>
				<p>Our business partners will not work with or arrange for purchase of any materials or 
				services thatsupports or utilizes forced labor, bonded labor or labor obtained through 
				human trafficking, coercion or slavery.</p>
				<p><b>DISCIPLINARY PRACTICES</b></p>
				<p>Our business partners will not employ or conduct any business activity with partners 
				who employanyform of physical or mental coercion, or punishment or monetary fines against 
				employees.DISCRIMINATION AND HARASSMENT - Our business partners will not practice any 
				form of discrimination or harassment inhiringand employment, based on race, color, 
				religion, sex, gender, sexual orientation, age, marital status, disability, and ethnic 
				ornational origin.</p>
				<p><b>FREEDOM OF ASSOCIATION</b></p>
				<p>Our business partners should respect the legal rights of employees to freely, and 
				withoutharassment, participate in organizations of their choice.</p>
				<p><b>SUBCONTRACTING</b></p>
				<p>Our business partners may only subcontract to previously approved suppliers for 
				manufacturing or servicesaccording to our corporate requirements. All subcontracted
				suppliers must meet the same criteria as our direct contractedproduct andservice 
				suppliers. We have a zero tolerance for unauthorized subcontracting of our products.</p>
				<p><b>ANIMAL SOURCING PRINCIPLES</b></p>
				<p>Our business partners must share our commitment to principles, practices, and 
				regulationsthat require animals in our supply chain to be treated with care and 
				respect.</p>
				<p><b>CUSTOMS COMPLIANCE AND PRODUCT SAFETY</b></p>
				<p>Our business partners must comply with applicable international customs’laws and 
				regulations, including but not limited to, participation in Customs-Trade Partnership 
				Against Terrorism Programs (C-TPAT)for all products and services as well as other 
				international security regulations. In addition, all products must specificallymeet 
				allConsumer Product Safety Commission regulations and requirements and all international 
				regulations and restrictions for productsafety and hazardous substances.</p>
				<p><b>CONFLICTS OF INTEREST/ANTI-BRIBERY</b></p>
				<p>Offering compensation of any value (gifts, discounts, services, loans, payments) 
				toanyRalph Lauren Corporation or affiliate employee, service provider or 
				government/political official to influence any act or decisionto secure a business 
				advantage is strictly prohibited.</p>
			</div>
		</div>
	</div>
	<?php //echo $swiperbybrand;?>

<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
<script src="<?php echo $path;?>plugin/select2/select2.min.js"></script>
<script src="<?php echo $path;?>js/regulations.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script type="application/x-javascript"> 
	$(document).ready(function(){
		openContent(<?php echo $loadid;?>);
	});
</script>
</body><!-- //Body -->
</html>