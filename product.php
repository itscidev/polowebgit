<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style1.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/product.css">
	<!-- Select2 -->
    <link rel="stylesheet" href="<?php echo $path;?>cms/plugins/select2/select2.min.css">
	
</head>
<!-- //Head -->
<!-- Body -->
<body>

<!-- Header -->
<?php include_once 'header.php';?>
<!-- //Header -->
	
<!-- content -->
<div class="container"><?php 
if ($errMsg == ''){?>
<div class="row rowheader">
	<div class="col-md-3 col-sm-12 hidefilter">
		<i class="fa fa-filter" aria-hidden="true"></i><span class="hidestring">Hide</span><span> Filters - 4449 items</span>
	</div>
	<div class="col-md-6 col-sm-8 titlecategory">
		<h4><?php echo $titlecatfil;?></h4>
	</div>
	<div class="col-md-3 col-sm-4" align="right">
		Sort by : 
		<select id="sortby"><?php echo $optsortby;?></select>
	</div>
</div>
<div class="women_main">
	<!-- start sidebar -->
	<div class="col-sm-3 col-xs-12 s-d">
		<div class="w_sidebar">
			<?php echo $filtercat.$filtersize.$filterstyle.$filtercolor;?>
		<!-- <section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Categories<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<div class="col col-4 filcat">
					<div class="active"><span class="togcat">Men<i class="fa fa-angle-up"></i></span>
						<div><span class="togcat">Clothing<i class="fa fa-angle-up"></i></span>
							<div>
							<label class="checkbox"><input type="checkbox" name="checkbox" checked="checked"><i></i>Dresses</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Polo Shirt</label>
							</div>
						</div>
						<div><span class="togcat">Clothing<i class="fa fa-angle-up"></i></span>
							<div>
							<label class="checkbox"><input type="checkbox" name="checkbox" checked="checked"><i></i>Dresses</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Polo Shirt</label>
							</div>
						</div>
					</div>
					<div class="active"><span class="togcat">Men<i class="fa fa-angle-up"></i></span>
						<div><span class="togcat">Clothing<i class="fa fa-angle-up"></i></span>
							<div>
							<label class="checkbox"><input type="checkbox" name="checkbox" checked="checked"><i></i>Dresses</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Polo Shirt</label>
							</div>
						</div>
						<div><span class="togcat">Clothing<i class="fa fa-angle-up"></i></span>
							<div>
							<label class="checkbox"><input type="checkbox" name="checkbox" checked="checked"><i></i>Dresses</label>
							<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Polo Shirt</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
		<!-- <section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Size<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<div class="col col-4">
					<label class="checkbox"><input type="checkbox" name="checkbox" checked="checked"><i></i>All Size</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>S</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>M</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>L</label>
					<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>XL</label>
				</div>
			</div>
		</section>
		<section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Style<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<div class="col col-4">
					<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Regular Fit</label>
					<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Custom Fit</label>
				</div>
			</div>
		</section>
		<section class="sky-form">
			<h4 class="showhidefiltersection">Shop by Colour<i class="fa fa-chevron-down"></i></h4>
			<div class="row1 scroll-pane">
				<ul class="w_nav2 filtercolor">
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"><i class="fa fa-check"></i></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
					<li><a href="#" style="background-color: #E60D41;"></a></li>
				</ul>
			</div>
		</section> -->
		<!-- <section class="sky-form">
			<h4>discount</h4>
			<div class="row1 scroll-pane">
				<div class="col col-4">
					<label class="radio"><input type="radio" name="radio" checked=""><i></i>60 % and above</label>
					<label class="radio"><input type="radio" name="radio"><i></i>50 % and above</label>
					<label class="radio"><input type="radio" name="radio"><i></i>40 % and above</label>
				</div>
				<div class="col col-4">
					<label class="radio"><input type="radio" name="radio"><i></i>30 % and above</label>
					<label class="radio"><input type="radio" name="radio"><i></i>20 % and above</label>
					<label class="radio"><input type="radio" name="radio"><i></i>10 % and above</label>
				</div>
			</div>						
		</section> -->
		</div>
	</div>
	<div class="col-sm-9 col-xs-12 w_content">
		<?php echo $proditm;?>
		<!-- grids_of -->
		<!-- 
		<div class="col-sm-4 col-xs-6 proditm">
			<div class="content_box">
				<a href="<?php //echo $urlcat.rawurlencode('Flag Edition Brasil');?>">
					<img src="<?php //echo $path;?>images/productitem/FlagEdition-Brasil.jpg" class="img-responsive" alt="Flag Edition Brasil"/>
				</a>
				<h4>
					<a href="<?php //echo $urlcat.rawurlencode('Flag Edition Brasil');?>">Flag Edition Brasil</a>
					<br /><small>Custom Fit</small>
				</h4>
				<div class="grid_1 simpleCart_shelfItem">
					<div class="item_add"><span class="item_price"><h6>IDR 899.000,-</h6></span></div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-xs-6 proditm">
			<div class="content_box">
				<a href="<?php //echo $urlcat.rawurlencode('Flag Edition England');?>">
					<img src="<?php //echo $path;?>images/productitem/FlagEdition-England.jpg" class="img-responsive" alt="Flag Edition England"/>
				</a>
				<h4>
					<a href="<?php //echo $urlcat.rawurlencode('Flag Edition England')?>"> Flag Edition England</a>
					<br /><small>Regular Fit</small>
				</h4>
				<div class="grid_1 simpleCart_shelfItem">
					<div class="item_add"><span class="item_price"><h6>IDR 899.000,-</h6></span></div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-xs-6 proditm">
			<div class="content_box">
				<a href="<?php //echo $urlcat.rawurlencode('Flag Edition France')?>">
					<img src="<?php //echo $path;?>images/productitem/FlagEdition-France.jpg" class="img-responsive" alt="Flag Edition France"/>
				</a>
				<h4>
					<a href="<?php //echo $urlcat.rawurlencode('Flag Edition France')?>"> Flag Edition France</a>
					<br /><small>Slim Fit</small>
				</h4>
				<div class="grid_1 simpleCart_shelfItem">
					<div class="item_add"><span class="item_price"><h6>IDR 899.000,-</h6></span></div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-xs-6 proditm">
			<div class="content_box">
				<a href="<?php //echo $urlcat.'/Wangky'?>">
					<img src="<?php //echo $path;?>images/productitem/WANGKY-yellow.jpg" class="img-responsive" alt="Wangky"/>
					<div class="labeldiscount">
						<img alt="special promo discount" src="<?php //echo $path;?>images/icon/label-discount.png">
						<div class="discnumber">29</div>
					</div>
				</a>
				<h4 class="withlabeldisc">
					<a href="<?php //echo $urlcat.'/Wangky'?>"> Wangky</a>
					<br /><small>Regular Fit</small>
				</h4>
				<div class="grid_1 simpleCart_shelfItem withlabeldisc">
					<div class="item_add"><span class="item_price"><h6>IDR 899.000,-</h6></span></div>
				</div>
				<div class="grid_1 withlabeldisc">
					<img class="colortype" img-src="<?php //echo $path;?>images/productitem/WANGKY-yellow.jpg" alt="WANGKY yellow" 
					src="<?php //echo $path;?>images/productcolor/c-yellow-blue.jpg">
					<img class="colortype" img-src="<?php //echo $path;?>images/productitem/WANGKY-white.jpg" alt="WANGKY white" 
					src="<?php //echo $path;?>images/productcolor/c-grey-purple.jpg">
					<img class="colortype" img-src="<?php //echo $path;?>images/productitem/WANGKY-toscamuda.jpg" alt="WANGKY toscamuda" 
					src="<?php //echo $path;?>images/productcolor/c-blue-grey.jpg">
					<img class="colortype" img-src="<?php //echo $path;?>images/productitem/WANGKY-orange.jpg" alt="WANGKY orange" 
					src="<?php //echo $path;?>images/productcolor/c-orange-blue.jpg">
					<img class="colortype" img-src="<?php //echo $path;?>images/productitem/WANGKY-merahstg.jpg" alt="WANGKY merah" 
					src="<?php //echo $path;?>images/productcolor/c-pink-black.jpg">
					<img class="colortype" img-src="<?php //echo $path;?>images/productitem/WANGKY-birumuda.jpg" alt="WANGKY birunavy" 
					src="<?php //echo $path;?>images/productcolor/c-blue-purple.jpg">
					<img class="colortype" img-src="<?php //echo $path;?>images/productitem/WANGKY-birunavy.jpg" alt="WANGKY birumuda" 
					src="<?php //echo $path;?>images/productcolor/c-blue-red.jpg">
					<img class="colortype" img-src="<?php //echo $path;?>images/productitem/WANGKY-biru.jpg" alt="WANGKY biru" 
					src="<?php //echo $path;?>images/productcolor/c-blue-yellow.jpg">
				</div>
			</div>
		</div> -->
			
		<div class="clearfix"></div>
		<div class="col-xs-12 pagingsection" align="center">
			<?php echo $pagingsection;?><!-- 
			<a href="" class="fullborder">1</a><a href="" class="noborder" onclick="return false;">...</a>
			<a href="" class="active">3</a><a href="">4</a><a href="">5</a><a href="">6</a><a href="">7</a>
			<a href="">8</a><a href="" class="fullborder">9</a> 
			<a href="" class="noborder" onclick="return false;">...</a><a href="" class="fullborder">14</a> -->
		</div>
	</div>
	<!-- end grids_of -->
	
	

<!-- end content -->
</div><?php 
}else{?>
<div class="row rowErrMsg">
	<div class="col-xs-12 s-d errMsg"><?php echo $errMsg;?></div>
	<div class="clearfix"></div>
</div><?php 
}?>
</div>
<div class="clearfix"></div>

<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
<!-- Select2 -->
<script src="<?php echo $path;?>cms/plugins/select2/select2.full.min.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script src="<?php echo $path;?>js/product.js"></script>

</body>
</html>