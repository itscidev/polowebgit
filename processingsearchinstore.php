<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
function haversineGreatCircleDistance(
		$latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
	// convert from degrees to radians
	$latFrom = deg2rad($latitudeFrom);
	$lonFrom = deg2rad($longitudeFrom);
	$latTo = deg2rad($latitudeTo);
	$lonTo = deg2rad($longitudeTo);

	$latDelta = $latTo - $latFrom;
	$lonDelta = $lonTo - $lonFrom;

	$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
			cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
	return $angle * $earthRadius;
}

$result = array();
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/details/')==0){
	$result['Error']='Ilegal access detected!';
}elseif (!isset($_POST['sistext']) || (isset($_POST['sistext']) && $_POST['sistext']=='')) {
	$result['Error'] = 'No value detected!';
}elseif (!isset($_POST['sisidprod']) || (isset($_POST['sisidprod']) && $_POST['sisidprod']=='')) {
	$result['Error'] = 'No product variant choosed!';
}else{
	//path defined
	if (isset($_SERVER['HTTP_HOST']) && (
		$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
		|| $_SERVER['HTTP_HOST']=='172.16.1.19'
	)){
		$path = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/';
	}else{
		$path = 'http://'.$_SERVER['HTTP_HOST'].'/';
	}
	
	include_once 'cms/configuration/connect.php';
	
	$searchstore = $_POST['sistext'];
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$path."processinggpscoordinates.html");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query(array(
		'st_address'=>$searchstore,'tokensec'=>md5('/StoresClosest.Php:'.date("d"))
	)));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($ch);
	curl_close($ch);
	$res = json_decode(json_decode($output),true);
	if (isset($res['Error'])){
		if (isset($res['Error']['st_address'])){
			$result['Error'] = "Please fill the field!";
		}else{
			$result['Error'] = $res['Error'];
		}
	}elseif (isset($res['error'])){
		$result['Error'] = $res['error'];
	}else{
		$lat = $res[0]['lat'];
		$long = $res[0]['lon'];
		$limitout = 8;
		$limitdist = 50000;//meter
	
		$resout = array();
		$outlet = mysqli_query($sqlcon,"select store.* from product_store ".
			"inner join store on st_id=ps_st_id ".
			"where st_status=1 and ps_pv_id=".$_POST['sisidprod']." ".
			"group by st_id");
		$outlet1 = mysqli_num_rows($outlet);
		if ($outlet1>0){while ($outlet2 = mysqli_fetch_assoc($outlet)){
			$distance = haversineGreatCircleDistance($lat,$long,$outlet2['st_lat'],$outlet2['st_long']);
			$distance = intval($distance);
			if ($distance <= $limitdist){
				while (isset($resout[$distance])){
					$distance = $distance + 1;
				}
				$resout[$distance] = array(
						$outlet2['st_id'],
						$outlet2['st_dispname'],
						$outlet2['st_address'],
						$outlet2['st_zip']
				);
				if (count($resout) > $limitout){
					$delar = array_pop($resout);
					unset($delar);
				}
			}
		}}
		$array = array(
// 			"St. Moritz Mall<br />Jl. Puri Raya Boulevard 1.<br />Jakarta Barat - Indonesia (11610)",
// 			"Puri Indah Mall<br />Jl. Puri Agung, Puri Indah, Kembangan Selatan.<br />Jakarta Barat - Indonesia (11610)",
// 			"Central Park Mall<br />Jl. Let. Jend. S. Parman, Kav. 28.<br />Jakarta Barat - Indonesia (11470)",
// 			"Taman Anggrek Mall<br />Jl. Let. Jend. S. Parman, Kav. 21.<br />Jakarta Barat - Indonesia (11470)"
		);
		if (count($resout) > 0){
			foreach ($resout as $distance => $content){
				$addr = $content[1]."<br />".$content[2];
				if ($content[3]!='' && $content[3]!=0){
					$addr .= "<br />".$content[3];
				}
				array_push($array,$addr);
			}
			$result['Result'] = $array;
			$result['Status'] = 'Success!';
		}else{
			$result['Error'] = 'No closest outlet found at your location!';
		}
	}
	if ($sqlcon){mysqli_close($sqlcon);}
}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>