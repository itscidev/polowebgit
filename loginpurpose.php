<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/loginpurpose.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->
	<div class="container-fluid margintop">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 logpcontent"><?php 
				echo $error.$sukses;
if ($reset == 1){?>
				<div class="panel panel-danger resetpanel">
					<div class="panel-heading">Reset your password here.</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="emailR">Email address</label>
							<p id="emailR"><?php echo $emailR?></p>
							<p id="tokenR"><?php echo $tokenR?></p>
						</div>
						<div class="form-group">
							<label for="passwordR">New Password</label>
							<input type="password" class="password form-control" id="passwordR" placeholder="Password">
						</div>
						<div class="form-group">
							<label for="passwordRR">Retype New Password</label>
							<input type="password" class="password form-control" id="passwordRR" placeholder="Password">
						</div>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-default" 
						onclick="resetPassword(); return false;">Submit</button>
					</div>
				</div><?php 
}?>
			</div>
		</div>
	</div>
<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
	<!-- Add fancyBox main JS and CSS files -->
	<script src="<?php echo $path;?>js/loginpurpose.js"></script>
</body><!-- //Body -->
</html>