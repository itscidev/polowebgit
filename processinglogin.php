<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
function smail($sqlcon,$contactemail,$contactnama,$subject,$strhtml){
	$result = array();
	$cekpurp = @mysqli_query($sqlcon,
		"select * from mail_smtp where ms_purpose='web'"
	);
	$cekpurp1 = 0;
	if ($cekpurp){$cekpurp1 = mysqli_num_rows($cekpurp);}
	if ($cekpurp1 != 1){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'] .= 'SMTP not found!';
	}else{
		$sender = mysqli_fetch_array($cekpurp);
		$host = $sender['ms_host'];
		$port = $sender['ms_port'];
		$secure = $sender['ms_secure'];
		$emailfr = $sender['ms_sendermail'];
		$emailpwd = $sender['ms_password'];
		$emailname = $sender['ms_sendername'];
		
		require 'PHPMailer/PHPMailerAutoload.php';
		
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		// Set PHPMailer to use the sendmail transport
		$mail->isSMTP();
		//$mail->isSendmail();
		$mail->Host = $host;
		$mail->Port = $port;
		$mail->SMTPSecure = $secure;
		$mail->SMTPAuth = true;
		$mail->Username = $emailfr;
		$mail->Password = $emailpwd;
		//Set who the message is to be sent from
		$mail->setFrom( $emailfr,$emailname );
		//Set an alternative reply-to address
		$mail->addReplyTo( $emailfr,$emailname );
		//Set an alternative BCC address
		$mail->addBCC( $emailfr,$emailname );
		//Set who the message is to be sent to
		$mail->addAddress($contactemail,$contactnama);
		//Set the subject line
		$mail->Subject = $subject;
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$strhtml="<html>".
		"<head></head>".
		"<body>".
			$strhtml.
			"<br><br><br><br><br>Regards,<br>".$emailname.
		"</body>".
		"</html>";
		$mail->msgHTML($strhtml);
		//Replace the plain text body with one created manually
		$mail->AltBody = 'We are sorry, your email client is not support HTML format. Please open with other programs. Thank you.';
		//Attach an image file
		//	$mail->addAttachment('images/phpmailer_mini.png');
		
		//send the message, check for errors
		if (!$mail->send()) {
			//	    echo "Mailer Error: " . $mail->ErrorInfo;
			$result['Error'] = 'Can not send email. '.$mail->ErrorInfo;
		}else{
			$result['Status'] = 'Email send success!';
		}
	}
	return $result;
}
include_once 'cms/configuration/connect.php';
include_once 'cms/model/member.php';
$result = array();
//0:signIn,1:newRegister,2:requestPwdReset,3:requestUnBanned
$allowtype = array('0','1','2','3');
if (!isset($_SERVER['HTTP_REFERER'])){
	$result['Error']='Ilegal access detected!';
}elseif (substr_count($_SERVER['HTTP_REFERER'],'http://dev.poloindonesia.com/')==0 && 
	substr_count($_SERVER['HTTP_REFERER'],'https://dev.poloindonesia.com/')==0 && 
	substr_count($_SERVER['HTTP_REFERER'],'http://poloindonesia.com/')==0 && 
	substr_count($_SERVER['HTTP_REFERER'],'https://poloindonesia.com/')==0 && 
	substr_count($_SERVER['HTTP_REFERER'],'http://localhost/poloweb/')==0
){
	$result['Error']='Ilegal access detected!';
}elseif (!isset($_POST['poslogin'])){
	$result['Error']='Un-known request!';
}elseif (!in_array($_POST['poslogin'], $allowtype)){
	$result['Error']='Un-known request!';
}else{
	if (isset($_POST['emaillogin'])){$emaillogin=strtolower(trim($_POST['emaillogin']));}else{$emaillogin='';}
	if (isset($_POST['passlogin'])){$passlogin=trim($_POST['passlogin']);}else{$passlogin='';}
	if (isset($_POST['passretypelogin'])){$passretypelogin=trim($_POST['passretypelogin']);}else{$passretypelogin='';}
	if (isset($_POST['rememberlogin']) && $_POST['rememberlogin']=='true'){$rememberlogin=1;}else{$rememberlogin=0;}
	if (isset($_SERVER['HTTP_HOST']) && (
		$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
		|| $_SERVER['HTTP_HOST']=='172.16.1.19'
	)){
		$pathcook = '/poloweb/';
	}else{
		$pathcook = '/';
	}
	
	if ($emaillogin==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill Email Address field!';
	}elseif (filter_var($emaillogin, FILTER_VALIDATE_EMAIL)==false){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'] .= 'Wrong email format!';
	}
	if ($passlogin=='' && ($_POST['poslogin']==0 || $_POST['poslogin']==1)){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill Password field!';
	}elseif (strlen($passlogin)<8 && $_POST['poslogin']==1){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Password length can not less then 8 character!';
	}elseif ($_POST['poslogin']==1){
		$passnum = $passlower = $passupper = 0;
		for ($i=0;$i<strlen($passlogin);$i++){
			$a = substr($passlogin,$i,1);
			if ($passnum==0 && ord($a)>=48 && ord($a)<=57){
				$passnum=1;
			}elseif ($passlower==0 && ord($a)>=97 && ord($a)<=122){
				$passlower = 1;
			}elseif ($passupper==0 && ord($a)>=65 && ord($a)<=90){
				$passupper = 1;
			}
			if ($passnum==1 && $passlower==1 && $passupper==1){
				break;
			}
		}
		if ($passnum==0 || $passlower==0 || $passupper==0){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Passwords must contain numbers, lowercase and uppercase letters!';
		}
	}
	if ($passretypelogin=='' && $_POST['poslogin']==1){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill Retype Password field!';
	}elseif ($passretypelogin!=$passlogin && $_POST['poslogin']==1){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Retype Password field not same!';
	}
	if (!isset($result['Error'])){
		if (isset($_SERVER['HTTP_HOST']) && (
			$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
			|| $_SERVER['HTTP_HOST']=='172.16.1.19'
		)){
			$localserv = 1;
		}else{
			$localserv = 0;
		}
		//cek existing
		$field = 'mm_id,mm_banned,mm_password,mm_name';
		$join = '';
		$where = "mm_email='".mysqli_real_escape_string($sqlcon,$emaillogin)."'";
		$exis = member_s($sqlcon,$field,$join,$where);
		if (!is_array($exis)){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].=$exis;
		}elseif (($_POST['poslogin']==0 || $_POST['poslogin']==2) && $exis[0]==0){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Email login not exist!';
		}elseif ($_POST['poslogin']==1 && $exis[0]!=0){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Email already registered!';
		}elseif ($_POST['poslogin']==0){
			$exis1 = mysqli_fetch_assoc($exis[1]);
			if ($exis1['mm_banned'] == 8){
				if (!isset($result['Error'])){
					$result['Error'] = '';
				}else{
					$result['Error'] .= '<br />';
				}
				$result['Error'].='Account has been banned!';				
			}elseif ($exis1['mm_banned'] == 9){
				if (!isset($result['Error'])){
					$result['Error'] = '';
				}else{
					$result['Error'] .= '<br />';
				}
				if ($localserv==1){
					$result['Error'].='Account has been banned! Please ask our '.
						'<a href="http://'.$_SERVER['HTTP_HOST'].'/poloweb/regulations/contact" '.
						'title="Polo contact us">Customer Service</a>.';
				}else{
					$result['Error'].='Account has been banned! Please ask our '.
						'<a href="http://'.$_SERVER['HTTP_HOST'].'/regulations/contact" '.
						'title="Polo contact us">Customer Service</a>.';
				}
			}elseif ($exis1['mm_password'] != md5($passlogin)){
				if (!isset($result['Error'])){
					$result['Error'] = '';
				}else{
					$result['Error'] .= '<br />';
				}
				$result['Error'].='Wrong password!';
				$reason = '';
				if ($exis1['mm_banned']==7){
					$reason = ",mm_bannedreason='Login Failure'";
				}
				member_u($sqlcon,"mm_banned=(mm_banned+1)".$reason,$exis1['mm_id']);
			}
		}elseif ($_POST['poslogin']==2){
			$exis1 = mysqli_fetch_assoc($exis[1]);
		}
	}
}
if (!isset($result['Error']) && $_POST['poslogin']==0){
	$result['Status'] = 'Login success!';
	$set = "mm_banned=0,mm_lastlogin='".date("Y-m-d H:i:s")."'";
	member_u($sqlcon,$set,$exis1['mm_id']);
	$endsession = time()+60*60*7;
	if ($rememberlogin==1){
		$endsession = time()+60*60*24*7;
	}
	setcookie( "loginmem",$exis1['mm_id'].":".$endsession.":".$rememberlogin,$endsession,$pathcook );
}
if (!isset($result['Error']) && $_POST['poslogin']==1){
	$insert = member_i($sqlcon,$emaillogin,md5($passlogin));
	if (!is_array($insert)){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].=$insert;
	}else{
		$result['Status'] = 'Register success!';
		$set = "mm_create='".date("Y-m-d H:i:s")."',mm_update='".date("Y-m-d H:i:s")."',".
			"mm_updateby=".$insert[0];
		member_u($sqlcon,$set,$insert[0]);
		
		$subject = "Polo Indonesia visitor new registration";
		$strhtml="Dear Valuable Customer";
		$strhtml.="<br><br>Thank you for join with us.<br>Here is your login information : <br>";
		$strhtml.="Email login : ".$emaillogin."<br>";
		$strhtml.="Password : ".$passlogin."<br>";
		$strhtml.="Please keep this information safely.<br><br>";
		$strhtml.="Help us to know your email is valid by ";
		if ($localserv==1){
			$strhtml.="<a href=\"http://".$_SERVER['HTTP_HOST']."/poloweb/loginpurpose/activate/".md5($emaillogin).
				"\" title=\"activate my email account\">click this link</a>";
		}else{
			$strhtml.="<a href=\"http://".$_SERVER['HTTP_HOST']."/profile/loginpurpose/".md5($emaillogin).
				"\" title=\"activate my email account\">click this link</a>";
		}
		$strhtml.=".<br>After activated you can receive notice from us such as invoice billing, ".
				"payment confirmation, order progres, etc.";
		
		smail($sqlcon,$emaillogin,'',$subject,$strhtml);
	}
}
if (!isset($result['Error']) && ($_POST['poslogin']==2 || $_POST['poslogin']==3)){
	if ($exis1['mm_name']!=''){$contactnama=$exis1['mm_name'];}else{$contactnama='';}
	$contactemail=trim($emaillogin);
	
	if ($_POST['poslogin']==2){
		$subject = "Polo Indonesia visitor request for password reset at ".date("F m, Y ga");
	}else{
		$subject = "Polo Indonesia visitor request for self release ban at ".date("F m, Y ga");
	}
	if ($contactnama!=''){
		$strhtml="Dear ".$contactnama."";
	}else{
		$strhtml="Dear Valuable Customer";
	}
	if ($_POST['poslogin']==2){
		$strhtml.="<br><br>Please click this link to change your password.<br>";
	}else{
		$strhtml.="<br><br>Please click this link to release banned account.<br>";
	}
	$md5reset = md5($exis1['mm_password'].$exis1['mm_id'].date("Ymdhis"));
	if ($localserv==1){
		if ($_POST['poslogin']==2){
			$linkreset = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/loginpurpose/reset/'.$md5reset;
		}else{
			$linkreset = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/loginpurpose/banned/'.$md5reset;
		}
	}else{
		if ($_POST['poslogin']==2){
			$linkreset = 'http://'.$_SERVER['HTTP_HOST'].'/loginpurpose/reset/'.$md5reset;
		}else{
			$linkreset = 'http://'.$_SERVER['HTTP_HOST'].'/loginpurpose/banned/'.$md5reset;
		}
	}
	if ($_POST['poslogin']==2){
		$strhtml.="<a href=\"".$linkreset."\" title=\"Reset password\">Reset My Password</a><br>";
		$strhtml.="or copy this url to your browser<br>".$linkreset."<br>";
		$strhtml.="This link is no longer valid after successfully password reset.<br>";
	}else{
		$strhtml.="<a href=\"".$linkreset."\" title=\"Self release banned\">Release Banned Account</a><br>";
		$strhtml.="or copy this url to your browser<br>".$linkreset."<br>";
		$strhtml.="This link is no longer valid after successfully release ban account.<br>";
	}
	
	$send = smail($sqlcon,$contactemail,$contactnama,$subject,$strhtml);
	if (isset($send['Error'])){
		$result['Error'] = $send['Error'];
	}else {
		$set = "mm_update='".date("Y-m-d H:i:s")."',mm_passwordreset='".$md5reset."'";
		member_u($sqlcon,$set,$exis1['mm_id']);
		//	    echo "Message sent!";
		if ($_POST['poslogin']==2){
			$result['Status'] = 'Forgot Password success!';
		}else{
			$result['Status'] = 'Release Banned success!';
		}
	}
}
if (isset($sqlcon)){mysqli_close($sqlcon);}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>