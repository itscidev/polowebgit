<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/event.php' ) == 0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}
if ($draftmode == 1){
	$sqldraft = 'and bn_draft=1 ';
}else{
	$sqldraft = 'and bn_status=1 ';
}
$sld = @mysqli_query($sqlcon,
	"select * from event ".
	"where ev_start<='".date("Y-m-d H:i:s")."' ".
		"and ev_end>='".date("Y-m-d H:i:s")."' ".$sqldraft.
	"order by ev_start asc,ev_update desc"
);
$sld1 = 0;
if ($sld){$sld1 = mysqli_num_rows($sld);}
$listevent = '';
if ($sld1 > 0){while ($sld2 = mysqli_fetch_assoc($sld)){
	if (isset($sld2['ev_image']) && $sld2['ev_image']!=''
	&& file_exists("images/event/".$sld2['ev_image'])){
		$listevent .= '<div class="col-xs-12 eventhead">'.
			'<a href="'.$path.'eventdetail/'.urlencode($sld2['ev_title']).'" title="'.htmlentities($sld2['ev_title']).'">'.
				'<img src="'.$path.'images/event/'.$sld2['ev_image'].'" alt="'.htmlentities($sld2['ev_title']).'" />'.
			'</a>'.
		'</div>';
	}
}}
?>