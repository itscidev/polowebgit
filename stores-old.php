<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $heads;?>
	
	<!-- Meta-Tags -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- //Meta-Tags -->
	
	<!-- Custom-StyleSheet-Links -->
	<link rel="shortcut icon" type="image/png" href="<?php echo $path;?>images/favicon.png" >
	<!-- Bootstrap-CSS -->	   
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/bootstrap.css">
	<!-- Index-Page-CSS -->	   
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style.css">
	<!-- Header-Slider-CSS --> 
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fluid_dg.css" id="fluid_dg-css">
	
	<!-- Fonts -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fonts.css">
	<!-- //Fonts -->
	
	<!-- Font-Awesome-File-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/font-awesome.css">
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>fonts/fontawesome-webfont.ttf">
	<!-- //Font-Awesome-File-Links -->
</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<!-- Banner -->
	<div class="agileheader-banner">
		<img src="<?php echo $path;?>images/banner.jpg" alt="Groovy Apparel">
	</div>
	<!-- //Banner -->
		
	<!-- Locations -->
	<div class="agilelocationsw3ls">
		<div class="container">
			<div class="col-md-6 agilelocationsw3ls-grid agilelocationsw3ls-grid1">
				<h1>LOCATIONS</h1>
				<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
				<div class="wthreeshop-a">
					<a href="#">FIND NEARBY STORE <i class="fa fa-street-view" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="col-md-6 agilelocationsw3ls-grid agilelocationsw3ls-grid2">
				<img src="<?php echo $path;?>images/location.jpg" alt="Groovy Apparel">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //Locations -->



	<!-- Stores-Address -->
	<div class="aitsstoreaddressw3">
		<div class="container">

			<div class="aitsstoreaddressw3-grids">
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid1">
					<h5>Italy</h5>
					<address>
						<ul>
							<li>Studio No: 41</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Italy</li>
							<li>Tel: +99937774623</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid2">
					<h5>France</h5>
					<address>
						<ul>
							<li>Studio No: 42</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>France</li>
							<li>Tel: +9473274623</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid3">
					<h5>Germany</h5>
					<address>
						<ul>
							<li>Studio No: 43</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Germany</li>
							<li>Tel: +999375745</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid4">
					<h5>Switzerland</h5>
					<address>
						<ul>
							<li>Studio No: 44</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Switzerland</li>
							<li>Tel: +9996657523</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid5">
					<h5>Monaco</h5>
					<address>
						<ul>
							<li>Studio No: 45</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Monaco</li>
							<li>Tel: +2277874623</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid6">
					<h5>Spain</h5>
					<address>
						<ul>
							<li>Studio No: 46</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Spain</li>
							<li>Tel: +999774643</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid7">
					<h5>Portugal</h5>
					<address>
						<ul>
							<li>Studio No: 47</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Portugal</li>
							<li>Tel: +75675674623</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid8">
					<h5>UK</h5>
					<address>
						<ul>
							<li>Studio No: 47</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>UK</li>
							<li>Tel: +55788433333</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid9">
					<h5>Greece</h5>
					<address>
						<ul>
							<li>Studio No: 49</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Greece</li>
							<li>Tel: +99937774623</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid10">
					<h5>Sweden</h5>
					<address>
						<ul>
							<li>Studio No: 50</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Sweden</li>
							<li>Tel: +99937774623</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid11">
					<h5>Denmark</h5>
					<address>
						<ul>
							<li>Studio No: 51</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Denmark</li>
							<li>Tel: +99937774623</li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 aitsstoreaddressw3-grid aitsstoreaddressw3-grid12">
					<h5>Finland</h5>
					<address>
						<ul>
							<li>Studio No: 52</li>
							<li>Lorem Ipsum</li>
							<li>Dolores Amet</li>
							<li>Consect, 90210</li>
							<li>Finland</li>
							<li>Tel: +99937774623</li>
						</ul>
					</address>
				</div>
			</div>

		</div>
	</div>
	<!-- //Stores-Address -->



	<!-- Map -->
		<div id="map"></div>
	<!-- //Map -->



	<!-- Newsletter -->
	<!-- <div class="w3lsnewsletter" id="w3lsnewsletter">
		<div class="container">
			<div class="w3lsnewsletter-grids">
				<div class="col-md-5 w3lsnewsletter-grid w3lsnewsletter-grid-1 subscribe">
					<h2>Subscribe to our Newsletter</h2>
				</div>
				<div class="col-md-7 w3lsnewsletter-grid w3lsnewsletter-grid-2 email-form">
					<form action="#" method="post">
						<input class="email" type="email" name="Email" placeholder="Email Address" required="">
						<input type="submit" class="submit" value="SUBSCRIBE">
					</form>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div> -->
	<!-- //Newsletter -->

	<?php include_once 'footer.php';?>

<!-- Default-JavaScript -->
<script src="<?php echo $path;?>js/jquery-2.2.3.js"></script>
<!-- Supportive-Modernizr-JavaScript -->
<script src="<?php echo $path;?>js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- Custom-JavaScript-File-Links -->
<script src="<?php echo $path;?>js/modernizr.custom.js"></script>
<!-- cart-js -->
<script src="<?php echo $path;?>js/minicart.js"></script>
	<script>
        w3l.render();

        w3l.cart.on('w3agile_checkout', function (evt) {
        	var items, len, i;

        	if (this.subtotal() > 0) {
        		items = this.items();

        		for (i = 0, len = items.length; i < len; i++) { 
        		}
        	}
        });
    </script>  
	<!-- //cart-js --> 
		<!-- Shopping-Cart-JavaScript -->

		<!-- Dropdown-Menu-JavaScript -->
			<script>
				$(document).ready(function(){
					$(".dropdown").hover(
						function() {
							$('.dropdown-menu', this).stop( true, true ).slideDown("fast");
							$(this).toggleClass('open');
						},
						function() {
							$('.dropdown-menu', this).stop( true, true ).slideUp("fast");
							$(this).toggleClass('open');
						}
					);
				});
			</script>
		<!-- //Dropdown-Menu-JavaScript -->

<!-- Pricing-Popup-Box-JavaScript -->
<script src="<?php echo $path;?>js/jquery.magnific-popup.js" type="text/javascript"></script>
				<script>
				$(document).ready(function() {
					$('.popup-with-zoom-anim').magnificPopup({
						type: 'inline',
						fixedContentPos: false,
						fixedBgPos: true,
						overflowY: 'auto',
						closeBtnInside: true,
						preloader: false,
						midClick: true,
						removalDelay: 300,
						mainClass: 'my-mfp-zoom-in'
					});
				});
			</script>
		<!-- //Popup-Box-JavaScript -->

		<!-- Map-JavaScript -->
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>        
			<script type="text/javascript">
				google.maps.event.addDomListener(window, 'load', init);
				function init() {
					var mapOptions = {
						zoom: 11,
						center: new google.maps.LatLng(44.6648, 11.1342),
						styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
					};
					var mapElement = document.getElementById('map');
					var map = new google.maps.Map(mapElement, mapOptions);
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(44.6648, 11.1342),
						map: map,
					});
				}
			</script>
		<!-- //Map-JavaScript -->
		
<!-- Bootstrap-JavaScript -->
<script src="<?php echo $path;?>js/bootstrap.js"></script>
			
</body>
<!-- //Body -->



</html>