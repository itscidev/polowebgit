<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/orderhistory.php' )==0 || $memid==0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}

include_once 'cms/model/cart.php';
include_once 'cms/model/cartdetail.php';

$arrstatus = array('Waiting Payment','Canceled','Paid','Failed','On Packaging','On Delivery','Delivered');

$field = "*";
$join = "inner join member_address on mma_id=cr_mma_id ".
	"inner join paymenttype on pt_id=cr_pt_id";
$where = "mma_mm_id=".$memid;
$order = "cr_id desc";
$cart = cart_s($sqlcon,$field,$join,$where,'',$order);
$listbuy = '';
$no=0;
if (is_array($cart) && $cart[0]>0){while ($cart1 = mysqli_fetch_assoc($cart[1])){
	$total = $cart1['cr_ong_price'];
	$trlist = '';
	$field = "*";
	$join = "inner join product_variety on pv_id=crd_pv_id ".
		"inner join product on pr_id=pv_pr_id ".
		"left outer join product_image on pi_id=pr_main_pi_id ".
		"left outer join style on st_id=pv_st_id ".
		"left outer join size on sz_id=pv_sz_id ".
		"left outer join color_item on ci_id=pv_ci_id";
	$where = "crd_cr_id=".$cart1['cr_id'];
	$det = cartdetail_s($sqlcon,$field,$join,$where);
	if (is_array($det) && $det[0]>0){while ($det1 = mysqli_fetch_assoc($det[1])){
		$total = $total + ($det1['crd_qty']*$det1['crd_price']);
		$prodname = $det1['pr_name'];
		if (isset($det1['st_name']) && $det1['st_name']!=''){
			$prodname .= ' - '.$det1['st_name'];
		}
		if (isset($det1['sz_name']) && $det1['sz_name']!=''){
			$prodname .= ', Size '.$det1['sz_name'];
		}
		if (isset($det1['ci_image']) && $det1['ci_image']!='' && file_exists("images/productcolor/".$det1['ci_image'])){
			$prodname .= ', Color <img src="'.$path.'images/productcolor/'.$det1['ci_image'].'" alt="'.$det1['pr_name'].'" class="imgcolor">';
		}
		$prodimg = '';
		if (isset($det1['pi_image']) && $det1['pi_image']!='' && file_exists("images/productitem/".$det1['pi_image'])){
			$prodimg .= '<img src="'.$path.'images/productitem/'.$det1['pi_image'].'" alt="'.$det1['pr_name'].'">';
		}
		$trlist .= '<tr>'.
			'<td width="15%" align="center">'.$prodimg.'</td>'.
			'<td width="*">'.$prodname.'</td>'.
			'<td width="15%">'.number_format($det1['crd_qty'],0,',','.').' pcs<br>Rp '.number_format($det1['crd_price'],0,',','.').'</td>'.
			'<td width="15%"><label>Total</label>Rp '.number_format(($det1['crd_qty']*$det1['crd_price']),0,',','.').'</td>'.
		'</tr>';
	}}else{
		$trlist = '<tr><td>Data possibly deleted!</td></tr>';
	}
	
	$listbuy .= '<div class="panel panel-primary">'.
		'<div class="panel-heading">'.
			'<div class="row">'.
				'<div class="col-xs-4"><label>Purchase Date</label>'.date("d M Y",strtotime($cart1['cr_create'])).'</div>'.
				'<div class="col-xs-4"><label>Total Charge</label>'.$total.'</div>'.
				'<div class="col-xs-4"><label>Payment Status</label>'.$arrstatus[($cart1['cr_status'])].'</div>'.
			'</div>'.
		'</div>'.
		'<div class="panel-body">'.
			'<table>'.$trlist.'</table>'.
		'</div>'.
		'<div class="panel-footer">'.
			'<div class="row">'.
				'<div class="col-xs-6">Invoice : '.$cart1['cr_id'].'</div>'.
				'<div class="col-xs-6"><span class="floatright seedetail" onclick="openfancy(\''.$path.'orderhistorydetail.html?cartid='.$cart1['cr_id'].'\');"><i class="fa fa-info marginright"></i>See Detail</span></div>'.
			'</div>'.
		'</div>'.
	'</div>';
	$no++;
}}else{
	$listbuy = '<h3>No Order Found!</h3>';
}

?>