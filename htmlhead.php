<!-- Meta-Tags -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- //Meta-Tags -->

<!-- Custom-StyleSheet-Links -->
<!-- Bootstrap-CSS -->	   
<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/bootstrap.css">
<?php echo $addhead;?>
<link rel="shortcut icon" type="image/png" href="<?php echo $path;?>images/favicon.png" >
<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/mystyle.css">
<!-- Index-Page-CSS -->
<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/style.css">
<!-- Header-Slider-CSS --> 
<!-- <link type="text/css" media="all" rel="stylesheet" href="<?php //echo $path;?>css/fluid_dg.css" id="fluid_dg-css"> -->
<!-- //Custom-StyleSheet-Links -->

<!-- Fonts -->
<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/fonts.css">
<!-- //Fonts -->

<!-- Font-Awesome-File-Links -->
<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/font-awesome.css">
<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>fonts/fontawesome-webfont.ttf">
<!-- //Font-Awesome-File-Links -->

<!-- Swiper -->
<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/swiper.min.css">
<!-- //Swiper -->