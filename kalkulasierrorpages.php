<?php
if (substr_count ( $_SERVER['SCRIPT_NAME'], '/errorpages.php' ) == 0) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}

$resultdisp = '';
if (isset($_GET['id']) && substr($_GET['id'],0,1)=='4'){
	$resultdisp = '<div class="row">'.
		'<div class="col-sm-6 col-xs-12">'.
			'<img alt="POLO Indonesia page not found" src="'.$path.'images/error-404.jpg" width="100%">'.
		'</div>'.
		'<div class="col-sm-6 col-xs-12 errortext">'.
			'<span>Are you lost?</span><br />'.
			'Please back to our <a href="'.$path.'" title="Polo Indonesia">home page</a>.'.
		'</div>'.
	'</div>';
}elseif (isset($_GET['id'])){
	$resultdisp = '<div class="row">'.
		'<div class="col-sm-12 col-xs-12">'.
			'<img alt="POLO Indonesia something wrong" src="'.$path.'images/error-internal.jpg" width="100%">'.
		'</div>'.
		'<div class="col-sm-12 col-xs-12 errortext1">'.
			'<span>Oops something wrong..</span><br />'.
			'Try to refresh this page or feel free to contact us if the problem persist'.
		'</div>'.
	'</div>';
}	
?>