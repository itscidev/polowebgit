<?php
$result=array();
$arrtoken = array(
	md5('/Stores.Php:'.date("d")),
	md5('/StoresCMS.Php:'.date("d")),
	md5('/StoresClosest.Php:'.date("d"))
);
if (!isset($_POST['tokensec'])){
	$result['Error']='Ilegal access detected!';
}elseif (!in_array($_POST['tokensec'],$arrtoken)){
	$result['Error']='Session timeout! Please try again later.';
}
if (!isset($result['Error'])){
	if (!isset($_POST['st_address'])){
		$result['Error']['st_address'] = 1;
	}elseif (trim($_POST['st_address'])==''){
		$result['Error']['st_address'] = 1;
	}else{
		$srcar = array('<br>','<u>','</u>','<i>','</i>','<small>','</small>','<b>','</b>','<p>','</p>');
		$repar = array(' ','','','','','','','','','',' ');
		$address = str_replace($srcar, $repar, trim($_POST['st_address']));
 		$ch = curl_init();
 		curl_setopt($ch, CURLOPT_URL,
			"https://us1.locationiq.com/v1/search.php?key=4fb0523d85b618&q=".
			rawurlencode($address)."&format=json&countrycodes=id"
		);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$output = curl_exec($ch);
		curl_close($ch);
 		$result = $output;
	}
}

/*if (isset($_SERVER)){$result['SERVER']=$_SERVER;}

if (isset($_POST)){$result['POST']=$_POST;}
if (isset($_GET)){$result['GET']=$_GET;}
if (isset($_FILES)){$result['FILES']=$_FILES;}*/
header('Content-Type: application/json');
echo json_encode($result);
?>