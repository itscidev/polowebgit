<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
$result = array();
if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/loginpurpose/reset/')==0){
	$result['Error']='Ilegal access detected!';
}else{
	include_once 'cms/configuration/connect.php';
	include_once 'cms/model/member.php';
	if (isset($_POST['tokenlogin'])){$tokenlogin=strtolower(trim($_POST['tokenlogin']));}else{$tokenlogin='';}
	if (isset($_POST['passlogin'])){$passlogin=trim($_POST['passlogin']);}else{$passlogin='';}
	if (isset($_POST['passretypelogin'])){$passretypelogin=trim($_POST['passretypelogin']);}else{$passretypelogin='';}
	
	if ($tokenlogin==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Account not detected!';
	}
	if ($passlogin==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill Password field!';
	}elseif (strlen($passlogin)<8){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Password length can not less then 8 character!';
	}else{
		$passnum = $passlower = $passupper = 0;
		for ($i=0;$i<strlen($passlogin);$i++){
			$a = substr($passlogin,$i,1);
			if ($passnum==0 && ord($a)>=48 && ord($a)<=57){
				$passnum=1;
			}elseif ($passlower==0 && ord($a)>=97 && ord($a)<=122){
				$passlower = 1;
			}elseif ($passupper==0 && ord($a)>=65 && ord($a)<=90){
				$passupper = 1;
			}
			if ($passnum==1 && $passlower==1 && $passupper==1){
				break;
			}
		}
		if ($passnum==0 || $passlower==0 || $passupper==0){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Passwords must contain numbers, lowercase and uppercase letters!';
		}
	}
	if ($passretypelogin==''){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Please fill Retype Password field!';
	}elseif ($passretypelogin!=$passlogin){
		if (!isset($result['Error'])){
			$result['Error'] = '';
		}else{
			$result['Error'] .= '<br />';
		}
		$result['Error'].='Retype Password field not same!';
	}
	if (!isset($result['Error'])){
		if (isset($_SERVER['HTTP_HOST']) && (
			$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
			|| $_SERVER['HTTP_HOST']=='172.16.1.19'
		)){
			$localserv = 1;
		}else{
			$localserv = 0;
		}
		//cek existing
		$field = 'mm_id';
		$join = '';
		$where = "mm_passwordreset='".mysqli_real_escape_string($sqlcon,$tokenlogin)."'";
		$exis = member_s($sqlcon,$field,$join,$where);
		if (!is_array($exis)){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].=$exis;
		}elseif ($exis[0]==0){
			if (!isset($result['Error'])){
				$result['Error'] = '';
			}else{
				$result['Error'] .= '<br />';
			}
			$result['Error'].='Account not found!';
		}else{
			$exis1 = mysqli_fetch_assoc($exis[1]);
		}
	}
}
if (!isset($result['Error'])){
	$set = "mm_update='".date("Y-m-d H:i:s")."',mm_password='".md5($passlogin)."',mm_emailstatus=1".
		",mm_passwordreset=''";
	member_u($sqlcon,$set,$exis1['mm_id']);
	//	    echo "Message sent!";
	$result['Status'] = 'Password reset successfully!';
}
if (isset($sqlcon)){mysqli_close($sqlcon);}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>