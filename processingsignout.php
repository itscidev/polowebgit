<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
$result = array();
if (!isset($_SERVER['HTTP_REFERER'])){
	$result['Error']='Ilegal access detected!';
}elseif (substr_count($_SERVER['HTTP_REFERER'],'http://dev.poloindonesia.com/')==0 && 
	substr_count($_SERVER['HTTP_REFERER'],'https://dev.poloindonesia.com/')==0 && 
	substr_count($_SERVER['HTTP_REFERER'],'http://poloindonesia.com/')==0 && 
	substr_count($_SERVER['HTTP_REFERER'],'https://poloindonesia.com/')==0 && 
	substr_count($_SERVER['HTTP_REFERER'],'http://localhost/poloweb/')==0
){
	$result['Error']='Ilegal access detected!';
}elseif (!isset($_POST['signout'])){
	$result['Error']='Un-known request!';
}elseif ($_POST['signout']!=1){
	$result['Error']='Un-known request!';
}else{
	$result['Status'] = 'Sign out success!';
	if (isset($_SERVER['HTTP_HOST']) && (
		$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
		|| $_SERVER['HTTP_HOST']=='172.16.1.19'
	)){
		$pathcook = '/poloweb/';
	}else{
		$pathcook = '/';
	}
	unset($_COOKIE["loginmem"]);
	setcookie("loginmem", null, -1, $pathcook);
}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>