<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Fancybox simple pop up -->
    <link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>fancybox/jquery.fancybox.css?v=2.1.5" />
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/profile.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->
	<div class="container-fluid margintop">
		<div class="row">
			<div class="col-md-4 col-sm-5 col-xs-12">
			
				<div class="containerfoto">
					<div class="paddingfoto">
						<img alt="My Account" src="<?php echo $path;?>images/transparent.png" 
						style="background-image: url('<?php echo $path.$vstfoto;?>');">
						<p>Maximum file size 1.000.000 Bytes (1 MB)<br>
						Allowed file format are JPG, JPEG and PNG</p>
						<form id="khususfoto" method="post" enctype="multipart/form-data">
						<input id="fotouploader" name="fotouploader" type="file" style="visibility:hidden" />
						<input name="mode" type="hidden" value="4" />
						<div class="msguploadfoto"></div>
						<input class="btn btn-default btn-block btnfoto" type="button" value="Foto Uploader" 
						onclick="$('#fotouploader').click();" />
						</form>
					</div>
					<input href="#winpassword" type="button" class="btn btn-default btn-block margintop fancybox" 
					value="Change Password" />
				</div>
			</div>
			<div class="col-md-8 col-sm-7 col-xs-12 profInfo">
				<div class="page-header">
				  <h1>Customer Profile <small></small></h1>
				</div>
				<div class="row profiledt margintop">
					<div class="col-xs-3">Name</div>
					<div class="col-xs-9">
						: <span id="pfname"><?php echo $vstname;?></span>
						<a href="#winname" class="fancybox floatright" onclick="return false;">Change</a>
					</div>
				</div>
				<div class="row profiledt">
					<div class="col-xs-3">Email</div>
					<div class="col-xs-9">
						: <?php echo $vstemail;?>
						<divp class="emailver floatright"><?php echo $vstemailstatus;?></divp>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="winname" class="panel panel-default" style="display: none;">
		<div class="panel-body">
			<div class="form-group">
				<label for="pname">Name</label>
				<input type="text" class="profile form-control" id="pname" placeholder="Enter.." 
				value="<?php echo $vstname;?>">
			</div>
		</div>
		<div class="panel-footer">
			<div class="msgwinname"></div>
			<button type="submit" class="btn btn-warning submitwinname" 
			onclick="submitwinname(); return false;">Submit</button>
		</div>
	</div>
	<div id="winpassword" class="panel panel-default" style="display: none;">
		<div class="panel-body">
			<div class="form-group">
				<label for="ppassword">Old Password</label>
				<input type="password" class="profile form-control" id="ppassword">
			</div>
			<div class="form-group">
				<label for="pnpassword">New Password</label>
				<input type="password" class="profile form-control" id="pnpassword">
			</div>
			<div class="form-group">
				<label for="pnrpassword">Retype New Password</label>
				<input type="password" class="profile form-control" id="pnrpassword">
			</div>
		</div>
		<div class="panel-footer">
			<div class="msgwinpassword"></div>
			<button type="submit" class="btn btn-warning submitwinpassword" 
			onclick="submitwinpassword(); return false;">Submit</button>
		</div>
	</div>
<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
	<!-- Add fancyBox main JS and CSS files -->
	<script src="<?php echo $path;?>fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<script src="<?php echo $path;?>js/profile.js"></script>
</body><!-- //Body -->
</html>