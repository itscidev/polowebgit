<?php
if (substr_count( $_SERVER['SCRIPT_NAME'],'/profile.php' )==0 || !isset($memid) || (isset($memid) && $memid==0)) {
	header ( 'location:'.$path.'index.html' );
	exit ();
}
$error='';
if ($error != ''){
	$error = '<div class="alert alert-danger alert-dismissible" role="alert">'.
		'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
		'<span aria-hidden="true">&times;</span></button>'.
		'<strong>Warning!</strong> '.$error.
	'</div>';
}
$vstemail = $vstemailstatus = '';
if (isset($profvst['mm_email'])){
	$vstemail = $profvst['mm_email'];
	if ($profvst['mm_emailstatus']==1){
		$vstemailstatus .= '<span class="label label-success floatright">Verified!</span>';
	}else{
		$vstemailstatus .= '<span class="label label-danger pointer" '.
			'title="Send link to my email" onclick="sendActivation();">Not Verified!</span>';
	}
}
?>