<?php include_once 'kalkulasi.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'htmlhead.php';?>
	
	<!-- Own Custom-StyleSheet-Links -->
	<link type="text/css" media="all" rel="stylesheet" href="<?php echo $path;?>css/o-productparent.css">
	<!-- //Own Custom-StyleSheet-Links -->

</head>
<!-- //Head -->
<!-- Body -->
<body>

	<!-- Header -->
	<?php include_once 'header.php';?>
	<!-- //Header -->

	<?php echo $headerslide;?>
	
	<!-- Video -->
	<?php echo $videosection;?>
	<!-- //Video -->

	<!-- Shopping -->
	<?php echo $quickmenu;?>
	<!-- //Shopping -->

	<!-- Seasons Trend -->
	<?php echo $seasonsstyle;?><!-- 
	<div class="col-xs-12 seasonstrend">
		<h2>
			<i>POLO STORE</i>
		</h2>
		<div class="col-md-12 agileshopping">
			<div class="agileshoppinggrid agileshoppinggrid1">
				<div class="grid">
					<figure class="effect-apollo" style="border:none;">
						<img src="<?php //echo $path;?>images/styleseason2.jpg" alt="Current trend season style"/>
					</figure>
				</div>
			</div>
		</div>
		<div id="explorenow">
			<b><i class="fa fa-angle-double-right"></i> EXPLORE NOW</b>
		</div>
	</div> -->
	<!-- //Seasons Trend -->

<?php 
	include_once 'footer.php';
	include_once 'htmlfoot.php';
?>
	<script src="<?php echo $path;?>js/productparent.js"></script>
</body><!-- //Body -->
</html>