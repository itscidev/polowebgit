<?php
/* $strget = '';
if (isset($_GET)){
	$strget = json_encode($_GET);
}
$strpost = '';
if (isset($_POST)){
	$strpost = json_encode($_POST);
}
$strserver = '';
if (isset($_SERVER)){
	$strserver = json_encode($_SERVER);
}
$strbody = file_get_contents('php://input');

$file=fopen("log.txt","a+") or exit("Unable to open file!");
$strtowrite = PHP_EOL . date("d-m-Y H:i:s");
$strtowrite .= ", body : " . $strbody . ", get : " . $strget . ", post : " . $strpost .
", server : " . $strserver . PHP_EOL;
fwrite($file,$strtowrite);
fclose($file);
 */
//path defined
if (isset($_SERVER['HTTP_HOST']) && (
	$_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='172.16.30.12'
	|| $_SERVER['HTTP_HOST']=='172.16.1.19'
)){
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/poloweb/';
	$pathcook = '/poloweb/';
}else{
	$path = 'http://'.$_SERVER['HTTP_HOST'].'/';
	$pathcook = '/';
}
$arrmode = array('del','add','upd');
$result = array();
include_once 'cms/configuration/connect.php';
/* if (!isset($_SERVER['HTTP_REFERER']) || substr_count($_SERVER['HTTP_REFERER'], '/details/')==0){
	$result['Error']='Ilegal access detected!';
}else */if (!isset($_POST['mode']) || (isset($_POST['mode']) && !in_array($_POST['mode'],$arrmode))){
	$result['Error']='No action needed!';
}elseif (!isset($_POST['idvariety']) || (isset($_POST['idvariety']) && intval($_POST['idvariety'])==0)) {
	$result['Error'] = 'No value detected!';
}elseif ($_POST['mode']=='upd' && (!isset($_POST['qty']) || (isset($_POST['qty']) && intval($_POST['qty'])==0))) {
	$result['Error'] = 'Invalid Qty!';
}elseif ($_POST['mode']=='add'){
	$idvariety = intval($_POST['idvariety']);
	$getdata = 1;
	$cartsubtotal = 0;
	$cartqty = 0;
	$cartweight = 0;
	include_once 'cms/model/productvariety.php';
	if (isset($_COOKIE["cartvst"])){
		$cartvst = json_decode($_COOKIE["cartvst"],true);
		if (isset($cartvst[$idvariety])){
			$result['Error'] = 'Product was in cart!';
			$getdata = 0;
		}
		foreach ($cartvst as $idpv => $qty){
			$field = "pv_price";
			$where = "pv_id=".$idpv;
			$cekpv = productvariety_s($sqlcon,$field,'',$where);
			if (is_array($cekpv) && $cekpv[0]>0){
				$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
				$cartsubtotal = $cartsubtotal + ($qty*$cekpv1['pv_price']);
				$cartqty = $cartqty + $qty;
			}elseif (is_array($cekpv) && $cekpv[0]==0){
				unset($cartvst[$idpv]);
			}
		}
	}
	
	if ($getdata == 1){
		include_once 'cms/model/productimage.php';
		$field = "pv_qty,pv_price,pv_status,pv_pr_id,pr_name,pr_weight,pr_status,pv_st_id,st_name,pv_sz_id,sz_name,pv_ci_id,ci_image,cl_name";
		$join = "inner join product on pr_id=pv_pr_id ".
				"left outer join style on st_id=pv_st_id ".
				"left outer join size on sz_id=pv_sz_id ".
				"left outer join color_item on ci_id=pv_ci_id ".
				"left outer join color on cl_id=ci_cl_id";
		$where = "pv_id=".$idvariety;
// 		$result['qry'] = $where;
		$cekpv = productvariety_s($sqlcon,$field,$join,$where);
		if (!is_array($cekpv)){
			$result['Error'] = $cekpv;
		}elseif (is_array($cekpv) && $cekpv[0]==0){
			$result['Error'] = "Variant not exist anymore!";
		}elseif (is_array($cekpv) && $cekpv[0]>0){
			$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
			if ($cekpv1['pr_status'] != 1){
				$result['Error'] = 'Product no longer available!';
			}elseif ($cekpv1['pv_status'] != 1){
				$result['Error'] = 'Variant no longer available!';
			}elseif ($cekpv1['pv_qty'] < 1){
				$result['Error'] = 'Out of stock!';
			}else{
				$result['price'] = number_format($cekpv1['pv_price'],0,',','.');
				$field = "pi_image";
				$where = "pi_pr_id=".$cekpv1['pv_pr_id']." and pi_st_id=".$cekpv1['pv_st_id']." ".
						"and pi_ci_id=".$cekpv1['pv_ci_id']." and pi_status=1";
				$order = "pi_sequence asc";
				$getimg = productimage_s($sqlcon,$field,'',$where,'',$order,1);
				if (is_array($getimg) && $getimg[0]>0){
					$getimg1 = mysqli_fetch_assoc($getimg[1]);
					if ($getimg1['pi_image']!='' && file_exists("images/productitem/".$getimg1['pi_image'])){
						$result['image'] = $getimg1['pi_image'];
					}
				}
				if ($cekpv1['pv_st_id']!=0){
					$stname = ' - ';
					if ($cekpv1['st_name']==''){
						$stname .= 'Undefined!';
					}else{
						$stname .= $cekpv1['st_name'];
					}
				}
				$result['name'] = $cekpv1['pr_name'].$stname;
				if ($cekpv1['pv_ci_id'] != 0){
					$coloritm = 'Undefined!';
					if ($cekpv1['ci_image']!='' && file_exists("images/productcolor/".$cekpv1['ci_image'])){
						$coloritm = $cekpv1['ci_image'];
					}
					$result['color'] = $coloritm;
					$result['colorname'] = $cekpv1['cl_name'];
				}
				if ($cekpv1['pv_sz_id'] != 0){
					if ($cekpv1['sz_name']==''){
						$result['size'] = 'Undefined!';
					}else{
						$result['size'] = $cekpv1['sz_name'];
					}
				}
				
				$cartsubtotal = $cartsubtotal + $cekpv1['pv_price'];
				$cartvst[$idvariety] = 1;
				$cartqty = $cartqty + 1;
				$cartweight = $cartweight + $cekpv1['pr_weight'];
				$result['Status'] = "Add to cart success!";
			}
		}else{
			$result['Error'] = "Not define error!";
		}
	}
	
	$result['cartsubtotal'] = 'Rp '.number_format($cartsubtotal,0,',','.');
	$result['cartqty'] = number_format($cartqty,0,',','.');
	$result['cartweight'] = number_format($cartweight,0,',','.').' Gram';
	if (count($cartvst)>0){
		setcookie( 'cartvst',json_encode($cartvst),time()+60*60*24*30,$pathcook );
	}elseif (isset($_COOKIE["cartvst"])){
		unset($_COOKIE["cartvst"]);
		setcookie( 'cartvst',null,-1,$pathcook );
	}
}elseif ($_POST['mode']=='del'){
	$idvariety = intval($_POST['idvariety']);
	$cartsubtotal = 0;
	$cartqty = 0;
	$cartweight = 0;
	
	include_once 'cms/model/productvariety.php';
	if (isset($_COOKIE["cartvst"])){
		$cartvst = json_decode($_COOKIE["cartvst"],true);
		if (isset($cartvst[$idvariety])){
			unset($cartvst[$idvariety]);
		}
		if (count($cartvst)>0){foreach ($cartvst as $idpv => $qty){
			$field = "pv_price,pr_weight";
			$join = "inner join product on pr_id=pv_pr_id";
			$where = "pv_id=".$idpv;
			$cekpv = productvariety_s($sqlcon,$field,$join,$where);
			if (is_array($cekpv) && $cekpv[0]>0){
				$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
				$cartsubtotal = $cartsubtotal + ($qty*$cekpv1['pv_price']);
				$cartqty = $cartqty + $qty;
				$cartweight = $cartweight + ($qty*$cekpv1['pr_weight']);
			}elseif (is_array($cekpv) && $cekpv[0]==0){
				unset($cartvst[$idpv]);
			}
		}}
		if (count($cartvst)>0){
			setcookie( 'cartvst',json_encode($cartvst),time()+60*60*24*30,$pathcook );
		}else{
			setcookie( 'cartvst',null,-1,$pathcook );
		}
	}
	$result['Status'] = "Delete cart item success!";
	$result['cartqty'] = number_format($cartqty,0,',','.');
	$result['cartweight'] = number_format($cartweight,0,',','.').' Gram';
	$result['cartsubtotal'] = 'Rp '.number_format($cartsubtotal,0,',','.');
}elseif ($_POST['mode']=='upd'){
	$idvariety = intval($_POST['idvariety']);
	$qtyupd = intval($_POST['qty']);
	$cartsubtotal = 0;
	$cartqty = 0;
	$cartweight = 0;
	
	include_once 'cms/model/productvariety.php';
	if (isset($_COOKIE["cartvst"])){
		$cartvst = json_decode($_COOKIE["cartvst"],true);
		if (count($cartvst)>0){foreach ($cartvst as $idpv => $qty){
			$field = "pv_qty,pv_price,pr_weight";
			$join = "inner join product on pr_id=pv_pr_id";
			$where = "pv_id=".$idpv;
			$cekpv = productvariety_s($sqlcon,$field,'',$where);
			if (is_array($cekpv) && $cekpv[0]>0){
				$cekpv1 = mysqli_fetch_assoc($cekpv[1]);
				if ($idvariety == $idpv){
					if ($qtyupd <= $cekpv1['pv_qty']){
						$qty = $qtyupd;
						$cartvst[$idpv] = $qtyupd;
					}else{
						$result['Error'] = "Max. qty available now is ".$cekpv1['pv_qty']."!";
					}
				}
				$cartqty = $cartqty + $qty;
				$cartsubtotal = $cartsubtotal + ($qty*$cekpv1['pv_price']);
				$cartweight = $cartweight + ($qty*$cekpv1['pr_weight']);
			}elseif (is_array($cekpv) && $cekpv[0]==0){
				unset($cartvst[$idpv]);
			}
		}}
		if (count($cartvst)>0){
			setcookie( 'cartvst',json_encode($cartvst),time()+60*60*24*30,$pathcook );
		}else{
			setcookie( 'cartvst',null,-1,$pathcook );
		}
	}
	if (!isset($result['Error'])){
		$result['Status'] = "Update cart item success!";
	}
	$result['cartqty'] = number_format($cartqty,0,',','.');
	$result['cartweight'] = number_format($cartweight,0,',','.').' Gram';
	$result['cartsubtotal'] = 'Rp '.number_format($cartsubtotal,0,',','.');
}

if ($sqlcon){mysqli_close($sqlcon);}
/*
 * if (isset($_SERVER)){$result['SERVER']=$_SERVER;}
 *
 * if (isset($_POST)){$result['POST']=$_POST;}
 * if (isset($_GET)){$result['GET']=$_GET;}
 * if (isset($_FILES)){$result['FILES']=$_FILES;}
 */
header ( 'Content-Type: application/json' );
echo json_encode ( $result );
?>